<?php
function social_facebook_subpage()
{

    $shareMethod = get_option('fb_method_type'); if($shareMethod == ''){ $shareMethod = 'feed'; }

    if (get_option('fb_social') != 'on')
    {
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Facebook</h2>
            </div>
            <div class="social-sharing-page locked-up">
                <p><strong>This page is inactive</strong></p>
            </div>
        </div>

        <?php
        return;

    } else {

        if (isset($_POST['fb_submit_settings'])) {

            if(isset($_POST['fb_app_id'])){update_option("fb_app_id", $_POST['fb_app_id']);};

            if(isset($_POST['fb_share_icon'])){update_option("fb_share_icon", $_POST['fb_share_icon']);};
            if(isset($_POST['fb_method_type'])){update_option("fb_method_type", $_POST['fb_method_type']);};

            //Set default options
            if(isset($_POST['fb_default_share_title'])){update_option("fb_default_share_title", $_POST['fb_default_share_title']);};
            if(isset($_POST['fb_default_share_link'])){update_option("fb_default_share_link", $_POST['fb_default_share_link']);};
            if(isset($_POST['fb_default_share_caption'])){update_option("fb_default_share_caption", $_POST['fb_default_share_caption']);};
            if(isset($_POST['fb_default_share_description'])){update_option("fb_default_share_description", $_POST['fb_default_share_description']);};
            if(isset($_POST['fb_default_share_image_src'])){update_option("fb_default_share_image_src", $_POST['fb_default_share_image_src']);};

        }
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Facebook</h2>
            </div>
            <div class="social-sharing-page">
                <?php
                if (isset($_POST['fb_submit_settings'])) {
                    ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p><strong>Facebook settings saved.</strong></p>
                    </div>
                <?php
                }
                ?>
                <script language="JavaScript">
                    (function ($) {
                        $(document).ready(function () {
                            var icon = false;
                            $('#fb_share_icon_button').click(function () {
                                icon = true;
                                formfield = $('#fb_share_icon').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            $('#fb_default_share_image_src_button').click(function () {
                                icon = false;
                                formfield = $('#fb_default_share_image_src').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            window.send_to_editor = function (html) {
                                imgurl = $('img', html).attr('src');
                                if( icon ){
                                    $('#fb_share_icon').val(imgurl);
                                }
                                else {
                                    $('#fb_default_share_image_src').val(imgurl);
                                }
                                tb_remove();
                            }

                        });
                    })(jQuery);
                </script>


                <form method="post">

                    <table class="fb-share-settings" cellpadding="0" border="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td>
                                <label for="fb_app_id">App-Id:</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type='text' id="fb_app_id" name="fb_app_id" value="<?php echo get_option('fb_app_id'); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Sharing method:</td>
                        </tr>
                        <tr>
                            <td>
                                <input type='radio' id="fb_method_type_feed" name="fb_method_type" value="feed"<?php if(get_option('fb_method_type') === 'feed'){ echo ' checked="checked"';}?> />
                                <label for="fb_method_type_feed">Feed</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type='radio' id="fb_method_type_share" name="fb_method_type" value="share"<?php if(get_option('fb_method_type') === 'share'){ echo ' checked="checked"';}?> />
                                <label for="fb_method_type_share">Share</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type='radio' id="fb_method_type_send" name="fb_method_type"  value="send"<?php if(get_option('fb_method_type') === 'send'){ echo ' checked="checked"';}?> />
                                <label for="fb_method_type_send">Send</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="fb_share_icon">Upload Facebook icon:</label>
                            </td>
                        </tr>
                        <tr>
                            <?php $iconUrl = get_option('fb_share_icon'); ?>
                            <td class="upload-image">
                                <input id="fb_share_icon" type="text" size="36" name="fb_share_icon" value="<?php echo $iconUrl; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="upload-image">
                                <div class="upload-example">
                                    <span>
                                        <img src="<?php echo $iconUrl; ?>" alt="icon"/>
                                    </span>
                                    <input id="fb_share_icon_button" class="plugin-button" type="button" value="Upload Image"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <h3>Defaults:</h3>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <label for="fb_default_share_link">
                                    Default share link:
                                </label>
                                <input type="text" class="fb_default_share_link" name="fb_default_share_link" id="fb_default_share_link" value="<?php echo get_option('fb_default_share_link');?>"/>
                            </td>
                        </tr>
                        <?php if($shareMethod === 'feed'):?>
                            <tr>
                                <td>
                                    <label for="fb_default_share_title">
                                        Default share title:
                                    </label>
                                    <input type="text" class="fb_default_share_title" name="fb_default_share_title" id="fb_default_share_title" value="<?php echo get_option('fb_default_share_title');?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="fb_default_share_caption">
                                        Default share caption:
                                    </label>
                                    <input type="text" class="fb_default_share_caption" name="fb_default_share_caption" id="fb_default_share_caption" value="<?php echo get_option('fb_default_share_caption');?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="fb_default_share_description">
                                        Default share description:
                                    </label>
                                    <textarea name="fb_default_share_description" id="fb_default_share_description" cols="30" rows="4"><?php echo get_option('fb_default_share_description');?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="fb_default_share_image_src">Upload default image:</label>
                                </td>
                            </tr>
                            <tr>
                                <?php $defaultImageUrl = get_option('fb_default_share_image_src'); ?>
                                <td class="upload-image">
                                    <input id="fb_default_share_image_src" type="text" size="36" name="fb_default_share_image_src" value="<?php echo $defaultImageUrl; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="upload-image">
                                    <div class="upload-example">
                                        <span>
                                            <img src="<?php echo $defaultImageUrl; ?>" alt="icon"/>
                                        </span>
                                        <input id="fb_default_share_image_src_button" class="plugin-button" type="button" value="Upload Image"/>
                                    </div>
                                </td>
                            </tr>

                        <?php endif;?>
                        </tbody>
                    </table>

                    <p class="submit">
                        <input type="submit" name="fb_submit_settings" id="fb_submit_settings" class="plugin-button submit-btn" value="Save Changes">
                    </p>

                </form>
            </div>
        </div>


    <?php
    }
}