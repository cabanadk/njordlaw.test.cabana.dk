<?php
function social_email_subpage() {

    if (get_option('em_social') != 'on')
    {
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Email</h2>
            </div>
            <div class="social-sharing-page locked-up">
                <p><strong>This page is inactive</strong></p>
            </div>
        </div>

        <?php
        return;

    } else {

        if(isset($_POST['em_submit_settings'])) {

            if(isset($_POST['em_share_icon'])){update_option("em_share_icon", $_POST['em_share_icon']);};

            if(isset($_POST['em_default_subject'])){update_option("em_default_subject", $_POST['em_default_subject']);};
            if(isset($_POST['em_default_body'])){update_option("em_default_body", $_POST['em_default_body']);};

        }

        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Email</h2>
            </div>
            <div class="social-sharing-page">
                <?php
                if (isset($_POST['em_submit_settings'])) {
                    ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p><strong>Email settings saved.</strong></p>
                    </div>
                <?php
                }
                ?>
                <script language="JavaScript">
                    (function ($) {
                        $(document).ready(function () {
                            $('#em_share_icon_button').click(function () {
                                formfield = $('#em_share_icon').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            window.send_to_editor = function (html) {
                                imgurl = $('img', html).attr('src');
                                $('#em_share_icon').val(imgurl);
                                tb_remove();
                            }

                        });
                    })(jQuery);
                </script>


                <form method="post">

                    <table class="em-share-settings" cellpadding="0" border="0" cellspacing="0">
                        <tbody>

                        <tr>
                            <td>
                                <label for="em_share_icon">Upload Email icon:</label>
                            </td>
                        </tr>
                        <tr>
                            <?php $iconUrl = get_option('em_share_icon'); ?>
                            <td class="upload-image">
                                <input id="em_share_icon" type="text" size="36" name="em_share_icon" value="<?php echo $iconUrl; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="upload-image">
                                <div class="upload-example">
                                <span>
                                    <img src="<?php echo $iconUrl; ?>" alt="icon"/>
                                </span>
                                    <input id="em_share_icon_button" class="plugin-button" type="button" value="Upload Image"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <h3>Defaults:</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <label for="em_default_subject">
                                        Default Email Subject:
                                    </label>
                                    <input type="text" class="em_default_subject" name="em_default_subject" id="em_default_subject" value="<?php echo get_option('em_default_subject');?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="em_default_subject">
                                        Default Email Body text:
                                    </label>
                                    <br/>
                                    <textarea name="em_default_body" id="em_default_body" cols="30" rows="4"><?php echo get_option('em_default_body');?></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <p class="submit">
                        <input type="submit" name="em_submit_settings" id="em_submit_settings" class="plugin-button submit-btn" value="Save Changes">
                    </p>

                </form>

            </div>
        </div>

    <?php
    }
}