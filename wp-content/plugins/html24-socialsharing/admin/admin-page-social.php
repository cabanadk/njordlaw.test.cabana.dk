<?php

include_once ('facebook-subpage.php');
include_once ('twitter-subpage.php');
include_once ('linkedin-subpage.php');
include_once ('email-subpage.php');
include_once ('print-subpage.php');

/**
 * Add the add_submenu_page function into the anonymous function inside the admin_menu action
 * Codex: http://codex.wordpress.org/Function_Reference/add_submenu_page
 */
add_action('admin_menu', function ()
{
    /**
     * Adding the main menu item, called Social sharing
     */
    add_menu_page(
        'social-sharing',
        'Social sharing',
        'create_users',
        'social-sharing',
        'social_sharing_page',
        plugins_url('html24-socialsharing/images/icon.png'),
        66.1123
    );

    /**
     * Adding the submenu item for Twitter
     * Calls the function social_twitter_subpage()
     */
    add_submenu_page(
        'social-sharing',
        'Twitter',
        'Twitter',
        'create_users',
        'social-twitter',
        'social_twitter_subpage'
    );

    /**
     * Adding the submenu item for Facebook
     * Calls the function social_facebook_subpage()
     */
    add_submenu_page(
        'social-sharing',
        'Facebook',
        'Facebook',
        'create_users',
        'social-facebook',
        'social_facebook_subpage'
    );


    /**
     * Adding the submenu item for LinkedIn
     * Calls the function social_linkedin_subpage()
     */
    add_submenu_page(
        'social-sharing',
        'LinkedIn',
        'LinkedIn',
        'create_users',
        'social-linkedin',
        'social_linkedin_subpage'
    );

    /**
     * Adding the submenu item for Email sharing
     * Calls the function social_email_subpage()
     */
    add_submenu_page(
        'social-sharing',
        'Email',
        'Email',
        'create_users',
        'social-email',
        'social_email_subpage'
    );

    /**
     * Adding the submenu item for Print sharing
     * Calls the function social_print_subpage()
     */
    add_submenu_page(
        'social-sharing',
        'Print',
        'Print',
        'create_users',
        'social-print',
        'social_print_subpage'
    );

});

/**
 * Function for adding the plugin admin page
 */
function social_sharing_page() {

    $post_types = get_usable_post_types();

    if(isset($_POST['submitplatforms'])) {

        if(isset($_POST['fb_social'])){update_option("fb_social", $_POST['tw_social']);};
        if(isset($_POST['tw_social'])){update_option("tw_social", $_POST['tw_social']);};
        if(isset($_POST['li_social'])){update_option("li_social", $_POST['li_social']);};
        if(isset($_POST['em_social'])){update_option("em_social", $_POST['em_social']);};
        if(isset($_POST['pr_social'])){update_option("pr_social", $_POST['pr_social']);};

        $activePostTypes = array();

        if(!empty($post_types)):
            foreach ( $post_types as $post_type ) :
                if(isset($_POST["posttype-".$post_type]) && $_POST["posttype-".$post_type] == 'on'):
                    $activePostTypes[$post_type] = $_POST["posttype-".$post_type];
                    update_option("posttype-".$post_type, $_POST["posttype-".$post_type]);
                endif;
            endforeach;
        endif;

        update_option('active_posttypes', $activePostTypes);

        if(isset($_POST['metabox-toggle'])){update_option("metabox-toggle", $_POST['metabox-toggle']);};

    }
    ?>
    <div class="wrap social-sharing-wrapper">
        <div class="social-sharing-top-header">
            <h2>Social Settings</h2>
        </div>
        <div class="social-sharing-page">
            <?php
            if(isset($_POST['submitplatforms'])) {
                ?>
                <div id="setting-error-settings_updated" class="updated settings-error">
                    <p><strong>Social settings saved.</strong></p>
                </div>
            <?php
            }?>

            <h3>Platforms</h3>

            <form method="post" class="social-sharing-form">
                <table class="social-platforms" cellpadding="0" border="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td>
                            <label for="fb_social">
                                <input type='checkbox' id="fb_social" name="fb_social"<?php if ( get_option('fb_social') == 'on' ) echo ' checked="checked"'; ?> />
                                Facebook
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="tw_social">
                                <input type="checkbox" id="tw_social" name="tw_social"<?php if ( get_option('tw_social') == 'on' ) echo ' checked="checked"'; ?> />
                                Twitter
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="li_social">
                                <input type="checkbox" id="li_social" name="li_social"<?php if ( get_option('li_social') == 'on' ) echo ' checked="checked"'; ?> />
                                LinkedIn
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="em_social">
                                <input type="checkbox" id="em_social" name="em_social"<?php if ( get_option('em_social') == 'on' ) echo ' checked="checked"'; ?> />
                                Email
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pr_social">
                                <input type="checkbox" id="pr_social" name="pr_social"<?php if ( get_option('pr_social') == 'on' ) echo ' checked="checked"'; ?> />
                                Print
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>


                <hr/>
                <h3>Other settings</h3>

                <strong>Post Types:</strong>
                <i>Choose the post types to activate social sharing on:</i>
                <table class="post-types">
                    <tbody>
                        <tr>
                        <?php
                        if(!empty($post_types)):
                            foreach ( $post_types as $post_type ) : ?>
                                <td>
                                    <label for="posttype-<?php echo $post_type; ?>">
                                        <input type='checkbox' id="posttype-<?php echo $post_type; ?>" name="posttype-<?php echo $post_type; ?>"<?php if ( get_option('posttype-'.$post_type) == 'on' ) echo ' checked="checked"'; ?> />
                                        <?php echo ucfirst($post_type); ?>
                                    </label>
                                </td>
                            <?php endforeach;
                        endif;?>
                        </tr>
                    </tbody>
                </table>
                <hr/>
                <table class="social-settings" cellpadding="0" border="0" cellspacing="0">

                    <tbody>
                    <tr>
                        <td>
                            <label for="metabox-toggle">
                                <input type='checkbox' id="metabox-toggle" name="metabox-toggle"<?php if ( get_option('metabox-toggle') == 'on' ) echo ' checked="checked"'; ?> />
                                Show Metaboxes on chosen post types
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <p class="submit">
                    <input type="submit" name="submitplatforms" id="submitplatforms" class="plugin-button submit-btn" value="Save Changes">
                </p>

            </form>

        </div>
    </div>
<?php
}
