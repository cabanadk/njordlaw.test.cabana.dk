<?php
function social_print_subpage() {

    if (get_option('pr_social') != 'on')
    {
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Print</h2>
            </div>
            <div class="social-sharing-page locked-up">
                <p><strong>This page is inactive</strong></p>
            </div>
        </div>

        <?php
        return;

    } else {

        if(isset($_POST['pr_submit_settings'])) {

            if(isset($_POST['pr_share_icon'])){update_option("pr_share_icon", $_POST['pr_share_icon']);};

        }

        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Print</h2>
            </div>
            <div class="social-sharing-page">
                <?php
                if (isset($_POST['pr_submit_settings'])) {
                    ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p><strong>Print settings saved.</strong></p>
                    </div>
                <?php
                }
                ?>
                <script language="JavaScript">
                    (function ($) {
                        $(document).ready(function () {
                            $('#pr_share_icon_button').click(function () {
                                formfield = $('#pr_share_icon').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            window.send_to_editor = function (html) {
                                imgurl = $('img', html).attr('src');
                                $('#pr_share_icon').val(imgurl);
                                tb_remove();
                            }

                        });
                    })(jQuery);
                </script>


                <form method="post">

                    <table class="pr-share-settings" cellpadding="0" border="0" cellspacing="0">
                        <tbody>

                        <tr>
                            <td>
                                <label for="pr_share_icon">Upload Print icon:</label>
                            </td>
                        </tr>
                        <tr>
                            <?php $iconUrl = get_option('pr_share_icon'); ?>
                            <td class="upload-image">
                                <input id="pr_share_icon" type="text" size="36" name="pr_share_icon" value="<?php echo $iconUrl; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="upload-image">
                                <div class="upload-example">
                                <span>
                                    <img src="<?php echo $iconUrl; ?>" alt="icon"/>
                                </span>
                                    <input id="pr_share_icon_button" class="plugin-button" type="button" value="Upload Image"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>

                    <p class="submit">
                        <input type="submit" name="pr_submit_settings" id="pr_submit_settings" class="plugin-button submit-btn" value="Save Changes">
                    </p>

                </form>

            </div>
        </div>

    <?php
    }
}