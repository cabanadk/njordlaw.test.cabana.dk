<?php
function social_twitter_subpage() {

    if (get_option('tw_social') != 'on')
    {
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Twitter</h2>
            </div>
            <div class="social-sharing-page locked-up">
                <p><strong>This page is inactive</strong></p>
            </div>
        </div>

        <?php
        return;

    } else {

        if(isset($_POST['tw_submit_settings'])) {

            if(isset($_POST['tw_share_icon'])){update_option("tw_share_icon", $_POST['tw_share_icon']);};

            //Set default options
            if(isset($_POST['tw_default_share_text'])){update_option("tw_default_share_text", $_POST['tw_default_share_text']);};
            if(isset($_POST['tw_default_share_link'])){update_option("tw_default_share_link", $_POST['tw_default_share_link']);};
            if(isset($_POST['tw_default_share_hashtags'])){update_option("tw_default_share_hashtags", $_POST['tw_default_share_hashtags']);};

        }

        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>Twitter</h2>
            </div>
            <div class="social-sharing-page">
                <?php
                if (isset($_POST['tw_submit_settings'])) {
                    ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p><strong>Twitter settings saved.</strong></p>
                    </div>
                <?php
                }
                ?>
                <script language="JavaScript">
                    (function ($) {
                        $(document).ready(function () {
                            $('#tw_share_icon_button').click(function () {
                                formfield = $('#tw_share_icon').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            window.send_to_editor = function (html) {
                                imgurl = $('img', html).attr('src');
                                $('#tw_share_icon').val(imgurl);
                                tb_remove();
                            }

                        });
                    })(jQuery);
                </script>


                <form method="post">

                    <table class="tw-share-settings" cellpadding="0" border="0" cellspacing="0">
                        <tbody>

                        <tr>
                            <td>
                                <label for="tw_share_icon">Upload Twitter icon:</label>
                            </td>
                        </tr>
                        <tr>
                            <?php $iconUrl = get_option('tw_share_icon'); ?>
                            <td class="upload-image">
                                <input id="tw_share_icon" type="text" size="36" name="tw_share_icon" value="<?php echo $iconUrl; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="upload-image">
                                <div class="upload-example">
                                <span>
                                    <img src="<?php echo $iconUrl; ?>" alt="icon"/>
                                </span>
                                    <input id="tw_share_icon_button" class="plugin-button" type="button" value="Upload Image"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <h3>Defaults: (Character limit: 140)</h3>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <label for="tw_default_share_link">
                                    Default share link:
                                </label>
                                <input type="text" class="tw_default_share_link" name="tw_default_share_link" id="tw_default_share_link" value="<?php echo get_option('tw_default_share_link');?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="tw_default_share_text">
                                    Default share text:
                                </label>
                                <br/>
                                <textarea name="tw_default_share_text" id="tw_default_share_text" cols="30" rows="4"><?php echo get_option('tw_default_share_text');?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="tw_default_share_hashtags">
                                    Default share hashtags:
                                </label>
                                <input type="text" class="tw_default_share_hashtags" name="tw_default_share_hashtags" id="tw_default_share_hashtags" value="<?php echo get_option('tw_default_share_hashtags');?>" placeholder="comma,separated,hashtags,go,here"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="submit">
                        <input type="submit" name="tw_submit_settings" id="tw_submit_settings" class="plugin-button submit-btn" value="Save Changes">
                    </p>

                </form>
            </div>
        </div>

    <?php
    }
}