<?php
function social_linkedin_subpage() {

    if (get_option('li_social') != 'on')
    {
        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>LinkedIn</h2>
            </div>
            <div class="social-sharing-page locked-up">
                <p><strong>This page is inactive</strong></p>
            </div>
        </div>

        <?php
        return;

    } else {

        if(isset($_POST['li_submit_settings'])) {

            if(isset($_POST['li_share_icon'])){update_option("li_share_icon", $_POST['li_share_icon']);};

            //Set default options
            if(isset($_POST['li_default_share_link'])){update_option("li_default_share_link", $_POST['li_default_share_link']);};
            if(isset($_POST['li_default_share_title'])){update_option("li_default_share_title", $_POST['li_default_share_title']);};
            if(isset($_POST['li_default_share_text'])){update_option("li_default_share_text", $_POST['li_default_share_text']);};

        }

        ?>
        <div class="wrap social-sharing-wrapper">
            <div class="social-sharing-top-header">
                <h2>LinkedIn</h2>
            </div>
            <div class="social-sharing-page">
                <?php
                if (isset($_POST['li_submit_settings'])) {
                    ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p><strong>LinkedIn settings saved.</strong></p>
                    </div>
                <?php
                }
                ?>
                <script language="JavaScript">
                    (function ($) {
                        $(document).ready(function () {
                            $('#li_share_icon_button').click(function () {
                                formfield = $('#li_share_icon').attr('name');
                                tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                                return false;
                            });

                            window.send_to_editor = function (html) {
                                imgurl = $('img', html).attr('src');
                                $('#li_share_icon').val(imgurl);
                                tb_remove();
                            }

                        });
                    })(jQuery);
                </script>


                <form method="post">

                    <table class="li-share-settings" cellpadding="0" border="0" cellspacing="0">
                        <tbody>

                        <tr>
                            <td>
                                <label for="li_share_icon">Upload LinkedIn icon:</label>
                            </td>
                        </tr>
                        <tr>
                            <?php $iconUrl = get_option('li_share_icon'); ?>
                            <td class="upload-image">
                                <input id="li_share_icon" type="text" size="36" name="li_share_icon" value="<?php echo $iconUrl; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="upload-image">
                                <div class="upload-example">
                                <span>
                                    <img src="<?php echo $iconUrl; ?>" alt="icon"/>
                                </span>
                                    <input id="li_share_icon_button" class="plugin-button" type="button" value="Upload Image"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <h3>Defaults:</h3>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <label for="li_default_share_link">
                                    Default share link:
                                </label>
                                <input type="text" class="li_default_share_link" name="li_default_share_link" id="li_default_share_link" value="<?php echo get_option('li_default_share_link');?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="li_default_share_title">
                                    Default share title:
                                </label>
                                <input type="text" class="li_default_share_title" name="li_default_share_title" id="li_default_share_title" value="<?php echo get_option('li_default_share_title');?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="li_default_share_text">
                                    Default share text:
                                </label>
                                <br/>
                                <textarea name="li_default_share_text" id="li_default_share_text" cols="30" rows="4"><?php echo get_option('li_default_share_text');?></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="submit">
                        <input type="submit" name="li_submit_settings" id="li_submit_settings" class="plugin-button submit-btn" value="Save Changes">
                    </p>

                </form>

            </div>
        </div>

    <?php
    }
}