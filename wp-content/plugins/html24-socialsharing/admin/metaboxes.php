<?php
/**
 * Adds metaboxes to each post type (Exceptions: attachment, revisions, nav_menu_item, )
 */
add_action( 'wp_loaded', 'add_metabox_to_post_types', 0, 99 );
function add_metabox_to_post_types()
{

    /**
     * Adds metabox to each post type. (Including the custom ones)
     */
    add_action( 'add_meta_boxes', 'adding_socialsharing_meta_boxes' , 10, 2 );
    function adding_socialsharing_meta_boxes($postTypes, $post)
    {

        $activePostTypes = get_active_post_types();

        $id = 'share_buttons_settings';
        $title = 'Share Settings';
        $callback = 'add_share_settings';
        if(in_array($postTypes , $activePostTypes)){
            add_meta_box( $id, $title, $callback, $postTypes, 'normal', 'high' );
        }
    }

}


/**
 * Build the share settings metabox
 */
function add_share_settings(){

    global $post;

    wp_nonce_field( 'share_buttons_settings', 'share_buttons_settings_nonce' );

    $fbShareTitleValue = get_post_meta( $post->ID, '_custom_share_fb_share_title', true );
    $fbShareLinkValue = get_post_meta( $post->ID, '_custom_share_fb_share_link', true );
    $fbShareCaptionValue = get_post_meta( $post->ID, '_custom_share_fb_share_caption', true );
    $fbShareDescriptionValue = get_post_meta( $post->ID, '_custom_share_fb_share_description', true );
    $fbShareImageSrcValue = get_post_meta( $post->ID, '_custom_share_fb_share_image_src', true );


    $twShareLinkValue = get_post_meta( $post->ID, '_custom_share_tw_share_link', true );
    $twShareTextValue = get_post_meta( $post->ID, '_custom_share_tw_share_text', true );
    $twShareHashtagsValue = get_post_meta( $post->ID, '_custom_share_tw_share_hashtags', true );


    $liSharelinkValue = get_post_meta( $post->ID, '_custom_share_li_share_link', true );
    $liShareTextValue = get_post_meta( $post->ID, '_custom_share_li_share_text', true );
    $liShareTitleValue = get_post_meta( $post->ID, '_custom_share_li_share_title', true );

    $emSubjectValue = get_post_meta( $post->ID, '_custom_email_subject', true );
    $emBodyValue = get_post_meta( $post->ID, '_custom_email_body', true );
    ?>

    <div class="sharing-metabox-wrapper<?php if(!in_array($post->post_type, get_active_post_types())){echo ' disabled_post_type';}?>">
        <?php $shareMethod = get_option('fb_method_type');?>

        <?php if(get_option('fb_social') === 'on'): ?>
            <div class="platform-wrap platform-facebook<?php if($shareMethod != 'feed'){echo ' short';}?>">
                <strong>Facebook settings:</strong>

                <p>
                    <label for="fb_custom_share_link">
                        <input type="text" class="fb_custom_share_link" name="fb_custom_share_link" id="fb_custom_share_link" placeholder="<?php if(($defaultFBLink = get_option('fb_default_share_link')) != ''){echo 'Default: '.$defaultFBLink;}else{echo 'Share Link:';}?>" value="<?php echo $fbShareLinkValue;?>"/>
                    </label>
                </p>
                <?php if($shareMethod === 'feed'):?>
                    <p>
                        <label for="fb_custom_share_title">
                            <input type="text" class="fb_custom_share_title" name="fb_custom_share_title" id="fb_custom_share_title" placeholder="<?php if(($defaultFBTitle = get_option('fb_default_share_title')) != ''){echo 'Default: '.$defaultFBTitle;}else{echo 'Share Title:';}?>" value="<?php echo $fbShareTitleValue;?>"/>
                        </label>
                    </p>
                    <p>
                        <label for="fb_custom_share_caption">
                            <input type="text" class="fb_custom_share_caption" name="fb_custom_share_caption" id="fb_custom_share_caption" placeholder="<?php if(($defaultFBCaption = get_option('fb_default_share_caption')) != ''){echo 'Default: '.$defaultFBCaption;}else{echo 'Share Caption:';}?>" value="<?php echo $fbShareCaptionValue;?>"/>
                        </label>
                    </p>
                    <p>
                        <label for="fb_custom_share_description">
                            <textarea name="fb_custom_share_description" id="fb_custom_share_description" cols="30" rows="3" placeholder="<?php if(($defaultFBDescription = get_option('fb_default_share_description')) != ''){echo 'Default: '.$defaultFBDescription;}else{echo 'Share Description:';}?>"><?php echo $fbShareDescriptionValue;?></textarea>
                        </label>
                    </p>
                    <p>
                        <label for="fb_custom_share_image_src">
                            <input type="text" class="fb_custom_share_image_src" name="fb_custom_share_image_src" id="fb_custom_share_image_src" placeholder="<?php if(($defaultFBImageSrc = get_option('fb_default_share_image_src')) != ''){echo 'Default: '.$defaultFBImageSrc;}else{echo 'Share image src: "http://example.com/filename.jpg" (Leave empty to use posts featured image)';}?>" value="<?php echo $fbShareImageSrcValue;?>" />
                        </label>
                    </p>
                <?php endif;?>

            </div>
        <?php endif; ?>

        <?php if(get_option('tw_social') === 'on'): ?>
            <div class="platform-wrap platform-twitter">
                <strong>Twitter settings: (Character limit: 140)</strong>
                <p>
                    <label for="tw_custom_share_link">
                        <input type="text" class="tw_custom_share_link" name="tw_custom_share_link" id="tw_custom_share_link" placeholder="<?php if(($defaultTWLink = get_option('tw_default_share_link')) != ''){echo 'Default: '.$defaultTWLink;}else{echo 'Share Link:';}?>" value="<?php echo $twShareLinkValue;?>" />
                    </label>
                </p>
                <p>
                    <label for="tw_custom_share_text">
                        <textarea class="tw_custom_share_text" name="tw_custom_share_text" id="tw_custom_share_text" placeholder="<?php if(($defaultTWText = get_option('tw_default_share_text')) != ''){echo 'Default: '.$defaultTWText;}else{echo 'Share Text:';}?>" cols="30" rows="3"><?php echo $twShareTextValue;?></textarea>
                    </label>
                </p>
                <p>
                    <label for="tw_custom_share_hashtags">
                        <input type="text" class="tw_custom_share_hashtags" name="tw_custom_share_hashtags" id="tw_custom_share_hashtags" placeholder="<?php if(($defaultTWHashtags = get_option('tw_default_share_hashtags')) != ''){echo 'Default: '.$defaultTWHashtags;}else{echo 'Share hashtags: comma,separated,hashtags,go,here)';}?>" value="<?php echo $twShareHashtagsValue;?>" />
                    </label>
                </p>
            </div>
        <?php endif; ?>

        <?php if(get_option('li_social') === 'on'): ?>
            <div class="platform-wrap platform-twitter">
                <strong>LinkedIn settings:</strong>
                <p>
                    <label for="li_custom_share_link">
                        <input type="text" class="li_custom_share_link" name="li_custom_share_link" id="li_custom_share_link" placeholder="<?php if(($defaultLILink = get_option('li_default_share_link')) != ''){echo 'Default: '.$defaultLILink;}else{echo 'Share Link:';}?>" value="<?php echo $liSharelinkValue;?>" />
                    </label>
                </p>
                <p>
                    <label for="li_custom_share_title">
                        <input type="text" class="li_custom_share_title" name="li_custom_share_title" id="li_custom_share_title" placeholder="<?php if(($defaultLITitle = get_option('li_default_share_title')) != ''){echo 'Default: '.$defaultLITitle;}else{echo 'Share Title:';}?>" value="<?php echo $liShareTitleValue;?>" />
                    </label>
                </p>
                <p>
                    <label for="li_custom_share_text">
                        <textarea class="li_custom_share_text" name="li_custom_share_text" id="li_custom_share_text" placeholder="<?php if(($defaultLIText = get_option('li_default_share_text')) != ''){echo 'Default: '.$defaultLIText;}else{echo 'Share Text:';}?>" cols="30" rows="3"><?php echo $liShareTextValue;?></textarea>
                    </label>
                </p>
            </div>
        <?php endif; ?>

        <?php if(get_option('em_social') === 'on'): ?>
            <div class="platform-wrap platform-email">
                <strong>Email settings:</strong>
                <p>
                    <label for="em_custom_email_subject">
                        <input type="text" class="em_custom_email_subject" name="em_custom_email_subject" id="em_custom_email_subject" placeholder="<?php if(($defaultEmailSubject = get_option('em_default_subject')) != ''){echo 'Default: '.$defaultEmailSubject;}else{echo 'Email subject:';}?>" value="<?php echo $emSubjectValue;?>" />
                    </label>
                </p>
                <p>
                <label for="em_custom_email_body">
                    <textarea class="em_custom_email_body" name="em_custom_email_body" id="em_custom_email_body" placeholder="<?php if(($defaultEmailBody = get_option('em_default_body')) != ''){echo 'Default: '.$defaultEmailBody;}else{echo 'Email body:';}?>" cols="30" rows="3"><?php echo $emBodyValue;?></textarea>
                </label>
                </p>
            </div>
        <?php endif; ?>

    </div>

<?php

}

function save_share_settings( $post_id ){

    if ( ! isset( $_POST['share_buttons_settings_nonce'] ) ) {
        return;
    }

    if ( ! wp_verify_nonce( $_POST['share_buttons_settings_nonce'], 'share_buttons_settings' ) ) {
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    //Check all the fields
    if ( ! isset( $_POST['fb_custom_share_title'] ) ) { return; }
    if ( ! isset( $_POST['fb_custom_share_link'] ) ) { return; }
    if ( ! isset( $_POST['fb_custom_share_caption'] ) ) { return; }
    if ( ! isset( $_POST['fb_custom_share_description'] ) ) { return; }
    if ( ! isset( $_POST['fb_custom_share_image_src'] ) ) { return; }
    if ( ! isset( $_POST['tw_custom_share_link'] ) ) { return; }
    if ( ! isset( $_POST['tw_custom_share_text'] ) ) { return; }
    if ( ! isset( $_POST['tw_custom_share_hashtags'] ) ) { return; }
    if ( ! isset( $_POST['li_custom_share_link'] ) ) { return; }
    if ( ! isset( $_POST['li_custom_share_text'] ) ) { return; }
    if ( ! isset( $_POST['li_custom_share_title'] ) ) { return; }
    if ( ! isset( $_POST['em_custom_email_subject'] ) ) { return; }
    if ( ! isset( $_POST['em_custom_email_body'] ) ) { return; }



    // Sanitize user input.
    $fbShareTitle           = sanitize_text_field( $_POST['fb_custom_share_title'] );
    $fbShareLink            = sanitize_text_field( $_POST['fb_custom_share_link'] );
    $fbShareCaption         = sanitize_text_field( $_POST['fb_custom_share_caption'] );
    $fbShareDescription     = sanitize_text_field( $_POST['fb_custom_share_description'] );
    $fbShareImageSrc        = sanitize_text_field( $_POST['fb_custom_share_image_src'] );

    $twShareLink            = sanitize_text_field( $_POST['tw_custom_share_link'] );
    $twShareText            = sanitize_text_field( $_POST['tw_custom_share_text'] );
    $twShareHashtags        = sanitize_text_field( $_POST['tw_custom_share_hashtags'] );


    $liSharelink            = sanitize_text_field( $_POST['li_custom_share_link'] );
    $liShareText            = sanitize_text_field( $_POST['li_custom_share_text'] );
    $liShareTitle           = sanitize_text_field( $_POST['li_custom_share_title'] );

    $customEmailSubject     = sanitize_text_field( $_POST['em_custom_email_subject'] );
    $customEmailBody        = sanitize_text_field( $_POST['em_custom_email_body'] );


    // Update the meta field in the database.
    update_post_meta( $post_id, '_custom_share_fb_share_title', $fbShareTitle );
    update_post_meta( $post_id, '_custom_share_fb_share_link', $fbShareLink );
    update_post_meta( $post_id, '_custom_share_fb_share_caption', $fbShareCaption );
    update_post_meta( $post_id, '_custom_share_fb_share_description', $fbShareDescription );
    update_post_meta( $post_id, '_custom_share_fb_share_image_src', $fbShareImageSrc );

    update_post_meta( $post_id, '_custom_share_tw_share_link', $twShareLink );
    update_post_meta( $post_id, '_custom_share_tw_share_text', $twShareText );
    update_post_meta( $post_id, '_custom_share_tw_share_hashtags', $twShareHashtags );

    update_post_meta( $post_id, '_custom_share_li_share_link', $liSharelink );
    update_post_meta( $post_id, '_custom_share_li_share_text', $liShareText );
    update_post_meta( $post_id, '_custom_share_li_share_title', $liShareTitle );

    update_post_meta( $post_id, '_custom_email_subject', $customEmailSubject );
    update_post_meta( $post_id, '_custom_email_body', $customEmailBody );
}

add_action( 'save_post', 'save_share_settings', 10, 2);