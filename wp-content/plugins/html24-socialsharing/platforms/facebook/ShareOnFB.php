<?php

defined( 'ABSPATH' ) or die( 'Move on!' );


class ShareOnFBMethodType {
    const FEED  = "feed";    // Shares on users own wall
    const SHARE = "share";   // Shares a link
    const SEND  = "send";    // Sends to friend
}

class ShareOnFB
{
    /**
     * Used to initialize the API
     * Create an app on: @link: https://developers.facebook.com/
     * and get the App ID for your app
     * @var String
     */
    private $appID;

    /**
     * Method is the destination for where the share ends up
     * @var ShareOnFBMethodType
     */
    private $shareMethod;

    /**
     * The name that will appear as the sender of the share
     * @var String
     */
    private $shareName;

    /**
     * The link to the shared page
     * @var String
     */
    private $shareLink;

    /**
     * The shareCaption text that is shared
     * @var String $shareCaption
     */
    private $shareCaption;

    /**
     * The description text that is shared
     * @var String $shareDescription
     */
    private $shareDescription;

    /**
     * The image that is shared
     * @var String $shareImageSrc
     */
    private $shareImageSrc;

    /**
     * @var String $shareIcon
     */
    private $shareIcon;

    /**
     * Html markup that creates the button
     * @var String $shareButton
     */
    private $shareButton;

    /**
     * Html script that activates the button
     * @var String $shareScript
     */
    private $shareScript;

    /**
     * Check if the Facebook part of the plugin is active
     * @var bool
     */
    static private $IsActive = false;

    /**
     * Checks if the facebook share has been initialized
     * @var bool
     */
    static private $HasInitialized = false;

    /**
     * Adds a unique number to the id's of each button
     * @var int
     */
    static private $uniqueNumber = 0;

    /**
     * @param ShareOnFBMethodType $shareMethod
     * @param String $shareName
     * @param String $shareLink
     * @param String $shareCaption
     * @param String $shareDescription
     * @param String $shareImageSrc
     * @param String $shareIcon
     */
    public function __construct($shareMethod = null, $shareLink = null, $shareName = null, $shareCaption = null, $shareDescription = null, $shareImageSrc = null, $shareIcon = null)
    {

        $this::Initialize();

        global $post;

        //Check defaults
        $defaultTitle       = get_option('fb_default_share_title');
        $defaultLink        = get_option('fb_default_share_link');
        $defaultCaption     = get_option('fb_default_share_caption');
        $defaultDescription = get_option('fb_default_share_description');
        $defaultImageSrc    = get_option('fb_default_share_image_src');

        $checkForCustomContent = get_post_meta($post->ID);

        $this->appID = '';

        $this->shareButton = '';

        $this->shareScript = '';

        if ($shareMethod != null)
        {
            $this->shareMethod = $shareMethod;
        }

        if ($checkForCustomContent['_custom_share_fb_share_title'][0] != '')
        {
            $this->shareName = $checkForCustomContent['_custom_share_fb_share_title'][0];
        }
        else if($defaultTitle != '')
        {
            $this->shareName = $defaultTitle;
        }
        else if ($shareName === null)
        {
            $this->shareName  = get_bloginfo('name');
        }
        else
        {
            $this->shareName  = $shareName;
        }

        if ($checkForCustomContent['_custom_share_fb_share_link'][0] != '')
        {
            $this->shareLink = $checkForCustomContent['_custom_share_fb_share_link'][0];
        }
        else if($defaultLink != '')
        {
            $this->shareLink = $defaultLink;
        }
        else if($shareLink === null)
        {
            $this->shareLink = get_permalink($post->ID);
        }
        else
        {
            $this->shareLink = $shareLink;
        }

        if ($checkForCustomContent['_custom_share_fb_share_caption'][0] != '')
        {
            $this->shareCaption = $checkForCustomContent['_custom_share_fb_share_caption'][0];
        }
        else if ($defaultCaption != '')
        {
            $this->shareCaption = $defaultCaption;
        }
        else if($shareCaption === null)
        {
            $this->shareCaption = $post->post_title;
        }
        else
        {
            $this->shareCaption = $shareCaption;
        }

        if ($checkForCustomContent['_custom_share_fb_share_description'][0] != '')
        {
            $this->shareDescription = $checkForCustomContent['_custom_share_fb_share_description'][0];
        }
        else if ($defaultDescription != '')
        {
            $this->shareDescription = $defaultDescription;
        }
        else if($shareDescription === null)
        {
            $this->shareDescription = $post->post_content;
        }
        else
        {
            $this->shareDescription = $shareDescription;
        }

        $hasThumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

        if ($checkForCustomContent['_custom_share_fb_share_image_src'][0] != '')
        {
            $this->shareImageSrc = $checkForCustomContent['_custom_share_fb_share_image_src'][0];
        }
        else if ($defaultImageSrc != '')
        {
            $this->shareImageSrc = $defaultImageSrc;
        }
        else if($shareImageSrc != '')
        {
            $this->shareImageSrc = $shareImageSrc;
        }
        else if($shareImageSrc === null && $hasThumbnail != '')
        {
            $this->shareImageSrc = $hasThumbnail;
        }
        else
        {
            $imageFolder = plugin_dir_url('html24-socialsharing/images/missing-image.jpg');
            $this->shareImageSrc = $imageFolder.'missing-image.jpg';
        }

        if($shareIcon === null)
        {
            $this->shareIcon = get_option('fb_share_icon');
        }
        else if($shareIcon != '')
        {
            $this->shareIcon = $shareIcon;
        }
        else
        {
            $imageFolder = plugin_dir_url('html24-socialsharing/images/missing-icon.jpg');
            $this->shareIcon = $imageFolder.'missing-icon.jpg';
        }

        $this::sanitizeFields();

    }


    /**
     * Check if Facebook is activated
     */
    private function CheckIfActive(){

        if(get_option('fb_social') != 'on'){

            ShareOnFB::$IsActive = false;

        } else {

            ShareOnFB::$IsActive = true;

        }

    }


    /**
     * Cleans up the fields
     */
    private function sanitizeFields(){

        $this->shareName = sanitize_text_field($this->shareName);

        $this->shareCaption = sanitize_text_field($this->shareCaption);

        $this->shareDescription = sanitize_text_field($this->shareDescription);
        $this->shareDescription = str_replace('"', '%22', $this->shareDescription);

    }


    private function CheckForShareMethod(){

        switch(get_option('fb_method_type')){
            case 'feed':
                $this->setMethod(ShareOnFBMethodType::FEED);
                break;
            case 'share':
                $this->setMethod(ShareOnFBMethodType::SHARE);
                break;
            case 'send':
                $this->setMethod(ShareOnFBMethodType::SEND);
                break;
            default:
                $this->setMethod(ShareOnFBMethodType::FEED);
                break;
        }

    }


    /*==============================================
     *=================== APP ID ===================
     =============================================*/
    /**
     * @return string
     */
    public function getAppId()
    {
        $appID    = get_option('fb_app_id');

        if($appID === ''){

            return null;

        } else {

            $this->appID = $appID;
            return $this->appID;

        }

    }

    /*==============================================
     *================ SHARE METHOD ================
     =============================================*/
    /**
     * @return ShareOnFBMethodType
     */
    public function getMethod() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareMethod;

        }

    }

    /**
     * @param ShareOnFBMethodType::FEED | ShareOnFBMethodType::MESSAGE $FBMethodType
     */
    public function setMethod($FBMethodType) {

        if(!ShareOnFB::$IsActive){ return; }

        if ($FBMethodType === ShareOnFBMethodType::FEED || $FBMethodType === ShareOnFBMethodType::SHARE || $FBMethodType === ShareOnFBMethodType::SEND) {

            $this->shareMethod = $FBMethodType;

        }

    }

    /*==============================================
     *================ SHARE LINK ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareLink() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareLink;

        }

    }

    /**
     * @params String
     */
    public function setShareLink($FBShareLink){

        if(!ShareOnFB::$IsActive){ return; }

        $this->shareLink = $FBShareLink;

    }


    /*==============================================
     *================ SHARE NAME ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareName() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareName;

        }

    }

    /**
     * @params String
     */
    public function setShareName($FBShareName){

        if(!ShareOnFB::$IsActive){ return; }

        $this->shareName = $FBShareName;

    }


    /*==============================================
     *=============== CAPTION TEXT =================
     =============================================*/
    /**
     * @return String
     */
    public function getCaption() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareCaption;

        }

    }

    /**
     * @params String
     */
    public function setCaption($FBShareCaption){

        if(!ShareOnFB::$IsActive){ return; }

        $this->shareCaption = $FBShareCaption;

    }



    /*==============================================
     *============= DESCRIPTION TEXT ===============
     =============================================*/
    /**
     * @return String
     */
    public function getDescription() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareDescription;

        }

    }

    /**
     * @params String
     */
    public function setDescription($FBShareDescription){

        if(!ShareOnFB::$IsActive){ return; }

        $this->shareDescription = $FBShareDescription;

    }


    /*==============================================
     *============= SHARE IMAGE SRC ================
     =============================================*/
    /**
     * @return String
     */
    public function getImageScr() {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareImageSrc;

        }

    }

    /**
     * @params String
     */
    public function setImageScr($FBShareImageSrc){

        if(!ShareOnFB::$IsActive){ return; }

        $this->shareImageSrc = $FBShareImageSrc;

    }


    /*==============================================
     *================= SHARE ICON =================
     =============================================*/
    /**
     * @return string
     */
    public function getShareIcon()
    {

        if(!ShareOnFB::$IsActive){

            return null;

        } else {

            return $this->shareIcon;

        }

    }

    /**
     * @return string
     */
    public function setShareIcon($FBShareIcon)
    {

        if(!ShareOnFB::$IsActive){ return; }

        if($FBShareIcon != ''){
            $this->shareIcon = $FBShareIcon;
        }

    }


    /*==============================================
     *=============== SHARE BUTTON =================
     =============================================*/
    public function getShareButton()
    {

        if(!ShareOnFB::$IsActive){ return; }

        if (!ShareOnFB::$HasInitialized) {
            ShareOnFB::Initialize();
        }

        $buttonMarkup = '';

        $shareIcon = $this->shareIcon;

        $makeUnique = 'fb-publish-'.ShareOnFB::$uniqueNumber;

        if($shareIcon != '')
        {
            $buttonMarkup .= '<a href="#" class="facebook_button share_button" id="'.$makeUnique.'">';
            $buttonMarkup .= '<img src="'.$shareIcon.'" alt="Share on Facebook" style="max-width: 100%;max-height:100%;" />';
            $buttonMarkup .= '</a>';
        }
        else
        {
            $buttonMarkup .= '<a href="#" class="facebook_button" id="fb-publish">Share on Facebook</a>';
        }

        $this->shareButton =  $buttonMarkup;

        self::getShareScript($makeUnique);

        echo $this->shareButton;

        echo $this->shareScript;

        ShareOnFB::$uniqueNumber = ShareOnFB::$uniqueNumber+1;

    }

    /**
     * @param $FBShareButton
     */
    public function setShareButton($FBShareButton)
    {

        if(!ShareOnFB::$IsActive){ return; }

        if($FBShareButton != ''){
            $this->shareButton = $FBShareButton;
        }

    }


    /**
     * @param $makeUnique
     */
    public function getShareScript ($makeUnique)
    {

        if(!ShareOnFB::$IsActive){ return; }

        $uniqueID = '#'.$makeUnique;

        $shareScript = '';

        $shareScript .= '<script type="text/javascript">(function($){$("'.$uniqueID.'").click(function(){FB.ui({method:"'.$this->shareMethod.'",';

        switch($this->shareMethod){

            case 'share':
                if($this->shareLink != ''){
                    $shareScript .= 'href: "'.$this->shareLink.'"';
                }
                break;

            case 'feed':
                if( $this->shareLink != '' ){
                    $shareScript .= 'link: "'.$this->shareLink.'",';
                }
                if( $this->shareName != '' ){
                    $shareScript .= 'name: "'.$this->shareName.'",';
                }
                if( $this->shareCaption != '' ){
                    $shareScript .= 'caption: "'.$this->shareCaption.'",';
                }
                if( $this->shareDescription != ''){
                    $shareScript .= 'description: "'.$this->shareDescription.'",';
                }
                if( $this->shareImageSrc != ''){
                    $shareScript .= 'picture: "'.$this->shareImageSrc.'"';
                }
                break;

            case 'send':
                if($this->shareLink != ''){
                    $shareScript .= 'link: "'.$this->shareLink.'"';
                }
                break;

            default:
                $shareScript .= 'href: "'.$this->shareLink.'"';
                break;

        }

        $shareScript .= '},function(response) {if (response && !response.error_code) {/*console.log("Posted!")*/} else {/*console.log(response)*/;}});});})(jQuery);</script>';

        $this->shareScript = $shareScript;

    }


    /*==============================================
     *================= INITIALIZE =================
     =============================================*/
    private function Initialize()
    {

        $this->CheckIfActive();

        if(!ShareOnFB::$IsActive){ return; }

        $this->CheckForShareMethod();

        $appID    = $this->getAppId();

        if($appID === '')
        {
            return null;
        }
        else
        {
            $this->appID = $appID;
        }

        if ($this::$HasInitialized)
        {
            return;
        }

        $fbInitScript = '';

        if($appID != null) {

            $fbInitScript = "
                <div id='fb-root'></div>
                <script>
                    window.fbAsyncInit = function() {
                        FB.init({
                            appId      : '" . $appID . "',
                            xfbml      : true,
                            version    : 'v2.2'
                        });
                    };

                    (function(d, s, id){
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {return;}
                        js = d.createElement(s); js.id = id;
                        js.src = '//connect.facebook.net/en_US/sdk.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                ";

        }

        echo $fbInitScript;

        $this::$HasInitialized = true;

    }

}
