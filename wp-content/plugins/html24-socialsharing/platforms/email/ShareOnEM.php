<?php

class ShareOnEM {

    /**
     * @var string $shareEmailSubject
     */
    private $shareEmailSubject;

    /**
     * @var string $shareEmailBody
     */
    private $shareEmailBody;

    /**
     * @var String $shareIcon
     */
    private $shareIcon;

    /**
     * Check if the Email part of the plugin is active
     * @var bool
     */
    static private $IsActive = false;

    /**
     * Checks if the Email share has been initialized
     * @var bool
     */
    static private $HasInitialized = false;

    /**
     * Adds a unique number to the id's of each button
     * @var int
     */
    static private $uniqueNumber = 0;

    public function __construct($shareEmailSubject = null, $shareEmailBody = null, $shareIcon = null)
    {

        $this::Initialize();

        global $post;

        $checkForCustomContent = get_post_meta($post->ID);

        $this->shareButton = '';

        $defaultEmailSubject        = get_option('em_default_subject');
        $defaultEmailBody           = get_option('em_default_body');

        if($shareEmailSubject !== null)
        {
            $this->shareEmailSubject = $shareEmailSubject;
        }
        else if(isset($checkForCustomContent['_custom_email_subject']) && ($customEmailSubject = $checkForCustomContent['_custom_email_subject'][0]) != '')
        {
            $this->shareEmailSubject = $customEmailSubject;
        }
        else if ($defaultEmailSubject != '')
        {
            $this->shareEmailSubject = $defaultEmailSubject;
        }
        else
        {
            $this->shareEmailSubject = $post->post_title;
        }


        if($shareEmailBody !== null)
        {
            $this->shareEmailBody = $this::cleanUpEmailContent($shareEmailBody);
        }
        else if (isset($checkForCustomContent['_custom_email_body']) && ($checkForCustomContent = $checkForCustomContent['_custom_email_body'][0]) != '')
        {
            $this->shareEmailBody = $this::cleanUpEmailContent($checkForCustomContent);
        }
        else if ($defaultEmailBody != '')
        {
            $this->shareEmailBody = $this::cleanUpEmailContent($defaultEmailBody);
        }
        else
        {
            $body = $this::cleanUpEmailContent($post->post_content);

            $this->shareEmailBody = $body;
        }

        if($shareIcon === null)
        {
            $this->shareIcon = get_option('em_share_icon');
        }
        else if($shareIcon != '')
        {
            $this->shareIcon = $shareIcon;
        }
        else
        {
            $this->shareIcon = plugin_dir_path('admin/images/icon_missing.png');
        }

    }

    /**
     * Check if Email is activated
     */
    private function CheckIfActive(){
        if(get_option('em_social') != 'on'){
            ShareOnEM::$IsActive = false;
        } else {
            ShareOnEM::$IsActive = true;
        }

    }

    private function cleanUpEmailContent($content){

        global $post;

        $link = get_permalink($post->ID);

        $content = $link;

//        $content = nl2br($content);
//        $content = strip_tags(strip_shortcodes($content));
//        $content = rawurlencode($content);
//
//
//        //IE doesn't allow more than 2010 characters in email body
//        $maxLength      = 1500;
//        $linkLength     = strlen($link);
//        $subjectLength  = strlen($this->shareEmailSubject);
//        $newLength      = $maxLength - ($linkLength + 6 + $subjectLength);
//        $content        = strlen( $content ) > $newLength ? mb_substr( $content , 0 , ( $newLength - 3 ) ).'...' : $content;

        return $content;
    }

    /*==============================================
     *============== EMAIL SUBJECT =================
     =============================================*/
    /**
     * @return String
     */
    public function getEmailSubject() {

        if(!ShareOnEM::$IsActive){

            return null;

        } else {

            return $this->shareEmailSubject;

        }

    }

    /**
     * @params String
     */
    public function setEmailSubject($EMEmailSubject){

        if(!ShareOnEM::$IsActive){ return; }

        $this->shareEmailSubject = $EMEmailSubject;

    }


    /*==============================================
     *============== EMAIL BODY ====================
     =============================================*/
    /**
     * @return String
     */
    public function getEmailBody() {

        if(!ShareOnEM::$IsActive){

            return null;

        } else {

            return $this->shareEmailBody;

        }

    }

    /**
     * @params String
     */
    public function setEmailBody($EMEmailBody){

        if(!ShareOnEM::$IsActive){ return; }

        $this->shareEmailBody = $EMEmailBody;

    }

    /*==============================================
     *================= SHARE ICON =================
     =============================================*/
    /**
     * @return string
     */
    public function getShareIcon()
    {
        if(!ShareOnEM::$IsActive){
            return null;
        } else {
            return $this->shareIcon;
        }
    }

    /**
     * @param $EMShareIcon
     */
    public function setShareIcon($EMShareIcon)
    {
        if(!ShareOnEM::$IsActive){ return; }

        if($EMShareIcon != ''){
            $this->shareIcon = $EMShareIcon;
        }

    }


    /*==============================================
     *=============== SHARE BUTTON =================
     =============================================*/
    public function getShareButton()
    {
        if(!ShareOnEM::$IsActive){ return; }

        if (!ShareOnEM::$HasInitialized) {
            ShareOnEM::Initialize();
        }

        $buttonMarkup = '';

        global $post;

        $makeUnique = 'em-publish-'.ShareOnEM::$uniqueNumber;

        $emailSubject = $this->shareEmailSubject;

        $emailBody = $this->shareEmailBody;

        $buttonMarkup = '
            <a href="mailto:?subject='.$emailSubject.'&body='.$emailBody.'" class="email_button share_button" id="'.$makeUnique.'">
                <img src="'.$this->shareIcon.'" alt="Email share button"/>
            </a>
        ';

        $this->shareButton = $buttonMarkup;

        ShareOnEM::$uniqueNumber = ShareOnEM::$uniqueNumber+1;

        echo $this->shareButton;

    }





    /*==============================================
     *================= INITIALIZE =================
     =============================================*/
    private function Initialize()
    {

        $this->CheckIfActive();

        if(!ShareOnEM::$IsActive){return;}

        if ($this::$HasInitialized)
        {
            return;
        }

        $this::$HasInitialized = true;

    }

}