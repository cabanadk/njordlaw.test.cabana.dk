<?php

defined( 'ABSPATH' ) or die( 'Move on!' );

class ShareOnLI {

    const CHARLIMIT = 2000;

    /**
     * Url to share
     * @var String
     */
    private $shareLink;

    /**
     * UrlEncoded content to share
     * @var String
     */
    private $shareText;

    /**
     * Title of the share (e.g. Specific post title)
     * @var String
     */
    private $shareTitle;

    /**
     * The source of the share (e.g. The site title)
     * @var String
     */
    private $shareSrc;

    /**
     * @var String $shareIcon
     */
    private $shareIcon;

    /**
     * Check if the LinkedIn part of the plugin is active
     * @var bool
     */
    static private $IsActive = false;

    /**
     * Checks if the linkedIn share has been initialized
     * @var bool
     */
    static private $HasInitialized = false;

    /**
     * Adds a unique number to the id's of each button
     * @var int
     */
    static private $uniqueNumber = 0;

    public function __construct($shareIcon = null, $shareLink = null, $shareText = null, $shareTitle = null, $shareSrc = null)
    {

        $this::Initialize();

        global $post;

        $checkForCustomContent = get_post_meta($post->ID);

        $this->shareButton = '';

        $defaultLink        = get_option('li_default_share_link');
        $defaultTitle       = get_option('li_default_share_title');
        $defaultText        = get_option('li_default_share_text');

        if($checkForCustomContent['_custom_share_li_share_link'][0] != ''){
            $this->shareLink = $checkForCustomContent['_custom_share_li_share_link'][0];
        }
        else if($defaultLink != '')
        {
            $this->shareLink = $defaultLink;
        }
        else if($shareLink === null)
        {
            $this->shareLink = get_permalink($post->ID);
        }
        else
        {
            $this->shareLink = $shareLink;
        }

        if($shareIcon === null)
        {
            $this->shareIcon = get_option('li_share_icon');
        }
        else if($shareIcon != '')
        {
            $this->shareIcon = $shareIcon;
        }
        else
        {
            $this->shareIcon = plugin_dir_path('admin/images/icon_missing.png');
        }

        if($checkForCustomContent['_custom_share_li_share_text'][0] != '')
        {
            $this->shareText = $checkForCustomContent['_custom_share_li_share_text'][0];
        }
        else if ($defaultText != '')
        {
            $this->shareText = $defaultText;
        }
        else if($shareText === null)
        {
            $this->shareText = $post->post_content;
        } else
        {
            $this->shareText = $shareText;
        }

        if($checkForCustomContent['_custom_share_li_share_title'][0] != '')
        {
            $this->shareTitle = $checkForCustomContent['_custom_share_li_share_title'][0];
        }
        else if($defaultTitle != '')
        {
            $this->shareTitle = $defaultTitle;
        }
        else if($shareTitle === null)
        {
            $this->shareTitle = $post->post_title;
        }
        else
        {
            $this->shareTitle = $shareTitle;
        }

        if($shareSrc === null)
        {
            $this->shareSrc = get_bloginfo('name');
        }
        else
        {
            $this->shareSrc = $shareSrc;
        }

        $this::sanitizeFields();

    }


    /**
     * Check if LinkedIn is activated
     */
    private function CheckIfActive(){
        if(get_option('li_social') != 'on'){
            ShareOnLI::$IsActive = false;
        } else {
            ShareOnLI::$IsActive = true;
        }

    }

    /**
     * Cleans up the fields
     */
    private function sanitizeFields(){

        $this->shareText = sanitize_text_field($this->shareText);
        $this->shareText = strip_tags($this->shareText);
        $this->shareText = strip_shortcodes($this->shareText);

        $this->shareTitle = sanitize_text_field($this->shareTitle);

    }



    /*==============================================
     *================ SHARE LINK ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareLink() {

        if(!ShareOnLI::$IsActive){

            return null;

        } else {

            return $this->shareLink;

        }

    }

    /**
     * @params String
     */
    public function setShareLink($LIShareLink){

        if(!ShareOnLI::$IsActive){ return; }

        $this->shareLink = $LIShareLink;

    }



    /*==============================================
     *================ SHARE TEXT ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareText() {

        if(!ShareOnLI::$IsActive){

            return null;

        } else {

            return $this->shareText;

        }

    }

    /**
     * @params String
     */
    public function setShareText($LIShareText){

        if(!ShareOnLI::$IsActive){ return; }

        $this->shareText = $LIShareText;

    }



    /*==============================================
     *=============== SHARE TITLE ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareTitle() {

        if(!ShareOnLI::$IsActive){

            return null;

        } else {

            return $this->shareTitle;

        }

    }

    /**
     * @params String
     */
    public function setShareTitle($LIShareTitle){

        if(!ShareOnLI::$IsActive){ return; }

        $this->shareTitle = $LIShareTitle;
    }


    /*==============================================
     *=============== SHARE SOURCE =================
     =============================================*/
    /**
     * @return String
     */
    public function getShareSource() {

        if(!ShareOnLI::$IsActive){

            return null;

        } else {

            return $this->shareSrc;

        }

    }

    /**
     * @params String
     */
    public function setShareSource($LIShareSrc){

        if(!ShareOnLI::$IsActive){ return; }

        $this->shareSrc = $LIShareSrc;
    }

    /*==============================================
     *================= SHARE ICON =================
     =============================================*/
    /**
     * @return string
     */
    public function getShareIcon()
    {
        if(!ShareOnLI::$IsActive){
            return null;
        } else {
            return $this->shareIcon;
        }
    }

    /**
     * @param $LIShareIcon
     */
    public function setShareIcon($LIShareIcon)
    {
        if(!ShareOnLI::$IsActive){ return; }

        if($LIShareIcon != ''){
            $this->shareIcon = $LIShareIcon;
        }

    }



    /*==============================================
     *=============== SHARE BUTTON =================
     =============================================*/
    public function getShareButton()
    {
        if(!ShareOnLI::$IsActive){ return; }

        if (!ShareOnLI::$HasInitialized) {
            ShareOnLI::Initialize();
        }

        $buttonMarkup = '';

        $makeUnique = 'li-publish-'.ShareOnLI::$uniqueNumber;

        $shareIcon = $this->shareIcon;

        $shareLink = $this->shareLink;

        $shareTitle = $this->shareTitle;

        $shareText = $this->shareText;
        $shareText = strlen( $shareText ) > $this::CHARLIMIT ? mb_substr( $shareText,0, ( $this::CHARLIMIT - 3 ) ) . '...' : $shareText;

        $shareSrc = $this->shareSrc;

        if($shareIcon != '')
        {
            $buttonMarkup .= '<a href="https://www.linkedin.com/shareArticle?mini=true';

            if($this->shareLink != ''){
                $buttonMarkup .= '&url='.$shareLink;
            }

            if($this->shareTitle != ''){
                $buttonMarkup .= '&title='.$shareTitle;
            }

            if($this->shareText != ''){
                $buttonMarkup .= '&summary='.$shareText;
            }

            if($this->shareSrc != ''){
                $buttonMarkup .= '&source='.$shareSrc;
            }

            $buttonMarkup .= '" id="'.$makeUnique.'" class="linkedin_button share_button" target="_blank">';
            $buttonMarkup .= '<img src="'.$shareIcon.'" alt="Share on LinkedIn" style="max-width: 100%;max-height:100%;"/>';
            $buttonMarkup .= '</a>';
        }
        else
        {
            $buttonMarkup .= '<a href="https://www.linkedin.com/shareArticle?mini=true&url='.$shareLink.'&title='.$shareTitle.'&summary='.$shareText.'&source='.$shareSrc.'" class="linkedin_button share_button" id="'.$makeUnique.'" target="_blank">Share on LinkedIn</a>';
        }

        $this->shareButton =  $buttonMarkup;

        ShareOnLI::$uniqueNumber = ShareOnLI::$uniqueNumber+1;

        echo $buttonMarkup;
    }

    /**
     * @param $LIShareButton
     */
    public function setShareButton($LIShareButton)
    {
        if(!ShareOnLI::$IsActive){ return; }

        if($LIShareButton != ''){
            $this->shareButton = $LIShareButton;
        }

    }

    /*==============================================
     *================= INITIALIZE =================
     =============================================*/
    private function Initialize()
    {

        $this->CheckIfActive();

        if(!ShareOnLI::$IsActive){return;}

        if ($this::$HasInitialized)
        {
            return;
        }

        $this::$HasInitialized = true;

    }

}
