<?php

defined( 'ABSPATH' ) or die( 'Move on!' );

class ShareOnTW {

    const CHARLIMIT = 140;

    /**
     * The link to the shared page
     * @var String $shareLink
     */
    private $shareLink;

    /**
     * @var String $shareText
     */
    private $shareText;

    /**
     * @var String $shareIcon
     */
    private $shareIcon;

    /**
     * Commaseparated hashtags
     * @var String $shareHashTags
     */
    private $shareHashTags;


    /**
     * Check if the LinkedIn part of the plugin is active
     * @var bool
     */
    static private $IsActive = false;


    /**
     * Checks if the twitter share has been initialized
     * @var bool
     */
    static private $HasInitialized = false;


    /**
     * Adds a unique number to the id's of each button
     * @var int
     */
    static private $uniqueNumber = 0;


    public function __construct($shareIcon = null, $shareLink = null, $shareText = null, $shareHashTags = null)
    {

        $this::Initialize();

        global $post;

        $checkForCustomContent = get_post_meta($post->ID);

        $this->shareButton = '';

        $this->shareHashTagsCount = 0;

        $defaultText        = get_option('tw_default_share_text');
        $defaultLink        = get_option('tw_default_share_link');
        $defaultHashtags    = get_option('tw_default_share_hashtags');

        if($checkForCustomContent['_custom_share_tw_share_link'][0] != '')
        {
            $this->shareLink = $checkForCustomContent['_custom_share_tw_share_link'][0];
        }
        else if($defaultLink != '')
        {
            $this->shareLink = $defaultLink;
        }
        else if($shareLink === null)
        {
            $this->shareLink = get_permalink($post->ID);
        }
        else
        {
            $this->shareLink = $shareLink;
        }

        if($shareIcon === null)
        {
            $this->shareIcon = get_option('tw_share_icon');
        }
        else if($shareIcon != '')
        {
            $this->shareIcon = $shareIcon;
        }
        else
        {
            $this->shareIcon = plugin_dir_path('admin/images/icon_missing.png');
        }

        if($checkForCustomContent['_custom_share_tw_share_text'][0] != '')
        {
            $this->shareText = $checkForCustomContent['_custom_share_tw_share_text'][0];
        }
        else if($defaultText != '')
        {
            $this->shareText = $defaultText;
        }
        else if($shareText === null)
        {
            $this->shareText = $post->post_content;
        } else
        {
            $this->shareText = $shareText;
        }

        if($checkForCustomContent['_custom_share_tw_share_hashtags'][0] != '')
        {
            $this->shareHashTags = $checkForCustomContent['_custom_share_tw_share_hashtags'][0];
        }
        else if($defaultHashtags != '')
        {
            $this->shareHashTags = $defaultHashtags;
        }
        else if($shareHashTags === null)
        {
            $this->shareHashTags = '';
        }
        else
        {
            $this->shareHashTags = $shareHashTags;
        }

        $this::sanitizeFields();

    }


    /**
     * Check if Twitter is activated
     */
    private function CheckIfActive(){
        if(get_option('tw_social') != 'on'){
            ShareOnTW::$IsActive = false;
        } else {
            ShareOnTW::$IsActive = true;
        }

    }


    /**
     * Cleans up the fields
     */
    private function sanitizeFields(){

        $this->shareText = sanitize_text_field($this->shareText);
        $this->shareText = strip_shortcodes($this->shareText);
        $this->shareText = strip_tags($this->shareText);

    }


    /*==============================================
     *================ SHARE LINK ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareLink() {

        if(!ShareOnTW::$IsActive) {

            return null;

        } else {

            return $this->shareLink;

        }

    }

    /**
     * @params String
     */
    public function setShareLink($TWShareLink){

        if(!ShareOnTW::$IsActive) { return; }

        $this->shareLink = $TWShareLink;

    }



    /*==============================================
     *================ SHARE TEXT ==================
     =============================================*/
    /**
     * @return String
     */
    public function getShareText() {

        if(!ShareOnTW::$IsActive) {

            return null;

        } else {

            return $this->shareText;

        }


    }

    /**
     * @params String
     */
    public function setShareText($TWShareText){

        if(!ShareOnTW::$IsActive) { return; }

        $this->shareText = $TWShareText;

    }


    /*==============================================
     *============== SHARE HASH TAGS ===============
     =============================================*/
    /**
     * @return null|String
     */
    public function getShareHashTags() {

        if(!ShareOnTW::$IsActive) {

            return null;

        } else {

            return $this->shareHashTags;

        }


    }

    /**
     * @param $TWShareHashTags
     */
    public function setShareHashTags($TWShareHashTags = null){

        if(!ShareOnTW::$IsActive) { return; }

        if(!empty($TWShareHashTags) && is_array($TWShareHashTags)){

            $this->shareHashTagsCount = count($TWShareHashTags);

            $this->shareHashTags = join(',',$TWShareHashTags);

        } else if ($TWShareHashTags != ''){

            $this->shareHashTagsCount = count($TWShareHashTags);

            $this->shareHashTags = $TWShareHashTags;

        } else {

            $this->shareHashTags = '';

        }

    }

    public function getShareHashtagsCount() {

        if(!ShareOnTW::$IsActive) {

            return null;

        } else {

            return $this->shareHashTagsCount;

        }

    }


    /*==============================================
     *================= SHARE ICON =================
     =============================================*/
    /**
     * @return string
     */
    public function getShareIcon()
    {
        if(!ShareOnTW::$IsActive) {

            return null;

        } else {

            return $this->shareIcon;

        }

    }

    /**
     * @param $TWShareIcon
     */
    public function setShareIcon($TWShareIcon)
    {

        if(!ShareOnTW::$IsActive) {return;}

        if($TWShareIcon != ''){
            $this->shareIcon = $TWShareIcon;
        }

    }


    /*==============================================
     *=============== SHARE BUTTON =================
     =============================================*/
    public function getShareButton()
    {
        if(!ShareOnTW::$IsActive) {return;}

        if (!ShareOnTW::$HasInitialized) {
            ShareOnTW::Initialize();
        }

        $shareLength = $this::CHARLIMIT;

        $buttonMarkup = '';

        $makeUnique = 'tw-publish-'.ShareOnTW::$uniqueNumber;

        $shareIcon = $this->shareIcon;

        $shareLink = $this->shareLink;

        $shareText = $this->shareText;

        $hashTags = $this->shareHashTags;

        if($hashTags != '')
        {
            $shareLength = $shareLength - strlen($hashTags) - $this->shareHashTagsCount;
        }

        if($shareLink != '')
        {
            $shareLength = $shareLength - strlen($shareLink) - 1;
        }

        if($shareText != '')
        {
            $shareLength = $shareLength - 1;
        }

        $limitedShareText = strlen($shareText) > $shareLength ? mb_substr($shareText, 0, ($shareLength-3)).'...' : $shareText;

        if($shareIcon != '')
        {
            $buttonMarkup .= '<a href="https://twitter.com/share?';

            if($this->shareLink != ''){
                $buttonMarkup .= 'url='.$shareLink;
            }

            if($this->shareText != ''){
                $buttonMarkup .= '&text='.urlencode($limitedShareText);
            }

            if($this->shareHashTags != ''){
                $buttonMarkup .= '&hashtags='.urlencode($this->shareHashTags);
            }

            $buttonMarkup .= '" id="'.$makeUnique.'" class="twitter_button share_button" target="_blank">';
            $buttonMarkup .= '<img src="'.$shareIcon.'" alt="Share on Twitter" style="max-width: 100%;max-height:100%;"/>';
            $buttonMarkup .= '</a>';
        }
        else
        {
            $buttonMarkup .= '<a href="https://twitter.com/share?url='.$shareLink.'&text='.$limitedShareText.'" class="twitter_button share_button" id="'.$makeUnique.'" target="_blank">Share on Twitter</a>';
        }

        $this->shareButton =  $buttonMarkup;

        ShareOnTW::$uniqueNumber = ShareOnTW::$uniqueNumber+1;

        echo $buttonMarkup;
    }

    /**
     * @param $TWShareButton
     */
    public function setShareButton($TWShareButton)
    {

        if(!ShareOnTW::$IsActive) {return;}

        if($TWShareButton != ''){
            $this->shareButton = $TWShareButton;
        }

    }


    /*==============================================
     *================= INITIALIZE =================
     =============================================*/
    private function Initialize()
    {

        $this::CheckIfActive();

        if(!ShareOnTW::$IsActive) {return;}

        if ($this::$HasInitialized)
        {
            return;
        }

        $twInitScript = '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';

        echo $twInitScript;

        $this::$HasInitialized = true;

    }

}
