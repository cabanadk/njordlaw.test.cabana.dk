<?php

class ShareOnPrint {

    /**
     * @var String $shareIcon
     */
    private $shareIcon;

    /**
     * Check if the Print part of the plugin is active
     * @var bool
     */
    static private $IsActive = false;

    /**
     * Checks if the Print has been initialized
     * @var bool
     */
    static private $HasInitialized = false;

    /**
     * Adds a unique number to the id's of each button
     * @var int
     */
    static private $uniqueNumber = 0;

    public function __construct($shareIcon = null)

    {

        $this::Initialize();

        $this->shareButton = '';

        if($shareIcon === null)
        {
            $this->shareIcon = get_option('pr_share_icon');
        }
        else if($shareIcon != '')
        {
            $this->shareIcon = $shareIcon;
        }
        else
        {
            $this->shareIcon = plugin_dir_path('admin/images/icon_missing.png');
        }

    }
    /**
     * Check if Print is activated
     */
    private function CheckIfActive(){
        if(get_option('pr_social') != 'on'){
            ShareOnPrint::$IsActive = false;
        } else {
            ShareOnPrint::$IsActive = true;
        }

    }

    /*==============================================
     *================= SHARE ICON =================
     =============================================*/
    /**
     * @return string
     */
    public function getShareIcon()
    {
        if(!ShareOnPrint::$IsActive){
            return null;
        } else {
            return $this->shareIcon;
        }
    }

    /**
     * @param $PRShareIcon
     */
    public function setShareIcon($PRShareIcon)
    {
        if(!ShareOnPrint::$IsActive){ return; }

        if($PRShareIcon != ''){
            $this->shareIcon = $PRShareIcon;
        }

    }


    /*==============================================
     *=============== SHARE BUTTON =================
     =============================================*/
    public function getShareButton()
    {
        if(!ShareOnPrint::$IsActive){ return; }

        if (!ShareOnPrint::$HasInitialized) {
            ShareOnPrint::Initialize();
        }

        $buttonMarkup = '';

        $makeUnique = 'em-publish-'.ShareOnPrint::$uniqueNumber;

        $buttonMarkup = '
            <a href="javascript:window.print()" class="print_button share_button" id="'.$makeUnique.'">
                <img src="'.$this->shareIcon.'" alt="Print share button"/>
            </a>
        ';

        $this->shareButton = $buttonMarkup;

        ShareOnPrint::$uniqueNumber = ShareOnPrint::$uniqueNumber+1;

        echo $this->shareButton;

    }


    /*==============================================
     *================= INITIALIZE =================
     =============================================*/
    private function Initialize()
    {

        $this->CheckIfActive();

        if(!ShareOnPrint::$IsActive){return;}

        if ($this::$HasInitialized)
        {
            return;
        }

        $this::$HasInitialized = true;

    }

} 