<?php
function get_usable_post_types()
{

    /**
     * Exclude post types that won't be needed
     */
    $exceptions = array(
        'attachment',
        'revision',
        'nav_menu_item',
        'acf'
    );

    /**
     * Loop through all post types and add metaboxes to all of them
     */
    $types = get_post_types();
    $postTypes = array();
    foreach($types as $key => $type){

        if(in_array($type, $exceptions)){
            /**
             * Exclude exceptions
             */
            continue;
        }

        $postTypes[] = $type;

    }

    return $postTypes;
}

function get_active_post_types()
{

    $postTypesOptions = get_option('active_posttypes');
    $activePostTypes = array();

    if(!empty($postTypesOptions))
    {
        $i = 0;
        foreach($postTypesOptions as $key => $activePostType){
            $activePostTypes[$i] = $key;
            $i++;
        }

    }

    return $activePostTypes;

}