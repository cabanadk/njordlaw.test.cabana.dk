<?php
/**
 * Plugin Name: HTML24 Social Sharing
 * Description: Adds share buttons on pages
 * Version: 1.0.0
 * Author: HTML24
 * Author URI: http://html24.dk
 */
defined( 'ABSPATH' ) or die( 'Move on!' );

/**
 * Includes the Plugin Functions file
 */
include_once('includes/plugin-functions.php');

/**
 * Includes stylesheet for the backend page
 */
add_action( 'admin_enqueue_scripts', 'add_stylesheet_to_admin' );
function add_stylesheet_to_admin( $page ) {
    $pages = array(
        'post.php',
        'post-new.php',
        'toplevel_page_social-sharing',
        'social-sharing_page_social-twitter',
        'social-sharing_page_social-facebook',
        'social-sharing_page_social-linkedin',
        'social-sharing_page_social-email',
        'social-sharing_page_social-print'
    );
    if(!in_array($page, $pages))
    {
        return;
    }
    wp_enqueue_style( 'prefix-style', plugins_url('/css/social-sharing-admin.css' , __FILE__) );
}


include_once('admin/admin-page-social.php');

/**
 * Includes the Facebook Sharing Class if activated in the backend
 */
include_once('platforms/facebook/ShareOnFB.php');

/**
 * Includes the Twitter Sharing Class if activated in the backend
 */
include_once('platforms/twitter/ShareOnTW.php');

/**
 * Includes the LinkedIn Sharing Class if activated in the backend
 */
include_once('platforms/linkedin/ShareOnLI.php');

/**
 * Includes the Email Sharing Class if activated in the backend
 */
include_once('platforms/email/ShareOnEM.php');

/**
 * Includes the Print Class if activated in the backend
 */
include_once('platforms/print-out/ShareOnPrint.php');

/**
 * Includes the metaboxes for the posts if activated
 */
if(get_option('metabox-toggle') == 'on'){
    include_once('admin/metaboxes.php');
}