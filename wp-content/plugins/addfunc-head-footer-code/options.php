<div class="wrap">
  <?php screen_icon(); ?>
  <h2>Head & Footer Code</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder columns-2">
      <div id="post-body-content">
  <form action="options.php" method="post" id="<?php echo $plugin_id; ?>_options_form" name="<?php echo $plugin_id; ?>_options_form">
  <?php settings_fields($plugin_id.'_options'); ?>
    <label for="aFhfc_site_wide_head_code">
      <h2 class="title">Site-wide Head Code</h2>
      <p><textarea name="aFhfc_site_wide_head_code" rows="10" cols="50" id="aFhfc_site_wide_head_code" class="large-text code"><?php echo get_option('aFhfc_site_wide_head_code'); ?></textarea></p>
    </label>
    <label for="aFhfc_site_wide_footer_code">
      <h2 class="title">Site-wide Footer Code</h2>
      <p><textarea name="aFhfc_site_wide_footer_code" rows="10" cols="50" id="aFhfc_site_wide_footer_code" class="large-text code"><?php echo get_option('aFhfc_site_wide_footer_code'); ?></textarea></p>
    </label>
<?php submit_button(); ?>
  </form>
      </div> <!-- post-body-content -->
      <!-- sidebar -->
      <div id="postbox-container-1" class="postbox-container">
            <h2></h2>
            <p></p>
      </div> <!-- #postbox-container-1 .postbox-container -->
    </div> <!-- #post-body .metabox-holder .columns-2 -->
    <br class="clear">
  </div> <!-- #poststuff -->
</div>