<?php
/**
 * Plugin Name: Cabana Digital Downloads
 * Description: Stores files as blobs in the database and adds file manager.
 * Version: 1.0.0
 * Author: Cabana A/S
 * Author URI: http://cabana.dk
 */
defined('ABSPATH') or die('Silence');

$domain = 'cabana_digital_downloads_';

include_once(plugin_dir_path(__FILE__) . '/includes/activate.php');
include_once(plugin_dir_path(__FILE__) . '/includes/admin_menu.php');
include_once(plugin_dir_path(__FILE__) . '/includes/init.php');
include_once(plugin_dir_path(__FILE__) . '/includes/parse_request.php');

register_activation_hook(__FILE__, $domain . 'activate');
add_action('admin_menu', $domain . 'admin_menu');
add_action('init', $domain . 'init');
add_action('parse_request', $domain . 'parse_request');