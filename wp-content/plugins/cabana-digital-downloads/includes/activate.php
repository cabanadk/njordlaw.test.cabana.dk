<?php
function cabana_digital_downloads_activate(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'cabana_digital_downloads';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
		id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
		created DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
		filename TINYTEXT NOT NULL,
		extension TINYTEXT NOT NULL,
        mimetype TINYTEXT NOT NULL,
        data MEDIUMBLOB NOT NULL,
        extra_fields TEXT NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);
}