<?php
function cabana_digital_downloads_parse_request($wp){
    if(isset($wp->query_vars['cabana_digital_download_id'])){

        if(!is_user_logged_in()){
            wp_redirect(get_home_url());
            exit;
        }

        global $wpdb;
        $file_id = $wp->query_vars['cabana_digital_download_id'];
        $table_name = $wpdb->prefix . 'cabana_digital_downloads';
        $file = $wpdb->get_row("SELECT * FROM $table_name WHERE id = " . $file_id);

        if(!$file){
            wp_redirect(get_home_url());
            exit;
        }

        $file_size = strlen($file->data);
        $file_type = $file->mimetype;
        $file_name = $file->filename . '.' . $file->extension;

        $extra_fields = $file->extra_fields;
        if(empty($extra_fields)){
            $extra_fields = array('downloads_total' => 1);
        }else{
            $extra_fields = json_decode($extra_fields);
            $extra_fields->downloads_total = intval($extra_fields->downloads_total) + 1;
        }
        $wpdb->update($table_name, array('extra_fields' => json_encode($extra_fields)), array('id' => $file_id));

        header("Content-length: $file_size");
        header("Content-type: $file_type");
        header("Content-Disposition: attachment; filename=\"$file_name\"");

        ob_clean();
        flush();

        echo $file->data;

        exit();
    }
}