<?php
function cabana_digital_downloads_init(){
    add_rewrite_tag('%cabana_digital_download_id%', '([^&]+)');
    add_rewrite_rule( '^download-digital-file/([^&]+)', 'index.php?cabana_digital_download_id=$matches[1]', 'top' );
}