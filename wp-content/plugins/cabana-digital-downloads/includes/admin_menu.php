<?php
function cabana_digital_downloads_admin_menu(){
    add_menu_page(
           'Digital Downloads',
           'Digital Downloads',
           'create_users',
           'digital_downloads',
           'digital_downloads_menu_page_callback',
           'dashicons-admin-page',
           50
       );
    add_submenu_page(
         'digital_downloads',
         'Add new',
         'Add new',
         'create_users',
         'digital_downloads_new',
         'digital_downloads_new_submenu_page_callback'
     );
}

function digital_downloads_menu_page_callback(){
    include_once(realpath(__DIR__ . '/..') . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'list.php');
}

function digital_downloads_new_submenu_page_callback(){
    include_once(realpath(__DIR__ . '/..') . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'new.php');
}