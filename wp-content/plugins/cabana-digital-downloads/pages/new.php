<?php
// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function file_upload_max_size() {
    static $max_size = -1;

    if ($max_size < 0) {
        // Start with post_max_size.
        $max_size = parse_size(ini_get('post_max_size'));

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = parse_size(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }
    }

    return $max_size;
}

function parse_size($size) {
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
        // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    }
    else {
        return round($size);
    }
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' kB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }
    return $bytes;
}

$error = null;
$success = null;

if(isset($_POST['add_new_file'])){
    if(isset($_FILES['file'])){

        if(parse_size($_FILES['file']['size']) > file_upload_max_size() && file_upload_max_size() != 0){
            $error = 'File was too big.';
        }

        if($_FILES['file']['error'] == '1'){
            $error = 'There was an error.';
        }

        if(!$error){
            $file = $_FILES['file'];
            $path_parts = pathinfo($file['name']);
            $filename = $path_parts['filename'];
            $extension = $path_parts['extension'];
            $mimetype = $file['type'];
            $data = file_get_contents($file['tmp_name']);

            global $wpdb;
            $table_name = $wpdb->prefix . 'cabana_digital_downloads';
            $table_data = array(
                    'filename' => $filename,
                    'extension' => $extension,
                    'mimetype' => $mimetype,
                    'data' => $data,
                    'created' => current_time('mysql')
                );
            $result = $wpdb->insert($table_name, $table_data);

            if($result){
                $success = true;
            }else{
                $error = $result;
            }
        }
    }
}
?>
<div class="wrap">
    <h1>Add new file</h1>

    <?php if($error): ?>
    <div class="error">
        <p>
            <strong>ERROR</strong>: <?php echo $error; ?>
        </p>
    </div>
    <?php endif; ?>

    <?php if($success): ?>
    <div class="updated notice is-dismissible">
        <p>
            <strong>Success</strong>: file has been uploaded.
        </p>
    </div>
    <?php endif; ?>

    <form method="post" action="" enctype="multipart/form-data" name="add_new_file_form" id="add_new_file_form">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="file">Choose a file</label>
                    </th>
                    <td>
                        <input type="file" name="file" id="file" />
                        <?php if(file_upload_max_size() == 0): ?>
                        <p class="description" id="tagline-description">
                            Max file size is unlimited.
                        </p>
                        <?php else: ?>
                        <p class="description" id="tagline-description">
                            Max file size cannot exceed <?php echo formatSizeUnits(file_upload_max_size()); ?>
                        </p>
                        <?php endif; ?>                        
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="add_new_file" id="add_new_file" class="button button-primary" value="Upload" />
        </p>
    </form>
</div>