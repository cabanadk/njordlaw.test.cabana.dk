<?php
global $wpdb;

$files_page_size = 50;

$table_name = $wpdb->prefix . 'cabana_digital_downloads';
$base_url = get_admin_url() . 'admin.php?page=digital_downloads';
$paged = isset($_GET['paged']) ? intval($_GET['paged']) : 1;
$files_offset = ($paged - 1) * $files_page_size;
$files = $wpdb->get_results("SELECT id, created, filename, extension, extra_fields FROM $table_name ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT $files_page_size OFFSET $files_offset");
$fiels_count = $wpdb->get_var("SELECT COUNT(*) FROM $table_name");

if(isset($_POST['action'])){

    $action = $_POST['action'];

    if($action == 'delete_file'){
        $file_id = $_POST['file_id'];
        $result = $wpdb->delete($table_name, array('id' => $file_id));
        if($result){
            wp_redirect($base_url);
            exit();
        }
    }

    if($action == 'search'){
        $searchterm = $_POST['search'];
        $searchterm_sql = $wpdb->esc_like($searchterm);
        $searchterm_sql = '%' . $searchterm_sql . '%';
        $sql = "SELECT id, created, filename, extension, extra_fields
                FROM $table_name
                WHERE (filename LIKE %s)
                ORDER BY UNIX_TIMESTAMP(created) DESC
                LIMIT $files_page_size
                OFFSET $files_offset";
        $sql = $wpdb->prepare($sql, $searchterm_sql);
        $files = $wpdb->get_results($sql);
    }
}
?>
<div class="wrap">
    <h1>Digital Downloads</h1>
    <br />
    <p class="search-box">
        <form method="post" action="">
            <input type="search" id="post-search-input" name="search" value="<?php echo (isset($searchterm) ? $searchterm : ''); ?>" />
            <input type="submit" id="search-submit" class="button" value="Search" />
            <input type="hidden" name="action" value="search" />
        </form>
    </p>
    <table class="wp-list-table widefat">
        <thead>
            <tr>
                <td><strong>ID</strong></td>
                <td><strong>Filename</strong></td>
                <td><strong>Created</strong></td>
                <td><strong>Actions</strong></td>
                <td></td>
                <td><strong>Total Downloads</strong></td>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($files as $file):
                      $extra_fields = json_decode($file->extra_fields);
            ?>
            <tr>
                <td><?php echo $file->id; ?></td>
                <td><?php echo $file->filename; ?>.<?php echo $file->extension; ?></td>
                <td><?php echo $file->created; ?></td>
                <td>
                    <a href="<?php echo get_home_url() . '/download-digital-file/' . $file->id; ?>">Download</a>
                </td>
                <td>
                    <form method="post" action="">
                        <input type="hidden" name="file_id" value="<?php echo $file->id; ?>" />
                        <input type="hidden" name="action" value="delete_file" />
                        <a href="#" class="js-delete">Delete</a>
                    </form>
                </td>
                <td><?php echo (isset($extra_fields->downloads_total) ? $extra_fields->downloads_total : 0); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="tablenav bottom">
        <div class="tablenav-pages">
            <span class="displaying-num"><?php echo $fiels_count; ?> items</span>
            <span class="pagination-links">
                <?php if($paged > 1): ?>
                <a class="prev-page" href="<?php echo $base_url . '&paged=' . ($paged - 1); ?>">
                    <span aria-hidden="true">&larr;</span>
                </a>
                <?php endif; ?>
                <?php if($fiels_count > $files_page_size * $paged): ?>
                <a class="next-page" href="<?php echo $base_url . '&paged=' . ($paged + 1); ?>">
                    <span aria-hidden="true">&rarr;</span>
                </a>
                <?php endif; ?>
            </span>
        </div>
    </div>
</div>
<script>
    jQuery(function ($) {
        $('.js-delete').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $form = $this.parents('form');
            if (confirm('Are you sure?')) {
                $form.submit();
            }
        });
    });
</script>