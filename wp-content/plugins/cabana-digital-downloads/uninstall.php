<?php
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;
$table_name = $wpdb->prefix . 'cabana_digital_downloads';
$sql = "DROP TABLE IF EXISTS $table_name;";
$wpdb->query($sql);