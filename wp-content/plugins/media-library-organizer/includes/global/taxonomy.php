<?php
/**
 * Taxonomy class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Taxonomy {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Holds the Post Type name.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    public $taxonomy_name = 'mlo-category';

    /**
     * Constructor
     *
     * @since   1.0.0
     */
    public function __construct() {

        // Actions
        add_action( 'init', array( $this, 'register_taxonomy' ), 20 );

    }

    /**
     * Registers the Taxonomy
     *
     * @since   1.0.0
     */
    public function register_taxonomy() {
        
        // Register taxonomy
        register_taxonomy( $this->taxonomy_name, array( 'attachment' ), array(
            'labels'                => array(
                'name'              => __( 'Media Categories', 'media-library-organizer' ),
                'singular_name'     => __( 'Media Category', 'media-library-organizer' ),
                'search_items'      => __( 'Search Media Categories', 'media-library-organizer' ),
                'all_items'         => __( 'All Media Categories', 'media-library-organizer' ),
                'parent_item'       => __( 'Parent Media Category', 'media-library-organizer' ),
                'parent_item_colon' => __( 'Parent Media Category:', 'media-library-organizer' ),
                'edit_item'         => __( 'Edit Media Category', 'media-library-organizer' ),
                'update_item'       => __( 'Update Media Category', 'media-library-organizer' ),
                'add_new_item'      => __( 'Add New Media Category', 'media-library-organizer' ),
                'new_item_name'     => __( 'New Media Category', 'media-library-organizer' ),
                'menu_name'         => __( 'Media Categories', 'media-library-organizer' ),
            ),
            'public'                => false,
            'publicly_queryable'    => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_nav_menus'     => false,
            'show_in_rest'          => true,
            'show_tagcloud'         => false,
            'show_in_quick_edit'    => true,
            'show_admin_column'     => true,
            'hierarchical'          => true,
            'show_ui'               => true,

            // Force counts on Terms
            'update_count_callback' => '_update_generic_term_count',
        ) );

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}