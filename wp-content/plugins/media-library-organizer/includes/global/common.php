<?php
/**
 * Common class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Common {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Helper method to return the author options for WP_Query calls
     *
     * @since   1.0.0
     *
     * @return  array   Author options
     */
    public function get_author_options() {

        // Get users
        $users = get_users();

        // Build options
        foreach ( $users as $user ) {
            $options[ $user->ID ] = $user->user_login;
        }

        // Return filtered results
        return apply_filters( 'media_library_organizer_common_get_author_options', $options );

    }

    /**
     * Helper method to return the orderby options for WP_Query calls
     *
     * @since   1.0.0
     *
     * @return  array   orderby options
     */
    public function get_orderby_options() {

        // Build options
        $options = array(
            'ID'            => __( 'Attachment ID', 'media-library-organizer' ),
            'author'        => __( 'Author (Uploader)', 'media-library-organizer' ),
            'date'          => __( 'Date', 'media-library-organizer' ),
            'name'          => __( 'Filename', 'media-library-organizer' ),
            'modified'      => __( 'Modified Date', 'media-library-organizer' ),
            'parent'        => __( 'Uploaded to', 'media-library-organizer' ),
            'title'         => __( 'Title', 'media-library-organizer' ),
            'post_date'     => __( 'Uploaded Date', 'media-library-organizer' ),           
        );

        // Return filtered results
        return apply_filters( 'media_library_organizer_common_get_orderby_options', $options );

    }

    /**
     * Helper method to return the default orderby option
     *
     * @since   1.0.0
     *
     * @return  string  orderby default
     */
    public function get_orderby_default() {

        // Return filtered result
        return apply_filters( 'media_library_organizer_common_get_orderby_default', 'date' );

    }

    /**
     * Helper method to return the order options for WP_Query calls
     *
     * @since   1.0.0
     *
     * @return  array   order options
     */
    public function get_order_options() {

        // Build options
        $options = array(
            'ASC'     => __( 'Ascending (A-Z)', 'media-library-organizer' ),
            'DESC'    => __( 'Descending (Z-A)', 'media-library-organizer' ),
        );

        // Return filtered results
        return apply_filters( 'media_library_organizer_common_get_order_options', $options );

    }

    /**
     * Helper method to return the default order option
     *
     * @since   1.0.0
     *
     * @return  string  order default
     */
    public function get_order_default() {

        // Return filtered result
        return apply_filters( 'media_library_organizer_common_get_order_default', 'DESC' );

    }

    /**
     * Returns a flat array ordered by parent > child > child.
     * When iterated through and output, would produce structure
     * the same as wp_dropdown_cats().
     *
     * @since   1.0.0
     *
     * @param   string   $taxonomy  Taxonomy
     * @return  mixed           false | array of Terms
     */
    public function get_terms_hierarchical( $taxonomy ) {

        // Get Top Level Terms that don't have parents
        $terms = get_terms( array(
            'taxonomy'      => $taxonomy,
            'hide_empty'    => false,
            'parent'        => 0,
        ) );

        // Bail if this fails
        if ( empty( $terms ) ) {
            return false;
        }

        // Get hierarchy of Terms
        $hierarchy = _get_term_hierarchy( $taxonomy );

        // Build final term array, comprising of top level terms and all children
        $hierarchical_terms = array();
        foreach ( $terms as $term ) {
            $hierarchical_terms[] = $term;
            $hierarchical_terms = $this->add_child_terms_recursive( $taxonomy, $hierarchical_terms, $hierarchy, $term->term_id, 1 );
        }

        // Return
        return $hierarchical_terms;

    }

    /**
     * Recursive function to keep adding child terms through all depths until they are exhausted
     *
     * @since   1.0.0
     *
     * @param   string  $taxonomy               Taxonomy
     * @param   array   $hierarchical_terms     Hierarchical Terms
     * @param   array   $hierarchy              Term ID / Child ID Hierarchy
     * @param   int     $current_term_id        Current Term ID
     * @param   int     $current_depth          Current Depth
     * @return          $hierarchical_terms     Hierarchical Terms
     */
    private function add_child_terms_recursive( $taxonomy, $hierarchical_terms, $hierarchy, $current_term_id, $current_depth ) {

        // Bail if no Children exist for the current term
        if ( ! isset( $hierarchy[ $current_term_id ] ) ) {
            return $hierarchical_terms;
        }

        // Iterate through Child Term IDs, adding them to the array
        foreach ( $hierarchy[ $current_term_id ] as $child_term_id ) {
            // Get the Child Term
            $child_term = get_term( $child_term_id, $taxonomy );

            // Depending on its depth, pad the label
            $child_term->name = str_pad( '', $current_depth, '-', STR_PAD_LEFT ) . ' ' . $child_term->name;

            // Assign to the flat array of hierarchical terms
            $hierarchical_terms[] = $child_term;

            // Add Child Terms
            $hierarchical_terms = $this->add_child_terms_recursive( $taxonomy, $hierarchical_terms, $hierarchy, $child_term_id, ( $current_depth + 1 ) );
        }

        // If here, we've finished
        return $hierarchical_terms;

    }

    /**
     * Returns a string to indicate the current Media View the user is on (either list or grid)
     *
     * @since   1.0.0
     *
     * @return  string  View (list|grid)
     */
    public function get_media_view() {

        return ( get_user_option( 'media_library_mode', get_current_user_id() ) ? get_user_option( 'media_library_mode', get_current_user_id() ) : 'grid' );

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}