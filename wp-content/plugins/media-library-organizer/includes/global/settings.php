<?php
/**
 * Settings class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Settings {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * The key prefix to use for settings
     *
     * @since   1.0.0
     *
     * @var     string
     */
    private $key_prefix = '_mlo';

    /**
     * Returns a setting for the given Type
     *
     * @since   1.0.0
     *
     * @param   string  $type   Type
     * @param   string  $key    Setting Key
     * @return  mixed           Setting Value
     */ 
    public function get_setting( $type, $key ) {

        // Get settings
        $settings = $this->get_settings( $type );

        // Get setting
        $setting = ( isset( $settings[ $key ] ) ? $settings[ $key ] : '' );

        // Allow devs / addons to filter setting
        $setting = apply_filters( 'media_library_organizer_settings_get_setting', $setting, $type, $key );

        // Return
        return $setting;

    }

    /**
     * Returns all settings for the given Type
     *
     * @since   1.0.0
     *
     * @param    string     $type   Type
     * @return   array              Settings
     */
    public function get_settings( $type ) {

        // Get settings
        $settings = get_option( $this->key_prefix . '_' . $type );

        // Get default settings
        $defaults = $this->get_default_settings( $type );

        // If we couldn't fetch any defaults, we can only return the settings
        if ( empty( $defaults ) ) {
            return $settings;
        }

        // If no settings exists, fallback to the defaults
        if ( ! $settings ) {
            $settings = $defaults;
        } else {
            // Iterate through the defaults, checking if the settings have the same key
            // If not, add the setting key with the default value
            // This ensures that on a Plugin upgrade where new defaults are introduced,
            // they are immediately available for use without the user needing to save their
            // settings.
            foreach ( $defaults as $default_key => $default_value ) {
                if ( ! isset( $settings[ $default_key ] ) ) {
                    $settings[ $default_key ] = $default_value;
                }
            }
        }

        // Allow devs / addons to filter settings
        $settings = apply_filters( 'media_library_organizer_settings_get_settings', $settings, $type );

        // Return
        return $settings;

    }

    /**
     * Saves a single setting for the given Type and Key
     *
     * @since   1.0.0
     *
     * @param   string  $type   Type
     * @param   string  $key    Setting Key
     * @param   mixed   $value  Setting Value
     * @return  bool            Success
     */
    public function update_setting( $type, $key, $value ) {

        // Get settings
        $settings = $this->get_settings( $type );

        // Allow devs / addons to filter setting
        $value = apply_filters( 'media_library_organizer_settings_update_setting', $value, $type, $key );

        // Update single setting
        $settings[ $key ] = $value;

        // Update settings
        return $this->update_settings( $type, $settings );

    }

    /**
     * Saves all settings for the given Type
     *
     * @since 1.0.0
     *
     * @param    string  $type       Type
     * @param    array   $settings   Settings
     * @return   bool                Success
     */
    public function update_settings( $type, $settings ) {

        // Allow devs / addons to filter settings
        $settings = apply_filters( 'media_library_organizer_settings_update_settings', $settings, $type );

        // Update settings
        update_option( $this->key_prefix . '_' . $type, $settings );
        
        return true;

    }

    /**
     * Deletes a single setting for the given Type and Key
     *
     * @since   1.0.0
     *
     * @param   string  $type   Type
     * @param   string  $key    Key
     * @return  bool            Success
     */
    public function delete_setting( $type, $key ) {

        // Get settings
        $settings = $this->get_settings( $type );

        // Delete single setting
        if ( isset( $settings[ $key ] ) ) {
            unset( $settings[ $key ] );
        }

        // Allow devs / addons to filter settings
        $settings = apply_filters( 'media_library_organizer_settings_delete_setting', $settings, $type, $key );

        // Update settings
        return $this->update_settings( $type, $settings );

    }

    /**
     * Deletes all settings for the given Type
     *
     * @since   1.0.0
     *
     * @param   string  $type   Type
     * @return  bool            Success
     */
    public function delete_settings( $type ) {

        // Delete settings
        delete_option( $this->key_prefix . '_' . $type );

        // Allow devs / addons to run any other actions now
        do_action( 'media_library_organizer_settings_delete_settings', $type );

        return true;

    }

    /**
     * Returns the default settings for the given Type
     *
     * @since   1.0.0
     *
     * @param   string $type    Type
     * @return  mixed           Default Settings | empty string
     */
    private function get_default_settings( $type ) {

        // Define defaults
        $defaults = array(
            // General
            'general'   => array(
                'taxonomy_enabled'  => 1,
                'orderby_enabled'   => 1,
                'order_enabled'     => 1,
            ),

            // User Options
            'user-options' => array(
                'orderby_enabled'   => 1,
                'order_enabled'     => 1,
            ),
        );

        // Allow devs to filter defaults
        $defaults = apply_filters( 'media_library_organizer_settings_get_default_settings', $defaults, $type );

        // Return
        return ( isset( $defaults[ $type ] ) ? $defaults[ $type ] : '' );

    }

    /**
     * Recursive array_merge function
     *
     * @since   1.0.0
     *
     * @param   array   $array1
     * @param   array   $array2
     * @return  array               Merged Data
     */
    private function array_merge_assoc_recursive( $array1, $array2 ) {

        $merged = $array1;

        foreach ( $array2 as $key => $value ) {
            if ( is_array( $value ) && isset( $merged[ $key ] ) && is_array( $merged[ $key ] ) ) {
                $merged[ $key ] = $this->array_merge_assoc_recursive( $merged[ $key ], $value );
            } else if ( is_numeric( $key ) ) {
                if ( ! in_array( $value, $merged ) ) {
                    $merged[] = $value;
                }
            } else {
                $merged[ $key ] = $value;
            }
        }

        return $merged;

    }

    /**
     * Recursive array_diff function
     *
     * @since 1.0.0
     *
     * @param   array   $array1
     * @param   array   $array2
     * @return  array               Difference
     */
    private function array_diff_assoc_recursive( $array1, $array2 ) {

        $difference = array();

        foreach( $array1 as $key => $value ) {
            if( is_array( $value ) ) {
                if( ! isset( $array2[ $key ]) || ! is_array( $array2[ $key ]) ) {
                    $difference[ $key ] = $value;
                } else {
                    $new_diff = $this->array_diff_assoc_recursive( $value, $array2[ $key ] );
                    if( ! empty( $new_diff ) ) {
                        $difference[ $key ] = $new_diff;
                    }
                }
            } else if( ! array_key_exists( $key, $array2 ) || $array2[ $key ] != $value ) {
                $difference[ $key ] = $value;
            }
        }

        return $difference;

    }

    /**
     * Escapes the given string value, depending on whether we're in the WordPress Administration
     * interface or not.
     *
     * @since   1.0.0
     *
     * @param   string  $string     String to escape
     * @return  string              Escaped string
     */
    private function escape_string( $string ) {

        if ( is_admin() ) {
            // Escape the data, so it outputs in an input field correctly
            $string = esc_attr( $string );
        }

        // Stripslashes
        $string = stripslashes( $string );

        // Return
        return $string;
        
    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}