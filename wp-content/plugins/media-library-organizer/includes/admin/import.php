<?php
/**
 * Import class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Import {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Holds the base class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $base;

    /**
     * Constructor
     *
     * @since 1.0.0
     */
    public function __construct() {

        // Importers
        add_action( 'media_library_organizer_import', array( $this, 'import' ), 10, 2 );

        // Enhanced Media Library
        add_action( 'media_library_organizer_import_view_enhanced_media_library', array( $this, 'import_enhanced_media_library_view' ) );
        add_filter( 'media_library_organizer_import_enhanced_media_library', array( $this, 'import_enhanced_media_library' ) );

    }

    /**
     * Helper method to retrieve an array of import sources that this plugin
     * can import link data from.
     *
     * These will typically be other WordPress Plugins that have data stored
     * in this WordPress installation.
     *
     * @since   1.0.0
     *
     * @return  array   Import Sources
     */
    public function get_import_sources() {

        // Determine which Plugins the user might be able to import data from
        $import_sources = array();

        // Enhanced Media Library
        $eml = get_option( 'wpuxss_eml_version' );
        if ( ! empty( $eml ) && $eml != false ) {
            $import_sources['enhanced_media_library'] = array(
                'name'          => 'enhanced_media_library',
                'label'         => __( 'Enhanced Media Library', 'media-library-organizer' ),
                'documentation' => 'https://wpaffiliatelinker.com/documentation/import-easy-affiliate-links/',
            );
        }

        // Allow devs to filter import sources
        $import_sources = apply_filters( 'media_library_organizer_import_get_import_sources', $import_sources );

        // Return
        return $import_sources;
        
    }

    /**
     * Import data created by this Plugin's export functionality
     *
     * @since   1.0.0
     *
     * @param   bool    $success    Success
     * @param   array   $data       Array
     * @return  mixed               WP_Error | bool
     */
    public function import( $success, $data ) {

        // Check data is an array
        if ( ! is_array( $data ) ) {
            return new WP_Error( 'media_library_organizer_error', __( 'Supplied file is not a valid JSON settings file, or has become corrupt.', 'media-library-organizer' ) );
        }

        // Get settings instance
        $settings_instance = Media_Library_Organizer_Settings::get_instance();
        
        // Iterate through settings screens ($data), saving the settings
        foreach ( $data as $type => $value ) {
            $settings_instance->update_settings( $type, $value );
        }

        // Done
        return true;
        
    }

    /**
     * Outputs options on the Import screen for Enhanced Media Library
     *
     * @since   1.0.0
     */
    public function import_enhanced_media_library_view() {

        // Get base instance
        $this->base = Media_Library_Organizer::get_instance();

        // Get the Taxonomies that Enhanced Media Library has registered
        $taxonomies = get_option( 'wpuxss_eml_taxonomies' );

        // Load view
        include_once( $this->base->plugin->folder . 'views/admin/settings-import-enhanced-media-library.php' ); 

    }

    /**
     * Import data from Enhanced Media Library
     *
     * @since   1.0.0
     *
     * @param   bool    Success
     * @return  mixed   WP_Error | bool
     */
    public function import_enhanced_media_library( $success = false ) {

        // Get instances
        $settings_instance = Media_Library_Organizer_Settings::get_instance();

        // Get POST data
        $data = $_POST;

        // Bail if no Taxonomies were selected
        if ( ! isset( $data['taxonomies'] ) || empty( $data['taxonomies'] ) ) {
            return new WP_Error( 'media_library_organizer_import_enhanced_media_library', __( 'Please select at least one Taxonomy to import.', 'media-library-organizer' ) );
        }

        /**
         * 1. General
         */
        // N/A

        /**
         * 2. Taxonomy Terms
         */
        foreach ( $data['taxonomies'] as $taxonomy ) {

            // Fetch Taxonomy Terms
            $terms = $this->get_terms( $taxonomy );

            // Define an array to store old to new Term mappings
            $terms_mappings = array();

            // If no Terms were found, skip
            if ( empty( $terms ) || ! $terms ) {
                continue;
            }

            // For each Term, add it to this Plugin's Taxonomy
            foreach ( $terms as $import_term_id => $import_term ) {

                // For this Term, iterate through any parent term(s) that might exist
                // until the Top Level Term is reached.  This builds an array
                // of child --> child --> parent.
                $terms_stack = array();
                $has_parent = true;
                while ( $has_parent ) {
                    // Note that this is a Child Term
                    $terms_stack[ $import_term->term_taxonomy_id ] = $import_term;

                    // If this Term does not have a Parent, exit the loop
                    if ( $import_term->parent == 0 ) {
                        $has_parent = true;
                        break;
                    }

                    // If here, a Parent Term exists.
                    // Get Parent Term
                    $import_term = $terms[ $import_term->parent ];
                }

                // Reverse the array of stacked terms, so we're working from Parent --> Child --> Child etc
                $terms_stack = array_reverse( $terms_stack );

                // We can now safely iterate through this collection of Terms, assigning each to its parent
                // if a Parent exists.
                // Because it's ordered Parent --> Child --> Child, no Child Term can be assigned to a Parent
                // that does not exist.

                // Iterate through the Terms Stack, creating them for this Plugin's Taxonomy
                foreach ( $terms_stack as $child_term ) {
                    // Create Term
                    $result = $this->create_term( $child_term->name, $child_term->description, ( isset( $term_mappings[ $child_term->parent ] ) ? $term_mappings[ $child_term->parent ] : '' ) );

                    // Bail if an error occured
                    if ( is_wp_error( $result ) ) {
                        return $result;
                    }

                    // Map this Term
                    $term_mappings[ $child_term->term_taxonomy_id ] = $result;
                }
                
            }

            // Get Term Relationships with Attachments
            $attachments = $this->get_term_relationships( array_keys( $term_mappings ) );

            // Iterate through Attachments, creating new Term Relationships
            if ( is_array( $attachments ) && count( $attachments ) > 0 ) {
                foreach ( $attachments as $attachment_id => $old_term_ids ) {
                    // Build an array of the new Plugin Taxonomy Term IDs for this Attachment
                    $term_ids = array();
                    foreach ( $old_term_ids as $old_term_id ) {
                        // Skip if, for some reason, the old Term doesn't have a new Plugin Taxonomy Term ID
                        if ( ! isset( $term_mappings[ $old_term_id ] ) ) {
                            continue;
                        }

                        // Add the new Plugin Taxonomy Term ID to the Attachment
                        $term_ids[] = absint( $term_mappings[ $old_term_id ] );
                    }

                    // If no Plugin Taxonomy Term IDs were mapped, skip
                    if ( count( $term_ids ) == 0 ) {
                        continue;
                    }

                    // Assign the Plugin Taxonomy Term IDs to the Attachment
                    $result = wp_set_object_terms( $attachment_id, $term_ids, Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name, false );

                    // Bail if something went wrong
                    if ( is_wp_error( $result ) ) {
                        return $result;
                    }
                }
            }

        }

        // Done
        return true;

    }

    /**
     * Creates a new Term for this Plugin's Taxonomy, if it does not already exist.
     *
     * @since   1.0.0
     *
     * @param   string  $term_name      Term Name
     * @param   string  $description    Term Description
     * @param   int     $parent_id      Parent Term ID
     * @return  int                     Term ID
     */
    private function create_term( $term_name, $description = '', $parent_id = '' ) {

        // Check if this Term Name already exists in this Plugin's Taxonomy
        // If so, return its ID
        $existing_term = get_term_by( 'name', $term_name, Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name );
        if ( $existing_term != false ) {
            return $existing_term->term_id;
        }

        // Term Name does not exist.
        // Create Term for this Plugin's Taxonomy
        $result = wp_insert_term( $term_name, Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name, array(
            'description' => $description,
            'parent'      => $parent_id,
        ) );
                
        // Bail if an error occured
        if ( is_wp_error( $result ) ) {
            return $result;
        }

        // Return the ID
        return $result['term_id'];

    }

    /**
     * Returns an array of Term IDs and Names for the given Taxonomy, when the Taxonomy
     * might not be registered in WordPress (i.e. it's a Taxonomy registered through
     * a third party Plugin that isn't active).
     *
     * @since   1.0.0
     *
     * @param   string  $taxonomy   Taxonomy Name
     * @return  array               Taxonomy Term IDs
     */
    private function get_terms( $taxonomy ) {

        global $wpdb;

        // Get Term data for the given Taxonomy
        $terms = $wpdb->get_results( 
            $wpdb->prepare(     "SELECT  " . $wpdb->term_taxonomy . ".term_taxonomy_id,
                                " . $wpdb->term_taxonomy . ".description,
                                " . $wpdb->term_taxonomy . ".parent,
                                " . $wpdb->terms . ".name 
                                FROM " . $wpdb->term_taxonomy . "
                                LEFT JOIN  " . $wpdb->terms . "
                                ON  " . $wpdb->term_taxonomy . ".term_id =  " . $wpdb->terms . ".term_id
                                WHERE " . $wpdb->term_taxonomy . ".taxonomy = '%s'",
                                $taxonomy )
        );

        // If no Terms, bail
        if ( empty( $terms ) ) {
            return $terms;
        }

        // Make Terms associative, so the keys are the Term IDs
        $terms_assoc = array();
        foreach ( $terms as $term ) {
            $terms_assoc[ $term->term_taxonomy_id ] = $term;
        }

        // Return
        return $terms_assoc;

    }

    /**
     * Returns results from _terms_relationships, comprising of Attachment IDs and their
     * Taxonomy Term ID, for the given array of Term IDs, when the Taxonomy
     * might not be registered in WordPress (i.e. it's a Taxonomy registered through
     * a third party Plugin that isn't active).
     *
     * @since   1.0.0
     *
     * @param   array   $term_ids   Term IDs
     * @return  array               Attachment to Term ID Relationships
     */
    private function get_term_relationships( $term_ids ) {

        global $wpdb;

        // Get Attachment IDs that have any of the given Term IDs assigned to them
        $attachments = $wpdb->get_results(
            "SELECT  " . $wpdb->term_relationships . ".object_id,
            " . $wpdb->term_relationships . ".term_taxonomy_id
            FROM " . $wpdb->term_relationships . "
            WHERE " . $wpdb->term_relationships . ".term_taxonomy_id IN (" . implode( ',', $term_ids ) . ")"
        );

        // If no Attachments, bail
        if ( empty( $attachments ) ) {
            return $attachments;
        }

        // Iterate through results, storing by Attachment ID
        $attachments_assoc = array();
        foreach ( $attachments as $attachment ) {
            if ( ! isset( $attachments_assoc ) ) {
                $attachments_assoc[ $attachment->object_id ] = array( absint( $attachment->term_taxonomy_id ) );
            } else {
                $attachments_assoc[ $attachment->object_id ][] = absint( $attachment->term_taxonomy_id );
            }
        }

        // Return
        return $attachments_assoc;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}