<?php
/**
 * Handles output of Taxonomy Filters in List, Grid and Modal views.
 * Saves Taxonomy Term data when changed / saved on Attachments.
 * Stores User Preferences for Order By and Order filters.
 *
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Media {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Holds the base class object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $base;

    /**
     * Holds the Taxonomy
     *
     * @since   1.0.0
     *
     * @var     object
     */
    private $taxonomy = '';

    /**
     * Primary class constructor.
     *
     * @since 1.0.0
     */
    public function __construct() {

        // Add the Taxonomy as a dropdown filter to the WP_List_Table List view
        add_action( 'restrict_manage_posts', array( $this, 'output_list_table_filters' ), 10, 2 );

        // Add the Taxonomy as a dropdown filter to the Grid view
        add_action( 'wp_enqueue_media', array( $this, 'output_backbone_view_filters' ) );

        // Alter query arguments in WP_Query which is run when filtering Attachments in the Grid View
        add_filter( 'ajax_query_attachments_args', array( $this, 'filter_attachments_grid' ) );

        // Add query arguments to the WP_Query which is run when filtering Attachments in the List View
        add_filter( 'pre_get_posts', array( $this, 'filter_attachments_list' ) );

        // Define fields to display when editing an attachment in a modal
        add_filter( 'attachment_fields_to_edit', array( $this, 'add_fields' ), 10, 2 );

        // Save Categories when the attachment is saved in a modal
        add_filter( 'attachment_fields_to_save', array( $this, 'save_fields' ), 10, 2 );

    }

    /**
     * Outputs Taxonomy Filters and Sorting in the Attachment WP_List_Table
     *
     * @since   1.0.0
     *
     * @param   string  $post_type  Post Type
     * @param   string  $view_name  View Name
     */
    public function output_list_table_filters( $post_type, $view_name ) {

        // Bail if we're not viewing Attachments
        if ( $post_type != 'attachment' ) {
            return;
        }

        // Bail if we're not in the bar view
        if ( $view_name != 'bar' ) {
            return;
        }

        // Get instances
        $common         = Media_Library_Organizer_Common::get_instance();
        $settings       = Media_Library_Organizer_Settings::get_instance();
        $taxonomy       = Media_Library_Organizer_Taxonomy::get_instance();
        $user_options   = Media_Library_Organizer_User_Option::get_instance();

        // Determine the currently selected Taxonomy Term, orderby and order parameters
        $current_term = false;
        if ( isset( $_REQUEST[ $taxonomy->taxonomy_name ] ) ) {
            $current_term = sanitize_text_field( $_REQUEST[ $taxonomy->taxonomy_name ] );
        }

        // Determine the current orderby
        if ( isset( $_REQUEST['orderby'] ) ) {
            // Get from the <select> dropdown
            $current_orderby = sanitize_text_field( $_REQUEST['orderby'] );
        } else {
            // Get orderby default from the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'orderby_enabled' ) ) {
                $current_orderby = $user_options->get_option( get_current_user_id(), 'orderby' );
            } else {
                // Get from Plugin Defaults
                $current_orderby = $common->get_orderby_default();
            }
        }

        // Determine the current order
        if ( isset( $_REQUEST['order'] ) ) {
            // Get from the <select> dropdown
            $current_order = sanitize_text_field( $_REQUEST['order'] );
        } else {
            // Get orderby default from the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'order_enabled' ) ) {
                $current_order = $user_options->get_option( get_current_user_id(), 'order' );
            } else {
                // Get from Plugin Defaults
                $current_order = $common->get_order_default();
            }
        }

        // Taxonomy Filter
        if ( Media_Library_Organizer_Settings::get_instance()->get_setting( 'general', 'taxonomy_enabled' ) ) {
            wp_dropdown_categories( array(
                'show_option_all'   => __( 'All Media Categories', 'media-library-organizer' ),
                'show_option_none'   => __( '(Unassigned)', 'media-library-organizer' ),
                'option_none_value' => -1,
                'orderby'           => 'name',
                'order'             => 'ASC',
                'show_count'        => true,
                'hide_empty'        => false,
                'echo'              => true,
                'selected'          => $current_term,
                'hierarchical'      => true,
                'name'              => $taxonomy->taxonomy_name,
                'id'                => '',
                'class'             => '',
                'taxonomy'          => $taxonomy->taxonomy_name,
                'value_field'       => 'slug',
            ) );
        }

        // Order By Filter
        if ( $settings->get_setting( 'general', 'orderby_enabled' ) ) {
            $orderby = Media_Library_Organizer_Common::get_instance()->get_orderby_options();
            if ( ! empty( $orderby ) ) {
                ?>
                <select name="orderby" id="mlo-orderby" size="1">
                    <?php
                    foreach ( $orderby as $key => $value ) {
                        ?>
                        <option value="<?php echo $key; ?>"<?php selected( $current_orderby, $key ); ?>><?php echo $value; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <?php
            }
        }

        // Order Filter
        if ( $settings->get_setting( 'general', 'order_enabled' ) ) {
            $order = Media_Library_Organizer_Common::get_instance()->get_order_options();
            if ( ! empty( $order ) ) {
                ?>
                <select name="order" id="mlo-order" size="1">
                    <?php
                    foreach ( $order as $key => $value ) {
                        ?>
                        <option value="<?php echo $key; ?>"<?php selected( $current_order, $key ); ?>><?php echo $value; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <?php
            }
        }

    }

    /**
     * Enqueues JS and CSS whenever wp_enqueue_media() is called, which is used for
     * any media upload, management or selection screens / views.
     * 
     * Also Outputs Taxonomy Filters and Sorting in the Attachment Backbone Grid View
     * (wp.media.view.AttachmentsBrowser)
     *
     * @since   1.0.0
     */
    public function output_backbone_view_filters() {

        // Get instances
        $this->base     = Media_Library_Organizer::get_instance();
        $common         = Media_Library_Organizer_Common::get_instance();
        $settings       = Media_Library_Organizer_Settings::get_instance();
        $taxonomy       = Media_Library_Organizer_Taxonomy::get_instance();
        $user_options   = Media_Library_Organizer_User_Option::get_instance();

        // JS
        wp_enqueue_script( $this->base->plugin->name . '-media', $this->base->plugin->url . 'assets/js/media.js', array( 'media-editor', 'media-views' ), false, true );
        wp_localize_script( $this->base->plugin->name . '-media', 'media_library_organizer_media', array(
            'order'     => $common->get_order_options(),
            'orderby'   => $common->get_orderby_options(),
            'settings'  => $settings->get_settings( 'general' ),
            'terms'     => $common->get_terms_hierarchical( $taxonomy->taxonomy_name ),
            'taxonomy'  => get_taxonomy( $taxonomy->taxonomy_name ),

            // Defaults
            'defaults'  => array(
                'orderby'           => $common->get_orderby_default(),
                'order'             => $common->get_order_default(),
            ),

            // User Options
            'user_options' => array(
                'orderby_enabled'   => $settings->get_setting( 'user-options', 'orderby_enabled' ),
                'orderby'           => $user_options->get_option( get_current_user_id(), 'orderby' ),

                'order_enabled'     => $settings->get_setting( 'user-options', 'order_enabled' ),
                'order'             => $user_options->get_option( get_current_user_id(), 'order' ),
            ),
        ) );

        // CSS
        wp_enqueue_style( $this->base->plugin->name . '-media', $this->base->plugin->url . 'assets/css/media.css' );

    }

    /**
     * Extends the functionality of the Media Views WP_Query, by adding support
     * for the following filter options this Plugin provides:
     * - Apply the orderby User Option, if supplied
     * - Apply the order User Option, if supplied
     * - No Taxonomy Term assigned
     *
     * @since   1.0.0
     *
     * @param   array   $args   WP_Query Arguments
     * @return  array           WP_Query Arguments
     */
    public function filter_attachments_grid( $args ) {

        // Get this Plugin's Taxonomy Name
        $taxonomy = Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name;

        // Get the User Options instance
        $user_options = Media_Library_Organizer_User_Option::get_instance();

        // Update the orderby and order User Options
        if ( isset( $args['orderby'] ) ) {
            $user_options->update_option( get_current_user_id(), 'orderby', $args['orderby'] );
        }
        if ( isset( $args['order'] ) ) {
            $user_options->update_option( get_current_user_id(), 'order', $args['order'] );
        }

        // Don't filter the query if our Taxonomy is not set
        if ( ! isset( $args[ $taxonomy ] ) ) {
            return $args;
        }

        // Don't filter the query if our Taxonomy Term isn't -1 (i.e. Unassigned)
        $term = sanitize_text_field( $args[ $taxonomy ] );
        if ( $term != "-1" ) {
            return $args;
        }

        // Filter the query to include Attachments with no Term
        // Unset the Taxonomy query var, as we'll be using tax_query 
        unset( $args[ $taxonomy ] );

        $args['tax_query'] = array(
            array(
                'taxonomy'  => $taxonomy,
                'operator'  => 'NOT EXISTS',
            ),
        );
        
        // Allow devs / addons to filter the args
        $args = apply_filters( 'media_library_organizer_media_filter_attachments_grid', $args );

        // Return
        return $args;

    }

    /**
     * Extends the functionality of the Media Views WP_Query used in the List View, by adding support
     * for the following filter options this Plugin provides:
     * - Apply the orderby User Option, if supplied
     * - Apply the order User Option, if supplied
     * - No Taxonomy Term assigned
     *
     * @since   1.0.0
     *
     * @param   WP_Query    $query  WP_Query Object
     * @return  WP_Query            WP_Query Object
     */
    public function filter_attachments_list( $query ) {

        // Get instances
        $common         = Media_Library_Organizer_Common::get_instance();
        $settings       = Media_Library_Organizer_Settings::get_instance();
        $taxonomy       = Media_Library_Organizer_Taxonomy::get_instance();
        $user_options   = Media_Library_Organizer_User_Option::get_instance();

        // Don't filter the query if we're not querying for attachments
        if ( is_array( $query->query['post_type'] ) && ! in_array( 'attachment', $query->query['post_type'] ) ) {
            return $query;
        }
        if ( ! is_array( $query->query['post_type'] ) && strpos( $query->query['post_type'], 'attachment' ) === false ) {
            return $query;
        }

        // Order By: Get / set User Options, if enabled
        if ( isset( $_REQUEST['orderby'] ) ) {
            // Store the chosen filter in the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'orderby_enabled' ) ) {
                $user_options->update_option( get_current_user_id(), 'orderby', sanitize_text_field( $_REQUEST['orderby'] ) );
            }
        } else {
            // Get orderby default from the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'orderby_enabled' ) ) {
                $orderby = $user_options->get_option( get_current_user_id(), 'orderby' );
            } else {
                // Get from Plugin Defaults
                $orderby = $common->get_order_default();
            }

            // Update WP_Query with the orderby parameter
            $query->set( 'orderby',  $orderby );
            $query->query['orderby'] = $orderby;
        }

        // Order By: Get / set User Options, if enabled
        if ( isset( $_REQUEST['order'] ) ) {
            // Store the chosen filter in the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'order_enabled' ) ) {
                $user_options->update_option( get_current_user_id(), 'order', sanitize_text_field( $_REQUEST['order'] ) );
            }
        } else {
            // Get orderby default from the User's Options, if set to persist
            if ( $settings->get_setting( 'user-options', 'order_enabled' ) ) {
                $order = $user_options->get_option( get_current_user_id(), 'order' );
            } else {
                // Get from Plugin Defaults
                $order = $common->get_order_default();
            }

            // Update WP_Query with the order parameter
            $query->set( 'order',  $order );
            $query->query['order'] = $order;
        }

        // Don't filter the query if our Taxonomy is not set
        if ( ! isset( $_REQUEST[ $taxonomy->taxonomy_name ] ) ) {
            return $query;
        }

        // Don't filter the query if our Taxonomy Term isn't -1 (i.e. Unassigned)
        $term = sanitize_text_field( $_REQUEST[ $taxonomy->taxonomy_name ] );
        if ( $term != "-1" ) {
            return $query;
        }

        // Filter the query to include Attachments with no Term
        // Unset the Taxonomy query var, as we'll be using tax_query 
        unset( $query->query_vars[ $taxonomy->taxonomy_name ] );

        // Define the tax_query
        $tax_query = array(
            'taxonomy'  => $taxonomy->taxonomy_name,
            'operator'  => 'NOT EXISTS',
        );

        // Assign it to both the WP_Query and tax_query objects
        $query->set( 'tax_query', array( $tax_query ) );
        $query->tax_query = new WP_Tax_Query( array( $tax_query ) );

        // Allow devs / addons to filter the query
        $query = apply_filters( 'media_library_organizer_media_filter_attachments', $query, $_REQUEST );

        // Return
        return $query;

    }

    /**
     * Adds the Taxonomy as a checkbox list in the Attachment Modal view.
     *
     * By default, WordPress adds all Taxonomies as input[type=text] when editing an attachment.
     *
     * This defines the options and values for the Taxonomy, ensuring the field is output
     * as checkboxes when using the Backbone Modal view.
     *
     * @since   1.0.0
     *
     * @param   array       $form_fields    Attachment Form Fields
     * @param   WP_Post     $post           Attachment Post
     * @return  array                       Attachment Form Fields
     */
    public function add_fields( $form_fields, $post = null ) {

        // Determine the current screen we're on
        $screen = get_current_screen();

        // Bail if we're on the attachment post screen, as this screen outputs
        // the taxonomy correctly.
        if ( $screen->base == 'post' ) {
            return $form_fields;
        }

        // Get Taxonomy
        if ( empty( $this->taxonomy ) ) {
            $this->taxonomy = get_taxonomy( Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name );
        }

        // Define the Taxonomy Field as a Checkbox list
        $form_fields[ $this->taxonomy->name ] = array(
            'label'     => $this->taxonomy->label,
            'input'     => 'html',
            'html'      => $this->terms_checkbox( $post, $this->taxonomy ),
        );

        // Return
        return $form_fields;

    }


    /**
     * Similar to post_categories_meta_box(), but returns the output
     * instead of immediately outputting it.
     *
     * @since   1.0.0
     *
     * @param   WP_Post         $post       Post
     * @param   WP_Taxonomy     $taxonomy   Taxonomy
     * @return  string                      Taxonomy HTML Checkboxes
     */
    private function terms_checkbox( $post, $taxonomy ) {

        // Build HTML Output, using our custom Taxonomy Walker for wp_terms_checklist()
        $html = '
        <div id="taxonomy-' . $taxonomy->name . '" class="categorydiv">
            <div id="' . $taxonomy->name . '-all" class="tabs-panel">
                <ul id="' . $taxonomy->name . 'checklist" data-wp-lists="list:' . $taxonomy->name . '" class="categorychecklist form-no-clear"> ' . 
                    wp_terms_checklist( $post->ID, array( 
                        'taxonomy'      => $taxonomy->name, 
                        'walker'        => new Media_Library_Organizer_Taxonomy_Walker,
                        'echo'          => false,
                    ) ) 
                    . '
                </ul>
            </div>
        </div>';

        // Return output
        return $html;

    }

    /**
     * Saves the Terms to the Attachment when we're in the Attachment Modal view.
     *
     * Because the Backbone Modal view doesn't support field names of e.g. attachments[post_id][taxonomy],
     * we send the data as $_REQUEST['taxonomy_termID'] - so the $attachment argument is of no use to us.
     *
     * @since   1.0.0
     *
     * @param   array     $post             Attachment Post
     * @param   array     $attachment       Attachment $_POST['attachment'] data (not used)
     * @return  array                       Attachment Post
     */
    public function save_fields( $post, $attachment ) {

        // Determine the current screen we're on
        $screen = get_current_screen();

        // Bail if we're on the attachment post screen, as this screen saves
        // the taxonomy correctly.
        if ( $screen->base == 'post' ) {
            return $post;
        }

        // Get the programmatic Taxonomy Name
        $taxonomy = Media_Library_Organizer_Taxonomy::get_instance()->taxonomy_name;

        // Build an array of Term IDs that have been selected
        $term_ids = array();
        foreach ( $_REQUEST as $key => $value ) {
            // Sanitize the key
            $key = sanitize_text_field( $key );
            
            // Skip if the key doesn't contain our taxonomy name
            if ( strpos( $key, $taxonomy . '_' ) === false ) {
                continue;
            }

            // Extract the Term ID
            list( $prefix, $term_id ) = explode( '_', $key );

            // Add the Term ID to the array, as an integer
            $term_ids[] = absint( $term_id );
        }

        // If no Term IDs exist, delete all Terms associated with this Attachment
        if ( empty( $term_ids ) ) {
            wp_delete_object_term_relationships( $post['ID'], $taxonomy );

            // Return
            return $post;
        }

        // Term IDs were selected, so associate them with this Attachment
        wp_set_object_terms( $post['ID'], $term_ids, $taxonomy, false );

        // Return
        return $post;

    }
    
    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}