<?php
/**
 * User Option class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_User_Option {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * The key prefix to use for settings
     *
     * @since   1.0.0
     *
     * @var     string
     */
    private $key_prefix = '_mlo';

    /**
     * Returns a User option
     *
     * @since   1.0.0
     *
     * @param   string  $option_name    Option
     * @param   int     $user_id        User ID
     * @return  mixed                   Value
     */ 
    public function get_option( $user_id, $option_name ) {

        // Get option
        $value = get_user_option( $this->key_prefix . '_' . $option_name, $user_id );

        // If no option exists, fall back to the default
        if ( ! $value ) {
            $value = $this->get_default_option( $option_name );
        }

        // Allow devs / addons to filter option
        $value = apply_filters( 'media_library_organizer_user_option_get_option', $value, $option_name, $user_id );

        // Return
        return $value;

    }

    /**
     * Saves a single User option
     *
     * @since   1.0.0
     *
     * @param   string  $option_name    Option
     * @param   int     $user_id        User ID
     * @param   mixed                   Value
     * @return  bool                    Success
     */
    public function update_option( $user_id, $option_name, $value ) {

        // Allow devs / addons to filter setting
        $value = apply_filters( 'media_library_organizer_user_option_update_option', $value, $user_id, $option_name );

        // Get option
        return update_user_option( $user_id, $this->key_prefix . '_' . $option_name, $value );

    }

   
    /**
     * Deletes a single User option
     *
     * @since   1.0.0
     *
     * @param   string  $option_name    Option
     * @param   int     $user_id        User ID
     * @return  bool                    Success
     */
    public function delete_option( $user_id, $option_name ) {

        // Allow devs / addons to run actions now
        do_action( 'media_library_organizer_user_options_delete_option', $user_id, $option_name );

        return delete_user_option( $user_id, $this->key_prefix . '_' . $option_name );

    }

    /**
     * Returns the default User option
     *
     * @since   1.0.0
     *
     * @param   string  $option_name    Option Name
     */
    public function get_default_option( $option_name ) {

        // Get default options
        $defaults = $this->get_default_options();

        // Get default option
        $default = ( isset( $defaults[ $option_name ] ) ? $defaults[ $option_name ] : '' );

        // Allow devs / addons to filter default
        $default = apply_filters( 'media_library_organizer_user_option_get_default_option', $default );

        // Return
        return $default;

    }

    /**
     * Returns the default User options
     *
     * @since   1.0.0
     *
     * @return  array      Default Options
     */
    private function get_default_options() {

        // Define defaults
        $defaults = array(
            'orderby'   => 'date',
            'order'     => 'DESC',
        );

        // Allow devs to filter defaults
        $defaults = apply_filters( 'media_library_organizer_user_option_get_default_options', $defaults );

        // Return
        return $defaults;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}