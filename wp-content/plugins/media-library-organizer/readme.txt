=== Media Library Organizer ===
Contributors: wpmedialibrary
Donate link: https://wpmedialibrary.com
Tags: media categories, media organizer, media search, media library
Requires at least: 4.5
Tested up to: 4.9.1
Requires PHP: 5.2
Stable tag: trunk
License: GPLv2 or later

Organize and Search your Media Library, quicker and easier.

== Description ==

= Media Library Organization Plugin =

Media Library Organizer is a simple, effective WordPress Plugin that allows you to categorize and search images, video and other media
in your WordPress Media Library.

= Features =

* Categorize images, video and other media
* Works with all Media Library views (Featured Image, Image Picker, Media Library List, Media Library Grid, Editing a Media Library item)
* Search Media by Category
* Sort Media by Category, Date or Name
* Intuitive Category picker, seamlessly integrated into WordPress' native UI
* Import Categories and Categorization Data from JSON or WordPress standards export file
* Import Categories and Categorization Data from Enhanced Media Library and Media Library Assistant
* Export Categories and Categorization Data to JSON or a WordPress standards export file

= Migrations =

Media Library Organizer has in built importers, allowing you to migrate from other WordPress Media Library Plugins:

* Enhanced Media Library
* Media Library Assistant

> #### Premium Features
> For additional features, <a href="https://wpmedialibrary.com/pricing" title="WordPress Media Library Organizer">purchase a license</a> to access Addons:<br />
>
> - **Auto Categorization Addon:** Automatically categorize uploaded Images with automatic objects detection (e.g. a boat, animal or person)<br />
> - **Bulk Edit Addon:**  Bulk Edit Categorization for Media in <b>both</b> List and Grid views<br />
> - **Defaults Addon:** Define Default Categories for uploaded Images, and Attachment Display Settings (Alignment, Link To, Size) when inserting Media into Posts<br />
> - **EXIF & IPTC Addon:** Categorize Images by EXIF and IPTC Data<br />
> - **Filters Addon:** Add Author and MIME Type Filters to Media views, and options to enable/disable additional Filters<br />
> - **Folders Addon:** Store Images by Folder Name, instead of month and year based folders<br />
> - **Shortcodes Addon:** Extend WordPress' native [gallery] shortcode, to build dynamic Galleries by Category, Date and more<br />
> - **ZIP Addon:** Automatically extract / unzip uploaded ZIP files comprising of images; select multiple images and download a ZIP file of them<br />
> - **Support:** Access to one on one priority email support<br />
>
> [Upgrade](https://wpmedialibrary.com/pricing)

= Fully Documented =

We understand that no WordPress Plugin is good if you don't know how to use it.  That's why we provide extensive, full documentation
covering all aspects of Media Library Organizer:

<a href="https://wpmedialibrary.com/documentation" title="Media Library Organizer Documentation">https://wpmedialibrary.com/documentation</a>

Best of all, you'll find contextual Documentation links from within Media Library Organizer's interface.

= Fully Supported =

> We truly want Media Library Organizer to be the best WordPress Media Library Organization Plugin.  If you have any questions, or 
> something goes wrong, please reach out to us through the wordpress.org Support Forums. 
> This not only helps fix your support issue, but improves Media Library Organizer for everyone.

== Installation ==

1. Install Media Library Organizer via the Plugins > Add New section of your WordPress Installation, or by uploading the downloaded
ZIP file via Plugins > Add New > Upload Plugin.
2. Active the Media Library Organizer plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Where can I find Bulk Categorization? =

Our focus is to ensure that your core WordPress Media Library Management plugin remains stable and just works. Whilst other plugins offer these features for free,
the user experience isn't great - and some users simply don't need these features.

Bulk Categorization, Auto Categorization, Shortcodes, Folder, EXIF/IPTC and more will be made available through individual paid-for Addons for Media Library Organizer
at a later date.

== Screenshots ==

1. Categorization Filters and Sort Filters on Media Screen
2. Edit Categories when editing Media
3. Manage Categories
4. Plugin-wide Settings

== Changelog ==

= 1.0.1 =
* Added: Review Helper
* Added: Media View Mode (list or grid) is included in Admin Screen calls for Addons
* Fix: Removed unused upgrade() call
* Fix: Corrected Author information
* Fix: Settings > User Options were not always honored
* Fix: Settings > User Options > Sort Order description
* Fix: Media Library > Grid View > Order By value set to descending by default

= 1.0.0 =
* First release.

== Upgrade Notice ==
