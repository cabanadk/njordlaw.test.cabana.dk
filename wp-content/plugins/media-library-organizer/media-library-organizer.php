<?php
/**
* Plugin Name: Media Library Organizer
* Plugin URI: https://wpmedialibrary.com
* Version: 1.0.1
* Author: WP Media Library
* Author URI: https://wpmedialibrary.com
* Description: Organize and Search your Media Library, quicker and easier.
*/

/**
 * Media Library Organizer Class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Holds the plugin information object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $plugin = '';

    /**
     * Holds the licensing class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $licensing = '';

    /**
    * Constructor. Acts as a bootstrap to load the rest of the plugin
    *
    * @since 1.0.0
    */
    public function __construct() {

        // Plugin Details
        $this->plugin = new stdClass;
        $this->plugin->name         = 'media-library-organizer';
        $this->plugin->displayName  = 'Media Library Organizer';
        $this->plugin->folder       = plugin_dir_path( __FILE__ );
        $this->plugin->url          = plugin_dir_url( __FILE__ );
        $this->plugin->version      = '1.0.1';
        $this->plugin->home_url     = 'https://wpmedialibrary.com';
        $this->plugin->support_url  = 'https://wpmedialibrary.com/documentation';
        $this->plugin->purchase_url = 'https://wpmedialibrary.com/pricing';
        $this->plugin->review_notice = sprintf( __( 'Thanks for using %s to organize your Media Library!', $this->plugin->name ), $this->plugin->displayName );

        // Licensing Submodule
        if ( ! class_exists( 'Licensing_Update_Manager' ) ) {
            require_once( $this->plugin->folder . '_modules/licensing/lum.php' );
        }
        $this->licensing = new Licensing_Update_Manager( $this->plugin, 'https://wpmedialibrary.com', $this->plugin->name );

        // Initialize non-static classes that contain WordPress Actions or Filters
        // Admin
        if ( is_admin() ) {
            $media_library_organizer_admin          = Media_Library_Organizer_Admin::get_instance();
            $media_library_organizer_export         = Media_Library_Organizer_Export::get_instance();
            $media_library_organizer_media          = Media_Library_Organizer_Media::get_instance();
            $media_library_organizer_notices        = Media_Library_Organizer_Notices::get_instance();
        }

        // Global
        $media_library_organizer_taxonomy           = Media_Library_Organizer_Taxonomy::get_instance();

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}

/**
 * Define the autoloader for this Plugin
 *
 * @since   1.0.0
 *
 * @param   string  $class_name     The class to load
 */
function Media_Library_Organizer_Autoloader( $class_name ) {

    // Define the required start of the class name
    $class_start_name = 'Media_Library_Organizer';

    // Get the number of parts the class start name has
    $class_parts_count = count( explode( '_', $class_start_name ) );

    // Break the class name into an array
    $class_path = explode( '_', $class_name );

    // Bail if it's not a minimum length (i.e. doesn't potentially have Media_Library_Organizer Autolinker)
    if ( count( $class_path ) < $class_parts_count ) {
        return;
    }

    // Build the base class path for this class
    $base_class_path = '';
    for ( $i = 0; $i < $class_parts_count; $i++ ) {
        $base_class_path .= $class_path[ $i ] . '_';
    }
    $base_class_path = trim( $base_class_path, '_' );

    // Bail if the first parts don't match what we expect
    if ( $base_class_path != $class_start_name ) {
        return;
    }

    // Define the file name we need to include
    $file_name = strtolower( implode( '-', array_slice( $class_path, $class_parts_count ) ) ) . '.php';

    // Define the paths with file name we need to include
    $include_paths = array(
        dirname( __FILE__ ) . '/includes/admin/' . $file_name,
        dirname( __FILE__ ) . '/includes/global/' . $file_name,
    );

    // Iterate through the include paths to find the file
    foreach ( $include_paths as $path_file ) {
        if ( file_exists( $path_file ) ) {
            require_once( $path_file );
            return;
        }
    }

    // If here, we couldn't find the file!

}
spl_autoload_register( 'Media_Library_Organizer_Autoloader' );

// Initialise class
$media_library_organizer = Media_Library_Organizer::get_instance();

// Register activation hooks
register_activation_hook( __FILE__, array( 'Media_Library_Organizer_Install', 'activate' ) );
add_action( 'activate_wpmu_site', array( 'Media_Library_Organizer_Install', 'activate_wpmu_site' ) );