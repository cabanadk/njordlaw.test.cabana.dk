jQuery( document ).ready( function( $ ) {

	/**
	 * Category Tabs
	 */
	$( 'body' ).on( 'click', '#mlo-category-tabs a', function( e ) {

		// Prevent default action (would jump down the page)
		e.preventDefault();

		var t = $( this ).attr( 'href' );

		// Remove the tabs class from all tabs
		$( this ).parent().addClass( 'tabs' ).siblings( 'li' ).removeClass( 'tabs' );

		// Hide the tab panels
		$( '.tabs-panel' ).hide();

		// Show the selected tab panel
		$( t ).show();

	} );

} );

/**
 * Extend wp.media.view.AttachmentCompat to remove the checked
 * attribute on Taxonomy Term checkboxes when they are unticked,
 * before sending the data via AJAX to update the attachment.
 *
 * @since 	1.0.0
 */
( function() {

	var MediaLibraryOrganizerAttachmentCompat = wp.media.view.AttachmentCompat.extend( {

		save: function( event ) {

			var data = {};

			if ( event ) {
				event.preventDefault();
			}

			_.each( this.$el.serializeArray(), function( pair ) {
				data[ pair.name ] = pair.value;
			});

			// If the element that was changed is a checkbox and it is no longer
			// checked, remove this value from the data array.
			// This ensures that deselecting a Taxonomy Term is truly honored
			if ( event.target.type == 'checkbox' && ! event.target.checked ) {
				delete data[ event.target.name ];
			}

			this.controller.trigger( 'attachment:compat:waiting', ['waiting'] );
			this.model.saveCompat( data ).always( _.bind( this.postSave, this ) );
		},

	} );

	wp.media.view.AttachmentCompat.prototype = MediaLibraryOrganizerAttachmentCompat.prototype;

} ) ();

/**
 * Adds Filters to wp.media.view.AttachmentFilters, for Backbone views
 *
 * @since 	1.0.0
 */
( function() {

	/**
	 * Adds the Taxonomy Filter to wp.media.view.AttachmentFilters, for Backbone views
	 */
	if ( media_library_organizer_media.settings.taxonomy_enabled == 1 ) {
		var MediaLibraryOrganizerTaxonomyFilter = wp.media.view.AttachmentFilters.extend( {
			id: 'media-attachment-taxonomy-filter',

			/**
			 * Create Filter
			 *
			 * @since 	1.0.0
			 */
			createFilters: function() {

				var filters = {};

				// Build an array of filters based on the Terms supplied in media_library_organizer_media.terms,
				// set by wp_localize_script()
				_.each( media_library_organizer_media.terms || {}, function( term, index ) {
					filters[ index ] = {
						text: term.name + ' (' + term.count + ')',

						// Key = WP_Query taxonomy name, which ensures that mlo-category=1 is sent
						// as part of the search query when the filter is used.
						props: {
							'mlo-category': term.slug,
						}
					};
				});

				// Define the 'All' filter
				filters.all = {
					// Change this: use whatever default label you'd like
					text:  'All Media Categories',
					props: {
						// Key = WP_Query taxonomy name, which ensures that mlo-category=1 is sent
						// as part of the search query when the filter is used.
						'mlo-category': ''
					},
					priority: 10
				};

				// Define the 'Unassigned' filter
				filters.unassigned = {
					// Change this: use whatever default label you'd like
					text:  '(Unassigned)',
					props: {
						// Key = WP_Query taxonomy name, which ensures that mlo-category=1 is sent
						// as part of the search query when the filter is used.
						'mlo-category': "-1"
					},
					priority: 10
				};

				// Set this filter's data to the terms we've just built
				this.filters = filters;

			}

		} );
	}

	/**
	 * Adds the Order By Filter to wp.media.view.AttachmentFilters, for Backbone views
	 */
	if ( media_library_organizer_media.settings.orderby_enabled == 1 ) {
		var MediaLibraryOrganizerTaxonomyOrderBy = wp.media.view.AttachmentFilters.extend( {
			id: 'media-attachment-taxonomy-orderby',

			/**
			 * Create Filters
			 *
			 * @since 	1.0.0
			 */
			createFilters: function() {

				var filters = {};

				// Build an array of filters based on the Sorting options supplied in media_library_organizer_media.sorting,
				// set by wp_localize_script()
				_.each( media_library_organizer_media.orderby || {}, function( value, key ) {
					filters[ key ] = {
						text: value,

						// Key = WP_Query taxonomy name, which ensures that mlo-category=1 is sent
						// as part of the search query when the filter is used.
						props: {
							'orderby': key,
						}
					};
				});

				// Set this filter's data to the terms we've just built
				this.filters = filters;

				// If no orderby is defined, set one now from either the User Options (if enabled),
				// or the Plugin's default
				if ( ! this.model.get( 'orderby' ) ) {
					// Set orderby using User Options, if they're enabled to persist for the User
					if ( media_library_organizer_media.user_options.order_enabled == 1 ) {
						this.model.set( 'orderby', media_library_organizer_media.user_options.orderby );
					} else {
						// Set orderby using plugin default
						this.model.set( 'orderby', media_library_organizer_media.defaults.orderby );
					}
				}

			}

		} );
	}

	/**
	 * Adds the Order Filter to wp.media.view.AttachmentFilters, for Backbone views
	 */
	if ( media_library_organizer_media.settings.order_enabled == 1 ) {
		var MediaLibraryOrganizerTaxonomyOrder = wp.media.view.AttachmentFilters.extend( {
			id: 'media-attachment-taxonomy-order',

			/**
			 * Create Filter
			 *
			 * @since 	1.0.0
			 */
			createFilters: function() {

				var filters = {};

				// Build an array of filters based on the Sorting options supplied in media_library_organizer_media.sorting,
				// set by wp_localize_script()
				_.each( media_library_organizer_media.order || {}, function( value, key ) {
					filters[ key ] = {
						text: value,

						// Key = asc|desc
						props: {
							'order': key,
						}
					};
				});

				// Set this filter's data to the terms we've just built
				this.filters = filters;

				// If no order is defined, set one now from either the User Options (if enabled),
				// or the Plugin's default
				if ( ! this.model.get( 'order' ) ) {
					// Set order using User Options, if they're enabled to persist for the User
					if ( media_library_organizer_media.user_options.order_enabled == 1 ) {
						this.model.set( 'order', media_library_organizer_media.user_options.order );
					} else {
						// Set order using plugin default
						this.model.set( 'order', media_library_organizer_media.defaults.order );
					}
				}
			}

		} );
	}

	/**
	 * Extend and override wp.media.view.AttachmentsBrowser to include our custom filters
	 */
	var AttachmentsBrowser = wp.media.view.AttachmentsBrowser;
	wp.media.view.AttachmentsBrowser = wp.media.view.AttachmentsBrowser.extend( {

		/**
		 * When the toolbar is created, add our custom filters to it, which
		 * are rendered as select dropdowns
		 *
		 * @since 	1.0.0
		 */ 
		createToolbar: function() {

			// Make sure to load the original toolbar
			AttachmentsBrowser.prototype.createToolbar.call( this );

			// Add the taxonomy filter to the toolbar
			if ( media_library_organizer_media.settings.taxonomy_enabled == 1 ) {
				this.toolbar.set( 'MediaLibraryOrganizerTaxonomyFilter', new MediaLibraryOrganizerTaxonomyFilter( {
					controller: this.controller,
					model:      this.collection.props,
					priority: 	-75
				} ).render() );
			}

			// Add the orderby filter to the toolbar
			if ( media_library_organizer_media.settings.orderby_enabled == 1 ) {
				this.toolbar.set( 'MediaLibraryOrganizerTaxonomyOrderBy', new MediaLibraryOrganizerTaxonomyOrderBy( {
					controller: this.controller,
					model:      this.collection.props,
					priority: 	-75
				} ).render() );
			}

			// Add the order filter to the toolbar
			if ( media_library_organizer_media.settings.order_enabled == 1 ) {
				this.toolbar.set( 'MediaLibraryOrganizerTaxonomyOrder', new MediaLibraryOrganizerTaxonomyOrder( {
					controller: this.controller,
					model:      this.collection.props,
					priority: 	-75
				} ).render() );
			}

		}

	} );
} ) ()