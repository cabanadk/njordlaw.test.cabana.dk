<?php get_header(); ?>

<?php

$currentTerm = get_queried_object();
$currentTermThumbnail = get_field('taxon_specialty_image', $currentTerm);
?>

    <figure id="featured"><img src="<?php echo $currentTermThumbnail; ?>" alt="<?php echo $currentTermThumbnail['alt']?>" width="1572" height="500"></figure>


<?php if(get_field('taxon_inpage_navigation', $currentTerm)){include('template_parts/inpage-navigation.php');}?>

    <section id="content">

        <h1 class="ip-nav-heading"><?php echo $currentTerm->name; ?></h1>
        <?php echo get_field('taxon_specialty_content',$currentTerm); ?>


    </section>
    <div class="clear"></div>

<?php

$taxContactsSection = get_field('taxonomy_specialty_contacts_objects' , $currentTerm);

$taxLatestNewsSection = get_field('taxonomy_specialty_latest_news' , $currentTerm);

if($taxContactsSection){
    include('template_parts/contacts-section.php');
}


if($taxLatestNewsSection){
    $background = $taxLatestNewsSection;
    include('template_parts/latest-news-section.php');
}

?>

<?php get_footer();?>