<?php
/*
 Template Name: Press
 */
?>

<?php get_header(); ?>

<?php
    if(get_field('show_inpage_navigation') == 'yes'){
        include "template_parts/inpage-navigation.php";
    }
    ?>
<section id="content" class="cols-a">
    <article>
    <section>
        <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
        <?php the_content(); ?>
        <?php include('template_parts/social-sharing-buttons.php');?>
    </section>

        <?php
            // check if the flexible content field has rows of data
            if( have_rows('press_flexible_content') ):
                // loop through the rows of data
                while ( have_rows('press_flexible_content') ) : the_row();
                    // If layout is equal to
                    if( get_row_layout() == 'press_gallery_section' ):
                        include('template_parts/press_gallery-section.php');

                    // Elseif
                    elseif( get_row_layout() == 'download_logo-section' ):
                        include('template_parts/download_logo-section.php');

                    endif;
                endwhile;
            else :
                // no layouts found
            endif;
        ?>
    </article>
    <?php
        // check if the flexible content field has rows of data
        if( have_rows('press_flexible_content') ):
            // loop through the rows of data
            while ( have_rows('press_flexible_content') ) : the_row();
                // If layout is equal to
                if( get_row_layout() == 'post_employee_section' ):
                    include "template_parts/employee-section.php";
                endif;
            endwhile;
        else :
            // no layouts found
        endif;
    ?>
</section>

<?php get_footer(); ?>