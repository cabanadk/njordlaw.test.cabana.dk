<?php get_header(); ?>

    <figure id="featured">

        <?php echo get_the_post_thumbnail($post->ID, 'full', array('width'=>"1572",'height'=>"500"));?>

    </figure>


<?php
if(get_field('spec_inpage_navigation', $post->ID)){
    include('template_parts/inpage-navigation.php');
}
?>
<?php $current_area = $post->post_title; ?>
    <section id="content">

        <?php if (get_field('custom_knowledge_page_title')): ?>
            <h1>
            <strong>
                <?php the_field('custom_knowledge_page_title');?>
            </strong>
            </h1>
        <?php else: ?>
            <h1>    
                <strong>
                    <?php echo $post->post_title; ?>
                </strong>
            </h1>
        <?php endif; ?>
        <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
        <?php the_content(); ?>
        <?php include ('template_parts/social-sharing-buttons.php');?>
    </section>

    <?php include ('template_parts/area-knowledge-section.php');?>

    <div class="clear"></div>




<?php
// check if the flexible content field has rows of data
if( have_rows('defaultpage_flexible_content') ):
    // loop through the rows of data
    while ( have_rows('defaultpage_flexible_content') ) : the_row();
        if( get_row_layout() == 'contacts_section' ):
            include('template_parts/contacts-section.php');
        elseif( get_row_layout() == 'latest_news_section' ):
            $related_news_boolean = get_sub_field('related_news_boolean');

            if($related_news_boolean)
            {
                include('template_parts/pick-news-section.php');
            }
            else
            {
                include('template_parts/latest-news-section.php');
            }
        endif;
    endwhile;
else :
    // no layouts found
endif;
?>

<?php get_footer();?>