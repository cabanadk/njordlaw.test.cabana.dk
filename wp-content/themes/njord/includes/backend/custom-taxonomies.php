<?php
// hook into the init action and call create_employee_taxonomies when it fires
add_action( 'init', 'create_employee_taxonomies', 0 );

// create taxonomy for the emplyees' jobtitles
function create_employee_taxonomies() {

    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name'                       => _x( 'Jobtitles', 'taxonomy general name' ),
        'singular_name'              => _x( 'Jobtitle', 'taxonomy singular name' ),
        'search_items'               => __( 'Search jobtitles' ),
        'popular_items'              => __( 'Popular jobtitles' ),
        'all_items'                  => __( 'All jobtitles' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit jobtitle' ),
        'update_item'                => __( 'Update jobtitle' ),
        'add_new_item'               => __( 'Add New jobtitle' ),
        'new_item_name'              => __( 'New jobtitle Name' ),
        'separate_items_with_commas' => __( 'Separate jobtitles with commas' ),
        'add_or_remove_items'        => __( 'Add or remove jobtitles' ),
        'choose_from_most_used'      => __( 'Choose from the most used jobtitles' ),
        'not_found'                  => __( 'No jobtitles found.' ),
        'menu_name'                  => __( 'Jobtitles' ),
    );

    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'jobtitle' ),
    );

    register_taxonomy( 'jobtitle', array( 'employee' ), $args );
}


// hook into the init action and call create_employee_taxonomies when it fires
add_action( 'init', 'create_knowledge_taxonomies', 0 );

// create taxonomy for the emplyees' jobtitles
function create_knowledge_taxonomies() {

    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name'                       => _x( 'Types', 'taxonomy general name' ),
        'singular_name'              => _x( 'Type', 'taxonomy singular name' ),
        'search_items'               => __( 'Search types' ),
        'popular_items'              => __( 'Popular types' ),
        'all_items'                  => __( 'All types' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit type' ),
        'update_item'                => __( 'Update type' ),
        'add_new_item'               => __( 'Add New type' ),
        'new_item_name'              => __( 'New type Name' ),
        'separate_items_with_commas' => __( 'Separate types with commas' ),
        'add_or_remove_items'        => __( 'Add or remove types' ),
        'choose_from_most_used'      => __( 'Choose from the most used types' ),
        'not_found'                  => __( 'No types found.' ),
        'menu_name'                  => __( 'Types' ),
    );

    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'type-of-knowledge' ),
    );

    register_taxonomy( 'type-of-knowledge', array( 'knowledge' ), $args );
}




// hook into the init action and call create_country_taxonomy when it fires
add_action( 'init', 'create_country_taxonomy', 0 );

// create taxonomy for the emplyees' countries
function create_country_taxonomy() {

    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name'                       => _x( 'Countries', 'taxonomy general name' ),
        'singular_name'              => _x( 'Country', 'taxonomy singular name' ),
        'search_items'               => __( 'Search countries' ),
        'popular_items'              => __( 'Popular countries' ),
        'all_items'                  => __( 'All countries' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit country' ),
        'update_item'                => __( 'Update country' ),
        'add_new_item'               => __( 'Add New country' ),
        'new_item_name'              => __( 'New country Name' ),
        'separate_items_with_commas' => __( 'Separate country with commas' ),
        'add_or_remove_items'        => __( 'Add or remove countries' ),
        'choose_from_most_used'      => __( 'Choose from the most used countries' ),
        'not_found'                  => __( 'No countries found.' ),
        'menu_name'                  => __( 'Countries' ),
    );

    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'country' ),
    );

    register_taxonomy( 'country', array( 'employee' ), $args );
}




//// hook into the init action and call create_field_taxonomy when it fires
//add_action( 'init', 'create_field_taxonomy', 0 );
//
//// create taxonomy for the emplyees' field of work
//function create_field_taxonomy() {
//
//    // Add new taxonomy, NOT hierarchical (like tags)
//    $labels = array(
//        'name'                       => _x( 'Specialties', 'taxonomy general name' ),
//        'singular_name'              => _x( 'Specialty', 'taxonomy singular name' ),
//        'search_items'               => __( 'Search specialties' ),
//        'popular_items'              => __( 'Popular specialties' ),
//        'all_items'                  => __( 'All specialties' ),
//        'parent_item'                => null,
//        'parent_item_colon'          => null,
//        'edit_item'                  => __( 'Edit specialty' ),
//        'update_item'                => __( 'Update specialty' ),
//        'add_new_item'               => __( 'Add New specialty' ),
//        'new_item_name'              => __( 'New specialty Name' ),
//        'separate_items_with_commas' => __( 'Separate specialties with commas' ),
//        'add_or_remove_items'        => __( 'Add or remove specialties' ),
//        'choose_from_most_used'      => __( 'Choose from the most used specialties' ),
//        'not_found'                  => __( 'No specialties found.' ),
//        'menu_name'                  => __( 'Specialty' ),
//    );
//
//    $args = array(
//        'hierarchical'          => true,
//        'labels'                => $labels,
//        'show_ui'               => true,
//        'show_admin_column'     => true,
//        'update_count_callback' => '_update_post_term_count',
//        'query_var'             => true,
//        'rewrite'               => array( 'slug' => 'specialty' ),
//    );
//
//    register_taxonomy( 'specialty', array( 'employee' ), $args );
//}