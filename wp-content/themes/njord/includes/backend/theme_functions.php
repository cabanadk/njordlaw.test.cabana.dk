<?php

/**
 * File that contains all of the custom backend functions for the template
 */

/**
 * Gets the id of an attachment by the attachment guid
 *
 * @param string $guid
 * @return int attachment_id|null
 */
function get_attachment_id_by_guid($guid) {
    global $wpdb;
    $attachment_id = $wpdb->get_row($wpdb->prepare("SELECT ID FROM `wp_posts` WHERE guid = %s", $guid));
    if ($attachment_id->ID) {
        return (int) $attachment_id->ID;
    }
    return null;
}

/**
 * Changes the link of the login logo to the blog url
 * @return string|void
 */
function custom_login_headerurl() {
    return get_bloginfo('url');
}

/**
 * Get the current theme logo via wp_get_attachment_image_src
 *
 * @param string $size Optional, default is 'full'.
 * @param bool $icon Optional, default is false. Whether it is an icon.
 * @return bool|array Returns an array (url, width, height), or false, if no image is available.
 */
function get_theme_logo($size = 'full', $icon = false) {
    $attachment_id = get_attachment_id_by_guid(get_option("theme_logo"));
    return wp_get_attachment_image_src($attachment_id, $size, $icon);
}

/**
 * Adds classes to first and last items in navigation
 * @param $output
 * @param $args
 * @return mixed
 */
function add_first_and_last($output, $args) {
    if (is_object($args->walker)) {
        // If we are using a custom walker, do not apply below logic.
        return $output;
    }
    $output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
    if (substr_count($output, 'class="menu-item') == 0) {
        $output = preg_replace('/first-menu-item/', 'first-menu-item last-menu-item', $output, 1);
        return $output;
    }
    $output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
    return $output;
}

add_filter('wp_nav_menu', 'add_first_and_last', 10, 2);

/*
 * Strip special characters from uploaded file names
 * Attempt to convert danish special characters before stripping them
 */

function remove_special_characters_on_file_upload($file) {
    // Get lowercased filename
    $filename = mb_strtolower($file['name']);

    // Convert spaces to undercores (collapse multiple spaces to a single underscore)
    $filename = preg_replace("/[\s]+/u", "-", $filename);

    // Convert danish special characters
    $filename = preg_replace("/[æ]/u", "ae", $filename);
    $filename = preg_replace("/[ø]/u", "oe", $filename);
    $filename = preg_replace("/[å]/u", "aa", $filename);

    // Strip remaining special characters
    $filename = preg_replace("/[^a-z0-9\._-]/u", "_", $filename);

    $file['name'] = $filename;

    return $file;
}

add_filter('wp_handle_upload_prefilter', 'remove_special_characters_on_file_upload');

//get all the employees
function getTheEmployees($languageCode) {

    //Find the parent language code by splitting it up
    $parentLanguageTerm = get_parent_based_term($languageCode);

    $employeeArray = array();
    $empName = $_POST['ta'];
    $empField = $_POST['tf'];
    $empTitle = $_POST['tc'];
    $empCountry = $_POST['td'];

    $employeeArgs = array(
        'post_type' => 'employee',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'ignore_sticky_posts' => true,
    );

    if (!isset($_POST) || $empName == '' && $empField == '' && $empTitle == '' && $empCountry == '') {

        $filtering = false;
    } else {

        $filtering = true;
    }

    //Filtering function
    $customSearchResults = get_custom_query($empName, $empField, $empTitle, $empCountry, null);

    if ($filtering == true) {

        if (count($customSearchResults) > 0) {
            foreach ($customSearchResults as $empResult) {
                $employeeArray[] = $empResult->ID;
            }
            $employeeArgs['post__in'] = $employeeArray;
        } else {
            return false;
        }
        
        // Predefine language in search query for all pages, which is not en
        if ($languageCode !== 'en') {
            $employeeArgs['tax_query'] = array(
                array('taxonomy' => 'country',
                    'field' => 'slug',
                    'terms' => $parentLanguageTerm
                )
            );
        }
        
    } else {

        if($languageCode != 'en'){
            $employeeArgs['posts_per_page'] = 12;
            //By default get the correct country employees else when searching the other countries should be included
            $employeeArgs['tax_query'] = array(
                array('taxonomy' => 'country',
                    'field' => 'slug',
                    'terms' => $parentLanguageTerm
                )
            );
        }
    }



    $employeeQuery = new WP_Query($employeeArgs);

    if (count($employeeQuery->posts) > 0) {

        return $employeeQuery->posts;
    } else {

        return null;
    }
}

function get_custom_query($name, $specialty, $jobtitle, $country, $excludeEmployees = NULL) {

    $customQuery = '';
    $langCode = ICL_LANGUAGE_CODE;

    global $wpdb;

    $parameters = array();

    $termIDs = array();

    $termParameters = array();

    //Check for the parameters of the search and add them in the query
    if (!empty($name)) {

        $nameVariation = checkForVariation($langCode, $name);

        if ($nameVariation != '') {

            $parameters[] = array(
                'name' => 'employees.post_title',
                'operator' => 'LIKE',
                'value' => "'%" . $wpdb->_real_escape($name) . "%'",
                'variation' => "'%" . $wpdb->_real_escape($nameVariation) . "%'"
            );
        } else {
            $parameters[] = array(
                'name' => 'employees.post_title',
                'operator' => 'LIKE',
                'value' => "'%" . $wpdb->_real_escape($name) . "%'",
                'variation' => ""
            );
        }
    }

    if (!empty($specialty)) {
        $parameters[] = array(
            'name' => 'specialty.meta_value',
            'operator' => 'LIKE',
            'value' => "'%" . $wpdb->_real_escape($specialty) . "%'",
        );
    }

    if (!empty($jobtitle)) {
        $termParameters[] = array(
            'name' => 'wp_term_taxonomy.parent',
            'operator' => '=',
            'value' => "'" . $wpdb->_real_escape($jobtitle) . "'",
        );
    }

    if (!empty($country)) {
        $termParameters[] = array(
            'name' => 'wp_term_taxonomy.parent',
            'operator' => '=',
            'value' => "'" . $wpdb->_real_escape($country) . "'",
        );
    }

    if ($excludeEmployees != null) {

        $translations = exclude_translations($excludeEmployees);

        $parameters[] = array(
            'name' => 'ID',
            'operator' => 'NOT IN',
            'value' => "(" . $wpdb->_real_escape(join(',', $translations)) . ")",
        );
    }


    // Build the query
    $customQuery = '
    SELECT *
    FROM wp_posts as employees

    LEFT JOIN wp_icl_translations
    ON wp_icl_translations.element_id = employees.ID

    LEFT JOIN wp_postmeta as specialty
    ON employees.ID = specialty.post_id AND specialty.meta_key = "specialties_rel_employee"

    LEFT JOIN wp_term_relationships
    ON (employees.ID = wp_term_relationships.object_id)

    LEFT JOIN wp_term_taxonomy
    ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)

    LEFT JOIN wp_terms
    ON (wp_term_taxonomy.term_id = wp_terms.term_id)


    WHERE employees.post_status = "publish"
    AND language_code = "' . $wpdb->_real_escape($langCode) . '"
    AND employees.post_type = "employee"
    ';

    foreach ($parameters as $parameter) {
        $customQuery .= 'AND ';
        if ($parameter['variation'] != '') {
            $customQuery .= '(';
            $customQuery .= $parameter['name'] . ' ';
            $customQuery .= $parameter['operator'] . ' ';
            $customQuery .= $parameter['value'] . ' ';
            $customQuery .= 'OR ';
            $customQuery .= $parameter['name'] . ' ';
            $customQuery .= $parameter['operator'] . ' ';
            $customQuery .= $parameter['variation'] . ' ';
            $customQuery .= ') ';
        } else {
            $customQuery .= $parameter['name'] . ' ';
            $customQuery .= $parameter['operator'] . ' ';
            $customQuery .= $parameter['value'] . ' ';
        }
    }


    if (!empty($termParameters)) {
        $customQuery .= 'AND ( ';

        $countTerms = count($termParameters);
        foreach ($termParameters as $key => $termParameter) {

            $customQuery .= '(' . $termParameter['name'] . ' ' . $termParameter['operator'] . ' ' . $termParameter['value'] . ')';
            if ($key + 1 != $countTerms) {
                $customQuery .= ' OR ';
            }
        }

        $customQuery .= ' ) ';
    }

    $customQuery .= 'GROUP BY ID';

    $results = $wpdb->get_results($customQuery, OBJECT);

    if (count($results) > 0) {
        return $results;
    } else {
        return null;
    }
}

function exclude_translations($excludeEmployees) {

    global $wpdb;
    $excludeQuery = '
    SELECT element_id FROM wp_icl_translations WHERE trid IN (SELECT trid FROM wp_icl_translations WHERE element_id IN (' . $wpdb->_escape(join(',', $excludeEmployees)) . ') AND element_type = "post_employee") AND element_type = "post_employee"
    ';
    $results = $wpdb->get_col($excludeQuery);



    return $results;
}

function get_parent_based_term($languageCode) {

    $parentLanguageCode = substr($languageCode, 0, 2);
    $theTerm = '';
    if ($parentLanguageCode == 'da' || $parentLanguageCode == 'de') {
        $theTerm = get_term_by('slug', 'danmark', 'country');
    } else if ($parentLanguageCode == 'lv' || $parentLanguageCode == 'ru') {
        $theTerm = get_term_by('slug', 'letland', 'country');
    } else if ($parentLanguageCode == 'et') {
        $theTerm = get_term_by('slug', 'estland', 'country');
    } else if ($parentLanguageCode == 'lt') {
        $theTerm = get_term_by('slug', 'litauen', 'country');
    }

    return $theTerm->slug;
}

function get_specialties() {
    $specArgs = array(
        'post_type' => 'specialty',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    return query_posts($specArgs);
}

function check_for_intro_text($content) {

    preg_match('/\[([^\]]+)\](.*?)\[\/\1\]/uis', $content, $matches);
    $intro = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $matches[0]);

    return $intro;
}

function checkForVariation($lang, $input) {

    $danishLanguages = array(
        'da',
        'en',
        'de'
    );

    $variation = '';

    if (strpos($input, "aa") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('aa', 'å', $input);
    } else if (strpos($input, "å") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('å', 'aa', $input);
    } else if (strpos($input, "oe") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('oe', 'ø', $input);
    } else if (strpos($input, "ø") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('ø', 'oe', $input);
    } else if (strpos($input, "ae") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('ae', 'æ', $input);
    } else if (strpos($input, "æ") && in_array($lang, $danishLanguages)) {
        $variation = str_replace('æ', 'ae', $input);
    }

    return $variation;
}
