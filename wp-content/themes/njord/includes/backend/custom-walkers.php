<?php
    // Create counter to keep track of navigation index
    global $count;
    $count = 1;


class Menu_a extends Walker_Nav_Menu {
    private $hidden = false;

    function start_lvl(&$output, $depth = 0, $args = Array()) {
        if($depth == 0) {
            $style = $this->hidden ? "" : "display:none;";
        }
        $output .= "<ul>";
    }

    function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        if ($depth == 0 &&
            (in_array("current-menu-item", $classes) ||
                in_array('current-menu-parent', $classes) ||
                in_array('current-menu-ancestor', $classes)) ) {
            $this->hidden = true;
        } else {
            $this->hidden = false;
        }
        parent::start_el($output, $item, $depth, $args);
    }
}

class Menu_b extends Walker_Nav_Menu {
    private $hidden = false;

    function start_lvl(&$output, $depth = 0, $args = Array()) {
        if($depth == 0) {
            $style = $this->hidden ? "" : "display:none;";
        }

        $output .= "<div><ul>";
    }

    function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        if ($depth == 0 &&
            (in_array("current-menu-item", $classes) ||
                in_array('current-menu-parent', $classes) ||
                in_array('current-menu-ancestor', $classes)) ) {
            $this->hidden = true;
        } else {
            $this->hidden = false;
        }
        parent::start_el($output, $item, $depth, $args);
    }

    function end_lvl(&$output, $depth = 0, $args = Array()) {
        $output .= "</ul></div>";
    }
}
?>