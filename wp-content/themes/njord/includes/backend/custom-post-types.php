<?php
/**
 * Renaming the standard wordpress posts to Nyheder
 */
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add new';

    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add news';
    $labels->add_new_item = 'Add new news';
    $labels->edit_item = 'Edit news';
    $labels->new_item = 'New news';
    $labels->view_item = 'Show news';
    $labels->search_items = 'Search news';
    $labels->not_found = 'No news found';
    $labels->not_found_in_trash = 'No news found in the trash';
    $labels->all_items = 'All news';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

add_action( 'init', 'my_register_post_tags' );

function my_register_post_tags() {
    register_taxonomy( 'post_tag', array( 'my_post_type_here' ) );
}

/******************************************************************
 * Defining custom post type: Knowledge
 */
add_action('init',function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'Knowledge',
        'singular_name'      => 'Knowledge',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new knowledge post',
        'edit_item'          => 'Edit knowledge post',
        'new_item'           => 'New knowledge post',
        'all_items'          => 'All knowledge posts',
        'view_item'          => 'Show knowledge post',
        'search_items'       => 'Search knowledge posts',
        'not_found'          => 'No knowledge posts found',
        'not_found_in_trash' => 'No knowledge posts found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Knowledge'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'knowledge' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail')
    );

    register_post_type('knowledge',$args);
});
/******************************************************************/
/******************************************************************/

/**
 * Defining custom post type: Events
 */
add_action('init',function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'Events',
        'singular_name'      => 'Event',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new event',
        'edit_item'          => 'Edit event',
        'new_item'           => 'New event',
        'all_items'          => 'All events',
        'view_item'          => 'Show event',
        'search_items'       => 'Search events',
        'not_found'          => 'No event found',
        'not_found_in_trash' => 'No event found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Events'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'event' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail')
    );

    register_post_type('event',$args);
});

/**
 * Defining custom post type: Employees
 */
add_action('init',function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'Employees',
        'singular_name'      => 'Employee',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new employee',
        'edit_item'          => 'Edit employee',
        'new_item'           => 'New employee',
        'all_items'          => 'All employees',
        'view_item'          => 'Show employee',
        'search_items'       => 'Search employees',
        'not_found'          => 'No employee found',
        'not_found_in_trash' => 'No employee found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Employees'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'employee' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail')
    );

    register_post_type('employee',$args);
});



/**
 * Defining custom post type: Map markers
 */
add_action('init',function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'Map Markers',
        'singular_name'      => 'Map Marker',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new Map Marker',
        'edit_item'          => 'Edit Map Marker',
        'new_item'           => 'New Map Marker',
        'all_items'          => 'All Map Markers',
        'view_item'          => 'Show Map Marker',
        'search_items'       => 'Search Map Markers',
        'not_found'          => 'No Map Markers found',
        'not_found_in_trash' => 'No Map Markers found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Map Markers'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'marker' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author')
    );

    register_post_type('marker',$args);
});







/**
 * Defining custom post type: Areas of Expertise
 */
add_action('init',function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'Areas of Expertise',
        'singular_name'      => 'Area of Expertise',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new Area of Expertise',
        'edit_item'          => 'Edit Area of Expertise',
        'new_item'           => 'New Area of Expertise',
        'all_items'          => 'All Areas of Expertise',
        'view_item'          => 'Show Area of Expertise',
        'search_items'       => 'Search Areas of Expertise',
        'not_found'          => 'No Areas of Expertise found',
        'not_found_in_trash' => 'No Areas of Expertise found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Areas of Expertise'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'areas-of-expertise' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail')
    );

    register_post_type('specialty',$args);
});





function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;

    return array(
        'index.php', // Dashboard

        'separator1', // First separator

        'edit.php', // News
        'edit.php?post_type=knowledge', // Knowledge
        'edit.php?post_type=event', // Events
        'edit.php?post_type=page', // Sider
        'edit.php?post_type=marker', // Map Markers
        'edit.php?post_type=employee', // Employees
        'edit.php?post_type=specialty', // Employees

        'separator2', // Second separator

        'upload.php', // Media
        'link-manager.php', // Links

        'edit-comments.php', // Comments
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');