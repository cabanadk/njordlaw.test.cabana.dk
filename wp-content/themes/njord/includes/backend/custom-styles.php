<?php
function my_theme_add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/css/custom-editor-styles.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

/**
 * Adding new custom buttons to the editor
 */
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');

function my_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => "Special", //Overall style group title
            'items' => array(
                array(
                    'title' => 'Inpage Navigation Heading',
                    'block' => 'h2',
                    'classes' => 'ip-nav-heading',
                    'wrapper' => false
                )
            ),
        ),
        array(
            'title' => "CV", //Overall style group title
            'items' => array(
                array(
                    'title' => 'No bullets',
                    'selector' => 'ul',
                    'classes' => 'list-b',
                    'wrapper' => false
                )
            ),
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

