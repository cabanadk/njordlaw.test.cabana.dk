<?php
/**
 * Sets up theme defaults and registers the various WordPress features that
 * the HTML24 Theme supports.
 *
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses add_theme_support() To add support for post thumbnails.
 *
 */
add_action( 'after_setup_theme', function () {
    // This theme uses wp_nav_menu() in one location.
    register_nav_menu( 'primary', 'Main Navigation');
    add_theme_support( 'post-thumbnails' );

    // Print the analytics tracking code at the wp_footer tag, calling the print_tracking_code function
    add_action('wp_head', 'print_tracking_code');
});

/**
 * Auto Complete Search init
 */
function search_ac_init() {
    wp_enqueue_script( 'search_ac', get_template_directory_uri() . '/js/jquery-ui.min.js', array('jquery','jquery-ui-autocomplete'),null,true);
    wp_localize_script( 'search_ac', 'MyAcSearch', array('url' => admin_url( 'admin-ajax.php' )));
}
add_action( 'init', 'search_ac_init' );

/**
 * Register and load any scripts your theme requires.
 * In this example we will load a script.js with a dependency on jQuery.
 */
add_action('wp_enqueue_scripts', function () {
    // Register our script with and assign dependencie

    /*
    wp_register_script(
        'theme-script',
        get_template_directory_uri() . '/js/scripts.js',
        array(
            'jquery'
        )
    );
    */

    /*
    wp_register_script(
        'custom-script',
        get_template_directory_uri() . '/js/custom-scripts.js',
        array(
            'jquery'
        )
    );
    */

    /*
    wp_register_script(
        'autocomplete-script',
        get_template_directory_uri() . '/js/autocomplete-search.js',
        array(
            'jquery'
        )
    );
    */

    /*
    wp_register_script(
        'map-script',
        get_template_directory_uri() . '/js/map-script.js',
        array(
            'jquery'
        )
    );
    */

    /*
    wp_register_script(
        'cookie-script',
        get_template_directory_uri() . '/js/cookie.js',
        array(
            'jquery'
        )
    );
    */

    /*
    wp_register_script(
        'jquery-cookie-script',
        get_template_directory_uri() . '/js/jquery.cookie.js',
        array(
            'jquery'
        )
    );
	*/

    /*
	wp_register_script(
        'youtube-autoresize-script',
        get_template_directory_uri() . '/js/youtube-autoresize.js',
        array(
            'jquery'
        )
    );
    */

    // Enqueue script for output
                                     /*
    wp_enqueue_script('jquery-cookie-script');
    wp_enqueue_script('theme-script');
    wp_enqueue_script('custom-script');
    wp_enqueue_script('autocomplete-script');
    wp_enqueue_script('map-script');
    wp_enqueue_script('cookie-script');
	wp_enqueue_script('youtube-autoresize-script');
    */
    
    // Use bundle fron Cabana.Frontend proj.
    wp_enqueue_script(
        'app-script',
        sprintf('%s/js/app%s.js', get_template_directory_uri(), get_fingerprint_id()),
        array(
            'jquery'
        ),
        null
    );
});


//Add backend.js
function backend_script() {
    wp_register_script(
        'backend_js',
        get_template_directory_uri() . '/js/backend.js',
        array(
            'jquery'
        )
    );
    wp_enqueue_script('backend_js');
}
add_action('admin_enqueue_scripts', 'backend_script');

/**
 * Setting up the backend to use the custom admin image if there is one.
 * If the user hasn't uploaded a custom image, the default wp logo will be
 * used
 */
add_action('login_head', function () {
    if(get_option('theme_backend_logo')) {
        // Get the attachment id of the custom image
        $attachment_id = get_attachment_id_by_guid(get_option('theme_backend_logo'));
        $attachment_image_array = wp_get_attachment_image_src($attachment_id, 'medium');

        // Add the custom link to the new logo
        add_filter('login_headerurl', 'custom_login_headerurl');
?>
    <style>
        body.login #login h1 a {
            background: url('<?php echo $attachment_image_array[0]; ?>') no-repeat scroll center top transparent;
            height: <?php echo $attachment_image_array[2]; ?>px;
        }
    </style>
    <?php
    }
});

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
add_filter( 'wp_title', function ( $title, $sep ) {
    if (is_feed()) {
        return $title;
    }
    // Add the site name.
    $title .= get_bloginfo( 'name' );
    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }
    return $title;
}, 10, 2 );

/**
 * Add backend.css stylesheet, which can be used for custom styling
 * elements in the wordpress backend
 * Call stylesheet on admin init
 */
add_action('admin_init', function ()
{
    wp_register_style(
        'theme_backend_styles',
        get_template_directory_uri() . '/css/backend.css'
    );
    wp_enqueue_style('theme_backend_styles');
});

/**
 * Removing unnecessary admin pages which is not used for the theme.
 * By default the editor and comments are disabled.
 * You can customize as you want.
 *
 * If you want to delete more pages just use one of functions below
 * Codex: http://codex.wordpress.org/Function_Reference/remove_menu_page
 *
 */
add_action('admin_init', function()
{
    if (!defined( 'DOING_AJAX' ) || !DOING_AJAX) {
        remove_submenu_page('themes.php', 'theme-editor.php');
        remove_menu_page('edit-comments.php');
    }
});


/*
 * Get fingerprint
 * */
function get_fingerprint_id(){
    global $current_user;
    get_currentuserinfo();
    $_bust_path = get_template_directory() . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'fingerprint.json';
    $_bust_id = null;
    if(file_exists($_bust_path) && $current_user->user_login !== 'cabana'){
        $_bust_id = json_decode(file_get_contents($_bust_path), true);
        $_bust_id = '.' . $_bust_id['id'];
    }else{
        $_bust_id = '';
    }
    return $_bust_id;
}