<?php
function wrap_in_span( $atts, $content = "" ) {
    return "<span class='year-in-table'>".$content."</span>";
}
add_shortcode( 'year', 'wrap_in_span' );

function scheme_a( $atts, $content = "" ) {
    return "<p class='scheme-a'>".$content."</p>";
}
add_shortcode( 'intro', 'scheme_a' );



function call_out_box( $atts, $content = "" ) {
    return "<p class='call-out-box'>".$content."</p>";
}
add_shortcode( 'call-out', 'call_out_box' );


//Echo string in between shortcode tags
function get_string_between($start, $end, $string){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
}