<?php
/**
 * Search SQL filter for matching against post title only.
 */
function __search_by_title_only( $search, &$wp_query )
{
    global $wpdb;

    custom_search_join($search , $wp_query);

    if ( empty( $search ) )
        return $search; // skip processing - no search term in query

    //$customs = Array('email_address', 'phone_direct', 'phone_mobile');
    $q = $wp_query->query_vars;
    $n = ! empty( $q['exact'] ) ? '' : '%';

    $search = '';
    $searchand = '';

    foreach ( (array) $q['search_terms'] as $term ) {
        $term = esc_sql( $wpdb->esc_like($term) );
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";// OR ($wpdb->postmeta.meta_key ='email_address' AND $wpdb->postmeta.meta_velue LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }

    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
//add_filter( 'posts_search', '__search_by_title_only', 500, 2 );


//Auto complete search
function search_autocomplete() {

    global $wpdb;

    $final_posts = array();
    $repeated_posts = array();

    //$term = "ar@";
    /**
     * Define number of search results that should be returned
     * For no limit fill in: '-1'
     */
    $POSTS_PER_PAGE = -1;

    /**
     * Define which post types should be searched in
     */
    $POST_TYPES = array(
        'post',
        'page',
        'employee',
        'event',
        'specialty',
        'knowledge'
    );

    /**
     * Get posts based on the defined arguments
     */
    $searchTerm = $_REQUEST['term'];
    $term = trim( $searchTerm );

    /*
     * Search by title
     */
    $sql_by_title = "SELECT $wpdb->posts.* FROM $wpdb->posts
                    WHERE $wpdb->posts.post_status = 'publish'
                    AND $wpdb->posts.post_title LIKE '%$term%'
                        ";
    $posts_by_title = $wpdb->get_results($sql_by_title);


    /*
     * Search by metadata
     */
    $metakeys = ["email_address", "phone_direct", "phone_mobile"];
    $metadata_sql='';
    $count = 0;
    foreach ($metakeys as $metakey){
        if($count > 0){
            $metadata_sql .= " OR ($wpdb->postmeta.meta_key = '".$metakey."' AND $wpdb->postmeta.meta_value LIKE '%$term%')";
        }else{
            $metadata_sql .= " ($wpdb->postmeta.meta_key = '".$metakey."' AND $wpdb->postmeta.meta_value LIKE '%$term%')";
        }
        $count++;
    }

    $sql_by_meta = "SELECT $wpdb->posts.* FROM $wpdb->posts
                        LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
                    WHERE $metadata_sql
                    ";
    $posts_by_meta = $wpdb->get_results($sql_by_meta);






    /*
    $sql_phone_mobile = "SELECT $wpdb->postmeta.* FROM $wpdb->postmeta
                    WHERE $wpdb->postmeta.meta_key = 'phone_mobile'
                    ";

    $postmetatags_phone_mobile = $wpdb->get_results($sql_phone_mobile);

    //echo json_encode($postmetatags_phone_mobile);


    foreach ($postmetatags_phone_mobile as $postmeta) {
        $striped_meta_value = str_replace(' ', '', $postmeta->meta_value);
        $postmeta->meta_value = $striped_meta_value;
    }

    echo json_encode($postmetatags_phone_mobile);

    */






    /*
     * Merge posts by title and by metadata
     */
    $posts = array_merge($posts_by_title,$posts_by_meta);

    /*
     * We filter by language
     */
    $termVariation = checkForVariation($_REQUEST['lang_code'], $searchTerm);

    if($termVariation != '')
    {
        $variationSearchArgs = array(
            's'                 => $termVariation,
            'post_type'         => $POST_TYPES,
            'posts_per_page'    => $POSTS_PER_PAGE,
        );

        $morePosts = get_posts( $variationSearchArgs );

        $posts = array_merge($posts,$morePosts);

    }



    // Array which holds the found suggestions
    $suggestions            = array();

    $holdPostTypes          = array(
        'employee',
        'event'
    );
    $holdPost               = array();

    foreach ($posts as $post)
    {
        setup_postdata($post);

        //Exclude other languages
        $post_language_information = apply_filters( 'wpml_post_language_details', NULL, $post->ID ) ;
        $locale = $post_language_information['locale'];
        $currentLang = $_REQUEST['lang'];
        if($locale != $currentLang){ continue; }

        $suggestion = array(
            'langCode'=>$_REQUEST['lang_code'],
            'lang'  => $post_language_information['locale'],
            'id'    => $post->ID,
            'label' => esc_html($post->post_title),
            'link'  => get_permalink($post->ID)
        );


        if(in_array($post->post_type, $holdPostTypes))
        {
            array_push($holdPost, $suggestion);
        }
        else
        {
            array_push($suggestions, $suggestion);
        }
    }

    $suggestions = array_merge($suggestions, $holdPost);

    // If no results are found, send back an empty JSON encoded array
    if(count($posts) === 0) {
        $response = $_GET["callback"] . "(" . json_encode(array()) . ")";
    }
    // If results are found, send back JSON encoded $suggestions array
    else {
        $response = $_GET["callback"] . "(" . json_encode($suggestions) . ")";
    }

    header("Content-type: application/javascript");
    echo $response;
    exit;
}

add_action( 'wp_ajax_search_autocomplete', 'search_autocomplete' );
add_action( 'wp_ajax_nopriv_search_autocomplete', 'search_autocomplete' );
