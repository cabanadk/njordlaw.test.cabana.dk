<?php
/*
 * Extends WP user with custom fields required for the extranet
 * */
function cabana_additional_profile_fields($user){
    $user_roles = $user->roles;
    $user_role = array_shift($user_roles);
    if($user_role !== EXTRANET_USER_ROLE_NAME){
        return;
    }

    $company = get_the_author_meta(EXTRANET_FIELD_COMPANY, $user->ID);
    $email_verified = get_the_author_meta(EXTRANET_FIELD_EMAIL_VERIFIED, $user->ID);
    $phone = get_the_author_meta(EXTRANET_FIELD_PHONE, $user->ID);
    $membership_active = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_ACTIVE, $user->ID);
    $membership_expires = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user->ID);
    $activation_key = get_the_author_meta(EXTRANET_FIELD_ACTIVATION_KEY, $user->ID);
    $membership_expires_notification_sent = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT, $user->ID);
?>
    <h3>Extranet</h3>
    <table class="form-table">
        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_COMPANY; ?>">Company</label>
            </th>
            <td>
                <input type="text" class="regular-text" name="<?php echo EXTRANET_FIELD_COMPANY; ?>" id="<?php echo EXTRANET_FIELD_COMPANY; ?>" value="<?php echo $company; ?>" />                
            </td>
        </tr>
        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_EMAIL_VERIFIED; ?>">Email Verified</label>
            </th>
            <td>                
                <input type="checkbox" id="<?php echo EXTRANET_FIELD_EMAIL_VERIFIED; ?>" name="<?php echo EXTRANET_FIELD_EMAIL_VERIFIED; ?>" value="<?php echo $email_verified; ?>" <?php echo $email_verified == '1' ? 'checked' : ''; ?> />
            </td>
        </tr>
        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_PHONE; ?>">Phone Number</label>
            </th>
            <td>                
                <input type="text" id="<?php echo EXTRANET_FIELD_PHONE; ?>" name="<?php echo EXTRANET_FIELD_PHONE; ?>" value="<?php echo $phone; ?>" <?php echo $phone == '1' ? 'checked' : ''; ?> />
            </td>
        </tr>
        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_MEMBERSHIP_ACTIVE; ?>">Membership Active</label>
            </th>
            <td>
                <input type="checkbox" id="<?php echo EXTRANET_FIELD_MEMBERSHIP_ACTIVE; ?>" name="<?php echo EXTRANET_FIELD_MEMBERSHIP_ACTIVE; ?>" value="<?php echo $membership_active; ?>" <?php echo $membership_active == '1' ? 'checked' : ''; ?> />
                <input type="hidden" id="<?php echo EXTRANET_FIELD_ACTIVATION_KEY; ?>" name="<?php echo EXTRANET_FIELD_ACTIVATION_KEY; ?>" value="<?php echo $activation_key; ?>" />                
            </td>
        </tr>
        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_MEMBERSHIP_EXPIRES; ?>">Membership Expires</label>
            </th>
            <td>                
                <input type="text" readonly name="<?php echo EXTRANET_FIELD_MEMBERSHIP_EXPIRES; ?>" id="<?php echo EXTRANET_FIELD_MEMBERSHIP_EXPIRES; ?>" value="<?php echo $membership_expires; ?>" />
                <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
                <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script>
                    jQuery(function ($) {
                        var $input = $('#<?php echo EXTRANET_FIELD_MEMBERSHIP_EXPIRES; ?>');                        
                        $input.datepicker({
                            dateFormat: 'yy-mm-dd',
                            firstDay: 1
                        });
                    });
                </script>
            </td>

        </tr>

        <tr>
            <th>
                <label for="<?php echo EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT; ?>">Membership Expires Notification sent</label>
            </th>
            <td>
                <?php echo $membership_expires_notification_sent ?>
            </td>
        </tr>
    </table>
    <?php
}
add_action( 'show_user_profile', 'cabana_additional_profile_fields' );
add_action( 'edit_user_profile', 'cabana_additional_profile_fields' );
?>