<?php

$error = null;
$password_sent = false;
$user_email = filter_input(INPUT_POST, 'user_login', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$user_id = username_exists($user_email);

if($user_id){

    send_new_password($user_id);
    $password_sent = true;

}else{

    $error = apply_filters('wpml_translate_single_string', 'User does not exist.', EXTRANET_DOMAIN, 'Forgot Password Page - User does not exist');

}