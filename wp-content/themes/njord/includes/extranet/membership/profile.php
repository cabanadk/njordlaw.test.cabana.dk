<?php
if(!is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
    exit;
}
include_once('profile-handler.php');
get_header();
include_once(EXTRANET_ROOT_DIR . 'extranet-pages' . DIRECTORY_SEPARATOR . '_sidebar.php');
?>

<?php
$featured_image = wp_get_attachment_image_src(get_option(EXTRANET_PROFILE_PAGE_FEATURED_IMAGE_ID), 'full');
if(!empty($featured_image)):
?>
<figure id="featured">
    <img src="<?php echo $featured_image[0]; ?>" />
</figure>
<?php endif; ?>

<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Profile', EXTRANET_DOMAIN, 'Profile Page - Title'); ?>
    </h1>
    <p>
        <?php echo html_entity_decode(get_option(EXTRANET_PROFILE_PAGE_TEXT)); ?>
    </p>
    <style>
        input[readonly] {
            background: #ddd;
        }
        .error {
            color: red;
        }
        .success {
            background: #60dd49;
            color: white;
            padding: 5px 10px;
            font-size: 12px;
            display: inline-block;
        }
    </style>
    <form action="" method="post" id="ExtranetProfileForm">        

        <?php if($saved): ?>
        <p class="success">
            <?php echo apply_filters('wpml_translate_single_string', 'Thank you. Your profile info has been saved.', EXTRANET_DOMAIN, 'Profile Page - Info - Saved'); ?>
        </p>
        <?php endif; ?>

        <label for="firstname">
            <?php echo apply_filters('wpml_translate_single_string', 'First Name', EXTRANET_DOMAIN, 'Registration Page - Label - First Name'); ?>
        </label>
        <input type="text" name="firstname" value="<?php echo $first_name; ?>" />
        <?php if(isset($errors['firstname'])): ?>
        <span class="error">
            <?php echo $errors['firstname'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="lastname">
            <?php echo apply_filters('wpml_translate_single_string', 'Last Name', EXTRANET_DOMAIN, 'Registration Page - Label - Last Name'); ?>
        </label>
        <input type="text" name="lastname" value="<?php echo $last_name; ?>" />
        <?php if(isset($errors['lastname'])): ?>
        <span class="error">
            <?php echo $errors['lastname'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="company">
            <?php echo apply_filters('wpml_translate_single_string', 'Company', EXTRANET_DOMAIN, 'Registration Page - Label - Company'); ?>
        </label>
        <input type="text" name="company" value="<?php echo $company; ?>" />
        <?php if(isset($errors['company'])): ?>
        <span class="error">
            <?php echo $errors['company'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="email">
            <?php echo apply_filters('wpml_translate_single_string', 'Email', EXTRANET_DOMAIN, 'Registration Page - Label - Email'); ?>
        </label>
        <input type="text" name="email" value="<?php echo $email; ?>" readonly />

        <label for="phone">
            <?php echo apply_filters('wpml_translate_single_string', ' Phone number', EXTRANET_DOMAIN, 'Registration Page - Label - Phone'); ?>
        </label>
        <input type="text" name="phone" value="<?php echo $phone; ?>" readonly />


        <label for="subscription_until">
            <?php echo apply_filters('wpml_translate_single_string', 'Subscription Until', EXTRANET_DOMAIN, 'Profile Page - Label - Subscription'); ?>
        </label>
        <input type="text" name="subscription_until" value="<?php echo $subscription_until; ?>" readonly />

        <label for="password">
            <?php echo apply_filters('wpml_translate_single_string', 'New Password', EXTRANET_DOMAIN, 'Profile Page - Label - New Password'); ?>
        </label>
        <input type="password" name="password" value="" />
        <?php if(isset($errors['password'])): ?>
        <span class="error">
            <?php echo $errors['password'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="password_repeat">
            <?php echo apply_filters('wpml_translate_single_string', 'New Password Repeat', EXTRANET_DOMAIN, 'Profile Page - Label - New Password Repeat'); ?>
        </label>
        <input type="password" name="password_repeat" value="" />
        <?php if(isset($errors['password_repeat'])): ?>
        <span class="error">
            <?php echo $errors['password_repeat'][0]; ?>
        </span>
        <?php endif; ?>

        <br />
        <br />

        <input type="submit" name="save_profile" value="<?php echo apply_filters('wpml_translate_single_string', 'Save', EXTRANET_DOMAIN, 'Profile Page - Label - Save'); ?>" />

    </form>
</section>

<?php get_footer();?>