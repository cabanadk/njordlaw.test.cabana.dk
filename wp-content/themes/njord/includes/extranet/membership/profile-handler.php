<?php

$reg_errors = new WP_Error;
$errors = null;
$saved = false;

$user = wp_get_current_user();

$first_name =           get_the_author_meta('first_name', $user->ID);
$last_name =            get_the_author_meta('last_name', $user->ID);
$company =              get_the_author_meta(EXTRANET_FIELD_COMPANY, $user->ID);
$email =                get_the_author_meta('email', $user->ID);
$subscription_until =   new DateTime(get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user->ID));
$subscription_until =   $subscription_until->format('d-m-Y');
$password =         '';
$password_repeat =  '';

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['save_profile'])){

    // save

    $first_name = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $last_name = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $company = filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $password_repeat = filter_input(INPUT_POST, 'password_repeat', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

    $validation_generic =               apply_filters('wpml_translate_single_string', 'Required form field is missing', EXTRANET_DOMAIN, 'Registration Page - Validation - Generic');
    $validation_email =                 apply_filters('wpml_translate_single_string', 'Email address is not valid', EXTRANET_DOMAIN, 'Registration Page - Validation - Email');
    $validation_email_password_match =  apply_filters('wpml_translate_single_string', 'Passwords must match', EXTRANET_DOMAIN, 'Registration Page - Validation - Password Match');

    if(empty($first_name)){
        $reg_errors->add('firstname', $validation_generic);
    }

    if(empty($last_name)){
        $reg_errors->add('lastname', $validation_generic);
    }

    if(empty($company)){
        $reg_errors->add('company', $validation_generic);
    }

    if(empty($phone)){
        $reg_errors->add('phone', $validation_generic);
    }
    
    if(empty($email)){
        $reg_errors->add('email', $validation_generic);
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $reg_errors->add('email', $validation_email);
    }

    if(!empty($password) && !empty($password_repeat)){
        if(empty($password)){
            $reg_errors->add('password', $validation_generic);
        }
        if(empty($password_repeat)){
            $reg_errors->add('password_repeat', $validation_generic);
        }
        if((!empty($password) && !empty($password_repeat)) && ($password !== $password_repeat)){
            $reg_errors->add('password', $validation_email_password_match);
        }
    }

    if (count($reg_errors->errors) > 0){

        $errors = $reg_errors->errors;

    }else{

        // save user data

        update_user_meta($user->ID, 'first_name', $first_name);
        update_user_meta($user->ID, 'last_name', $last_name);
        update_user_meta($user->ID, EXTRANET_FIELD_COMPANY, $company);

        if(!empty($password) && !empty($password_repeat)){
            wp_set_password($password, $user->ID);
        }

        $saved = true;

    }

}