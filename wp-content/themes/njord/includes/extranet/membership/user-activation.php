<?php
if(!isset($_GET['uid']) || !isset($_GET['key'])){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
    exit;
}
$user_id = $_GET['uid'];
$activation_key = $_GET['key'];
$user = get_user_by('id', $user_id);
?>
<?php get_header(); ?>
<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Extranet Member Activation', EXTRANET_DOMAIN, 'Activation Page - Title'); ?>
    </h1>

    <?php if($user == false): ?>

        <p>
            <?php echo apply_filters('wpml_translate_single_string', 'User does not exist.', EXTRANET_DOMAIN, 'Activation Page - User does not exist'); ?>
        </p>

    <?php elseif($activation_key != get_the_author_meta(EXTRANET_FIELD_ACTIVATION_KEY, $user_id)): ?>
        
        <p>
            <?php echo apply_filters('wpml_translate_single_string', 'Activation key does not exist.', EXTRANET_DOMAIN, 'Activation Page - Activation key does not exist'); ?>
        </p>

    <?php else: ?>

        <?php activate_member($user_id); ?>
        <p>
            <?php echo apply_filters('wpml_translate_single_string', 'Your email is verified. You may now login.', EXTRANET_DOMAIN, 'Activation Page - Email Verified'); ?>
        </p>

    <?php endif; ?>
</section>
<?php get_footer();?>