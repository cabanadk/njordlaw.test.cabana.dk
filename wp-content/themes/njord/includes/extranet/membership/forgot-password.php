<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['forgot_password'])){
    include_once('forgot-password-handler.php');
}
if(is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)));
    exit;
}
?>
<?php get_header(); ?>

<?php
$featured_image = wp_get_attachment_image_src(get_option(EXTRANET_LOGIN_PAGE_FEATURED_IMAGE_ID), 'full');
if(!empty($featured_image)):
?>
<figure id="featured">
    <img src="<?php echo $featured_image[0]; ?>" />
</figure>
<?php endif; ?>

<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Forgot password', EXTRANET_DOMAIN, 'Forgot Password Page - Title'); ?>
    </h1>
    <?php if($password_sent): ?>
    <p>
        <?php echo apply_filters('wpml_translate_single_string', 'Password has been sent to your email.', EXTRANET_DOMAIN, 'Forgot Password Page - Password Sent'); ?>
    </p>
    <?php else: ?>
    <p>
        <?php echo html_entity_decode(get_option(EXTRANET_FORGOT_PASSWORD_PAGE_TEXT)); ?>
    </p>
    <?php endif; ?>    
    <form action="" method="post" id="ExtranetForgotPasswordForm">
        <?php if(isset($error)): ?>
        <div class="error">
            <p style="color:red">
                <?php echo $error; ?>
            </p>
        </div>
        <?php endif; ?>
        <div>
            <label>
                <?php echo apply_filters('wpml_translate_single_string', 'Email', EXTRANET_DOMAIN, 'Registration Page - Label - Email'); ?>
            </label>
            <input type="text" name="user_login" id="user_login" value="" autofocus />
        </div>
        <div>
            <br />
            <input type="submit" name="forgot_password" id="forgot_password" value="Send" />
        </div>
    </form>
</section>
<?php get_footer();?>