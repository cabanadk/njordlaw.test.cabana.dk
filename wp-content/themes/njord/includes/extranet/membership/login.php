<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['extranet_login'])){
    include_once('login-handler.php');
}
if(is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)));
    exit;
}
?>
<?php get_header(); ?>

<?php
$featured_image = wp_get_attachment_image_src(get_option(EXTRANET_LOGIN_PAGE_FEATURED_IMAGE_ID), 'full');
if(!empty($featured_image)):
?>
<figure id="featured">
    <img src="<?php echo $featured_image[0]; ?>" />
</figure>
<?php endif; ?>

<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Extranet Login', EXTRANET_DOMAIN, 'Login Page - Title'); ?>
    </h1>

    <?php echo html_entity_decode(get_option(EXTRANET_LOGIN_PAGE_TEXT)); ?>

    <form action="" method="post" id="ExtranetLoginForm">
        <?php if(isset($error)): ?>
        <div class="error">
            <p style="color:red">
                <?php echo $error; ?>
            </p>
        </div>
        <?php endif; ?>
        <div>
            <label>
                <?php echo apply_filters('wpml_translate_single_string', 'Email', EXTRANET_DOMAIN, 'Registration Page - Label - Email'); ?>
            </label>
            <input type="text" name="user_login" id="user_login" value="" autofocus />
        </div>
        <div>
            <label>
                <?php echo apply_filters('wpml_translate_single_string', 'Password', EXTRANET_DOMAIN, 'Registration Page - Label - Password'); ?>
            </label>
            <input type="password" name="user_password" id="user_password" value="" />
        </div>
        <div>
            <br />
            <input type="submit" name="extranet_login" id="extranet_login" value="<?php echo apply_filters('wpml_translate_single_string', 'Login', EXTRANET_DOMAIN, 'Login Page - Label - Login'); ?>" />
        </div>
        <div>
            <br />
            <a href="<?php echo sprintf('%s/%s/forgotpassword', EXTRANET_ENSURE_LANGUAGE_URL_PREFIX, EXTRANET_REWRITE_URL); ?>" style="margin-left: 20px;">
                <?php echo apply_filters('wpml_translate_single_string', 'Forgot Password?', EXTRANET_DOMAIN, 'Login Page - Label - Forgot Password'); ?>
            </a>
        </div>
    </form>
</section>
<?php get_footer();?>