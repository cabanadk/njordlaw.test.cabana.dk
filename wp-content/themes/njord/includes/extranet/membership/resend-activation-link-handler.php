<?php

$register_user_success = true;
$resend_activation_email = true;

$user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

send_activation_link($user_id);