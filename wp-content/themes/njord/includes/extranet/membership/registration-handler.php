<?php

$reg_errors = new WP_Error;
$errors = null;
$register_user_success = false;
$resend_activation_email = false;

$firstname =        filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$lastname =         filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$company =          filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$email =            filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$phone =        filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$password =         filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$password_repeat =  filter_input(INPUT_POST, 'password_repeat', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$terms_and_conditions =  filter_input(INPUT_POST, 'terms_and_conditions', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

$validation_generic =               apply_filters('wpml_translate_single_string', 'Required form field is missing', EXTRANET_DOMAIN, 'Registration Page - Validation - Generic');
$validation_email =                 apply_filters('wpml_translate_single_string', 'Email address is not valid', EXTRANET_DOMAIN, 'Registration Page - Validation - Email');
$validation_email_exist =           apply_filters('wpml_translate_single_string', 'User with this email already exists', EXTRANET_DOMAIN, 'Registration Page - Validation - Email Exist');
$validation_phone =                 apply_filters('wpml_translate_single_string', 'Phone Number is not valid', EXTRANET_DOMAIN, 'Registration Page - Validation - Phone');
$validation_email_password_match =  apply_filters('wpml_translate_single_string', 'Passwords must match', EXTRANET_DOMAIN, 'Registration Page - Validation - Password Match');
$validation_terms_and_conditions =  apply_filters('wpml_translate_single_string', 'You must accept our Terms and Conditions', EXTRANET_DOMAIN, 'Registration Page - Validation - Terms and Conditions');

if(empty($firstname)){
    $reg_errors->add('firstname', $validation_generic);
}

if(empty($lastname)){
    $reg_errors->add('lastname', $validation_generic);
}

if(empty($company)){
    $reg_errors->add('company', $validation_generic);
}

if(empty($email)){
    $reg_errors->add('email', $validation_generic);
}elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $reg_errors->add('email', $validation_email);
}elseif(get_user_by('email', $email)){
    $reg_errors->add('email', $validation_email_exist);
}
if(empty($phone)){
    $reg_errors->add('phone', $validation_phone);
}

if(empty($password)){
    $reg_errors->add('password', $validation_generic);
}

if(empty($password_repeat)){
    $reg_errors->add('password_repeat', $validation_generic);
}

if((!empty($password) && !empty($password_repeat)) && ($password !== $password_repeat)){
    $reg_errors->add('password', $validation_email_password_match);
}

if(!isset($terms_and_conditions)){
    $reg_errors->add('terms_and_conditions', $validation_terms_and_conditions);
}

if (count($reg_errors->errors) > 0){

    $errors = $reg_errors->errors;

}else{

    // developer.wordpress.org/reference/functions/wp_insert_user/

    $user_id = wp_insert_user(
		array(
			'user_email' => $email,
            'user_login' => $email,
			'user_pass'  => $password,
			'first_name' => $firstname,
			'last_name'  => $lastname,
            'role'       => EXTRANET_USER_ROLE_NAME
		)
	);

    if(is_wp_error($user_id)){

        $errors = $user_id;

    }else{

        $activation_key = uniqid();

        update_user_meta($user_id, EXTRANET_FIELD_COMPANY, $company);
        update_user_meta($user_id, EXTRANET_FIELD_EMAIL_VERIFIED, 0);
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_ACTIVE, 0);
        update_user_meta($user_id, EXTRANET_FIELD_ACTIVATION_KEY, $activation_key);
        update_user_meta($user_id, EXTRANET_FIELD_PHONE, $phone);

        // Send activation link
        send_activation_link($user_id);

        $register_user_success = true;

    }

}