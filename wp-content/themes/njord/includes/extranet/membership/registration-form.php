<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['extranet_registration'])){
    include_once('registration-handler.php');
}
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['extranet_resend_activation_link'])){
    include_once('resend-activation-link-handler.php');
}
if(is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)));
    exit;
}
?>
<?php get_header(); ?>

<?php
$featured_image = wp_get_attachment_image_src(get_option(EXTRANET_REGISTRATION_PAGE_FEATURED_IMAGE_ID), 'full');
if(!empty($featured_image)):
?>
<figure id="featured">
    <img src="<?php echo $featured_image[0]; ?>" />
</figure>
<?php endif; ?>

<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Extranet Registration', EXTRANET_DOMAIN, 'Registration Page - Title'); ?>
    </h1>   

    <?php if(!$register_user_success): ?>

    <?php echo html_entity_decode(get_option(EXTRANET_REGISTRATION_PAGE_TEXT)); ?>

    <style>
        #ExtranetRegistrationForm .error {
            color:red;
        }
    </style>

    <form action="" method="post" id="ExtranetRegistrationForm">

        <?php if(isset($errors)): ?>
        <!--<div class="error">
                    <?php print_r($errors); ?>
                </div>-->
        <?php endif; ?>

        <label for="firstname">
            <?php echo apply_filters('wpml_translate_single_string', 'First Name', EXTRANET_DOMAIN, 'Registration Page - Label - First Name'); ?>
        </label>
        <input type="text" name="firstname" value="<?php echo isset($firstname) ? $firstname : ''; ?>" />
        <?php if(isset($errors['firstname'])): ?>
        <span class="error">
            <?php echo $errors['firstname'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="lastname">
            <?php echo apply_filters('wpml_translate_single_string', 'Last Name', EXTRANET_DOMAIN, 'Registration Page - Label - Last Name'); ?>
        </label>
        <input type="text" name="lastname" value="<?php echo isset($lastname) ? $lastname : ''; ?>" />
        <?php if(isset($errors['lastname'])): ?>
        <span class="error">
            <?php echo $errors['lastname'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="company">
            <?php echo apply_filters('wpml_translate_single_string', 'Company', EXTRANET_DOMAIN, 'Registration Page - Label - Company'); ?>
        </label>
        <input type="text" name="company" value="<?php echo isset($company) ? $company : ''; ?>" />
        <?php if(isset($errors['company'])): ?>
        <span class="error">
            <?php echo $errors['company'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="email">
            <?php echo apply_filters('wpml_translate_single_string', 'Email', EXTRANET_DOMAIN, 'Registration Page - Label - Email'); ?>
        </label>
        <input type="text" name="email" value="<?php echo isset($email) ? $email : ''; ?>" />
        <?php if(isset($errors['email'])): ?>
        <span class="error">
            <?php echo $errors['email'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="phone">
            <?php echo apply_filters('wpml_translate_single_string', 'Phone', EXTRANET_DOMAIN, 'Registration Page - Label - Phone'); ?>
        </label>
        <input type="text" name="phone" value="<?php echo isset($phone) ? $phone : ''; ?>" />
        <?php if(isset($errors['phone'])): ?>
        <span class="error">
            <?php echo $errors['phone'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="password">
            <?php echo apply_filters('wpml_translate_single_string', 'Password', EXTRANET_DOMAIN, 'Registration Page - Label - Password'); ?>
        </label>
        <input type="password" name="password" value="" />
        <?php if(isset($errors['password'])): ?>
        <span class="error">
            <?php echo $errors['password'][0]; ?>
        </span>
        <?php endif; ?>

        <label for="password_repeat">
            <?php echo apply_filters('wpml_translate_single_string', 'Repeat Password', EXTRANET_DOMAIN, 'Registration Page - Label - Password Repeat'); ?>
        </label>
        <input type="password" name="password_repeat" value="" />
        <?php if(isset($errors['password_repeat'])): ?>
        <span class="error">
            <?php echo $errors['password_repeat'][0]; ?>
        </span>
        <?php endif; ?>

        <br />
        <br />        
        <label>            
            <input type="checkbox" name="terms_and_conditions" value="" />
            Jeg accepterer 
            <a href="<?php echo get_option(EXTRANET_TERMS_AND_CONDITIONS_URL); ?>" target="_blank">handelsbetingelserne.</a>
        </label>        
        <?php if(isset($errors['terms_and_conditions'])): ?>
        <span class="error">
            <?php echo $errors['terms_and_conditions'][0]; ?>
        </span>
        <?php endif; ?>

        <br />
        <br />

        <input type="submit" name="extranet_registration" value="<?php echo apply_filters('wpml_translate_single_string', 'Register', EXTRANET_DOMAIN, 'Registration Page - Label - Register'); ?>" />

    </form>

    <?php elseif(!$resend_activation_email): ?>

    <p>
        <?php echo sprintf(apply_filters('wpml_translate_single_string', 'Thank you. An email has been sent to %s for further instructions.', EXTRANET_DOMAIN, 'Registration Page - Notice - Activation email sent'), get_the_author_meta('user_email', $user_id)); ?>
    </p>
    <form action="" method="post" id="ExtranetResendActivationLinkForm">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
        <input type="submit" name="extranet_resend_activation_link" value="Resend Email" />
    </form>

    <?php else: ?>

    <p>
        <?php echo sprintf(apply_filters('wpml_translate_single_string', 'Activation email has been resent to %s.', EXTRANET_DOMAIN, 'Registration Page - Notice - Activation email resent'), get_the_author_meta('user_email', $user_id)); ?>
    </p>
    <form action="" method="post" id="ExtranetResendActivationLinkForm">        
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
        <input type="submit" name="extranet_resend_activation_link" value="Resend Email" />
    </form>

    <?php endif; ?>
</section>

<?php get_footer();?>