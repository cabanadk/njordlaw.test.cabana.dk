<?php

$error = null;

$username =        filter_input(INPUT_POST, 'user_login', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$password =        filter_input(INPUT_POST, 'user_password', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

$credentials = array(
    'user_login' => $username,
    'user_password' => $password,
    'remember' => false
    );
$result = wp_signon($credentials);

if(is_wp_error($result)){
    $error = $result->get_error_message();
}else{
    $email_verified = get_the_author_meta(EXTRANET_FIELD_EMAIL_VERIFIED, $result->ID);
    $membership_active = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_ACTIVE, $result->ID);
    $membership_expires = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $result->ID);

    if($email_verified != '1'){
        wp_logout();
        $error = apply_filters('wpml_translate_single_string', 'Email not verified.', EXTRANET_DOMAIN, 'Login Page - Error - Email not verified');
    }elseif($membership_active != '1'){
        wp_logout();
        $error = apply_filters('wpml_translate_single_string', 'Your membership is inactive.', EXTRANET_DOMAIN, 'Login Page - Error - Membership inactive');
    }elseif(strtotime($membership_expires) <= time()){
        //wp_logout();
        //$error = apply_filters('wpml_translate_single_string', 'Your membership has expired.', EXTRANET_DOMAIN, 'Login Page - Error - Membership expired');        
        wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)));
        exit;
    }else{
        wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)));
        exit;
    }
}