<?php
/**
 * Defining custom user roles for the Extranet
 */
add_action('init', function(){
    if(get_role(EXTRANET_USER_ROLE_NAME) == null){
        $result = add_role(EXTRANET_USER_ROLE_NAME, __(EXTRANET_USER_ROLE_DISPLAY_NAME), array(
           'read' => true, // true allows this capability
           'edit_posts' => false, // Allows user to edit their own posts
           'edit_pages' => false, // Allows user to edit pages
           'edit_others_posts' => false, // Allows user to edit others posts not just their own
           'create_posts' => false, // Allows user to create new posts
           'manage_categories' => false, // Allows user to manage post categories
           'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
           'edit_themes' => false, // false denies this capability. User can�t edit your theme
           'install_plugins' => false, // User cant add new plugins
           'update_plugin' => false, // User can�t update any plugins
           'update_core' => false // user cant perform core updates
           )
       );
    }
});

/**
 * Restrict users from accessing the WP backend
 */
add_action('admin_init', function(){
    $redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url('/');
    global $current_user;
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    if($user_role === EXTRANET_USER_ROLE_NAME){
        exit(wp_redirect($redirect));
    }
}, 100);

/*
 * Do not show admin bar for logged in extranet members
 * */
add_filter('show_admin_bar', function($content){
    $user = wp_get_current_user();
    if(in_array(EXTRANET_USER_ROLE_NAME, (array) $user->roles)){
        return false;
    }else{
        return $content;
    }
});

?>