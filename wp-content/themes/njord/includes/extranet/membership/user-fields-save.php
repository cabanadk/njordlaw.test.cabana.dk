<?php
/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
function cabana_save_profile_fields($user_id) {

    if (!is_admin()){
        return false;
    }

    $company_fld_value = filter_input(INPUT_POST, EXTRANET_FIELD_COMPANY, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    if(isset($company_fld_value)){
        update_user_meta($user_id, EXTRANET_FIELD_COMPANY, $company_fld_value);
    }

    $email_verified_fld_value = filter_input(INPUT_POST, EXTRANET_FIELD_EMAIL_VERIFIED, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    if(isset($email_verified_fld_value)){
        update_user_meta($user_id, EXTRANET_FIELD_EMAIL_VERIFIED, 1);
    }else{
        update_user_meta($user_id, EXTRANET_FIELD_EMAIL_VERIFIED, 0);
    }

    $phone_value = filter_input(INPUT_POST, EXTRANET_FIELD_PHONE, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    if(isset($phone_value)){
        update_user_meta($user_id, EXTRANET_FIELD_PHONE, $phone_value);
    }

    $membership_active_fld_value = filter_input(INPUT_POST, EXTRANET_FIELD_MEMBERSHIP_ACTIVE, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    if(isset($membership_active_fld_value)){
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_ACTIVE, 1);
        mailchimp_member_add_or_update($user_id, 'subscribed');
    }else{
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_ACTIVE, 0);
        mailchimp_member_add_or_update($user_id, 'unsubscribed');
    }

    $membership_expires_fld_value = filter_input(INPUT_POST, EXTRANET_FIELD_MEMBERSHIP_EXPIRES, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    if(isset($membership_expires_fld_value)){
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $membership_expires_fld_value);
    }

}

add_action( 'personal_options_update', 'cabana_save_profile_fields' );
add_action( 'edit_user_profile_update', 'cabana_save_profile_fields' );