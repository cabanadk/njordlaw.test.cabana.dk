<?php
/*
 * Custom rewrite rule for extranet
 * Crfeates njord.com/extranet/[controller]
 * flush_rewrite_rules() is needed for first use
 * */

add_action('init', function(){

    //flush_rewrite_rules();

    //allow redirection, even if my theme starts to send output to the browser
    ob_start();

    global $wp;

    add_rewrite_tag('%'.EXTRANET_REWRITE_TAG_ROOT.'%', '([^&]+)');
    add_rewrite_rule( '^'.EXTRANET_REWRITE_URL.'/?([^&]*)/?$', 'index.php?'.EXTRANET_REWRITE_TAG_ROOT.'=$matches[1]', 'top' );

}, 10, 0);

add_action('parse_request', function($wp){
    /*
    if(empty($wp->query_vars[EXTRANET_REWRITE_TAG_ROOT])
        && empty($wp->query_vars[EXTRANET_REWRITE_TAG_SECTION])
        && empty($wp->query_vars[EXTRANET_REWRITE_TAG_PAGE])){
        wp_redirect(get_home_url());
        exit;
    }
    */
    if(isset($wp->query_vars[EXTRANET_REWRITE_TAG_ROOT])){

        $query_var_root = $wp->query_vars[EXTRANET_REWRITE_TAG_ROOT];
        $path = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;

        if(!empty($query_var_root)){

            if($query_var_root === 'registration'){
                require_once $path . 'membership' . DIRECTORY_SEPARATOR . 'registration-form.php';
                exit();
            }

            if($query_var_root === 'activation'){
                require_once $path . 'membership' . DIRECTORY_SEPARATOR . 'user-activation.php';
                exit();
            }

            if($query_var_root === 'login'){
                require_once $path . 'membership' . DIRECTORY_SEPARATOR . 'login.php';
                exit();
            }

            if($query_var_root === 'forgotpassword'){
                require_once $path . 'membership' . DIRECTORY_SEPARATOR . 'forgot-password.php';
                exit();
            }

            if($query_var_root === 'profile'){
                require_once $path . 'membership' . DIRECTORY_SEPARATOR . 'profile.php';
                exit();
            }

            if($query_var_root === 'paradigm/all'){
                require_once $path . 'extranet-pages' . DIRECTORY_SEPARATOR . 'paradigms.php';
                exit();
            }

            if($query_var_root === 'search'){
                require_once $path . 'extranet-pages' . DIRECTORY_SEPARATOR . 'search.php';
                exit();
            }

            require_once $path . 'index.php';
            exit();

        }else{

            wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
            exit;

        }

    }
});

add_filter('wp_title', function($title){
    global $wp;
    global $post;
    if(isset($wp->query_vars[EXTRANET_REWRITE_TAG_ROOT])){
        $query_var_root = $wp->query_vars[EXTRANET_REWRITE_TAG_ROOT];

        if(!empty($query_var_root)){
            if($query_var_root === 'registration'){
                return apply_filters('wpml_translate_single_string', 'Extranet Registration', EXTRANET_DOMAIN, 'Registration Page - Title') . ' | ' . $title;
            }
            if($query_var_root === 'activation'){
                return apply_filters('wpml_translate_single_string', 'Extranet Member Activation', EXTRANET_DOMAIN, 'Activation Page - Title') . ' | ' . $title;
            }
            if($query_var_root === 'login'){
                return apply_filters('wpml_translate_single_string', 'Extranet Login', EXTRANET_DOMAIN, 'Login Page - Title') . ' | ' . $title;
            }
            if($query_var_root === 'forgotpassword'){
                return apply_filters('wpml_translate_single_string', 'Forgot Password', EXTRANET_DOMAIN, 'Forgot Password Page - Title') . ' | ' . $title;
            }
            if($query_var_root === 'paradigm/all'){
                return apply_filters('wpml_translate_single_string', 'All Paradigms', EXTRANET_DOMAIN, 'Paradigms Page - Title') . ' | ' . $title;
            }
            if($query_var_root === 'search'){
                return apply_filters('wpml_translate_single_string', 'Search', EXTRANET_DOMAIN, 'Search Page - Title') . ' | ' . $title;
            }
            return $post->post_title . ' | ' . $title;
        }

    }
});

add_action('admin_menu', function(){
    add_menu_page(
        'HR e-Book',
        'HR e-Book',
        'create_users',
        EXTRANET_DOMAIN,
        'extranet_menu_page_callback',
        'dashicons-admin-page',
        0
    );
    add_submenu_page(
         EXTRANET_DOMAIN,
         'Add new page',
         'Add new page',
         'create_users',
         'extranet_new_page',
         'extranet_new_page_menu_page_callback'
     );
    add_submenu_page(
         EXTRANET_DOMAIN,
         'HR e-Book Settings',
         'HR e-Book Settings',
         'create_users',
         'extranet_settings',
         'extranet_settings_menu_page_callback'
     );
});

function extranet_menu_page_callback(){
    wp_redirect(get_admin_url() . 'edit.php?post_type=' . EXTRANET_DOMAIN);
    exit;
}
function extranet_new_page_menu_page_callback(){
    wp_redirect(get_admin_url() . 'post-new.php?post_type=' . EXTRANET_DOMAIN);
    exit;
}
function extranet_settings_menu_page_callback(){
    include_once('admin-pages/settings.php');
}

function is_extranet_user_logged_in(){
    $current_user = wp_get_current_user();
    if($current_user->exists()){
        if(in_array(EXTRANET_USER_ROLE_NAME, $current_user->roles)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function mailchimp_member_add_or_update($user_id, $status){
    $api_key = MAILCHIMP_API_KEY;
    $api_root = MAILCHIMP_API_ROOT;
    $api_login = 'anystring';
    $email = get_the_author_meta('user_email', $user_id);
    $firstname = get_the_author_meta('first_name', $user_id);
    $lastname = get_the_author_meta('last_name', $user_id);
    $subscriber_hash = md5(strtolower($email));
    $query_url = $api_root . '/lists/' . get_option(MAILCHIMP_LIST_ID_OPTION_NAME) . '/members/' . $subscriber_hash;
    $payload = array(
        'status' => $status,
        'status_if_new' => $status,
        'email_address' => $email,
        'merge_fields' => array(
            'FNAME' => $firstname,
            'LNAME' => $lastname
        )
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $query_url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "$api_login:$api_key");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("content-type: application/json"));
    $result = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
}

function get_digital_download($id){
    global $wpdb;
    $table_name = $wpdb->prefix . 'cabana_digital_downloads';
    $file = $wpdb->get_row("SELECT id, created, filename, extension, extra_fields FROM $table_name WHERE id = $id");
    if(isset($file)){
        $file->url = '/download-digital-file/' . $id;
    }
    return $file;
}

function send_activation_link($user_id){
    $activation_key = get_the_author_meta(EXTRANET_FIELD_ACTIVATION_KEY, $user_id);
    $activation_url = get_home_url() . '/'.EXTRANET_REWRITE_URL.'/activation?uid=' . $user_id . '&key=' . $activation_key;
    $firstname = get_the_author_meta('first_name', $user_id);
    $lastname = get_the_author_meta('last_name', $user_id);

    $to = get_the_author_meta('user_email', $user_id);

    $subject = get_option(EXTRANET_REGISTRATION_MESSAGE_SUBJECT);
    $subject = str_replace('{{firstname}}', $firstname, $subject);
    $subject = str_replace('{{lastname}}', $lastname, $subject);

    $message = get_option(EXTRANET_REGISTRATION_MESSAGE);
    $message = str_replace('{{activation_link_url}}', $activation_url, $message);
    $message = str_replace('{{firstname}}', $firstname, $message);
    $message = str_replace('{{lastname}}', $lastname, $message);
    $message = str_replace('{{email}}', $to, $message);
    $message = nl2br(html_entity_decode($message));

    wp_mail($to, $subject, $message);
}

function send_new_password($user_id){

    $firstname = get_the_author_meta('first_name', $user_id);
    $lastname = get_the_author_meta('last_name', $user_id);
    $user_email = get_the_author_meta('user_email', $user_id);
    $new_password = wp_generate_password(8, false);
    wp_set_password($new_password, $user_id);

    $to = $user_email;
    $subject = get_option(EXTRANET_FORGOT_PASSWORD_EMAIL_SUBJECT);
    $message = get_option(EXTRANET_FORGOT_PASSWORD_EMAIL_BODY);
    $message = str_replace('{{password}}', $new_password, $message);
    $message = str_replace('{{firstname}}', $firstname, $message);
    $message = str_replace('{{lastname}}', $lastname, $message);
    $message = nl2br(html_entity_decode($message));

    wp_mail($to, $subject, $message);

}

function activate_member($user_id){

    $email = get_the_author_meta('user_email', $user_id);
    $firstname = get_the_author_meta('first_name', $user_id);
    $lastname = get_the_author_meta('last_name', $user_id);
    $company = get_the_author_meta(EXTRANET_FIELD_COMPANY, $user_id);

    // Update user meta
    update_user_meta($user_id, EXTRANET_FIELD_ACTIVATION_KEY, '');
    update_user_meta($user_id, EXTRANET_FIELD_EMAIL_VERIFIED, '1');
    update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_ACTIVE, '1');

    $expiration_date = new DateTime();
    $expiration_date->setTimestamp(strtotime('+12 month'));
    $expiration_date = $expiration_date->format('Y-m-d');
    update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $expiration_date);

    $registered = new DateTime(get_the_author_meta('user_registered', $user_id));
    $registered = $registered->format('Y-m-d');
    update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT, $registered);


    // Send notification to admin
    $to = get_option(EXTRANET_ADMIN_EMAIL);
    $subject = 'NJORD Admin: New user registration';
    $message = sprintf('Fornavn: %s<br/>Efternavn: %s<br/>Virksomhed: %s<br/>E-mail: %s', $firstname, $lastname, $company, $email);

    if(!empty($to)){
        wp_mail($to, $subject, $message);
    }

    // Send Welcome email to user
    $to = $email;

    $subject = get_option(EXTRANET_WELCOME_MESSAGE_SUBJECT);
    $subject = str_replace('{{firstname}}', $firstname, $subject);
    $subject = str_replace('{{lastname}}', $lastname, $subject);

    $message = get_option(EXTRANET_WELCOME_MESSAGE);
    $message = str_replace('{{firstname}}', $firstname, $message);
    $message = str_replace('{{lastname}}', $lastname, $message);
    $message = str_replace('{{email}}', $to, $message);
    $message = nl2br(html_entity_decode($message));

    wp_mail($to, $subject, $message);

}

add_action('delete_user', function($user_id){
    mailchimp_member_add_or_update($user_id, 'unsubscribed');
});

function is_child($pid) {
    // $pid = The ID of the ancestor page
    global $post; // load details about this page
    $anc = get_post_ancestors($post->ID);
    foreach($anc as $ancestor) {
        if($ancestor == $pid) {
            return true;
        }
    }
    return false; // we�re elsewhere
}

add_filter('lostpassword_url', function(){
    return home_url(sprintf('%s/%s/forgotpassword', EXTRANET_ENSURE_LANGUAGE_URL_PREFIX, EXTRANET_REWRITE_URL));
}, 10, 2);


function user_expiration_check(){
    $users = get_users(array('role'=>EXTRANET_USER_ROLE_NAME));
    if(count($users)){
        foreach ($users as $user)
        {
            send_expiration_notification($user->ID);
            renew_membership($user->ID);
        }
    }
}
function send_expiration_notification($user_id){

    $membership_expires = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user_id);

    $month_before_expiration = new DateTime($membership_expires);
    $month_before_expiration->setTimestamp(strtotime('-1 week', $month_before_expiration->getTimestamp()));

    $expiration_notification_sent_field = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT, $user_id);

    if(!empty($expiration_notification_sent_field)){
        $last_expiration_notification_sent_date = new DateTime($expiration_notification_sent_field);
    }else{
        $last_expiration_notification_sent_date = $month_before_expiration;
    }

    // membership will expire in less than a month. send a notification to admin
    if($month_before_expiration->getTimestamp() <= time() && $last_expiration_notification_sent_date->getTimestamp() < $month_before_expiration->getTimestamp()){

        $email = get_the_author_meta('user_email', $user_id);
        $firstname = get_the_author_meta('first_name', $user_id);
        $lastname = get_the_author_meta('last_name', $user_id);
        $company = get_the_author_meta(EXTRANET_FIELD_COMPANY, $user_id);
        $registered = get_the_author_meta('user_registered', $user_id);
        $membership_expires = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user_id);
        $to = get_option(EXTRANET_ADMIN_EMAIL);
        $subject = 'NJORD Admin: user expires soon';
        $message = sprintf('Fornavn: %s<br/>Efternavn: %s<br/>Virksomhed: %s<br/>E-mail: %s<br/>', $firstname, $lastname, $company, $email);
        $message .= sprintf('Registered: %s<br/>', $registered);
        $message .= sprintf('Membership expires: %s<br/>', $membership_expires);
        $message .= '<br/>Membership will been re-newed automatically after expiration date.';

        wp_mail($to, $subject, $message);

        // temporary debug
        wp_mail('tha@cabana.dk', $subject, $message);

        $new_expiration_notification_date = new DateTime();
        $new_expiration_notification_date = $new_expiration_notification_date->format('Y-m-d');
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT, $new_expiration_notification_date);
    }
}
function renew_membership($user_id){
    // if membership expired, update automatically
    $membership_expires = get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user_id);
    if(strtotime($membership_expires) <= time()){
        $expiration_date = new DateTime($membership_expires);
        $expiration_date->setTimestamp(strtotime('+12 month', $expiration_date->getTimestamp()));
        $expiration_date = $expiration_date->format('Y-m-d');
        update_user_meta($user_id, EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $expiration_date);
    }
}
// cron job action
add_action('extranet_user_expiration_check', function(){
    user_expiration_check();
});

function user_expiration_clear(){

    $users = get_users(array('role'=>EXTRANET_USER_ROLE_NAME));
    if(count($users)){
        foreach ($users as $user)
        {   
            update_user_meta($user->ID, EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT, '2017-01-01');
        }
    }
}

add_action('extranet_user_expiration_clear', function(){
    user_expiration_clear();
});



// add Registered and Expires columns to users table
add_action('manage_users_columns', function($column_headers){
    unset($column_headers['posts']);
    $column_headers['registered'] = 'Registered';
    $column_headers['expires'] = 'Expires';
    $column_headers['phone'] = 'Phone Number';
    return $column_headers;
});
add_action('manage_users_custom_column', function($value, $column_name, $user_id){
    if($column_name == 'registered'){
        return get_the_author_meta('user_registered', $user_id);
    }
    if($column_name == 'expires'){
        return get_the_author_meta(EXTRANET_FIELD_MEMBERSHIP_EXPIRES, $user_id);
    }
    if($column_name == 'phone'){
        return get_the_author_meta(EXTRANET_FIELD_PHONE, $user_id);
    }
    return $value;
}, 10, 3);
?>