<?php
function get_article_contacts($current_post_id){

    //Put all the active employees in an array
    $activeEmployees = array();
    $the_employee_query = new WP_Query( array(
        'post_type'     => 'employee',
        'post_status'   => 'publish',
        'posts_per_page'=> '-1'
    ) );
    if ( $the_employee_query->have_posts() ) {
        while ( $the_employee_query->have_posts() ) {
            $the_employee_query->the_post();
            $activeEmployees[] = get_the_title();
        }
    }
    /* Restore original Post Data */
    wp_reset_postdata();
?>

<?php $contactsRepeater = get_field('add_contacts_repeater', $current_post_id); ?>

<?php if(!empty( $contactsRepeater )):?>

<section class="content a">
    <h2 class="header-b ip-nav-heading" id="employee_contact_section_anchor">
        <strong>
            <?php _e('Contact', 'html24'); ?>
        </strong>
    </h2>
    <ul class="gallery-a">
        <?php
          foreach($contactsRepeater as $contact):
              $singleContact = $contact['single_contact'];
              $contactName = $singleContact->post_title;
              //Check if we have a contact and if it is in the array of active emploees
              if(!empty($singleContact) && in_array($contactName, $activeEmployees) ):
        ?>

        <li class="vcard" itemscope itemtype="http://schema.org/Person">

            <a class="img" href="<?php echo get_permalink($singleContact->ID); ?>">

                <?php echo get_the_post_thumbnail($singleContact->ID, 'employee-thumb', array('class' => 'photo', 'itemprop' => 'image')); ?>

            </a>

            <span class="fn" itemprop="name">
                <?php echo $singleContact->post_title;?>
            </span>

            <?php
                  $customJobtitles = get_field('jobtitles_repeater', $singleContact->ID);
                  if(!empty($customJobtitles)):
                      foreach($customJobtitles as $customJobtitle) : ?>
            <span itemprop="jobtitle">
                <?php echo $customJobtitle['jobtitle_single']->name; ?>
            </span>,
            <?php endforeach;
                  else :
                      $aJobtitles = wp_get_post_terms( $singleContact->ID, 'jobtitle' );
                      if(!empty($aJobtitles)):
                          foreach($aJobtitles as $jobtitle) :
                              if($jobtitle->parent != 0): ?>
            <span itemprop="jobtitle">
                <?php echo $jobtitle->name; ?>
            </span>,
            <?php endif;
                          endforeach;
                      endif;
                  endif; ?>

            <span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

                <?php
                  $locations = wp_get_post_terms($singleContact->ID, 'country');
                  if(!empty($locations)){
                      foreach($locations as $location) {
                          if($location->parent != 0) {
                              echo '<span class="locality" itemprop="addressLocality">'.$location->name.'</span>';
                          }
                      }
                  }
                ?>

            </span>

            <br />

            <br />

            <?php
                  $directPhone    = get_field('phone_direct', $singleContact->ID);
                  $mobilePhone    = get_field('phone_mobile', $singleContact->ID);
                  $email          = get_field('email_address', $singleContact->ID);
            ?>

            <?php if(!empty($directPhone)):?>
            <?php _e('Direct phone', 'html24'); ?>:
            <span class="tel" itemprop="telephone">
                <?php echo $directPhone; ?>
            </span>
            <br />
            <?php endif; ?>

            <?php if(!empty($mobilePhone)):?>
            <?php _e('Mobile phone', 'html24'); ?>:
            <span class="tel" itemprop="telephone">
                <?php echo $mobilePhone; ?>
            </span>
            <br />
            <?php endif; ?>

            <?php if(!empty($email)):?>
            <a href="mailto:<?php echo $email; ?>" class="email" itemprop="email">
                <?php echo $email; ?>
            </a>
            <?php endif; ?>

        </li>

        <?php
              endif;
          endforeach; ?>
    </ul>
</section>
<?php endif; ?>
<?php
} // end function get_article_contacts
?>