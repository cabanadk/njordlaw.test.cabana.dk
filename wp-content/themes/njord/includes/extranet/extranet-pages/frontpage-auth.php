<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>
<?php include_once('_featured-image.php'); ?>
<section id="content">
    <h1>
        <?php the_title(); ?>
    </h1>
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>
</section>
<?php get_footer();?>