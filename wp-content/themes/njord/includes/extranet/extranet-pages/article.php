<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>
<?php include_once('_featured-image.php'); ?>

<?php $current_post_id = get_the_ID(); ?>
<section id="content" class="cols-a">
    <article>
        <h1>
            <?php the_title(); ?>
        </h1>
        <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
        <?php the_content(); ?>        

        <?php
        $articles = get_posts(array(
                'post_type' => 'extranet',
                'post_parent' => get_the_ID(),
                'suppress_filters' => 0,
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'posts_per_page' => -1
            ));
        if(count($articles)):
        ?>
        <ul>
            <?php foreach ($articles as $article): ?>
            <li>
                <a href="<?php echo get_permalink($article->ID); ?>">
                    <?php echo $article->post_title; ?>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <?php
        $paradigms = get_field('paradigms');
        if($paradigms):
        ?>
        <h5>
            <?php echo apply_filters('wpml_translate_single_string', 'Article Paradigms:', EXTRANET_DOMAIN, 'Article Page - Paradigms'); ?>
        </h5>
        <ul>
            <?php foreach($paradigms as $post): ?>
            <li>
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </li>
            <?php endforeach; wp_reset_postdata(); ?>
        </ul>
        <?php endif; ?>

        <?php include_once('_contact-box.php'); ?>

    </article>      
</section>

<?php
if(count(get_field('add_contacts_repeater', $current_post_id))){
    include '_contacts.php';
    get_article_contacts($current_post_id);
}
?>

<?php get_footer();?>