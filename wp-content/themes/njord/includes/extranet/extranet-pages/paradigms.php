<?php
if(!is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
    exit;
}
$search_term = $_GET['search_term'];
$is_search = isset($search_term) && !empty($search_term);
if($is_search){
    $search_term = filter_input(INPUT_GET, 'search_term', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $search_args = array(
        'post_type' => 'paradigm',
        'suppress_filters' => 0,
        's' => $search_term,
        'posts_per_page' => -1,
        'orderby' => 'relevance'
        );
    $search_items = get_posts($search_args);
}else{
    $terms = get_terms(array('taxonomy' => 'paradigm_category', 'hide_empty' => true));
    $term_args = array(
            'post_type' => 'paradigm',
            'suppress_filters' => 0,
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'title',
            'tax_query' => array(
                array(
                    'taxonomy' => 'paradigm_category',
                    'field'    => 'term_id',
                    'terms'    => null,
                ),
            ),
        );
}
?>
<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>
<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'All Paradigms', EXTRANET_DOMAIN, 'Paradigms Page - Title'); ?>
    </h1>

    <form method="get" action="">
        <label for="search_term">
            <?php echo apply_filters('wpml_translate_single_string', 'Search', EXTRANET_DOMAIN, 'Paradigms Page - Search Label'); ?>
        </label>
        <input type="text" id="search_term" name="search_term" value="<?php echo $search_term; ?>" placeholder="<?php echo apply_filters('wpml_translate_single_string', 'Search paradigms...', EXTRANET_DOMAIN, 'Paradigms Page - Search Placeholder'); ?>" />
        <input type="submit" value="<?php echo apply_filters('wpml_translate_single_string', 'Search', EXTRANET_DOMAIN, 'Paradigms Page - Search Button'); ?>" />
    </form>

    <?php if($is_search): ?>

        <br />
        <?php if(count($search_items)): ?>
        <ul>
            <?php foreach($search_items as $search_item): ?>
            <li>
                <a href="<?php echo get_the_permalink($search_item->ID); ?>"><?php echo $search_item->post_title; ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php else: ?>
        <p>
            <?php echo sprintf(apply_filters('wpml_translate_single_string', 'No results found for term %s.', EXTRANET_DOMAIN, 'Paradigms Page - No Results'), '<b>'.$search_term.'</b>'); ?>
        </p>
        <?php endif; ?>

    <?php else: ?>
        
        <?php if(count($terms)): ?>
        
            <?php foreach($terms as $term): ?>
            <h3>
                <?php echo $term->name; ?>
            </h3>
            <?php
                      $term_args['tax_query'][0]['terms'] = $term->term_id;
                      $term_items = get_posts($term_args);
            ?>

            <ul>
                <?php foreach($term_items as $term_item): ?>
                <li>
                    <a href="<?php echo get_the_permalink($term_item->ID); ?>">
                        <?php echo $term_item->post_title; ?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php endforeach; ?>

        <?php else: ?>
        
            <p><br />No paradigms found.</p>

        <?php endif; ?>        

    <?php endif; ?>

</section>
<?php get_footer(); ?>