<?php
$sidebar_nav_items_skip = array(get_option(EXTRANET_FRONTPAGE_OPTION_NAME), get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME));
$sidebar_nav_items = get_posts(array(
        'post_type' => 'extranet',
        'post_parent' => 0,
        'suppress_filters' => 0,
        'posts_per_page' => -1,
        'post__not_in' => $sidebar_nav_items_skip,
        'order' => 'ASC',
        'orderby' => 'menu_order'
    ));
?>
<nav id="aside" class="inpage-nav nav nav-tabs nav-stacked affix">    
    <form class="extranet-search-form" method="get" action="<?php echo sprintf('%s/%s/search', EXTRANET_ENSURE_LANGUAGE_URL_PREFIX, EXTRANET_REWRITE_URL); ?>">
        <input type="text" name="search" value="" placeholder="<?php echo apply_filters('wpml_translate_single_string', 'Search articles...', EXTRANET_DOMAIN, 'Sidebar - Search placeholder'); ?>" />
        <input type="submit" value="<?php echo apply_filters('wpml_translate_single_string', 'Search', EXTRANET_DOMAIN, 'Sidebar - Search button text'); ?>" />
    </form>
    <ul class="inpage-nav-ul">        
        <?php foreach($sidebar_nav_items as $sidebar_nav_item): ?>
        <li class="<?php echo ($post->ID == $sidebar_nav_item->ID || is_child($sidebar_nav_item->ID)) ? 'active' : ''; ?>">
            <a href="<?php echo get_permalink($sidebar_nav_item->ID); ?>">
                <?php echo $sidebar_nav_item->post_title; ?>
            </a>
        </li>
        <?php endforeach; ?>
        <li>            
            <a href="<?php echo sprintf('%s/%s/paradigm/all', EXTRANET_ENSURE_LANGUAGE_URL_PREFIX, EXTRANET_REWRITE_URL); ?>">
                <?php echo apply_filters('wpml_translate_single_string', 'All Paradigms', EXTRANET_DOMAIN, 'Sidebar - All Paradigms'); ?>
            </a>
        </li>
        <li>
            <a href="<?php echo sprintf('%s/%s/profile', EXTRANET_ENSURE_LANGUAGE_URL_PREFIX, EXTRANET_REWRITE_URL); ?>">
                <?php echo apply_filters('wpml_translate_single_string', 'Profile', EXTRANET_DOMAIN, 'Sidebar - Profile'); ?>
            </a>
        </li>
        <li>
            <a href="<?php echo wp_logout_url(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME))); ?>">
                <?php echo apply_filters('wpml_translate_single_string', 'Logout', EXTRANET_DOMAIN, 'Sidebar - Logout'); ?>
            </a>
        </li>
    </ul>
</nav>