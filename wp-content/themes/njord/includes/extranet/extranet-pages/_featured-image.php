<?php
$newsThumbnail = get_the_post_thumbnail($post->ID, 'full', array('width'=>"1572",'height'=>"500"));
if(!empty($newsThumbnail)):
?>
<figure id="featured">

    <?php echo $newsThumbnail;?>

</figure>
<?php endif; ?>