<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>
<section id="content">
    <h1>
        <?php the_title(); ?>
    </h1>
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>

    <?php if(have_rows('paradigm')): ?>
    <?php
 while(have_rows('paradigm')):
                the_row();
    ?>

    <?php if(get_row_layout() == 'digital_download'): ?>
    <?php
                $file_id = get_sub_field('file_id');
                $file = get_digital_download($file_id);
    ?>
    <?php if(isset($file)): ?>
    <p class="link-a">
        <a href="<?php echo $file->url; ?>">
            <?php echo $file->filename . '.' . $file->extension; ?>
        </a>
    </p>
    <?php endif; ?>
    <?php endif; ?>

    <?php if(get_row_layout() == 'link'): ?>
    <p class="link-a">
        <a href="<?php the_sub_field('link_url'); ?>" target="_blank">
            <?php the_sub_field('link_title'); ?>
        </a>
    </p>
    <?php endif; ?>

    <?php endwhile; ?>


    <?php endif; ?>
</section>
<?php get_footer(); ?>