<?php
if(!is_extranet_user_logged_in()){
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
    exit;
}
$search_term = $_GET['search'];
$is_search = isset($search_term) && !empty($search_term);
if($is_search){
    $search_term = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $search_args = array(
        'post_type' => 'extranet',
        'suppress_filters' => 0,
        'posts_per_page' => -1,
        's' => $search_term,
        'orderby' => 'relevance',
        'post__not_in' => array(
            get_option(EXTRANET_FRONTPAGE_OPTION_NAME),
            get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME)
        ));
    $search_items = get_posts($search_args);
}
?>
<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>

<section id="content">
    <h1>
        <?php echo apply_filters('wpml_translate_single_string', 'Search', EXTRANET_DOMAIN, 'Search Page - Title'); ?>
    </h1>
    <?php if($is_search && count($search_items)): ?>
    <ul>
        <?php foreach($search_items as $search_item): ?>
        <li>
            <a href="<?php echo get_the_permalink($search_item->ID); ?>">
                <?php echo $search_item->post_title; ?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
    <p>No articles found.</p>
    <?php endif; ?>
</section>

<?php get_footer();?>