<?php get_header(); ?>
<?php include_once('_sidebar.php'); ?>
<?php include_once('_featured-image.php'); ?>

<section id="content">
    <h1>
        <?php the_title(); ?>
    </h1>
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>

    <?php
    $articles = get_posts(array(
            'post_type' => 'extranet',
            'post_parent' => get_the_ID(),
            'suppress_filters' => 0,
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'menu_order'
        ));
    if(count($articles)):
    ?>
    <ul>
        <?php foreach ($articles as $article): ?>
        <li>
            <a href="<?php echo get_permalink($article->ID); ?>">
                <?php echo $article->post_title; ?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
    <!--<p>
        <?php echo apply_filters('wpml_translate_single_string', 'This section has no articles.', EXTRANET_DOMAIN, 'Section Page - No Articles'); ?>
    </p>-->
    <?php endif; ?>
</section>
<?php get_footer();?>