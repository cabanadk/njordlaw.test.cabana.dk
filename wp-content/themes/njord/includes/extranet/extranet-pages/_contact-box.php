<div style="padding:20px 20px 0 20px;border:1px solid #999;margin-bottom:20px;">
    <?php
    $mailto_address = get_option(EXTRANET_CONTACT_EMAIL);
    $mailto_subject = get_option(EXTRANET_CONTACT_BOX_MAILTO_SUBJECT);
    $mailto_body = str_replace('{{page_url}}', get_the_permalink(), get_option(EXTRANET_CONTACT_BOX_MAILTO_BODY));
    $mailto = sprintf('mailto:%s?subject=%s&body=%s', $mailto_address, $mailto_subject, $mailto_body);
    ?>
    <p>
        <a href="<?php echo $mailto; ?>">
            <?php echo apply_filters('wpml_translate_single_string', 'Contact Us', EXTRANET_DOMAIN, 'Article Page - Contact Box'); ?>
        </a>
    </p>
</div>