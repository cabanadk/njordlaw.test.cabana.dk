<?php

wp_enqueue_media();

$extranet_top_level_pages = get_posts(array('post_type' => 'extranet', 'post_parent' => 0, 'suppress_filters' => 0, 'posts_per_page' => -1));

$extranet_frontpage_input_name = 'extranet_frontpage';
$extranet_frontpage_auth_input_name = 'extranet_frontpage_auth';
$extranet_mailchimp_list_id_input_name = 'extranet_mailchimp_list_id';
$extranet_admin_email_input_name = 'extranet_admin_email';
$extranet_contact_email_input_name = 'extranet_contact_email';
$extranet_terms_and_conditions_input_name = 'extranet_terms_and_conditions';
$extranet_registration_message_subject_input_name = 'extranet_registration_message_subject';
$extranet_registration_message_input_name = 'extranet_registration_message';
$extranet_welcome_message_subject_input_name = 'extranet_welcome_message_subject';
$extranet_welcome_message_input_name = 'extranet_welcome_message';
$extranet_registration_page_featured_image_id_input_name = 'registration_page_featured_image_id';
$extranet_registration_page_text_input_name = 'registration_page_text';
$extranet_login_page_featured_image_id_input_name = 'login_page_featured_image_id';
$extranet_login_page_text_input_name = 'login_page_text';
$extranet_profile_page_featured_image_id_input_name = 'profile_page_featured_image_id';
$extranet_profile_page_text_input_name = 'login_profile_text';
$extranet_forgot_password_page_text_input_name = 'forgot_password_page_text';
$extranet_forgot_password_email_subject_input_name = 'forgot_password_email_subject';
$extranet_forgot_password_email_body_input_name = 'forgot_password_email_body';
$extranet_contact_box_mailto_subject_input_name = 'contact_box_mailto_subject';
$extranet_contact_box_mailto_body_input_name = 'contact_box_mailto_body';

if(isset($_POST['save_extranet_settings'])) {

    ini_set('default_charset', 'utf-8');

    $extranet_frontpage_input_value = filter_input(INPUT_POST, $extranet_frontpage_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_FRONTPAGE_OPTION_NAME, $extranet_frontpage_input_value);

    $extranet_frontpage_auth_input_value = filter_input(INPUT_POST, $extranet_frontpage_auth_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME, $extranet_frontpage_auth_input_value);

    $extranet_mailchimp_list_id_input_value = filter_input(INPUT_POST, $extranet_mailchimp_list_id_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(MAILCHIMP_LIST_ID_OPTION_NAME, $extranet_mailchimp_list_id_input_value);

    $extranet_admin_email_input_value = filter_input(INPUT_POST, $extranet_admin_email_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_ADMIN_EMAIL, $extranet_admin_email_input_value);

    $extranet_contact_email_input_value = filter_input(INPUT_POST, $extranet_contact_email_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_CONTACT_EMAIL, $extranet_contact_email_input_value);

    $extranet_terms_and_conditions_input_value = filter_input(INPUT_POST, $extranet_terms_and_conditions_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_TERMS_AND_CONDITIONS_URL, $extranet_terms_and_conditions_input_value);

    $extranet_registration_message_subject_input_value = filter_input(INPUT_POST, $extranet_registration_message_subject_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_REGISTRATION_MESSAGE_SUBJECT, $extranet_registration_message_subject_input_value);

    $extranet_registration_message_input_value = filter_input(INPUT_POST, $extranet_registration_message_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_REGISTRATION_MESSAGE, $extranet_registration_message_input_value);

    $extranet_welcome_message_subject_input_value = filter_input(INPUT_POST, $extranet_welcome_message_subject_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_WELCOME_MESSAGE_SUBJECT, $extranet_welcome_message_subject_input_value);

    $extranet_welcome_message_input_value = filter_input(INPUT_POST, $extranet_welcome_message_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_WELCOME_MESSAGE, $extranet_welcome_message_input_value);

    $extranet_registration_page_featured_image_id_input_value = filter_input(INPUT_POST, $extranet_registration_page_featured_image_id_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_REGISTRATION_PAGE_FEATURED_IMAGE_ID, $extranet_registration_page_featured_image_id_input_value);

    $extranet_registration_page_text_input_value = filter_input(INPUT_POST, $extranet_registration_page_text_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_REGISTRATION_PAGE_TEXT, $extranet_registration_page_text_input_value);

    $extranet_login_page_featured_image_id_input_value = filter_input(INPUT_POST, $extranet_login_page_featured_image_id_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_LOGIN_PAGE_FEATURED_IMAGE_ID, $extranet_login_page_featured_image_id_input_value);

    $extranet_login_page_text_input_value = filter_input(INPUT_POST, $extranet_login_page_text_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_LOGIN_PAGE_TEXT, $extranet_login_page_text_input_value);

    $extranet_profile_page_featured_image_id_input_value = filter_input(INPUT_POST, $extranet_profile_page_featured_image_id_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_PROFILE_PAGE_FEATURED_IMAGE_ID, $extranet_profile_page_featured_image_id_input_value);

    $extranet_profile_page_text_input_value = filter_input(INPUT_POST, $extranet_profile_page_text_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_PROFILE_PAGE_TEXT, $extranet_profile_page_text_input_value);

    $extranet_forgot_password_page_text_input_value = filter_input(INPUT_POST, $extranet_forgot_password_page_text_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_FORGOT_PASSWORD_PAGE_TEXT, $extranet_forgot_password_page_text_input_value);

    $extranet_forgot_password_email_subject_input_value = filter_input(INPUT_POST, $extranet_forgot_password_email_subject_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);        
    update_option(EXTRANET_FORGOT_PASSWORD_EMAIL_SUBJECT, $extranet_forgot_password_email_subject_input_value);

    $extranet_forgot_password_email_body_input_value = filter_input(INPUT_POST, $extranet_forgot_password_email_body_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_FORGOT_PASSWORD_EMAIL_BODY, $extranet_forgot_password_email_body_input_value);

    $extranet_contact_box_mailto_subject_input_value = filter_input(INPUT_POST, $extranet_contact_box_mailto_subject_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_CONTACT_BOX_MAILTO_SUBJECT, $extranet_contact_box_mailto_subject_input_value);

    $extranet_contact_box_mailto_body_input_value = filter_input(INPUT_POST, $extranet_contact_box_mailto_body_input_name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);     
    update_option(EXTRANET_CONTACT_BOX_MAILTO_BODY, $extranet_contact_box_mailto_body_input_value);

}
?>
<div class="wrap">
    <h1>Extranet Settings</h1>
    <form method="post" action="" name="extranet_settings" id="extranet_settings">

        <h2>Miscellaneous</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_frontpage_input_name; ?>">Extranet Welcome page</label>
                    </th>
                    <td>
                        <select name="<?php echo $extranet_frontpage_input_name; ?>" id="<?php echo $extranet_frontpage_input_name; ?>">
                            <?php foreach ($extranet_top_level_pages as $extranet_top_level_page): ?>
                            <?php if(get_option(EXTRANET_FRONTPAGE_OPTION_NAME) == $extranet_top_level_page->ID): ?>
                            <option selected value="<?php echo $extranet_top_level_page->ID; ?>"><?php echo $extranet_top_level_page->post_title; ?></option>
                            <?php else: ?>
                            <option value="<?php echo $extranet_top_level_page->ID; ?>"><?php echo $extranet_top_level_page->post_title; ?></option>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                        <p class="description" id="tagline-description">Frontpage for anonymous users.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_frontpage_auth_input_name; ?>">Extranet Frontpage</label>
                    </th>
                    <td>
                        <select name="<?php echo $extranet_frontpage_auth_input_name; ?>" id="<?php echo $extranet_frontpage_auth_input_name; ?>">
                            <?php foreach ($extranet_top_level_pages as $extranet_top_level_page): ?>
                            <?php if(get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME) == $extranet_top_level_page->ID): ?>
                            <option selected value="<?php echo $extranet_top_level_page->ID; ?>"><?php echo $extranet_top_level_page->post_title; ?></option>
                            <?php else: ?>
                            <option value="<?php echo $extranet_top_level_page->ID; ?>"><?php echo $extranet_top_level_page->post_title; ?></option>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                        <p class="description" id="tagline-description">Frontpage for logged-in users.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_mailchimp_list_id_input_name; ?>">Mailchimp List ID</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_mailchimp_list_id_input_name; ?>" id="<?php echo $extranet_mailchimp_list_id_input_name; ?>" value="<?php echo get_option(MAILCHIMP_LIST_ID_OPTION_NAME); ?>" />
                        <p class="description" id="tagline-description">All new Extranet Members will be subscribed to this list.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_admin_email_input_name; ?>">Admin email address</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_admin_email_input_name; ?>" id="<?php echo $extranet_admin_email_input_name; ?>" value="<?php echo get_option(EXTRANET_ADMIN_EMAIL); ?>" />
                        <p class="description" id="tagline-description">Will be used for notifications.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_contact_email_input_name; ?>">Contact email address</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_contact_email_input_name; ?>" id="<?php echo $extranet_contact_email_input_name; ?>" value="<?php echo get_option(EXTRANET_CONTACT_EMAIL); ?>" />
                        <p class="description" id="tagline-description">Will be used for contacting.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_terms_and_conditions_input_name; ?>">Terms and Conditions</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_terms_and_conditions_input_name; ?>" id="<?php echo $extranet_terms_and_conditions_input_name; ?>" value="<?php echo get_option(EXTRANET_TERMS_AND_CONDITIONS_URL); ?>" />
                        <p class="description" id="tagline-description">Terms and conditions page URL</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Registration Email</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_registration_message_subject_input_name; ?>">Registration message subject</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_registration_message_subject_input_name; ?>" id="<?php echo $extranet_registration_message_subject_input_name; ?>" value="<?php echo get_option(EXTRANET_REGISTRATION_MESSAGE_SUBJECT); ?>" />
                        <p class="description" id="tagline-description">Will be send to a newly registered user. Tags available: {{firstname}}, {{lastname}}</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_registration_message_input_name; ?>">Registration message body</label>
                    </th>
                    <td>
                        <textarea class="large-text" rows="15" name="<?php echo $extranet_registration_message_input_name; ?>" id="<?php echo $extranet_registration_message_input_name; ?>"><?php echo get_option(EXTRANET_REGISTRATION_MESSAGE); ?></textarea>
                        <p class="description" id="tagline-description">Will be send to a newly registered user. Tags available: {{activation_link_url}}, {{firstname}}, {{lastname}}, {{email}}</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Welcome Email</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_welcome_message_subject_input_name; ?>">Welcome message subject</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_welcome_message_subject_input_name; ?>" id="<?php echo $extranet_welcome_message_subject_input_name; ?>" value="<?php echo get_option(EXTRANET_WELCOME_MESSAGE_SUBJECT); ?>" />
                        <p class="description" id="tagline-description">Will be send to a newly registered user. Tags available: {{firstname}}, {{lastname}}</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_welcome_message_input_name; ?>">Welcome message body</label>
                    </th>
                    <td>
                        <textarea class="large-text" rows="15" name="<?php echo $extranet_welcome_message_input_name; ?>" id="<?php echo $extranet_welcome_message_input_name; ?>"><?php echo get_option(EXTRANET_WELCOME_MESSAGE); ?></textarea>
                        <p class="description" id="tagline-description">Will be send to a newly registered user. Tags available: {{firstname}}, {{lastname}}, {{email}}</p>
                    </td>
                </tr>
            </tbody>
        </table>        

        <h2>Registration Page</h2>
        <table class="form-table">
            <tbody>                
                <tr>
                    <th scope="row">
                        <label>Featured image</label>
                    </th>
                    <td>
                        <div id="meta-box-registration-featured-image">
                            <?php     
                            // codex.wordpress.org/Javascript_Reference/wp.media
                            $upload_link = esc_url(get_upload_iframe_src('image'));                           
                            $your_img_id = get_option(EXTRANET_REGISTRATION_PAGE_FEATURED_IMAGE_ID);                            
                            $your_img_src = wp_get_attachment_image_src($your_img_id);                            
                            $you_have_img = is_array($your_img_src);
                            ?>                            
                            <div class="custom-img-container">
                                <?php if($you_have_img): ?>
                                    <img src="<?php echo $your_img_src[0] ?>" alt="" style="max-width:100%;" />
                                <?php endif; ?>
                            </div>                            
                            <p class="hide-if-no-js">
                                <a class="upload-custom-img <?php if ($you_have_img) { echo 'hidden'; } ?>" href="<?php echo $upload_link ?>">
                                    <?php _e('Set custom image') ?>
                                </a>
                                <a class="delete-custom-img <?php if (!$you_have_img) { echo 'hidden'; } ?>" href="#">
                                    <?php _e('Remove this image') ?>
                                </a>
                            </p>                            
                            <input class="custom-img-id" name="<?php echo $extranet_registration_page_featured_image_id_input_name; ?>" type="hidden" value="<?php echo esc_attr($your_img_id); ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_registration_page_text_input_name; ?>">Text</label>
                    </th>
                    <td>                        
                        <?php
                        wp_editor(
                            html_entity_decode(get_option(EXTRANET_REGISTRATION_PAGE_TEXT)),
                            $extranet_registration_page_text_input_name,
                            array(
                                'media_buttons' => false,
                                'quicktags' => array('buttons' => 'strong,em,ul,ol,li,link,img'),
                                'textarea_rows' => 5
                                )
                            );
                        ?>
                        <p class="description" id="tagline-description">Registration page text before the form.</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Login Page</h2>
        <table class="form-table">
            <tbody>                
                <tr>
                    <th scope="row">
                        <label>Featured image</label>
                    </th>
                    <td>
                        <div id="meta-box-login-featured-image">
                            <?php     
                            // codex.wordpress.org/Javascript_Reference/wp.media
                            $upload_link = esc_url(get_upload_iframe_src('image'));                           
                            $your_img_id = get_option(EXTRANET_LOGIN_PAGE_FEATURED_IMAGE_ID);                            
                            $your_img_src = wp_get_attachment_image_src($your_img_id);                            
                            $you_have_img = is_array($your_img_src);
                            ?>                            
                            <div class="custom-img-container">
                                <?php if($you_have_img): ?>
                                    <img src="<?php echo $your_img_src[0] ?>" alt="" style="max-width:100%;" />
                                <?php endif; ?>
                            </div>                            
                            <p class="hide-if-no-js">
                                <a class="upload-custom-img <?php if ($you_have_img) { echo 'hidden'; } ?>" href="<?php echo $upload_link ?>">
                                    <?php _e('Set custom image') ?>
                                </a>
                                <a class="delete-custom-img <?php if (!$you_have_img) { echo 'hidden'; } ?>" href="#">
                                    <?php _e('Remove this image') ?>
                                </a>
                            </p>                            
                            <input class="custom-img-id" name="<?php echo $extranet_login_page_featured_image_id_input_name; ?>" type="hidden" value="<?php echo esc_attr($your_img_id); ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_login_page_text_input_name; ?>">Text</label>
                    </th>
                    <td>                        
                        <?php
                        wp_editor(
                            html_entity_decode(get_option(EXTRANET_LOGIN_PAGE_TEXT)),
                            $extranet_login_page_text_input_name,
                            array(
                                'media_buttons' => false,
                                'quicktags' => array('buttons' => 'strong,em,ul,ol,li,link,img'),
                                'textarea_rows' => 5
                                )
                            );
                        ?>
                        <p class="description" id="tagline-description">Login page text before the form.</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Profile Page</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label>Featured image</label>
                    </th>
                    <td>
                        <div id="meta-box-profile-featured-image">
                            <?php     
                            // codex.wordpress.org/Javascript_Reference/wp.media
                            $upload_link = esc_url(get_upload_iframe_src('image'));                           
                            $your_img_id = get_option(EXTRANET_PROFILE_PAGE_FEATURED_IMAGE_ID);                            
                            $your_img_src = wp_get_attachment_image_src($your_img_id);                            
                            $you_have_img = is_array($your_img_src);
                            ?>                            
                            <div class="custom-img-container">
                                <?php if($you_have_img): ?>
                                    <img src="<?php echo $your_img_src[0] ?>" alt="" style="max-width:100%;" />
                                <?php endif; ?>
                            </div>                            
                            <p class="hide-if-no-js">
                                <a class="upload-custom-img <?php if ($you_have_img) { echo 'hidden'; } ?>" href="<?php echo $upload_link ?>">
                                    <?php _e('Set custom image') ?>
                                </a>
                                <a class="delete-custom-img <?php if (!$you_have_img) { echo 'hidden'; } ?>" href="#">
                                    <?php _e('Remove this image') ?>
                                </a>
                            </p>                            
                            <input class="custom-img-id" name="<?php echo $extranet_profile_page_featured_image_id_input_name; ?>" type="hidden" value="<?php echo esc_attr($your_img_id); ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_profile_page_text_input_name; ?>">Text</label>
                    </th>
                    <td>                        
                        <?php
                        wp_editor(
                            html_entity_decode(get_option(EXTRANET_PROFILE_PAGE_TEXT)),
                            $extranet_profile_page_text_input_name,
                            array(
                                'media_buttons' => false,
                                'quicktags' => array('buttons' => 'strong,em,ul,ol,li,link,img'),
                                'textarea_rows' => 5
                                )
                            );
                        ?>
                        <p class="description" id="tagline-description">User profile page text before the form.</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Forgot Password Page</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_forgot_password_page_text_input_name; ?>">Text</label>
                    </th>
                    <td>                        
                        <?php
                        wp_editor(
                            html_entity_decode(get_option(EXTRANET_FORGOT_PASSWORD_PAGE_TEXT)),
                            $extranet_forgot_password_page_text_input_name,
                            array(
                                'media_buttons' => false,
                                'quicktags' => array('buttons' => 'strong,em,ul,ol,li,link,img'),
                                'textarea_rows' => 5
                                )
                            );
                        ?>
                        <p class="description" id="tagline-description">Forgot Password page text before the form.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_forgot_password_email_subject_input_name; ?>">Forgot password email subject</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_forgot_password_email_subject_input_name; ?>" id="<?php echo $extranet_forgot_password_email_subject_input_name; ?>" value="<?php echo get_option(EXTRANET_FORGOT_PASSWORD_EMAIL_SUBJECT); ?>" />                        
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_forgot_password_email_body_input_name; ?>">Forgot password email body</label>
                    </th>
                    <td>
                        <textarea class="large-text" rows="5" name="<?php echo $extranet_forgot_password_email_body_input_name; ?>" id="<?php echo $extranet_forgot_password_email_body_input_name; ?>"><?php echo get_option(EXTRANET_FORGOT_PASSWORD_EMAIL_BODY); ?></textarea>
                        <p class="description" id="tagline-description">Password reset request email. Tags available: {{password}}, {{firstname}}, {{lastname}}</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <h2>Contact Box</h2>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_contact_box_mailto_subject_input_name; ?>">Contact Box Mailto Subject</label>
                    </th>
                    <td>
                        <input type="text" class="regular-text" name="<?php echo $extranet_contact_box_mailto_subject_input_name; ?>" id="<?php echo $extranet_contact_box_mailto_subject_input_name; ?>" value="<?php echo get_option(EXTRANET_CONTACT_BOX_MAILTO_SUBJECT); ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $extranet_contact_box_mailto_body_input_name; ?>">Contact Box Mailto Body</label>
                    </th>
                    <td>
                        <textarea class="large-text" rows="3" name="<?php echo $extranet_contact_box_mailto_body_input_name; ?>" id="<?php echo $extranet_contact_box_mailto_body_input_name; ?>"><?php echo get_option(EXTRANET_CONTACT_BOX_MAILTO_BODY); ?></textarea>
                        <p class="description" id="tagline-description">Tags available: {{page_url}}</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <p class="submit">
            <input type="submit" name="save_extranet_settings" id="save_extranet_settings" class="button button-primary" value="Save Changes">
        </p>
    </form>
</div>
<script>
    jQuery(function ($) {
        
        var metaBoxes = [
            $('#meta-box-registration-featured-image'),
            $('#meta-box-login-featured-image'),
            $('#meta-box-profile-featured-image')
        ];

        $.each(metaBoxes, function (index, element) {
            var $this = $(element);

            // Set all variables to be used in scope
            var frame,
                metaBox = $this,
                addImgLink = metaBox.find('.upload-custom-img'),
                delImgLink = metaBox.find('.delete-custom-img'),
                imgContainer = metaBox.find('.custom-img-container'),
                imgIdInput = metaBox.find('.custom-img-id');

            // ADD IMAGE LINK
            addImgLink.on('click', function (event) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if (frame) {
                    frame.open();
                    return;
                }

                // Create a new media frame
                frame = wp.media({
                    title: 'Select or Upload Media Of Your Chosen Persuasion',
                    button: {
                        text: 'Use this media'
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                });


                // When an image is selected in the media frame...
                frame.on('select', function () {

                    // Get media attachment details from the frame state
                    var attachment = frame.state().get('selection').first().toJSON();

                    // Send the attachment URL to our custom image input field.
                    imgContainer.append('<img src="' + attachment.sizes.thumbnail.url + '"/>');

                    // Send the attachment id to our hidden input
                    imgIdInput.val(attachment.id);

                    // Hide the add image link
                    addImgLink.addClass('hidden');

                    // Unhide the remove image link
                    delImgLink.removeClass('hidden');
                });

                // Finally, open the modal on click
                frame.open();
            });


            // DELETE IMAGE LINK
            delImgLink.on('click', function (event) {

                event.preventDefault();

                // Clear out the preview image
                imgContainer.html('');

                // Un-hide the add image link
                addImgLink.removeClass('hidden');

                // Hide the delete image link
                delImgLink.addClass('hidden');

                // Delete the image id from the hidden input
                imgIdInput.val('');

            });
        });       

    });
</script>