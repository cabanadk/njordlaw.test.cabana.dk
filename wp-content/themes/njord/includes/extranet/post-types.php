<?php
/**
 * Defining custom post type: hr-e-bogen
 */
add_action('init', function(){
    /* Setting the names displayed to the backend user */
    $labels = array(
        'name'               => 'HR e-Book',
        'singular_name'      => 'HR e-Book',
        'add_new'            => 'Add new HR e-Book page',
        'add_new_item'       => 'Add new HR e-Book page',
        'edit_item'          => 'Edit HR e-Book page',
        'new_item'           => 'New HR e-Book page',
        'all_items'          => 'All HR e-Book pages',
        'view_item'          => 'Show HR e-Book page',
        'search_items'       => 'Search HR e-Book pages',
        'not_found'          => 'No HR e-Book pages found',
        'not_found_in_trash' => 'No HR e-Book pages found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'HR e-Book'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'exclude_from_search'=> true,
        'publicly_queryable' => false,
        'show_in_nav_menus' => true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => EXTRANET_REWRITE_URL, 'with_front' => true ),
        'capability_type'    => 'page',
        'has_archive'        => false,
        'hierarchical'       => true,
        'menu_position'      => 0,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'page-attributes')
    );

    register_post_type(EXTRANET_DOMAIN, $args);


    $labels = array(
        'name'               => 'HR e-Tools',
        'singular_name'      => 'HR e-Tool',
        'add_new'            => 'Add new HR e-Tool',
        'add_new_item'       => 'Add new HR e-Tool',
        'edit_item'          => 'Edit HR e-Tool',
        'new_item'           => 'New HR e-Tool',
        'all_items'          => 'All HR e-Tools',
        'view_item'          => 'Show HR e-Tool',
        'search_items'       => 'Search HR e-Tools',
        'not_found'          => 'No HR e-Tools found',
        'not_found_in_trash' => 'No HR e-Tools found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'HR e-Tools'
    );

    /* Setting the options for post type */
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'exclude_from_search'=> true,
        'publicly_queryable' => false,
        'show_in_nav_menus' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => EXTRANET_REWRITE_URL . '/paradigm', 'with_front' => true),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'supports'           => array('title', 'editor', 'author', 'custom-fields')
    );

    register_post_type('paradigm', $args);
});