<?php
/*
 * Extranet Settings
 * */
define('EXTRANET_DOMAIN', 'extranet');
define('EXTRANET_ROOT_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('EXTRANET_REWRITE_TAG_ROOT', 'extranet_root');
define('EXTRANET_REWRITE_URL', 'hr-e-bogen');
define('EXTRANET_FRONTPAGE_OPTION_NAME', 'extranet_frontpage_' . ICL_LANGUAGE_CODE);
define('EXTRANET_FRONTPAGE_AUTH_OPTION_NAME', 'extranet_frontpage_auth_' . ICL_LANGUAGE_CODE);
define('EXTRANET_ADMIN_EMAIL', 'extranet_admin_email_' . ICL_LANGUAGE_CODE);
define('EXTRANET_CONTACT_EMAIL', 'extranet_contact_email_' . ICL_LANGUAGE_CODE);
define('EXTRANET_TERMS_AND_CONDITIONS_URL', 'extranet_terms_and_conditions_url_' . ICL_LANGUAGE_CODE);
define('EXTRANET_REGISTRATION_MESSAGE_SUBJECT', 'extranet_registration_message_subject_' . ICL_LANGUAGE_CODE);
define('EXTRANET_REGISTRATION_MESSAGE', 'extranet_registration_message_' . ICL_LANGUAGE_CODE);
define('EXTRANET_WELCOME_MESSAGE_SUBJECT', 'extranet_welcome_message_subject_' . ICL_LANGUAGE_CODE);
define('EXTRANET_WELCOME_MESSAGE', 'extranet_welcome_message_' . ICL_LANGUAGE_CODE);
define('EXTRANET_ENSURE_LANGUAGE_URL_PREFIX', ICL_LANGUAGE_CODE == 'en' ? '' : '/' . ICL_LANGUAGE_CODE);
define('EXTRANET_REGISTRATION_PAGE_FEATURED_IMAGE_ID', 'extranet_registration_page_featured_image_id_' . ICL_LANGUAGE_CODE);
define('EXTRANET_REGISTRATION_PAGE_TEXT', 'extranet_registration_page_text_' . ICL_LANGUAGE_CODE);
define('EXTRANET_LOGIN_PAGE_FEATURED_IMAGE_ID', 'extranet_login_page_featured_image_id_' . ICL_LANGUAGE_CODE);
define('EXTRANET_LOGIN_PAGE_TEXT', 'extranet_login_page_text_' . ICL_LANGUAGE_CODE);
define('EXTRANET_PROFILE_PAGE_FEATURED_IMAGE_ID', 'extranet_profile_page_featured_image_id_' . ICL_LANGUAGE_CODE);
define('EXTRANET_PROFILE_PAGE_TEXT', 'extranet_profile_page_text_' . ICL_LANGUAGE_CODE);
define('EXTRANET_FORGOT_PASSWORD_PAGE_TEXT', 'extranet_forgot_password_page_text_' . ICL_LANGUAGE_CODE);
define('EXTRANET_FORGOT_PASSWORD_EMAIL_SUBJECT', 'extranet_forgot_password_email_subject_' . ICL_LANGUAGE_CODE);
define('EXTRANET_FORGOT_PASSWORD_EMAIL_BODY', 'extranet_forgot_password_email_body_' . ICL_LANGUAGE_CODE);
define('EXTRANET_CONTACT_BOX_MAILTO_SUBJECT', 'extranet_contact_box_mailto_subject_' . ICL_LANGUAGE_CODE);
define('EXTRANET_CONTACT_BOX_MAILTO_BODY', 'extranet_contact_box_mailto_boy_' . ICL_LANGUAGE_CODE);


/*
 * Extranet User
 * */
define('EXTRANET_USER_ROLE_NAME', 'extranet_member');
define('EXTRANET_USER_ROLE_DISPLAY_NAME', 'Extranet Member');
define('EXTRANET_FIELD_COMPANY', 'extranet_company');
define('EXTRANET_FIELD_EMAIL_VERIFIED', 'extranet_email_verified');
define('EXTRANET_FIELD_MEMBERSHIP_ACTIVE', 'extranet_membership_active');
define('EXTRANET_FIELD_MEMBERSHIP_EXPIRES', 'extranet_membership_expires');
define('EXTRANET_FIELD_MEMBERSHIP_EXPIRATION_NOTIFICATION_SENT', 'extranet_membership_expiration_notification_sent');
define('EXTRANET_FIELD_ACTIVATION_KEY', 'extranet_activation_key');
define('EXTRANET_FIELD_PHONE', 'extranet_phone');


/*
 * Mailchimp / Mandrill
 * */
define('MAILCHIMP_API_KEY', 'eaa50562db0c3864f400aeea65735e72-us3');
define('MAILCHIMP_API_ROOT', 'https://us3.api.mailchimp.com/3.0');
define('MAILCHIMP_LIST_ID_OPTION_NAME', 'extranet_mailchimp_list_id_' . ICL_LANGUAGE_CODE);


/*
 * Extranet translation strings, WPML
 * */

// registration page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Title', 'Extranet Registration');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - First Name', 'First Name');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Last Name', 'Last Name');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Company', 'Company');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Email', 'Email');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Phone', 'Phone');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Passwod', 'Password');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Passwod Repeat', 'Repeat Password');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Label - Register', 'Register');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Notice - Activation email sent', 'Thank you. An email has been sent to %s for further instructions.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Notice - Activation email resent', 'Activation email has been resent to %s.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Generic', 'Required form field is missing');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Email', 'Email address is not valid');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Email Exist', 'User with this email already exists');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Phone', 'Phone number is not valid');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Phone Exist', 'User with this phone number already exists');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Password Match', 'Passwords must match');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Registration Page - Validation - Terms and Conditions', 'You must accept our Terms and Conditions');


// login page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Title', 'Extranet Login');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Label - Login', 'Login');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Label - Forgot Password', 'Forgot Password?');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Error - Email not verified', 'Email not verified.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Error - Membership inactive', 'Your membership is inactive.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Login Page - Error - Membership expired', 'Your membership has expired.');

// forgot password page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Forgot Password Page - Title', 'Forgot password');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Forgot Password Page - Password Sent', 'Password has been sent to your email.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Forgot Password Page - User does not exist', 'User does not exist.');

// activation page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Activation Page - Title', 'Extranet Member Activation');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Activation Page - Email Verified', 'Your email is verified. You may now login.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Activation Page - User does not exist', 'User does not exist.');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Activation Page - Activation key does not exist', 'Activation key does not exist.');

// all paradigms page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Paradigms Page - Title', 'All Paradigms');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Paradigms Page - Search Label', 'Search');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Paradigms Page - Search Button', 'Search');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Paradigms Page - Search Placeholder', 'Search paradigms...');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Paradigms Page - No Results', 'No results found for term %s.');

// section page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Section Page - No Articles', 'This section has no articles.');

//article page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Article Page - Paradigms', 'Article Paradigms:');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Article Page - Contact Box', 'Contact Us');

// search page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Search Page - Title', 'Search');

// sidebar
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Sidebar - Search placeholder', 'Search articles...');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Sidebar - Search button text', 'Search');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Sidebar - All Paradigms', 'All Paradigms');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Sidebar - Profile', 'Profile');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Sidebar - Logout', 'Logout');

// profile page
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Title', 'Profile');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Label - Subscription', 'Subscription Until');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Label - New Password', 'New Password');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Label - New Password Repeat', 'New Password Repeat');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Label - Save', 'Save');
do_action('wpml_register_single_string', EXTRANET_DOMAIN, 'Profile Page - Info - Saved', 'Thank you. Your profile info has been saved.');