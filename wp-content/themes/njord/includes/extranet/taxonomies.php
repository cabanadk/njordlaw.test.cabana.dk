<?php
add_action('init', function(){

    $labels = array(
        'name'                       => _x( 'HR e-Tools Categories', 'taxonomy general name' ),
        'singular_name'              => _x( 'HR e-Tools Category', 'taxonomy singular name' ),
        'search_items'               => __( 'Search HR e-Tools Categories' ),
        'popular_items'              => __( 'Popular HR e-Tools Categories' ),
        'all_items'                  => __( 'All HR e-Tools Categories' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit HR e-Tools Category' ),
        'update_item'                => __( 'Update HR e-Tools Category' ),
        'add_new_item'               => __( 'Add New HR e-Tools Category' ),
        'new_item_name'              => __( 'New HR e-Tools Category Name' ),
        'separate_items_with_commas' => __( 'Separate HR e-Tools Categories with commas' ),
        'add_or_remove_items'        => __( 'Add or remove HR e-Tools Categories' ),
        'choose_from_most_used'      => __( 'Choose from the most used HR e-Tools Categories' ),
        'not_found'                  => __( 'No HR e-Tools Categories found.' ),
        'menu_name'                  => __( 'HR e-Tools Categories' ),
    );

    $args = array(
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'paradigm-category' ),
    );

    register_taxonomy( 'paradigm_category', array( 'paradigm' ), $args );

});