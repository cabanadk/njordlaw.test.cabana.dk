<?php
/*
 * Extranet Root
 * */
$query_var = $wp->query_vars[EXTRANET_REWRITE_TAG_ROOT];
$_query_args = null;

if(strpos($query_var, 'paradigm/') === 0){
    $_query_args = array('post_type' => 'paradigm', 'name' => $query_var);
}else{
    $_query_args = array('post_type' => EXTRANET_DOMAIN, 'pagename' => $query_var);
}

$the_query = new WP_Query($_query_args);

if(isset($the_query)){
    if($the_query->have_posts()){
        $the_query->the_post();
        global $post;
        $extranet_root_page_id = get_option(EXTRANET_FRONTPAGE_OPTION_NAME);
        $extranet_root_auth_page_id = get_option(EXTRANET_FRONTPAGE_AUTH_OPTION_NAME);

        if($extranet_root_page_id != get_the_ID() && !is_extranet_user_logged_in()){
            wp_redirect(get_permalink($extranet_root_page_id));
            exit;
        }

        if($extranet_root_page_id == get_the_ID() && is_extranet_user_logged_in()){
            wp_redirect(get_permalink($extranet_root_auth_page_id));
            exit;
        }

        if($extranet_root_page_id == get_the_ID()){
            include_once('extranet-pages/frontpage.php');

        }elseif($extranet_root_auth_page_id == get_the_ID()){
            include_once('extranet-pages/frontpage-auth.php');

        }elseif(strpos($query_var, 'paradigm/') === 0){
            include_once('extranet-pages/paradigm.php');

        }elseif($post->post_parent == 0){
            include_once('extranet-pages/section.php');

        }else{
            include_once('extranet-pages/article.php');
        }

        wp_reset_postdata();
        $the_query = null;
    }else{
        wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
        exit;
    }
}else{
    wp_redirect(get_permalink(get_option(EXTRANET_FRONTPAGE_OPTION_NAME)));
    exit;
}