<?php
if(function_exists("register_field_group"))
{
    //START NEWS PAGE OPTIONS DA
    register_field_group(array (
        'id' => 'acf_page-links-da',
        'title' => 'News page options da',
        'fields' => array (
            array (
                'key' => 'field_549da1f9cff2b',
                'label' => 'News list page',
                'name' => 'da_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549lt1fdcfa2b',
                'label' => 'Knowledge list page',
                'name' => 'da_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54914f7ab1cb8',
                'label' => 'Event list page',
                'name' => 'da_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54899c68dd6d2',
                'label' => 'Employee list page',
                'name' => 'da_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-da',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS DA


    //START NEWS PAGE OPTIONS EN
    register_field_group(array (
        'id' => 'acf_page-links-en',
        'title' => 'News page options en',
        'fields' => array (
            array (
                'key' => 'field_5491e3f9cfn8b',
                'label' => 'News list page',
                'name' => 'en_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549st1vdcea2n',
                'label' => 'Knowledge list page',
                'name' => 'en_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54919daebncb8',
                'label' => 'Event list page',
                'name' => 'en_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54891c68da6dg',
                'label' => 'Employee list page',
                'name' => 'en_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-en',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS EN


    //START NEWS PAGE OPTIONS DE
    register_field_group(array (
        'id' => 'acf_page-links-de',
        'title' => 'News page options de',
        'fields' => array (
            array (
                'key' => 'field_549d42f8cef7a',
                'label' => 'News list page',
                'name' => 'de_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5492t1gdcew28',
                'label' => 'Knowledge list page',
                'name' => 'de_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54925t7rb1cb8',
                'label' => 'Event list page',
                'name' => 'de_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_548s1c68va62g',
                'label' => 'Employee list page',
                'name' => 'de_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-de',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS DE


    //START NEWS PAGE OPTIONS ET
    register_field_group(array (
        'id' => 'acf_page-links-et',
        'title' => 'News page options et',
        'fields' => array (
            array (
                'key' => 'field_549et1f4vff29',
                'label' => 'News list page',
                'name' => 'et_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5495t13dcfw2t',
                'label' => 'Knowledge list page',
                'name' => 'et_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5498437tb1eb8',
                'label' => 'Event list page',
                'name' => 'et_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54811vd8vd629',
                'label' => 'Employee list page',
                'name' => 'et_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-et',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS ET


    //START NEWS PAGE OPTIONS ET_EN
    register_field_group(array (
        'id' => 'acf_page-links-et_en',
        'title' => 'News page options et_en',
        'fields' => array (
            array (
                'key' => 'field_549et1fevfn14',
                'label' => 'News list page',
                'name' => 'et_en_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5495w135cfv21',
                'label' => 'Knowledge list page',
                'name' => 'et_en_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5491te3fb2cb8',
                'label' => 'Event list page',
                'name' => 'et_en_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_548aavs8vhf23',
                'label' => 'Employee list page',
                'name' => 'et_en_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-et_en',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS ET_EN


    //START NEWS PAGE OPTIONS ru-ee
    register_field_group(array (
        'id' => 'acf_page-links-ru-ee',
        'title' => 'News page options ru-ee',
        'fields' => array (
            array (
                'key' => 'field_549et1frvfu15',
                'label' => 'News list page',
                'name' => 'ru-ee_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5495s185gfr22',
                'label' => 'Knowledge list page',
                'name' => 'ru-ee_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549e4tr2b4ub8',
                'label' => 'Event list page',
                'name' => 'ru-ee_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54saa2s5vff23',
                'label' => 'Employee list page',
                'name' => 'ru-ee_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-ru-ee',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS ru-ee


    //START NEWS PAGE OPTIONS LV
    register_field_group(array (
        'id' => 'acf_page-links-lv',
        'title' => 'News page options lv',
        'fields' => array (
            array (
                'key' => 'field_549lv1lrvfv15',
                'label' => 'News list page',
                'name' => 'lv_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5491s145g6rj2',
                'label' => 'Knowledge list page',
                'name' => 'lv_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549l4f7nb3c29',
                'label' => 'Event list page',
                'name' => 'lv_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54ssa2s53f733',
                'label' => 'Employee list page',
                'name' => 'lv_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-lv',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS LV


    //START NEWS PAGE OPTIONS LV_EN
    register_field_group(array (
        'id' => 'acf_page-links-lv_en',
        'title' => 'News page options lv_en',
        'fields' => array (
            array (
                'key' => 'field_549lv1ernfn12',
                'label' => 'News list page',
                'name' => 'lv_en_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5435sc852frk2',
                'label' => 'Knowledge list page',
                'name' => 'lv_en_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549rlvenb1ebn',
                'label' => 'Event list page',
                'name' => 'lv_en_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_54891cs8fa3ds',
                'label' => 'Employee list page',
                'name' => 'lv_en_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 0,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-lv_en',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS LV_EN


    //START NEWS PAGE OPTIONS RU
    register_field_group(array (
        'id' => 'acf_page-links-ru',
        'title' => 'News page options ru',
        'fields' => array (
            array (
                'key' => 'field_549lv1runfr1u',
                'label' => 'News list page',
                'name' => 'ru_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5495s1753fc2o',
                'label' => 'Knowledge list page',
                'name' => 'ru_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5491el72v1rbu',
                'label' => 'Event list page',
                'name' => 'ru_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_548s1cs8f73f2',
                'label' => 'Employee list page',
                'name' => 'ru_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-ru',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS RU


    //START NEWS PAGE OPTIONS LT
    register_field_group(array (
        'id' => 'acf_page-links-lt',
        'title' => 'News page options lt',
        'fields' => array (
            array (
                'key' => 'field_549l31t9clf2t',
                'label' => 'News list page',
                'name' => 'lt_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5497s1353ccb1',
                'label' => 'Knowledge list page',
                'name' => 'lt_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549l4t7lbncl8',
                'label' => 'Event list page',
                'name' => 'lt_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_548slts8f231d',
                'label' => 'Employee list page',
                'name' => 'lt_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-lt',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS LT


    //START NEWS PAGE OPTIONS LT_EN
    register_field_group(array (
        'id' => 'acf_page-links-lt_en',
        'title' => 'News page options lt_en',
        'fields' => array (
            array (
                'key' => 'field_549lt1e9cnfen',
                'label' => 'News list page',
                'name' => 'lt_en_news_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the news listing page (This adds the correct url to the \'See more news\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5445d1f5gfj21',
                'label' => 'Knowledge list page',
                'name' => 'lt_en_knowledge_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the knowledge listing page (This adds the correct url to the \'See more knowledge articles\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_549l4ftabecbn',
                'label' => 'Event list page',
                'name' => 'lt_en_event_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the event listing page (This adds the correct url to the \'See more events\' link)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_548aaltenhf34',
                'label' => 'Employee list page',
                'name' => 'lt_en_employee_list_page',
                'type' => 'page_link',
                'instructions' => 'Choose which page is the employee listing page (This is required to make the employee filter/search work)',
                'post_type' => array (
                    0 => 'page',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-page-links-lt_en',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    //END NEWS PAGE OPTIONS LT_EN

}
