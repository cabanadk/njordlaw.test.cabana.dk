<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_paradigm',
		'title' => 'Paradigm',
		'fields' => array (
			array (
				'key' => 'field_5841746f25242',
				'label' => 'Paradigm',
				'name' => 'paradigm',
				'type' => 'flexible_content',
				'required' => 1,
				'layouts' => array (
					array (
						'label' => 'Digital Download',
						'name' => 'digital_download',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_584174f425243',
								'label' => 'File Id',
								'name' => 'file_id',
								'type' => 'number',
								'instructions' => 'Find the file id in the <a href="/wp-admin/admin.php?page=digital_downloads" target="_blank">Digital Downloads</a> section.',
								'required' => 1,
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'min' => '',
								'max' => '',
								'step' => '',
							),
						),
					),
					array (
						'label' => 'Link',
						'name' => 'link',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_5841752025245',
								'label' => 'Link Title',
								'name' => 'link_title',
								'type' => 'text',
								'required' => 1,
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_5841753b25246',
								'label' => 'Link URL',
								'name' => 'link_url',
								'type' => 'text',
								'required' => 1,
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
						),
					),
				),
				'button_label' => 'Add Paradigm',
				'min' => 1,
				'max' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'paradigm',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}