<?php
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_cookiebox_da',
        'title' => 'Cookiebox (DA)',
        'fields' => array (
            array (
                'key' => 'field_5501b72440f77',
                'label' => 'Text Content',
                'name' => 'da_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-da',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));


    register_field_group(array (
        'id' => 'acf_cookiebox_en',
        'title' => 'Cookiebox (EN)',
        'fields' => array (
            array (
                'key' => 'field_550eb7c44nf7o',
                'label' => 'Text Content',
                'name' => 'en_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-en',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));



    register_field_group(array (
        'id' => 'acf_cookiebox_de',
        'id' => 'acf_cookiebox_de',
        'title' => 'Cookiebox (DE)',
        'fields' => array (
            array (
                'key' => 'field_550sbacg2nc7o',
                'label' => 'Text Content',
                'name' => 'de_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-de',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array (
        'id' => 'acf_cookiebox_et',
        'title' => 'Cookiebox (ET)',
        'fields' => array (
            array (
                'key' => 'field_et3g1dbacg2mgf17o',
                'label' => 'Text Content',
                'name' => 'et_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-et',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    
    register_field_group(array (
        'id' => 'acf_cookiebox_ru-ee',
        'title' => 'Cookiebox (ru-ee)',
        'fields' => array (
            array (
                'key' => 'field_etru51dbacg2mu7o',
                'label' => 'Text Content',
                'name' => 'ru-ee_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-ru-ee',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array (
        'id' => 'acf_cookiebox_lv',
        'title' => 'Cookiebox (LV)',
        'fields' => array (
            array (
                'key' => 'field_lv51dacsqqg2432hu7o',
                'label' => 'Text Content',
                'name' => 'lv_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-lv',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array (
        'id' => 'acf_cookiebox_ru',
        'title' => 'Cookiebox (RU)',
        'fields' => array (
            array (
                'key' => 'field_lvru51bacg2mu7o',
                'label' => 'Text Content',
                'name' => 'ru_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-ru',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array (
        'id' => 'acf_cookiebox_lt',
        'title' => 'Cookiebox (LT)',
        'fields' => array (
            array (
                'key' => 'field_52dac4qsg2437h9o',
                'label' => 'Text Content',
                'name' => 'lt_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-lt',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array (
        'id' => 'acf_cookiebox_iv_ru',
        'title' => 'Cookiebox (IV_RU)',
        'fields' => array (
            array (
                'key' => 'field_ivru2dac4qsg2437h9o',
                'label' => 'Text Content',
                'name' => 'iv_ru_cookie_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer-iv_ru',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));



}
