<?php
/**
 *  Install Add-ons
 *
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *
 *  All fields must be included during the 'acf/register_fields' action.
 *  Other types of Add-ons (like the options page) can be included outside of this action.
 *
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme as outlined in the terms and conditions.
 *  However, they are NOT to be included in a premium / free plugin.
 *  For more information, please read http://www.advancedcustomfields.com/terms-conditions/
 */

//define( 'ACF_LITE', true );

include_once('advanced-custom-fields/acf.php');

// Fields
add_action('acf/register_fields', 'register_fields');


/**
 * Register Fields
 *
 * Out and In comment the fields you need for your solution
 */
function register_fields()
{
    // Repeater Field
	include_once('add-ons/acf-repeater/repeater.php');
    // Flexible Content Field
    include_once('add-ons/acf-flexible-content/flexible-content.php');
    // Options Page
    include_once('add-ons/acf-options-page/acf-options-page.php');
}


//Split options page into more pages
add_action('init', function(){
    if( function_exists('acf_add_options_sub_page') )
    {
        acf_add_options_sub_page('General');

        acf_add_options_sub_page('Page links da');
        acf_add_options_sub_page('Page links en');
        acf_add_options_sub_page('Page links de');
        acf_add_options_sub_page('Page links et');
        acf_add_options_sub_page('Page links et_en');
        acf_add_options_sub_page('Page links ru-ee');
        acf_add_options_sub_page('Page links lv');
        acf_add_options_sub_page('Page links lv_en');
        acf_add_options_sub_page('Page links ru');
        acf_add_options_sub_page('Page links lt');
        acf_add_options_sub_page('Page links lt_en');

        acf_add_options_sub_page('Footer da');
        acf_add_options_sub_page('Footer en');
        acf_add_options_sub_page('Footer de');
        acf_add_options_sub_page('Footer et');
        acf_add_options_sub_page('Footer et_en');
        acf_add_options_sub_page('Footer ru-ee');
        acf_add_options_sub_page('Footer lv');
        acf_add_options_sub_page('Footer lv_en');
        acf_add_options_sub_page('Footer ru');
        acf_add_options_sub_page('Footer lt');
        acf_add_options_sub_page('Footer lt_en');

        acf_add_options_sub_page('Newsletter Popup da');
        acf_add_options_sub_page('Newsletter Popup en');
        acf_add_options_sub_page('Newsletter Popup de');
        acf_add_options_sub_page('Newsletter Popup et');
        acf_add_options_sub_page('Newsletter Popup et_en');
        acf_add_options_sub_page('Newsletter Popup ru-ee');
        acf_add_options_sub_page('Newsletter Popup lv');
        acf_add_options_sub_page('Newsletter Popup lv_en');
        acf_add_options_sub_page('Newsletter Popup ru');
        acf_add_options_sub_page('Newsletter Popup lt');
        acf_add_options_sub_page('Newsletter Popup lt_en');
    }
}, 1);



/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

include('acf_footer_options.php');
include('acf_page_links_options.php');
include('acf_cookie_text.php');
include('acf_paradigm.php');
include('acf_paradigms.php');
include('acf_contacts.php');
include('acf_newsletter_popup.php');
include('acf_block_grid.php');
