<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_paradigms',
		'title' => 'Paradigms',
		'fields' => array (
			array (
				'key' => 'field_58417e296669f',
				'label' => 'Paradigms',
				'name' => 'paradigms',
				'type' => 'relationship',
				'return_format' => 'object',
				'post_type' => array (
					0 => 'paradigm',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'filters' => array (
					0 => 'search',
				),
				'result_elements' => array (
					0 => 'post_title',
				),
				'max' => 20,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'extranet',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_type',
					'operator' => '!=',
					'value' => 'top_level',
					'order_no' => 1,
					'group_no' => 0,
				)
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}