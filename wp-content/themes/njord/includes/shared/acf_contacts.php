<?php
/*
 * Contacts repeater field for extranet pages
 * */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contacts',
		'title' => 'Contacts',
		'fields' => array (
			array (
				'key' => 'field_584fc4ab031da',
				'label' => 'Contacts section',
				'name' => 'add_contacts_repeater',
				'type' => 'repeater',
				'instructions' => 'Choose contacts to add to this page',
				'sub_fields' => array (
					array (
						'key' => 'field_584fc4d0031db',
						'label' => 'Contact',
						'name' => 'single_contact',
						'type' => 'post_object',
						'column_width' => '',
						'post_type' => array (
							0 => 'employee',
						),
						'taxonomy' => array (
							0 => 'all',
						),
						'allow_null' => 1,
						'multiple' => 0,
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Contact',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'extranet',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_type',
					'operator' => '!=',
					'value' => 'top_level',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}