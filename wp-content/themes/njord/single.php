<?php get_header(); ?>

    <?php
    $newsThumbnail = get_the_post_thumbnail($post->ID, 'full', array('width'=>"1572",'height'=>"500"));
    if(!empty($newsThumbnail)):
    ?>
    <figure id="featured">

        <?php echo $newsThumbnail;?>

    </figure>
    <?php endif; ?>
    <section id="content" class="cols-a">
        <article>
            <?php 
            if (get_field('event_date') == null) {
            ?>
                <p class="scheme-b date_post">
                    <?php if(get_post_type() == 'event'){ } else { the_date('d.m.Y'); } ?>
                </p>
            
            <?php } ?>
            <p class="scheme-b before_title">
                <?php if(get_post_type() == 'event'){ _e('Event', 'html24'); } else { _e('News', 'html24'); } ?>
            </p>


            <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
            <h1><?php the_title();?></h1>
            <?php

                // check if the repeater field has rows of data
                if( have_rows('event_dates') ):
                    ?>
                    <p class="scheme-b date_post event_date">
                    <?php
                        while ( have_rows('event_dates') ) : the_row();

                            $date = DateTime::createFromFormat('dmY', get_sub_field('event_date'));
                            
                            if(get_post_type() == 'event'){ 
                                echo the_sub_field('event_name') . " - " . $date->format('d.m.Y') . "</br>";
                            }  

                        endwhile;
                    ?>
                    </p>

                <?php
                endif;

                ?>
                <?php 
                
                $content = do_shortcode(wpautop($post->post_content));
                echo str_replace(get_the_title(), '', $content); 
            ?>
            <?php include ('template_parts/social-sharing-buttons.php');?>
        </article>

        <?php

        //Set latest news to false and do a check for the section in the loop
        $latestNews = false;

        // check if the flexible content field has rows of data
        if( have_rows('post_flexible_content') ):

            // loop through the rows of data
            while ( have_rows('post_flexible_content') ) : the_row();

            if( get_row_layout() == 'post_employee_section' ):
                include "template_parts/employee-section.php";

            elseif(get_row_layout('post_latest_news_section')):

                $latestNews = true;
                $background = get_sub_field('post_latest_news_background');

            endif;

            endwhile;
        else :
                // no layouts found
        endif;
        ?>


    </section>

            <?php

                if ($latestNews == true){

                    include('template_parts/latest-news-section.php');

                }

            ?>

<?php get_footer(); ?>