<?php

/**
 * Loads all the needed files for setting up the theme
 */
require "includes/theme_loader.php";

/**
 * Adds a class to paragraph output by the_content()
 */

function append_class_to_paragraph($content, $class = "scheme-a"){
    return preg_replace('/<p([^>]+)?>/', '<p$1 class="'.$class.'">', $content);
}
add_filter('the_content', 'append_class_to_paragraph');


add_image_size( 'employee-thumb', 226, 226, array('right', 'center') ); // (cropped)
add_image_size( 'press-thumb', 300, 210, array('center', 'center') ); // (cropped)

include_once "includes/backend/custom-walkers.php";


if(ICL_LANGUAGE_CODE == 'lv_en' || ICL_LANGUAGE_CODE == 'lt_en' || ICL_LANGUAGE_CODE == 'et_en'){
    header('Location:'.site_url());
}



// Prevent TinyMCE from stripping out schema.org metadata
function schema_TinyMCE_init($in)
{
    /**
     *   Edit extended_valid_elements as needed. For syntax, see
     *   http://www.tinymce.com/wiki.php/Configuration:valid_elements
     *
     *   NOTE: Adding an element to extended_valid_elements will cause TinyMCE to ignore
     *   default attributes for that element.
     *   Eg. a[title] would remove href unless included in new rule: a[title|href]
     */
    if(!empty($in['extended_valid_elements']))
        $in['extended_valid_elements'] .= ',';

    $in['extended_valid_elements'] .= '@[id|class|style|title|itemscope|itemtype|itemprop|datetime|rel],div,dl,ul,dt,dd,li,span,a|rev|charset|href|lang|tabindex|accesskey|type|name|href|target|title|class|onfocus|onblur]';

    return $in;
}
add_filter('tiny_mce_before_init', 'schema_TinyMCE_init' );


// Disables theme editor
function remove_editor_menu() {
    remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);