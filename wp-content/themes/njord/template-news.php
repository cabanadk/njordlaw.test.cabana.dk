<?php
/*
 Template Name: News list
 */

$languageCode = ICL_LANGUAGE_CODE;
?>

<?php get_header(); ?>

<nav id="aside">
    <ul>
        <?php
            // Get years that have posts
            $years = $wpdb->get_results( 'SELECT YEAR(post_date) AS year FROM wp_posts JOIN wp_icl_translations ON wp_icl_translations.element_id = wp_posts.ID WHERE post_type = "post" AND post_status = "publish" AND language_code = "'.$languageCode.'" GROUP BY year DESC' );

            //  For each year, do the following
            foreach ( $years as $year ) {

                // Get all posts for the year
                $posts_this_year = $wpdb->get_results( "SELECT ID, post_title FROM wp_posts JOIN wp_icl_translations ON wp_icl_translations.element_id = wp_posts.ID WHERE post_type = 'post' AND post_status = 'publish' AND YEAR(post_date) = '" . $year->year . "' AND language_code = '".$languageCode."'" );

                // Display the year as list item
                echo '<li id="nav_btn_'.$year->year.'" class="'.($year->year == date("Y") ? "active " : false).'btn_year" date-current-lang="'.$languageCode.'"><a>'.$year->year.'</a></li>';

            }
        ?>
    </ul>
</nav>
<section id="content">
    <h1><?php the_title(); ?></h1>
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>
    <div class="news-a" id="news_list">
    <?php
        // The Query
        $the_query = new WP_Query( array(
            'post_type' => 'post'
        ) );

        // The Loop
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>
                <article data-article-id="<?php the_ID(); ?>" data-article-year="<?php echo get_the_date('Y'); ?>">
                    <header>
                        <p><?php echo get_the_date('d.m.Y'); ?></p>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </header>
                    <?php
                    if(has_shortcode($content = get_the_content(),'intro')){
                        $introText = check_for_intro_text($content);
                        echo wpautop($introText);
                    } else {
                        echo wpautop(str_replace(get_the_title(), '', get_the_excerpt()));
                    }
                    ?>
                </article>
           <?php }
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
    </div>
    <p class="loading-a" id="icon_spinner" style="display: none;"><?php _e('Loading', 'html24');?></p>
</section>

<?php get_footer(); ?>
