<?php
/*
 Template Name: Front Page
 */
?>
<?php get_header();?>
<!--  Dots scrollbar   -->
<div class="scroll_dots"></div>
<div class="frontpage">
    <?php
    // check if the flexible content field has rows of data
    if( have_rows('frontpage_flexible_content') ):
        // loop through the rows of data
        while ( have_rows('frontpage_flexible_content') ) : the_row();
            // If layout is equal to
            if( get_row_layout() == 'slider_section' ){
                include('template_parts/bxslider-section.php');
            }elseif( get_row_layout() == 'text_section' ){
                include('template_parts/text-section.php');
            }elseif( get_row_layout() == 'events_section' ){
                include('template_parts/events-section.php');
            }elseif( get_row_layout() == 'news_section' ){
                $related_news_boolean = get_sub_field('related_news_boolean');
                if($related_news_boolean)
                {
                    include('template_parts/pick-news-section.php');
                }
                else
                {
                    include('template_parts/latest-news-section.php');
                }
            }elseif( get_row_layout() == 'map_section' ){
                include('template_parts/map-section.php');
            }elseif( get_row_layout() == 'employee_section' ){
                include('template_parts/employee-filter.php');
            }
        endwhile;
    else :
        // no layouts found
    endif;
    ?>
</div>
<?php get_footer();?>