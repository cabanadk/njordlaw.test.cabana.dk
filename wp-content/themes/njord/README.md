# NJORDLAW

## Project Description

Custom wordpress theme with WPML, SEO and a lot of functions


## Project Information

### Links

Link to live solution: http://njordlaw.com/

Link to development solution: html24-dev.dk/njord

Link to Twentyfour project: http://html24.online-fakturering.dk/Customer-584332177/

Slack channel: [#3371-njordlaw] (https://html24.slack.com/messages/html24/)

### Responsibles / Developers

* Responsibles: @m_dam
* Developers: @saravicente @JJGaardbo 

  
  
**All above this point is required for each README.md file.**

**The rest of the README file from here is optional information and are simply inspiration of setting up your README file.**

### Testing

Last tested: 04-01-2016

Latest working software / CMS versions: Wordpress 4.2.3

Browsers: Firefox 39, Chrome 44, Safari 8.0.7 and Internet Explorer 10 & 11

Mobile OS versions: iOS 8, Android 5.0 and Windows 10 Mobile


### HTML24 Products in use

Here's a list of all HTML24 Products / Plugins used in this project:

* [WP Cookie Plugin](https://bitbucket.org/HTML24DK/product_wp_cookie)
* [WP Autocomplete Search Plugin](https://bitbucket.org/HTML24DK/product_wp_cookie)

### 3rd party usage

* Yoast SEO plugin from Wordpress


### Special Notices

** Options pages **

- The footer options custom fields are included in acf.php because there are 11 different languages. And this is cleaner in the backend. --> **includes/shared/acf_footer_options.php**

- Added an options page for choosing the page links to use on the site in a php file --> **includes/shared/acf_page_links_options.php**

- There has also been added a backend.js which is used to show the current language's options page and hide all the others. Also to make it cleaner in the backend.


** Newsletter **

- A newsletter template folder has also been added inside the theme


**Changes for a proper SEO with the language codes**

To change the language code, since this cannot be changed we did the following:

-Create/add a new language with the code we want to use.

-With Translation Management plugin duplicate ALL the content from one language to the new one.

-Look in the folder structer of the theme for the language code we want to change, and replace it for the new (theme-functions.php, acf.php, acf-cookie-text.php, acf-footer-options.php, acf-page-links-options.php, language_picker.php, etc).

-Go to the database, table: 'wp_icl_string_translations' and replace the old language code in 'language' column for the new one (this is to move all the string translations so we don't have to translate them again).

-Make sure that the footer and page options are also filled up.