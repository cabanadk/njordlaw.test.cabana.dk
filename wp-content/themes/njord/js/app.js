/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

/*!
    Head JS     The only script in your <HEAD>
    Copyright   Tero Piirainen (tipiirai)
    License     MIT / http://bit.ly/mit-license
    Version     1.0.2

    http://headjs.com
*/
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(7(n,t){"1A 1B";7 r(n){a[a.A]=n}7 k(n){m t=31 32(" ?\\\\b"+n+"\\\\b");c.19=c.19.29(t,"")}7 p(n,t){C(m i=0,r=n.A;i<r;i++)t.J(n,n[i],i)}7 Y(){m t,e,f,o;c.19=c.19.29(/ (w-|Z-|V-|E-|K-|F-|1C|1a-1C|1D|1a-1D)\\d+/g,"");t=n.2a||c.33;e=n.2b||n.L.1E;u.L.2a=t;u.L.2b=e;r("w-"+t);p(i.2c,7(n){t>n?(i.Q.V&&r("V-"+n),i.Q.E&&r("E-"+n)):t<n?(i.Q.K&&r("K-"+n),i.Q.F&&r("F-"+n)):t===n&&(i.Q.F&&r("F-"+n),i.Q.Z&&r("e-q"+n),i.Q.E&&r("E-"+n))});f=n.2d||c.34;o=n.2e||n.L.1F;u.L.2d=f;u.L.2e=o;u.G("1C",f>t);u.G("1D",f<t)}7 12(){n.1b(b);b=n.14(Y,1G)}m y=n.1H,1c=n.35,1n=n.36,c=y.1I,a=[],i={2c:[37,38,39,3a,3b,3c,3d,3e,3f,3g,3h],Q:{V:!0,E:!1,K:!0,F:!1,Z:!1},1d:[{1o:{2f:6,2g:11}}],R:{V:!0,E:!1,K:!0,F:!1,Z:!0},2h:!0,1J:"-1J",1e:"-1e",M:"M"},v,u,s,w,o,h,l,d,f,g,15,e,b;x(n.S)C(v N n.S)n.S[v]!==t&&(i[v]=n.S[v]);u=n[i.M]=7(){u.16.W(D,1p)};u.G=7(n,t,i){j n?(1K.1L.1M.J(t)==="[1f 2i]"&&(t=t.J()),r((t?"":"1a-")+n),u[n]=!!t,i||(k("1a-"+n),k(n),u.G()),u):(c.19+=" "+a.1g(" "),a=[],u)};u.G("2j",!0);s=1c.3i.1q();w=/2k|1N|3j|3k|3l|3m|(3n .+3o|2l)/.1h(s);u.G("2k",w,!0);u.G("3p",!w,!0);s=/(2m|2n)[ \\/]([\\w.]+)/.17(s)||/(2o|2p|2q)(?:.*1i)?[ \\/]([\\w.]+)/.17(s)||/(1N)(?:.*1i)?[ \\/]([\\w.]+)/.17(s)||/(1r|1O)(?:.*1i)?[ \\/]([\\w.]+)/.17(s)||/(2r) ([\\w.]+)/.17(s)||/(2s).+3q:(\\w.)+/.17(s)||[];o=s[1];h=3r(s[2]);2t(o){z"2r":z"2s":o="1o";h=y.1P||h;1Q;z"2n":o="2u";1Q;z"2q":z"2p":z"2o":o="2v";1Q;z"1r":o="3s"}C(u.1s={P:o,1i:h},u.1s[o]=!0,l=0,d=i.1d.A;l<d;l++)C(f N i.1d[l])x(o===f)C(r(f),g=i.1d[l][f].2f,15=i.1d[l][f].2g,e=g;e<=15;e++)h>e?(i.R.V&&r("V-"+f+e),i.R.E&&r("E-"+f+e)):h<e?(i.R.K&&r("K-"+f+e),i.R.F&&r("F-"+f+e)):h===e&&(i.R.F&&r("F-"+f+e),i.R.Z&&r("Z-"+f+e),i.R.E&&r("E-"+f+e));1j r("1a-"+f);r(o);r(o+3t(h,10));i.2h&&o==="1o"&&h<9&&p("3u|3v|3w|3x|3y|3z|3A|3B|3C|3D|3E|3F|3G|3H|3I|3J|3K|1e|3L|3M|3N".T("|"),7(n){y.1k(n)});p(1n.3O.T("/"),7(n,u){x(1R.A>2&&1R[u+1]!==t)u&&r(1R.1l(u,u+1).1g("-").1q()+i.1e);1j{m f=n||"3P",e=f.2w(".");e>0&&(f=f.2x(0,e));c.3Q=f.1q()+i.1J;u||r("3R"+i.1e)}});u.L={1F:n.L.1F,1E:n.L.1E};Y();b=0;n.18?n.18("3S",12,!1):n.1S("3T",12)})(1T);(7(n,t){"1A 1B";7 a(n){C(m r N n)x(i[n[r]]!==t)j!0;j!1}7 r(n){m t=n.3U(0).3V()+n.3W(1),i=(n+" "+c.1g(t+" ")+t).T(" ");j!!a(i)}m h=n.1H,o=h.1k("i"),i=o.1U,s=" -o- -3X- -2y- -1r- -3Y- ".T(" "),c="3Z 40 O 2y 41".T(" "),l=n.S&&n.S.M||"M",u=n[l],f={1V:7(){m n="1t-42:";j i.1W=(n+s.1g("1V(2z,1X 2A,43 44,45(#46),47(#2B));"+n)+s.1g("2z-1V(1X 2A,#48,#2B);"+n)).1l(0,-n.A),!!i.49},2C:7(){j i.1W="1t-4a:2C(0,0,0,0.5)",!!i.4b},2D:7(){j o.1U.2D===""},4c:7(){j i.4d===""},4e:7(){i.1W="1t:B(1Y://),B(1Y://),4f B(1Y://)";m n=(i.1t||"").4g(/B/g);j 1K.1L.1M.J(n)==="[1f 2E]"&&n.A===3},4h:7(){j r("4i")},4j:7(){j r("4k")},4l:7(){j r("4m")},4n:7(){j r("4o")},4p:7(){j r("4q")},4r:7(){j r("4s")},2l:7(){j"4t"N n},4u:7(){j n.4v>1},4w:7(){m t=u.1s.P,n=u.1s.1i;2t(t){z"1o":j n>=9;z"2m":j n>=13;z"2u":j n>=6;z"2v":j n>=5;z"1N":j!1;z"1r":j n>=5.1;z"1O":j n>=10;4x:j!1}}};C(m e N f)f[e]&&u.G(e,f[e].J(),!0);u.G()})(1T);(7(n,t){"1A 1B";7 w(){}7 u(n,t){x(n){1u n=="1f"&&(n=[].1l.J(n));C(m i=0,r=n.A;i<r;i++)t.J(n,n[i],i)}}7 12(n,i){m r=1K.1L.1M.J(i).1l(8,-1);j i!==t&&i!==D&&r===n}7 s(n){j 12("2i",n)}7 a(n){j 12("2E",n)}7 2F(n){m i=n.T("/"),t=i[i.A-1],r=t.2w("?");j r!==-1?t.2x(0,r):t}7 f(n){(n=n||w,n.2G)||(n(),n.2G=1)}7 2H(n,t,r,u){m f=1u n=="1f"?n:{1h:n,1v:!t?!1:a(t)?t:[t],1w:!r?!1:a(r)?r:[r],1Z:u||w},e=!!f.1h;j e&&!!f.1v?(f.1v.X(f.1Z),i.H.W(D,f.1v)):e||!f.1w?u():(f.1w.X(f.1Z),i.H.W(D,f.1w)),i}7 v(n){m t={},i,r;x(1u n=="1f")C(i N n)!n[i]||(t={P:i,B:n[i]});1j t={P:2F(n),B:n};j(r=c[t.P],r&&r.B===t.B)?r:(c[t.P]=t,t)}7 y(n){n=n||c;C(m t N n)x(n.4y(t)&&n[t].I!==l)j!1;j!0}7 2I(n){n.I=2J;u(n.21,7(n){n.J()})}7 2K(n){n.I===t&&(n.I=15,n.21=[],1c({B:n.B,U:"4z"},7(){2I(n)}))}7 2L(){m n=1p,t=n[n.A-1],r=[].1l.J(n,1),f=r[0];j(s(t)||(t=D),a(n[0]))?(n[0].X(t),i.H.W(D,n[0]),i):(f?(u(r,7(n){s(n)||!n||2K(v(n))}),b(v(n[0]),s(f)?f:7(){i.H.W(D,r)})):b(v(n[0])),i)}7 K(){m n=1p,t=n[n.A-1],r={};j(s(t)||(t=D),a(n[0]))?(n[0].X(t),i.H.W(D,n[0]),i):(u(n,7(n){n!==t&&(n=v(n),r[n.P]=n)}),u(n,7(n){n!==t&&(n=v(n),b(n,7(){y(r)&&f(t)}))}),i)}7 b(n,t){x(t=t||w,n.I===l){t();j}x(n.I===Y){i.16(n.P,t);j}x(n.I===15){n.21.X(7(){b(n,t)});j}n.I=Y;1c(n,7(){n.I=l;t();u(h[n.P],7(n){f(n)});o&&y()&&u(h.1x,7(n){f(n)})})}7 2M(n){n=n||"";m t=n.T("?")[0].T(".");j t[t.A-1].1q()}7 1c(t,i){7 e(t){t=t||n.2N;u.1y=u.1m=u.22=D;i()}7 o(f){f=f||n.2N;(f.U==="H"||/4A|23/.1h(u.24)&&(!r.1P||r.1P<9))&&(n.1b(t.2O),n.1b(t.25),u.1y=u.1m=u.22=D,i())}7 s(){x(t.I!==l&&t.26<=20){C(m i=0,f=r.2P.A;i<f;i++)x(r.2P[i].27===u.27){o({U:"H"});j}t.26++;t.25=n.14(s,4B)}}m u,h,f;i=i||w;h=2M(t.B);h==="2Q"?(u=r.1k("4C"),u.U="2R/"+(t.U||"2Q"),u.4D="4E",u.27=t.B,t.26=0,t.25=n.14(s,4F)):(u=r.1k("28"),u.U="2R/"+(t.U||"4G"),u.4H=t.B);u.1y=u.1m=o;u.22=e;u.2S=!1;u.4I=!1;t.2O=n.14(7(){e({U:"4J"})},4K);f=r.M||r.2T("M")[0];f.4L(u,f.4M)}7 2U(){C(m t,u=r.2T("28"),n=0,f=u.A;n<f;n++)x(t=u[n].4N("4O-4P-H"),!!t){i.H(t);j}}7 2V(n,t){m v,p,e;j n===r?(o?f(t):d.X(t),i):(s(n)&&(t=n,n="1x"),a(n))?(v={},u(n,7(n){v[n]=c[n];i.16(n,7(){y(v)&&f(t)})}),i):1u n!="4Q"||!s(t)?i:(p=c[n],p&&p.I===l||n==="1x"&&y()&&o)?(f(t),i):(e=h[n],e?e.X(t):e=h[n]=[t],i)}7 e(){x(!r.4R){n.1b(i.1z);i.1z=n.14(e,1G);j}o||(o=!0,2U(),u(d,7(n){f(n)}))}7 k(){r.18?(r.4S("2W",k,!1),e()):r.24==="23"&&(r.4T("1m",k),e())}m r=n.1H,d=[],h={},c={},1n="2S"N r.1k("28")||"4U"N r.1I.1U||n.1O,o,g=n.S&&n.S.M||"M",i=n[g]=n[g]||7(){i.16.W(D,1p)},15=1,2J=2,Y=3,l=4,p;x(r.24==="23")e();1j x(r.18)r.18("2W",k,!1),n.18("H",e,!1);1j{r.1S("1m",k);n.1S("1y",e);p=!1;2X{p=!n.4V&&r.1I}2Y(4W){}p&&p.2Z&&7 30(){x(!o){2X{p.2Z("1X")}2Y(t){n.1b(i.1z);i.1z=n.14(30,1G);j}e()}}()}i.H=i.2j=1n?K:2L;i.1h=2H;i.16=2V;i.16(r,7(){y()&&u(h.1x,7(n){f(n)});i.G&&i.G("4X",!0)})})(1T);',62,308,'|||||||function||||||||||||return|||var|||||||||||if||case|length|url|for|null|gte|lte|feature|load|state|call|lt|screen|head|in||name|screensCss|browserCss|head_conf|split|type|gt|apply|push|tt|eq|||it||setTimeout|nt|ready|exec|addEventListener|className|no|clearTimeout|rt|browsers|section|object|join|test|version|else|createElement|slice|onreadystatechange|ut|ie|arguments|toLowerCase|webkit|browser|background|typeof|success|failure|ALL|onload|readyTimeout|use|strict|portrait|landscape|width|height|50|document|documentElement|page|Object|prototype|toString|android|opera|documentMode|break|this|attachEvent|window|style|gradient|cssText|left|https|callback||onpreload|onerror|complete|readyState|cssTimeout|cssRetries|href|script|replace|innerWidth|outerWidth|screens|innerHeight|outerHeight|min|max|html5|Function|js|mobile|touch|chrome|firefox|iphone|ipad|ipod|msie|trident|switch|ff|ios|indexOf|substring|ms|linear|top|fff|rgba|opacity|Array|et|_done|ot|st|ft|ht|ct|at|event|errorTimeout|styleSheets|css|text|async|getElementsByTagName|vt|yt|DOMContentLoaded|try|catch|doScroll|pt|new|RegExp|clientWidth|clientHeight|navigator|location|240|320|480|640|768|800|1024|1280|1440|1680|1920|userAgent|kindle|silk|midp|phone|windows|arm|desktop|rv|parseFloat|safari|parseInt|abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|progress|summary|time|video|pathname|index|id|root|resize|onresize|charAt|toUpperCase|substr|moz|khtml|Webkit|Moz|Khtml|image|right|bottom|from|9f9|to|eee|backgroundImage|color|backgroundColor|textshadow|textShadow|multiplebgs|red|match|boxshadow|boxShadow|borderimage|borderImage|borderradius|borderRadius|cssreflections|boxReflect|csstransforms|transform|csstransitions|transition|ontouchstart|retina|devicePixelRatio|fontface|default|hasOwnProperty|cache|loaded|250|link|rel|stylesheet|500|javascript|src|defer|timeout|7e3|insertBefore|lastChild|getAttribute|data|headjs|string|body|removeEventListener|detachEvent|MozAppearance|frameElement|wt|domloaded'.split('|'),0,{}));
/*!
 * jQuery Migrate - 1.2.1
 * https://github.com/jquery/jquery-migrate
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('B.16===2p 0&&(B.16=!0),9(e,t,n){9 r(n){D r=t.17;i[n]||(i[n]=!0,e.1n.1K(n),r&&r.1L&&!e.16&&(r.1L("1o: "+n),e.1p&&r.1M&&r.1M()))}9 a(t,a,i,o){1q(1N.1O)2q{z 1N.1O(t,a,{2r:!0,2s:!0,18:9(){z r(o),i},19:9(e){r(o),i=e}}),n}2t(s){}e.2u=!0,t[a]=i}D i={};e.1n=[],!e.16&&t.17&&t.17.1P&&t.17.1P("1o: 2v F 2w"),e.1p===n&&(e.1p=!0),e.2x=9(){i={},e.1n.Z=0},"2y"===K.2z&&r("B F 2A 1Q 1r 2B 2C");D o=e("<13/>",{1R:1}).U("1R")&&e.1s,s=e.U,u=e.V.L&&e.V.L.18||9(){z 10},c=e.V.L&&e.V.L.19||9(){z n},l=/^(?:13|1a)$/i,d=/^[2D]$/,p=/^(?:2E|2F|2G|1S|2H|2I|2J|2K|2L|2M|2N|2O|2P|2Q|1T)$/i,f=/^(?:1S|1T)$/i;a(e,"1s",o||{},"B.1s F I"),e.U=9(t,a,i,u){D c=a.11(),g=t&&t.1U;z u&&(4>s.Z&&r("B.q.U( 1b, 2R ) F I"),t&&!d.O(g)&&(o?a 1c o:e.1t(e.q[a])))?e(t)[a](i):("1d"===a&&i!==n&&l.O(t.1e)&&t.1u&&r("2S\'t 2T 2U \'1d\' 1v 2V 13 2W 1a 1c 2X 6/7/8"),!e.V[c]&&p.O(c)&&(e.V[c]={18:9(t,r){D a,i=e.2Y(t,r);z i===!0||"2Z"!=1w i&&(a=t.30(r))&&a.31!==!1?r.11():n},19:9(t,n,r){D a;z n===!1?e.32(t,r):(a=e.33[r]||r,a 1c t&&(t[a]=!0),t.34(r,r.11())),r}},f.O(c)&&r("B.q.U(\'"+c+"\') 35 1V 36 37 1v 38")),s.X(e,t,a,i))},e.V.L={18:9(e,t){D n=(e.1e||"").11();z"1a"===n?u.J(5,G):("13"!==n&&"1W"!==n&&r("B.q.U(\'L\') 1X 1Y 39 1Z"),t 1c e?e.L:10)},19:9(e,t){D a=(e.1e||"").11();z"1a"===a?c.J(5,G):("13"!==a&&"1W"!==a&&r("B.q.U(\'L\', 3a) 1X 1Y 3b 1Z"),e.L=t,n)}};D g,h,v=e.q.Y,m=e.1f,y=/^([^<]*)(<[\\w\\W]+>)([^>]*)$/;e.q.Y=9(t,n,a){D i;z t&&"14"==1w t&&!e.3c(n)&&(i=y.12(e.3d(t)))&&i[0]&&("<"!==t.20(0)&&r("$(21) 1x 3e 3f 22 1r \'<\' 23"),i[3]&&r("$(21) 1x 24 3g 3h 3i F 3j"),"#"===i[0].20(0)&&(r("1x 14 3k 22 1r a \'#\' 23"),e.1g("1o: 3l 1y 14 (3m)")),n&&n.1h&&(n=n.1h),e.25)?v.X(5,e.25(i[2],n,!0),n,a):v.J(5,G)},e.q.Y.1i=e.q,e.1f=9(e){z e||10===e?m.J(5,G):(r("B.1f 3n a 3o 3p 14"),10)},e.26=9(e){e=e.11();D t=/(27)[ \\/]([\\w.]+)/.12(e)||/(1z)[ \\/]([\\w.]+)/.12(e)||/(3q)(?:.*1j|)[ \\/]([\\w.]+)/.12(e)||/(3r) ([\\w.]+)/.12(e)||0>e.3s("1Q")&&/(3t)(?:.*? 3u:([\\w.]+)|)/.12(e)||[];z{P:t[1]||"",1j:t[2]||"0"}},e.P||(g=e.26(3v.3w),h={},g.P&&(h[g.P]=!0,h.1j=g.1j),h.27?h.1z=!0:h.1z&&(h.3x=!0),e.P=h),a(e,"P",e.P,"B.P F I"),e.1k=9(){9 t(e,n){z 3y t.q.Y(e,n)}e.3z(!0,t,5),t.3A=5,t.q=t.1i=5(),t.q.3B=t,t.1k=5.1k,t.q.Y=9(r,a){z a&&a 28 e&&!(a 28 t)&&(a=t(a)),e.q.Y.X(5,r,a,n)},t.q.Y.1i=t.q;D n=t(K);z r("B.1k() F I"),t},e.3C({3D:{"24 3E":e.1f}});D b=e.q.1l;e.q.1l=9(t){D a,i,o=5[0];z!o||"1m"!==t||1!==G.Z||(a=e.1l(o,t),i=e.15(o,t),a!==n&&a!==i||i===n)?b.J(5,G):(r("3F 1v B.q.1l(\'1m\') F I"),i)};D j=/\\/(3G|3H)1A/i,w=e.q.1B||e.q.29;e.q.1B=9(){z r("B.q.1B() 3I 3J B.q.29()"),w.J(5,G)},e.1C||(e.1C=9(t,a,i,o){a=a||K,a=!a.1U&&a[0]||a,a=a.3K||a,r("B.1C() F I");D s,u,c,l,d=[];1q(e.2a(d,e.3L(t,a).3M),i)2b(c=9(e){z!e.1d||j.O(e.1d)?o?o.1K(e.1u?e.1u.3N(e):e):i.2c(e):n},s=0;10!=(u=d[s]);s++)e.1e(u,"1A")&&c(u)||(i.2c(u),u.2d!==n&&(l=e.3O(e.2a([],u.2d("1A")),c),d.2e.J(d,[s+1,0].3P(l)),s+=l.Z));z d});D Q=e.E.1D,x=e.E.1E,k=e.E.1F,N=e.q.1G,T=e.q.1H,M=e.q.1I,S="3Q|3R|3S|3T|3U|3V",C=3W("\\\\b(?:"+S+")\\\\b"),H=/(?:^|\\s)1J(\\.\\S+|)\\b/,A=9(t){z"14"!=1w t||e.E.2f.1J?t:(H.O(t)&&r("\'1J\' 3X-E F I, 1V \'2g 2h\'"),t&&t.3Y(H,"2g$1 2h$1"))};e.E.1b&&"2i"!==e.E.1b[0]&&e.E.1b.3Z("2i","40","41","42"),e.E.2j&&a(e.E,"2k",e.E.2j,"B.E.2k F 2l 2m I"),e.E.1D=9(e,t,n,a,i){e!==K&&C.O(t)&&r("43 1m 44 45 46 47 K: "+t),Q.X(5,e,A(t||""),n,a,i)},e.E.1E=9(e,t,n,r,a){x.X(5,e,A(t)||"",n,r,a)},e.q.1g=9(){D e=48.1i.49.X(G,0);z r("B.q.1g() F I"),e.2e(0,0,"1g"),G.Z?5.4a.J(5,e):(5.4b.J(5,e),5)},e.q.1G=9(t,n){1q(!e.1t(t)||!e.1t(n))z N.J(5,G);r("B.q.1G(2n, 2n...) F I");D a=G,i=t.R||e.R++,o=0,s=9(n){D r=(e.15(5,"2o"+t.R)||0)%o;z e.15(5,"2o"+t.R,r+1),n.4c(),a[r].J(5,G)||!1};2b(s.R=i;a.Z>o;)a[o++].R=i;z 5.4d(s)},e.q.1H=9(t,n,a){z r("B.q.1H() F I"),T?T.J(5,G):(e(5.1h).4e(t,5.1y,n,a),5)},e.q.1I=9(t,n){z r("B.q.1I() F I"),M?M.J(5,G):(e(5.1h).4f(t,5.1y||"**",n),5)},e.E.1F=9(e,t,n,a){z n||C.O(e)||r("4g 1m 4h 2l 2m I"),k.X(5,e,t,n||K,a)},e.4i(S.4j("|"),9(t,n){e.E.2f[n]={4k:9(){D t=5;z t!==K&&(e.E.1D(K,n+"."+e.R,9(){e.E.1F(n,10,t,!0)}),e.15(5,n,e.R++)),!1},4l:9(){z 5!==K&&e.E.1E(K,n+"."+e.15(5,n)),!1}}})}(B,4m);',62,271,'|||||this||||function|||||||||||||||||fn|||||||||return||jQuery||var|event|is|arguments||deprecated|apply|document|value|||test|browser||guid|||attr|attrHooks||call|init|length|null|toLowerCase|exec|input|string|_data|migrateMute|console|get|set|button|props|in|type|nodeName|parseJSON|error|context|prototype|version|sub|data|events|migrateWarnings|JQMIGRATE|migrateTrace|if|with|attrFn|isFunction|parentNode|of|typeof|HTML|selector|webkit|script|andSelf|clean|add|remove|trigger|toggle|live|die|hover|push|warn|trace|Object|defineProperty|log|compatible|size|checked|selected|nodeType|use|option|no|longer|properties|charAt|html|start|character|text|parseHTML|uaMatch|chrome|instanceof|addBack|merge|for|appendChild|getElementsByTagName|splice|special|mouseenter|mouseleave|attrChange|dispatch|handle|undocumented|and|handler|lastToggle|void|try|configurable|enumerable|catch|_definePropertyBroken|Logging|active|migrateReset|BackCompat|compatMode|not|Quirks|Mode|238|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|pass|Can|change|the|an|or|IE|prop|boolean|getAttributeNode|nodeValue|removeAttr|propFix|setAttribute|may|property|instead|attribute|gets|val|sets|isPlainObject|trim|strings|must|after|last|tag|ignored|cannot|Invalid|XSS|requires|valid|JSON|opera|msie|indexOf|mozilla|rv|navigator|userAgent|safari|new|extend|superclass|constructor|ajaxSetup|converters|json|Use|java|ecma|replaced|by|ownerDocument|buildFragment|childNodes|removeChild|grep|concat|ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess|RegExp|pseudo|replace|unshift|attrName|relatedNode|srcElement|AJAX|should|be|attached|to|Array|slice|bind|triggerHandler|preventDefault|click|on|off|Global|are|each|split|setup|teardown|window'.split('|'),0,{}));
/*!
 * CSS Browser Selector - 0.4.0
 * http://rafael.adm.br/css_browser_selector
 */
function css_browser_selector(u){var ua=u.toLowerCase(),is=function(t){return ua.indexOf(t)>-1},g='gecko',w='webkit',s='safari',o='opera',m='mobile',h=document.documentElement,b=[(!(/opera|webtv/i.test(ua))&&/msie\s(\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?g+' ff2':is('firefox/3.5')?g+' ff3 ff3_5':is('firefox/3.6')?g+' ff3 ff3_6':is('firefox/3')?g+' ff3':is('gecko/')?g:is('opera')?o+(/version\/(\d+)/.test(ua)?' '+o+RegExp.$1:(/opera(\s|\/)(\d+)/.test(ua)?' '+o+RegExp.$2:'')):is('konqueror')?'konqueror':is('blackberry')?m+' blackberry':is('android')?m+' android':is('chrome')?w+' chrome':is('iron')?w+' iron':is('applewebkit/')?w+' '+s+(/version\/(\d+)/.test(ua)?' '+s+RegExp.$1:''):is('mozilla/')?g:'',is('j2me')?m+' j2me':is('iphone')?m+' iphone':is('ipod')?m+' ipod':is('ipad')?m+' ipad':is('mac')?'mac':is('darwin')?'mac':is('webtv')?'webtv':is('win')?'win'+(is('windows nt 6.0')?' vista':''):is('freebsd')?'freebsd':(is('x11')||is('linux'))?'linux':'','js']; c = b.join(' '); h.className += ' '+c; return c;}; css_browser_selector(navigator.userAgent);
/*!
 * jQuery.browser.mobile
 * http://detectmobilebrowser.com
 */
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
/*!
 * matchHeight - 0.5.2
 * https://github.com/liabru/jquery-match-height
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(2(b){7 f=-1,e=-1,m=2(a){7 d=D,c=[];b(a).8(2(){7 a=b(3),h=a.W().9-g(a.6("E-9")),k=0<c.q?c[c.q-1]:D;D===k?c.F(a):1>=I.X(I.Y(d-h))?c[c.q-1]=k.J(a):c.F(a);d=h});i c},g=2(b){i Z(b)||0};b.5.4=2(a){v("10"===a){7 d=3;3.6("j","");b.8(b.5.4.w,2(b,a){a.x=a.x.11(d)});i 3}v(1>=3.q)i 3;a="12"!==13 a?a:!0;b.5.4.w.F({x:3,K:a});b.5.4.G(3,a);i 3};b.5.4.w=[];b.5.4.L=14;b.5.4.M=!1;b.5.4.G=2(a,d){7 c=b(a),e=[c],h=b(r).N(),k=b("O").y(!0),f=c.15().16(":17");f.6("n","o");d&&(c.8(2(){7 a=b(3),c="z-o"===a.6("n")?"z-o":"o";a.p("A-P",a.B("A"));a.6({n:c,"C-9":"0","C-s":"0","E-9":"0","E-s":"0","t-9-u":"0","t-s-u":"0",j:"18"})}),e=m(c),c.8(2(){7 a=b(3);a.B("A",a.p("A-P")||"").6("j","")}));b.8(e,2(a,c){7 e=b(c),f=0;d&&1>=e.q||(e.8(2(){7 a=b(3),c="z-o"===a.6("n")?"z-o":"o";a.6({n:c,j:""});a.y(!1)>f&&(f=a.y(!1));a.6("n","")}),e.8(2(){7 a=b(3),c=0;"t-Q"!==a.6("Q-19")&&(c+=g(a.6("t-9-u"))+g(a.6("t-s-u")),c+=g(a.6("C-9"))+g(a.6("C-s")));a.6("j",f-c)}))});f.6("n","");b.5.4.M&&b(r).N(h/k*b("O").y(!0));i 3};b.5.4.R=2(){7 a={};b("[p-S-j], [p-T]").8(2(){7 d=b(3),c=d.B("p-S-j")||d.B("p-T");a[c]=c 1a a?a[c].J(d):d});b.8(a,2(){3.4(!0)})};7 l=2(){b.8(b.5.4.w,2(){b.5.4.G(3.x,3.K)})};b.5.4.H=2(a,d){v(d&&"U"===d.1b){7 c=b(r).u();v(c===f)i;f=c}a?-1===e&&(e=1c(2(){l();e=-1},b.5.4.L)):l()};b(b.5.4.R);b(r).V("1d",2(a){b.5.4.H()});b(r).V("U 1e",2(a){b.5.4.H(!0,a)})})(1f);',62,78,'||function|this|matchHeight|fn|css|var|each|top|||||||||return|height||||display|block|data|length|window|bottom|border|width|if|_groups|elements|outerHeight|inline|style|attr|padding|null|margin|push|_apply|_update|Math|add|byRow|_throttle|_maintainScroll|scrollTop|html|cache|box|_applyDataApi|match|mh|resize|bind|offset|floor|abs|parseFloat|remove|not|undefined|typeof|80|parents|filter|hidden|100px|sizing|in|type|setTimeout|load|orientationchange|jQuery'.split('|'),0,{}));
/*!
 * ltIE9 placeholder - 2.0.8
 * https://github.com/mathiasbynens/jquery-placeholder
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}(';(5(f,g,$){2 h=\'1\'F g.G(\'p\');2 j=\'1\'F g.G(\'r\');2 k=$.R;2 l=$.S;2 m=$.T;2 n;2 o;3(h&&j){o=k.1=5(){7 8};o.p=o.r=v}s{o=k.1=5(){2 a=8;a.U((h?\'r\':\':p\')+\'[1]\').V(\'.1\').y({\'z.1\':t,\'H.1\':w}).6(\'1-A\',v).W(\'H.1\');7 a};o.p=h;o.r=j;n={\'X\':5(a){2 b=$(a);2 c=b.6(\'1-u\');3(c){7 c[0].4}7 b.6(\'1-A\')&&b.B(\'1\')?\'\':a.4},\'Y\':5(a,b){2 c=$(a);2 d=c.6(\'1-u\');3(d){7 d[0].4=b}3(!c.6(\'1-A\')){7 a.4=b}3(b==\'\'){a.4=b;3(a!=g.I){w.J(a)}}s 3(c.B(\'1\')){t.J(a,v,b)||(a.4=b)}s{a.4=b}7 c}};3(!h){l.p=n;m.4=n}3(!j){l.r=n;m.4=n}$(5(){$(g).Z(\'11\',\'12.1\',5(){2 a=$(\'.1\',8).x(t);13(5(){a.x(w)},10)})});$(f).y(\'14.1\',5(){$(\'.1\').x(5(){8.4=\'\'})})}5 K(b){2 c={};2 d=/^L\\d+$/;$.x(b.15,5(i,a){3(a.16&&!d.17(a.C)){c[a.C]=a.4}});7 c}5 t(a,b){2 c=8;2 d=$(c);3(c.4==d.q(\'1\')&&d.B(\'1\')){3(d.6(\'1-u\')){d=d.M().18().N().q(\'9\',d.D(\'9\').6(\'1-9\'));3(a===v){7 d[0].4=b}d.z()}s{c.4=\'\';d.O(\'1\');c==g.I&&c.19()}}}5 w(){2 a;2 b=8;2 c=$(b);2 d=8.9;3(b.4==\'\'){3(b.E==\'u\'){3(!c.6(\'1-P\')){1a{a=c.1b().q({\'E\':\'Q\'})}1c(e){a=$(\'<p>\').q($.1d(K(8),{\'E\':\'Q\'}))}a.D(\'C\').6({\'1-u\':c,\'1-9\':d}).y(\'z.1\',t);c.6({\'1-P\':a,\'1-9\':d}).1e(a)}c=c.D(\'9\').M().1f().q(\'9\',d).N()}c.1g(\'1\');c[0].4=c.q(\'1\')}s{c.O(\'1\')}}}(8,1h,L));',62,80,'|placeholder|var|if|value|function|data|return|this|id||||||||||||||||input|attr|textarea|else|clearPlaceholder|password|true|setPlaceholder|each|bind|focus|enabled|hasClass|name|removeAttr|type|in|createElement|blur|activeElement|call|args|jQuery|hide|show|removeClass|textinput|text|fn|valHooks|propHooks|filter|not|trigger|get|set|delegate||form|submit|setTimeout|beforeunload|attributes|specified|test|next|select|try|clone|catch|extend|before|prev|addClass|document'.split('|'),0,{}));
/*!
 * Semantic Select
 * http://wisniowski.pro
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(7($){$.H.I=7(){$(0).l(7(){$(0).m(\'w\').J(\'<b f="2-1-n"></b>\').K(\'<b f="2-1"><b f="x"><b f="8">\'+$(0).4().y().8()+\'</b><b f="L"></b></b><i f="1-\'+$(0).g(\'M\')+\'"></i></b>\').5(\'p\').l(7(){$(0).N().O($(0).c(\'.2-1-n\').5(\'i\'))});k($(0).4(\'p\').s()>6){$(0).c(\'.2-1-n\').5(\'.2-1\').m(\'P\')}});$(\'.2-1\').l(7(){k($(0).5(\'[j]\').s()){z=$(0).5(\'[j]\').8();$(0).5(\'.8\').8(z)}});$(\'.2-1\').4(\'.x\').q(\'r\',7(){k($(0).d().Q(\'3\')){$(0).d().o(\'3\')}t{$(\'.2-1.3\').o(\'3\');$(0).d().m(\'3\')}}).R().4(\'p\').l(7(){k($(0).A(\'[S]\')){$(0).u(\'<9 f="w" v-h="\'+$(0).g(\'h\')+\'"><a>\'+$(0).8()+\'</a></9>\')}t k($(0).A(\'[j]\')){$(0).u(\'<9 f="3" v-h="\'+$(0).g(\'h\')+\'"><a>\'+$(0).8()+\'</a></9>\')}t{$(0).u(\'<9 v-h="\'+$(0).g(\'h\')+\'"><a>\'+$(0).8()+\'</a></9>\')}});$(\'.2-1\').q(\'r B\',7(e){e.T()}).5(\'9\').4(\'a\').q(\'r\',7(){$(0).d(\'9\').d(\'i\').5(\'.3\').o(\'3\');$(0).d().m(\'3\').c(\'.2-1\').o(\'3\').5(\'.8\').8($(0).8()).c(\'.2-1\').c(\'.2-1-n\').5(\'1\').g(\'h\',$(0).c(\'.2-1-n\').5(\'1\').4(\'p\').g(\'j\',C).D(\'j\',C).d().4(\':E(\'+$(0).c(\'i\').4(\'9\').F($(0).d())+\')\').g(\'h\')).4(\':E(\'+$(0).c(\'i\').4(\'9\').F($(0).d())+\')\').g(\'j\',G).D(\'j\',G).U()}).c(\'.2-1\').5(\'i\').l(7(){k(!$(0).4(\'.3\').s()){$(0).4(\'9:y-V\').m(\'3\')}});$(\'W\').q(\'r B\',7(){$(\'.2-1.3\').o(\'3\')})}})(X);',60,60,'this|select|semantic|active|children|find||function|text|li||div|parents|parent||class|attr|value|ul|selected|if|each|addClass|wrapper|removeClass|option|on|click|size|else|replaceWith|data|hidden|input|first|selectedText|is|touchstart|false|prop|eq|index|true|fn|semanticSelect|wrap|before|ticker|id|clone|appendTo|scrolled|hasClass|next|disabled|stopPropagation|change|child|html|jQuery'.split('|'),0,{}));
/*!
 * jQuery bxSlider - 4.1.1
 * http://bxslider.com
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('!4(t){7 e={},s={9:"10",2s:"",1r:!0,2Z:!1,1u:30,1P:1s,1q:0,1J:0,31:!1,32:!1,1v:!1,33:!1,1W:!1,2t:30,34:!1,35:!0,2u:"36",2v:!0,37:!0,2w:2x,38:!0,39:!0,3a:!1,19:!0,3b:"3M",3c:" / ",2y:1s,2d:1s,2e:1s,6:!0,3d:"3N",3e:"3O",2f:1s,2g:1s,1Q:!1,3f:"3P",3g:"3Q",2z:!1,2A:1s,1l:!1,3h:3R,2B:!0,2h:"14",3i:!1,2C:0,1a:1,11:1,1w:0,1m:0,3j:4(){},3k:4(){},3l:4(){},3m:4(){},3n:4(){}};t.3S.3o=4(n){G(0==J.Q)17 J;G(J.Q>1)17 J.1x(4(){t(J).3o(n)}),J;7 o={},r=J;e.1z=J;7 a=t(1R).1g(),l=t(1R).1A(),d=4(){o.2=t.3T({},s,n),o.2.1m=1X(o.2.1m),o.5=r.5(o.2.2s),o.5.Q<o.2.1a&&(o.2.1a=o.5.Q),o.5.Q<o.2.11&&(o.2.11=o.5.Q),o.2.31&&(o.2.1J=1n.3p(1n.3U()*o.5.Q)),o.8={j:o.2.1J},o.1K=o.2.1a>1||o.2.11>1,o.1K&&(o.2.2u="3q"),o.2D=o.2.1a*o.2.1m+(o.2.1a-1)*o.2.1q,o.2E=o.2.11*o.2.1m+(o.2.11-1)*o.2.1q,o.1Y=!1,o.6={},o.1B=1s,o.1L="1h"==o.2.9?"1d":"1b",o.2i=o.2.35&&"1S"!=o.2.9&&4(){7 t=3V.3W("15"),e=["3X","3Y","3Z","40"];2j(7 i 41 e)G(1y 0!==t.1M[e[i]])17 o.1Z=e[i].42("43","").44(),o.1L="-"+o.1Z+"-45",!0;17!1}(),"1h"==o.2.9&&(o.2.11=o.2.1a),r.1C("1T",r.1U("1M")),r.5(o.2.2s).1x(4(){t(J).1C("1T",t(J).1U("1M"))}),c()},c=4(){r.46(\'<15 18="F-47"><15 18="F-K"></15></15>\'),o.K=r.2F(),o.2G=t(\'<15 18="F-48" />\'),o.K.2H(o.2G),r.R({1g:"10"==o.2.9?2I*o.5.Q+49+"%":"1l",12:"2J"}),o.2i&&o.2.1P?r.R("-"+o.1Z+"-2K-3r-4",o.2.1P):o.2.1P||(o.2.1P="4a"),f(),o.K.R({1g:"2I%",4b:"4c",12:"2J"}),o.K.2F().R({4d:v()}),o.2.19||o.K.2F().R({4e:"0 1l 4f"}),o.5.R({"4g":"10"==o.2.9?"1b":"2L",4h:"2L",12:"2J"}),o.5.R("1g",u()),"10"==o.2.9&&o.2.1q>0&&o.5.R("4i",o.2.1q),"1h"==o.2.9&&o.2.1q>0&&o.5.R("4j",o.2.1q),"1S"==o.2.9&&(o.5.R({12:"4k",20:0,3s:"2L"}),o.5.U(o.2.1J).R({20:2x,3s:"4l"})),o.6.1z=t(\'<15 18="F-6" />\'),o.2.32&&P(),o.8.1o=o.2.1J==x()-1,o.2.34&&r.4m();7 e=o.5.U(o.2.1J);"3q"==o.2.2u&&(e=o.5),o.2.1v?o.2.19=!1:(o.2.19&&T(),o.2.6&&C(),o.2.1l&&o.2.1Q&&E(),(o.2.6||o.2.1Q||o.2.19)&&o.K.4n(o.6.1z)),g(e,h)},g=4(e,i){7 s=e.1t("2M, 3t").Q;G(0==s)17 i(),1y 0;7 n=0;e.1t("2M, 3t").1x(4(){t(J).4o("3u",4(){++n==s&&i()}).1x(4(){J.4p&&t(J).3u()})})},h=4(){G(o.2.1r&&"1S"!=o.2.9&&!o.2.1v){7 e="1h"==o.2.9?o.2.1a:o.2.11,i=o.5.3v(0,e).1i().1c("F-1i"),s=o.5.3v(-e).1i().1c("F-1i");r.1j(i).2H(s)}o.2G.1D(),S(),"1h"==o.2.9&&(o.2.1W=!0),o.K.1A(p()),r.2N(),o.2.3j(o.8.j),o.2O=!0,o.2.2v&&t(1R).1E("3w",B),o.2.1l&&o.2.2B&&H(),o.2.1v&&L(),o.2.19&&I(o.2.1J),o.2.6&&W(),o.2.37&&!o.2.1v&&O()},p=4(){7 e=0,s=t();G("1h"==o.2.9||o.2.1W)G(o.1K){7 n=1==o.2.1w?o.8.j:o.8.j*m();2j(s=o.5.U(n),i=1;i<=o.2.11-1;i++)s=n+i>=o.5.Q?s.2P(o.5.U(i-1)):s.2P(o.5.U(n+i))}13 s=o.5.U(o.8.j);13 s=o.5;17"1h"==o.2.9?(s.1x(4(){e+=t(J).2Q()}),o.2.1q>0&&(e+=o.2.1q*(o.2.1a-1))):e=1n.4q.4r(1n,s.4s(4(){17 t(J).2Q(!1)}).4t()),e},v=4(){7 t="2I%";17 o.2.1m>0&&(t="10"==o.2.9?o.2.11*o.2.1m+(o.2.11-1)*o.2.1q:o.2.1m),t},u=4(){7 t=o.2.1m,e=o.K.1g();17 0==o.2.1m||o.2.1m>e&&!o.1K||"1h"==o.2.9?t=e:o.2.11>1&&"10"==o.2.9&&(e>o.2E||e<o.2D&&(t=(e-o.2.1q*(o.2.1a-1))/o.2.1a)),t},f=4(){7 t=1;G("10"==o.2.9&&o.2.1m>0)G(o.K.1g()<o.2D)t=o.2.1a;13 G(o.K.1g()>o.2E)t=o.2.11;13{7 e=o.5.21().1g();t=1n.3p(o.K.1g()/e)}13"1h"==o.2.9&&(t=o.2.1a);17 t},x=4(){7 t=0;G(o.2.1w>0)G(o.2.1r)t=o.5.Q/m();13 2j(7 e=0,i=0;e<o.5.Q;)++t,e=i+f(),i+=o.2.1w<=f()?o.2.1w:f();13 t=1n.3x(o.5.Q/f());17 t},m=4(){17 o.2.1w>0&&o.2.1w<=f()?o.2.1w:f()},S=4(){G(o.5.Q>o.2.11&&o.8.1o&&!o.2.1r){G("10"==o.2.9){7 t=o.5.1o(),e=t.12();b(-(e.1b-(o.K.1g()-t.1g())),"1e",0)}13 G("1h"==o.2.9){7 i=o.5.Q-o.2.1a,e=o.5.U(i).12();b(-e.1d,"1e",0)}}13{7 e=o.5.U(o.8.j*m()).12();o.8.j==x()-1&&(o.8.1o=!0),1y 0!=e&&("10"==o.2.9?b(-e.1b,"1e",0):"1h"==o.2.9&&b(-e.1d,"1e",0))}},b=4(t,e,i,s){G(o.2i){7 n="1h"==o.2.9?"3y(0, "+t+"3z, 0)":"3y("+t+"3z, 0, 0)";r.R("-"+o.1Z+"-2K-4u",i/4v+"s"),"22"==e?(r.R(o.1L,n),r.1E("2k 2l 2m 2n",4(){r.23("2k 2l 2m 2n"),D()})):"1e"==e?r.R(o.1L,n):"1v"==e&&(r.R("-"+o.1Z+"-2K-3r-4","3A"),r.R(o.1L,n),r.1E("2k 2l 2m 2n",4(){r.23("2k 2l 2m 2n"),b(s.2R,"1e",0),N()}))}13{7 a={};a[o.1L]=t,"22"==e?r.2o(a,i,o.2.1P,4(){D()}):"1e"==e?r.R(o.1L,t):"1v"==e&&r.2o(a,1u,"3A",4(){b(s.2R,"1e",0),N()})}},w=4(){2j(7 e="",i=x(),s=0;i>s;s++){7 n="";o.2.2d&&t.4w(o.2.2d)?(n=o.2.2d(s),o.1k.1c("F-4x-19")):(n=s+1,o.1k.1c("F-4y-19")),e+=\'<15 18="F-19-2S"><a 24="" 1C-22-j="\'+s+\'" 18="F-19-4z">\'+n+"</a></15>"}o.1k.25(e)},T=4(){o.2.2e?o.1k=t(o.2.2e):(o.1k=t(\'<15 18="F-19" />\'),o.2.2y?t(o.2.2y).25(o.1k):o.6.1z.1c("F-2T-19").1j(o.1k),w()),o.1k.2U("a","26",q)},C=4(){o.6.14=t(\'<a 18="F-14" 24="">\'+o.2.3d+"</a>"),o.6.1f=t(\'<a 18="F-1f" 24="">\'+o.2.3e+"</a>"),o.6.14.1E("26",y),o.6.1f.1E("26",z),o.2.2f&&t(o.2.2f).1j(o.6.14),o.2.2g&&t(o.2.2g).1j(o.6.1f),o.2.2f||o.2.2g||(o.6.2V=t(\'<15 18="F-6-3B" />\'),o.6.2V.1j(o.6.1f).1j(o.6.14),o.6.1z.1c("F-2T-6-3B").1j(o.6.2V))},E=4(){o.6.16=t(\'<15 18="F-6-1l-2S"><a 18="F-16" 24="">\'+o.2.3f+"</a></15>"),o.6.1N=t(\'<15 18="F-6-1l-2S"><a 18="F-1N" 24="">\'+o.2.3g+"</a></15>"),o.6.1p=t(\'<15 18="F-6-1l" />\'),o.6.1p.2U(".F-16","26",k),o.6.1p.2U(".F-1N","26",M),o.2.2z?o.6.1p.1j(o.6.16):o.6.1p.1j(o.6.16).1j(o.6.1N),o.2.2A?t(o.2.2A).25(o.6.1p):o.6.1z.1c("F-2T-6-1l").1j(o.6.1p),A(o.2.2B?"1N":"16")},P=4(){o.5.1x(4(){7 e=t(J).1t("2M:21").1U("4A");1y 0!=e&&(""+e).Q&&t(J).1j(\'<15 18="F-3C"><3D>\'+e+"</3D></15>")})},y=4(t){o.2.1l&&r.1F(),r.27(),t.1G()},z=4(t){o.2.1l&&r.1F(),r.28(),t.1G()},k=4(t){r.29(),t.1G()},M=4(t){r.1F(),t.1G()},q=4(e){o.2.1l&&r.1F();7 i=t(e.4B),s=1X(i.1U("1C-22-j"));s!=o.8.j&&r.2p(s),e.1G()},I=4(e){7 i=o.5.Q;17"4C"==o.2.3b?(o.2.11>1&&(i=1n.3x(o.5.Q/o.2.11)),o.1k.25(e+1+o.2.3c+i),1y 0):(o.1k.1t("a").1V("8"),o.1k.1x(4(i,s){t(s).1t("a").U(e).1c("8")}),1y 0)},D=4(){G(o.2.1r){7 t="";0==o.8.j?t=o.5.U(0).12():o.8.j==x()-1&&o.1K?t=o.5.U((x()-1)*m()).12():o.8.j==o.5.Q-1&&(t=o.5.U(o.5.Q-1).12()),"10"==o.2.9?b(-t.1b,"1e",0):"1h"==o.2.9&&b(-t.1d,"1e",0)}o.1Y=!1,o.2.3l(o.5.U(o.8.j),o.2a,o.8.j)},A=4(t){o.2.2z?o.6.1p.25(o.6[t]):(o.6.1p.1t("a").1V("8"),o.6.1p.1t("a:4D(.F-"+t+")").1c("8"))},W=4(){1==x()?(o.6.1f.1c("1H"),o.6.14.1c("1H")):!o.2.1r&&o.2.2Z&&(0==o.8.j?(o.6.1f.1c("1H"),o.6.14.1V("1H")):o.8.j==x()-1?(o.6.14.1c("1H"),o.6.1f.1V("1H")):(o.6.1f.1V("1H"),o.6.14.1V("1H")))},H=4(){o.2.2C>0?4E(r.29,o.2.2C):r.29(),o.2.3i&&r.3E(4(){o.1B&&(r.1F(!0),o.2W=!0)},4(){o.2W&&(r.29(!0),o.2W=1s)})},L=4(){7 e=0;G("14"==o.2.2h)r.1j(o.5.1i().1c("F-1i"));13{r.2H(o.5.1i().1c("F-1i"));7 i=o.5.21().12();e="10"==o.2.9?-i.1b:-i.1d}b(e,"1e",0),o.2.19=!1,o.2.6=!1,o.2.1Q=!1,o.2.33&&!o.2i&&o.K.3E(4(){r.1N()},4(){7 e=0;o.5.1x(4(){e+="10"==o.2.9?t(J).2X(!0):t(J).2Q(!0)});7 i=o.2.1u/e,s="10"==o.2.9?"1b":"1d",n=i*(e-1n.2b(1X(r.R(s))));N(n)}),N()},N=4(t){1u=t?t:o.2.1u;7 e={1b:0,1d:0},i={1b:0,1d:0};"14"==o.2.2h?e=r.1t(".F-1i").21().12():i=o.5.21().12();7 s="10"==o.2.9?-e.1b:-e.1d,n="10"==o.2.9?-i.1b:-i.1d,a={2R:n};b(s,"1v",1u,a)},O=4(){o.Z={16:{x:0,y:0},1O:{x:0,y:0}},o.K.1E("4F",X)},X=4(t){G(o.1Y)t.1G();13{o.Z.2c=r.12();7 e=t.2Y;o.Z.16.x=e.1I[0].2q,o.Z.16.y=e.1I[0].2r,o.K.1E("3F",Y),o.K.1E("3G",V)}},Y=4(t){7 e=t.2Y,i=1n.2b(e.1I[0].2q-o.Z.16.x),s=1n.2b(e.1I[0].2r-o.Z.16.y);G(3*i>s&&o.2.39?t.1G():3*s>i&&o.2.3a&&t.1G(),"1S"!=o.2.9&&o.2.38){7 n=0;G("10"==o.2.9){7 r=e.1I[0].2q-o.Z.16.x;n=o.Z.2c.1b+r}13{7 r=e.1I[0].2r-o.Z.16.y;n=o.Z.2c.1d+r}b(n,"1e",0)}},V=4(t){o.K.23("3F",Y);7 e=t.2Y,i=0;G(o.Z.1O.x=e.1I[0].2q,o.Z.1O.y=e.1I[0].2r,"1S"==o.2.9){7 s=1n.2b(o.Z.16.x-o.Z.1O.x);s>=o.2.2w&&(o.Z.16.x>o.Z.1O.x?r.27():r.28(),r.1F())}13{7 s=0;"10"==o.2.9?(s=o.Z.1O.x-o.Z.16.x,i=o.Z.2c.1b):(s=o.Z.1O.y-o.Z.16.y,i=o.Z.2c.1d),!o.2.1r&&(0==o.8.j&&s>0||o.8.1o&&0>s)?b(i,"1e",3H):1n.2b(s)>=o.2.2w?(0>s?r.27():r.28(),r.1F()):b(i,"1e",3H)}o.K.23("3G",V)},B=4(){7 e=t(1R).1g(),i=t(1R).1A();(a!=e||l!=i)&&(a=e,l=i,r.2N())};17 r.2p=4(e,i){G(!o.1Y&&o.8.j!=e)G(o.1Y=!0,o.2a=o.8.j,o.8.j=0>e?x()-1:e>=x()?0:e,o.2.3k(o.5.U(o.8.j),o.2a,o.8.j),"14"==i?o.2.3m(o.5.U(o.8.j),o.2a,o.8.j):"1f"==i&&o.2.3n(o.5.U(o.8.j),o.2a,o.8.j),o.8.1o=o.8.j>=x()-1,o.2.19&&I(o.8.j),o.2.6&&W(),"1S"==o.2.9)o.2.1W&&o.K.1A()!=p()&&o.K.2o({1A:p()},o.2.2t),o.5.4G(":36").4H(o.2.1u).R({20:0}),o.5.U(o.8.j).R("20",4I).4J(o.2.1u,4(){t(J).R("20",2x),D()});13{o.2.1W&&o.K.1A()!=p()&&o.K.2o({1A:p()},o.2.2t);7 s=0,n={1b:0,1d:0};G(!o.2.1r&&o.1K&&o.8.1o)G("10"==o.2.9){7 a=o.5.U(o.5.Q-1);n=a.12(),s=o.K.1g()-a.2X()}13{7 l=o.5.Q-o.2.1a;n=o.5.U(l).12()}13 G(o.1K&&o.8.1o&&"1f"==i){7 d=1==o.2.1w?o.2.11-m():(x()-1)*m()-(o.5.Q-o.2.11),a=r.5(".F-1i").U(d);n=a.12()}13 G("14"==i&&0==o.8.j)n=r.1t("> .F-1i").U(o.2.11).12(),o.8.1o=!1;13 G(e>=0){7 c=e*m();n=o.5.U(c).12()}G("4K"!=4L n){7 g="10"==o.2.9?-(n.1b-s):-n.1d;b(g,"22",o.2.1u)}}},r.27=4(){G(o.2.1r||!o.8.1o){7 t=1X(o.8.j)+1;r.2p(t,"14")}},r.28=4(){G(o.2.1r||0!=o.8.j){7 t=1X(o.8.j)-1;r.2p(t,"1f")}},r.29=4(t){o.1B||(o.1B=4M(4(){"14"==o.2.2h?r.27():r.28()},o.2.3h),o.2.1Q&&1!=t&&A("1N"))},r.1F=4(t){o.1B&&(3I(o.1B),o.1B=1s,o.2.1Q&&1!=t&&A("16"))},r.4N=4(){17 o.8.j},r.4O=4(){17 o.5.Q},r.2N=4(){o.5.2P(r.1t(".F-1i")).2X(u()),o.K.R("1A",p()),o.2.1v||S(),o.8.1o&&(o.8.j=x()-1),o.8.j>=x()&&(o.8.1o=!0),o.2.19&&!o.2.2e&&(w(),I(o.8.j))},r.3J=4(){o.2O&&(o.2O=!1,t(".F-1i",J).1D(),o.5.1x(4(){1y 0!=t(J).1C("1T")?t(J).1U("1M",t(J).1C("1T")):t(J).3K("1M")}),1y 0!=t(J).1C("1T")?J.1U("1M",t(J).1C("1T")):t(J).3K("1M"),t(J).3L().3L(),o.6.1z&&o.6.1z.1D(),o.6.14&&o.6.14.1D(),o.6.1f&&o.6.1f.1D(),o.1k&&o.1k.1D(),t(".F-3C",J).1D(),o.6.1p&&o.6.1p.1D(),3I(o.1B),o.2.2v&&t(1R).23("3w",B))},r.4P=4(t){1y 0!=t&&(n=t),r.3J(),d()},d(),J}}(4Q);',62,301,'||settings||function|children|controls|var|active|mode||||||||||index||||||||||||||||||||||bx|if|||this|viewport||||||length|css|||eq|||||touch|horizontal|maxSlides|position|else|next|div|start|return|class|pager|minSlides|left|addClass|top|reset|prev|width|vertical|clone|append|pagerEl|auto|slideWidth|Math|last|autoEl|slideMargin|infiniteLoop|null|find|speed|ticker|moveSlides|each|void|el|height|interval|data|remove|bind|stopAuto|preventDefault|disabled|changedTouches|startSlide|carousel|animProp|style|stop|end|easing|autoControls|window|fade|origStyle|attr|removeClass|adaptiveHeight|parseInt|working|cssPrefix|zIndex|first|slide|unbind|href|html|click|goToNextSlide|goToPrevSlide|startAuto|oldIndex|abs|originalPos|buildPager|pagerCustom|nextSelector|prevSelector|autoDirection|usingCSS|for|transitionend|webkitTransitionEnd|oTransitionEnd|MSTransitionEnd|animate|goToSlide|pageX|pageY|slideSelector|adaptiveHeightSpeed|preloadImages|responsive|swipeThreshold|50|pagerSelector|autoControlsCombine|autoControlsSelector|autoStart|autoDelay|minThreshold|maxThreshold|parent|loader|prepend|100|relative|transition|none|img|redrawSlider|initialized|add|outerHeight|resetValue|item|has|delegate|directionEl|autoPaused|outerWidth|originalEvent|hideControlOnEnd|500|randomStart|captions|tickerHover|video|useCSS|visible|touchEnabled|oneToOneTouch|preventDefaultSwipeX|preventDefaultSwipeY|pagerType|pagerShortSeparator|nextText|prevText|startText|stopText|pause|autoHover|onSliderLoad|onSlideBefore|onSlideAfter|onSlideNext|onSlidePrev|bxSlider|floor|all|timing|display|iframe|load|slice|resize|ceil|translate3d|px|linear|direction|caption|span|hover|touchmove|touchend|200|clearInterval|destroySlider|removeAttr|unwrap|full|Next|Prev|Start|Stop|4e3|fn|extend|random|document|createElement|WebkitPerspective|MozPerspective|OPerspective|msPerspective|in|replace|Perspective|toLowerCase|transform|wrap|wrapper|loading|215|swing|overflow|hidden|maxWidth|margin|0px|float|listStyle|marginRight|marginBottom|absolute|block|fitVids|after|one|complete|max|apply|map|get|duration|1e3|isFunction|custom|default|link|title|currentTarget|short|not|setTimeout|touchstart|filter|fadeOut|51|fadeIn|undefined|typeof|setInterval|getCurrentSlide|getSlideCount|reloadSlider|jQuery'.split('|'),0,{}));
/*
 * ScrollToFixed
 * https://github.com/bigspotteddog/ScrollToFixed
 */
(function(a){a.isScrollToFixed=function(b){return !!a(b).data("ScrollToFixed")};a.ScrollToFixed=function(d,i){var l=this;l.$el=a(d);l.el=d;l.$el.data("ScrollToFixed",l);var c=false;var F=l.$el;var G;var D;var e;var C=0;var q=0;var j=-1;var f=-1;var t=null;var y;var g;function u(){F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed");f=-1;C=F.offset().top;q=F.offset().left;if(l.options.offsets){q+=(F.offset().left-F.position().left)}if(j==-1){j=q}G=F.css("position");c=true;if(l.options.bottom!=-1){F.trigger("preFixed.ScrollToFixed");w();F.trigger("fixed.ScrollToFixed")}}function n(){var H=l.options.limit;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function p(){return G==="fixed"}function x(){return G==="absolute"}function h(){return !(p()||x())}function w(){if(!p()){t.css({display:F.css("display"),width:F.outerWidth(true),height:F.outerHeight(true),"float":F.css("float")});cssOptions={position:"fixed",top:l.options.bottom==-1?s():"",bottom:l.options.bottom==-1?"":l.options.bottom,"margin-left":"0px"};if(!l.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);F.addClass(l.options.baseClassName);if(l.options.className){F.addClass(l.options.className)}G="fixed"}}function b(){var I=n();var H=q;if(l.options.removeOffsets){H="";I=I-C}cssOptions={position:"absolute",top:I,left:H,"margin-left":"0px",bottom:""};if(!l.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);G="absolute"}function k(){if(!h()){f=-1;t.css("display","none");F.css({width:"",position:D,left:"",top:e,"margin-left":""});F.removeClass("scroll-to-fixed-fixed");if(l.options.className){F.removeClass(l.options.className)}G=null}}function v(H){if(H!=f){F.css("left",q-H);f=H}}function s(){var H=l.options.marginTop;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function z(){if(!a.isScrollToFixed(F)){return}var J=c;if(!c){u()}var H=a(window).scrollLeft();var K=a(window).scrollTop();var I=n();if(l.options.minWidth&&a(window).width()<l.options.minWidth){if(!h()||!J){o();F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed")}}else{if(l.options.maxWidth&&a(window).width()>l.options.maxWidth){if(!h()||!J){o();F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed")}}else{if(l.options.bottom==-1){if(I>0&&K>=I-s()){if(!x()||!J){o();F.trigger("preAbsolute.ScrollToFixed");b();F.trigger("unfixed.ScrollToFixed")}}else{if(K>=C-s()){if(!p()||!J){o();F.trigger("preFixed.ScrollToFixed");w();f=-1;F.trigger("fixed.ScrollToFixed")}v(H)}else{if(!h()||!J){o();F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed")}}}}else{if(I>0){if(K+a(window).height()-F.outerHeight(true)>=I-(s()||-m())){if(p()){o();F.trigger("preUnfixed.ScrollToFixed");if(D==="absolute"){b()}else{k()}F.trigger("unfixed.ScrollToFixed")}}else{if(!p()){o();F.trigger("preFixed.ScrollToFixed");w()}v(H);F.trigger("fixed.ScrollToFixed")}}else{v(H)}}}}}function m(){if(!l.options.bottom){return 0}return l.options.bottom}function o(){var H=F.css("position");if(H=="absolute"){F.trigger("postAbsolute.ScrollToFixed")}else{if(H=="fixed"){F.trigger("postFixed.ScrollToFixed")}else{F.trigger("postUnfixed.ScrollToFixed")}}}var B=function(H){if(F.is(":visible")){c=false;z()}};var E=function(H){z()};var A=function(){var I=document.body;if(document.createElement&&I&&I.appendChild&&I.removeChild){var K=document.createElement("div");if(!K.getBoundingClientRect){return null}K.innerHTML="x";K.style.cssText="position:fixed;top:100px;";I.appendChild(K);var L=I.style.height,M=I.scrollTop;I.style.height="3000px";I.scrollTop=500;var H=K.getBoundingClientRect().top;I.style.height=L;var J=(H===100);I.removeChild(K);I.scrollTop=M;return J}return null};var r=function(H){H=H||window.event;if(H.preventDefault){H.preventDefault()}H.returnValue=false};l.init=function(){l.options=a.extend({},a.ScrollToFixed.defaultOptions,i);l.$el.css("z-index",l.options.zIndex);t=a("<div />");G=F.css("position");D=F.css("position");e=F.css("top");if(h()){l.$el.after(t)}a(window).bind("resize.ScrollToFixed",B);a(window).bind("scroll.ScrollToFixed",E);if(l.options.preFixed){F.bind("preFixed.ScrollToFixed",l.options.preFixed)}if(l.options.postFixed){F.bind("postFixed.ScrollToFixed",l.options.postFixed)}if(l.options.preUnfixed){F.bind("preUnfixed.ScrollToFixed",l.options.preUnfixed)}if(l.options.postUnfixed){F.bind("postUnfixed.ScrollToFixed",l.options.postUnfixed)}if(l.options.preAbsolute){F.bind("preAbsolute.ScrollToFixed",l.options.preAbsolute)}if(l.options.postAbsolute){F.bind("postAbsolute.ScrollToFixed",l.options.postAbsolute)}if(l.options.fixed){F.bind("fixed.ScrollToFixed",l.options.fixed)}if(l.options.unfixed){F.bind("unfixed.ScrollToFixed",l.options.unfixed)}if(l.options.spacerClass){t.addClass(l.options.spacerClass)}F.bind("resize.ScrollToFixed",function(){t.height(F.height())});F.bind("scroll.ScrollToFixed",function(){F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed");z()});F.bind("detach.ScrollToFixed",function(H){r(H);F.trigger("preUnfixed.ScrollToFixed");k();F.trigger("unfixed.ScrollToFixed");a(window).unbind("resize.ScrollToFixed",B);a(window).unbind("scroll.ScrollToFixed",E);F.unbind(".ScrollToFixed");t.remove();l.$el.removeData("ScrollToFixed")});B()};l.init()};a.ScrollToFixed.defaultOptions={marginTop:0,limit:0,bottom:-1,zIndex:1000,baseClassName:"scroll-to-fixed-fixed"};a.fn.scrollToFixed=function(b){return this.each(function(){(new a.ScrollToFixed(this,b))})}})(jQuery);


/*!
 * Scripts
 */
(function($) {


head.ready(function() {
	(function(globals){
		"use strict";
		globals.GLOB = {};
	}( (1,eval)('this') ));
	var Default = {
		utils : {
			links : function(){
				$('a[rel*=external]').on('click',function(e){
					e.preventDefault();
					window.open($(this).attr('href'));
				});
			},
			mails : function(){
				$('.email').each(function(index){
					em = $(this).text().replace('//','@').replace(/\//g,'.');
					$(this).text(em).attr('href','mailto:'+em);
				});
			},
			forms : function(){
				$('.input-a input, .form-a input, #search input, #team input, #nav-mobile form input').each(function(){
					if($(this).val() !== '') $(this).parent().children('label').css('margin-top','-3000em');
				}).on('focus',function(){
					$(this).parent().children('label').css('margin-top','-3000em');
				}).on('blur',function(){
					if($(this).val() === '') $(this).parent().children('label').css('margin-top',0);
				});
				xa = $('fieldset > *');
				xb = parseInt(xa.size());
				xa.each(function(){ $(this).css({'z-index':xb}); xb--; });
				$('input[type="checkbox"][checked], input[type="radio"][checked]').each(function(){ $(this).attr('checked',true).parent('label').addClass('active'); });
				$('input[type="checkbox"]:not([checked]), input[type="radio"]:not([checked])').attr('checked',false);
				$('.checklist-a label').append('<div class="fit-a"></div>').each(function(){$(this).addClass($(this).children('input').attr('type'));});
				$('.checklist-a input').addClass('hidden').on('click',function(){
					if($(this).parent().hasClass('radio')) {
						$(this).parent('label').parents('p,ul').find('label').removeClass('active');
					}
					$(this).parent('label').toggleClass('active');
				});
				if(!$.browser.mobile){
					$('select').semanticSelect();
				}
			},
			date : function(){
				$('#footer .date').text((new Date).getFullYear());
			},
			heights : function(){
				$('.news-b').each(function(){ $(this).children('article').matchHeight(); });
				$('#nav > ul.a > li > div').each(function(){ $(this).children('ul').matchHeight(); });
			},
			responsive : function(){
				if($.browser.mobile){
					$('#nav > ul.c').on('click',function(){ $(this).toggleClass('toggle'); });
				}
			},
			miscellaneous : function(){
                if($('body').hasClass('single-employee')){
                    $('#aside').each(function(){ $(this).css('top',$('#top').outerHeight()+$('#cv').outerHeight()+$('#featured').outerHeight()+$('#team').outerHeight()+102); });
                } else {
                    $('#aside').each(function(){ $(this).css('top',$('#top').outerHeight()+$('#featured').outerHeight()+$('#team').outerHeight()+102); });
                }
				$('figure#featured, #cv, #featured article').each(function(){ $(this).css({'background-image':'url("'+$(this).find('img').attr('src')+'")'}); });
				$('#search, #top').append('<div class="fit-a"></div>');
				$('#nav > ul.a > li.a > a, #search .close, #search > .fit-a').on('click',function(){ $('#search').toggleClass('active'); return false });
				$('.news-b.b').wrap('<div class="news-b-wrapper"></div>');
				$('.news-b article .link-a').parent().addClass('has-link-a');
				$('#featured article').parent().wrapInner('<div class="inner"></div>');
                $('#featured > .inner').each(function(){ $(this).bxSlider({ pause: 10000, pager: true, controls: false, useCSS: false, adaptiveHeight: true, auto: true }); });
				$('#map article').append('<a class="close">Close</a>');
				$('#aside').scrollToFixed({ marginTop: 140 });
				$('#top').after('<ul id="nav-mobile"></ul>');
				$('#nav ul.a > li').each(function(){ $(this).clone().appendTo($('#nav-mobile')); });
				$('#nav ul.b > li').each(function(){ $(this).clone().appendTo($('#nav-mobile')); });
				$('#top > .fit-a').on('click',function(){ $(this).parent().toggleClass('toggle'); return false });
				$('#search').each(function(){ $(this).clone().removeAttr('id').appendTo('#nav-mobile'); });
				$('#nav-mobile label').attr('for','mobile-search');
				$('#nav-mobile input').attr('name','mobile-search').attr('id','mobile-search').each(function(){ $(this).attr('placeholder',$(this).parent().children('label').text()); });;
				//$('#nav-mobile div').wrapInner('<ul></ul>').children('ul').unwrap().children('ul').wrap('<li></li>').before('<a>-</a>');
				$('#nav-mobile li > div, #nav-mobile li > ul').parent().append('<span class="fit-a"></span>');
				$('#nav-mobile li > .fit-a').on('click',function(){ $(this).parent().toggleClass('toggle'); return false });
			}
		},
		ie : {
			css : function() {
				if($.browser.msie && parseInt($.browser.version,10) < 9){
					$('input[placeholder], textarea[placeholder]').placeholder();
					$(':last-child').addClass('last-child');
				}
			}
		}

	};

	Default.utils.links();
	Default.utils.mails();
	Default.utils.date();
	Default.utils.heights();
	Default.utils.miscellaneous();
	Default.utils.forms();
	Default.utils.responsive();
	Default.ie.css();
});

})(jQuery);

/*!*/
(function($) {

    var allowedToFetch = true;
    var noMorePosts = false;
    var allowedToFetchEmployees = true;
    var fetch_this_year_only = null;

    $(document).on('ready', function(){

        //Check in which country page are we to display the correct language switchers
        var current_country = $('#nav ul.c li#current_country_page span').text();
        current_country = current_country.replace(/ /g,'');
        current_country = current_country.replace(/\r?\n|\r/g,'');
        var lang = $('html').attr('lang');
        if(current_country == 'Denmark' || current_country=='Danmark' || current_country=='Dänemark' /*|| current_country== 'Taani' */){
            $('#nav ul.d li.dk_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Estonia' || current_country== 'Eesti' || current_country=='Эстония' /*|| current_country== 'Estland'*/ ){
            $('#nav ul.d li.et_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Latvia' || current_country=='Latvija' || current_country== 'Латвия'){
            $('#nav ul.d li.lv_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Lithuania' || current_country=='Lietuva'){
            $('#nav ul.d li.lt_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        }

        //Set cookie based on country, when you change a language
        var countryCookie = {
            NAME : 'country_code',
            VALUE : '',
            EXPIRE_DAYS : 7,
            PATH : '/',
            setCookie : function(){
                $.cookie(this.NAME, this.VALUE, { expires : this.EXPIRE_DAYS, path : this.PATH } );
            },
            exists : function(){
                this.setCookie();
            },
            init : function(){
                $('.switch_country').on('click', function(){
                    countryCookie.VALUE = $(this).data('country-code');
                    countryCookie.exists();
                });
            }
        }; countryCookie.init();


        //Prevent the header search form to submit to empty search page
        $('#search').submit(function(){
            return false;
        });


        //Detect email in footer and create mailto link
        var text = $('#footer div');
        var regEx = /(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)/;
        text.filter(function() {
            return $(this).html().match(regEx);
        }).each(function() {
            $(this).html($(this).html().replace(regEx, "<a href=\"mailto:$1\">$1</a>"));
        });

        //Split sub menu items into more lists
        var navigationParent = $('ul.a > li.menu-item-has-children div');

        if(navigationParent.length > 0){

            $.each(navigationParent, function(){

                if($(this).parent('li').hasClass('no-split')){
                    return;
                }

                var navigationUl = $(this).find('ul');

                var childItems = navigationUl.children();

                //remove the old list
                navigationUl.remove();

                //append all the list items again without a wrapper
                $(this).append(childItems);

                //wrap every nth list item in a seperate lists
                var x = childItems.length;
                var liBreakpoint;

                switch(true) {
                    case(x <= 4):
                        liBreakpoint = 1;
                        break;
                    case(x <= 8):
                        liBreakpoint = 2;
                        break;
                    case(x <= 12):
                        liBreakpoint = 3;
                        break;
                    case(x <= 16):
                        liBreakpoint = 4;
                        break;
                    case(x <= 20):
                        liBreakpoint = 5;
                        break;
                    case(x <= 24):
                        liBreakpoint = 6;
                        break;
                    case(x <= 28):
                        liBreakpoint = 7;
                        break;
                    case(x <= 32):
                        liBreakpoint = 8;
                        break;
                    case(x <= 36):
                        liBreakpoint = 9;
                        break;
                    case(x <= 40):
                        liBreakpoint = 10;
                        break;
                    case(x <= 44):
                        liBreakpoint = 11;
                        break;
                    case(x <= 48):
                        liBreakpoint = 12;
                        break;
                    case(x <= 52):
                        liBreakpoint = 13;
                        break;
                    case(x <= 56):
                        liBreakpoint = 14;
                        break;
                    case(x <= 60):
                        liBreakpoint = 15;
                        break;
                    case(x <= 64):
                        liBreakpoint = 16;
                        break;
                    case(x <= 68):
                        liBreakpoint = 17;
                        break;
                }

                for (var i = 0; i < childItems.length; i += liBreakpoint) {
                    var $ul = $("<ul></ul>");
                    childItems.slice(i, i + liBreakpoint).wrapAll($ul);
                }

            });

        }

        //This conflicts with around line 135 in scripts.js and thats why it has been commented out
        //$('#nav-mobile div').wrapInner('<ul></ul>').children('ul').unwrap().children('ul').wrap('<li></li>').before('<a>-</a>');


        var checkForDevices = {
            window: $('window'),
            body: $('body'),
            tabletBreakPoint: '(max-width: 1000px)',
            mobileBreakPoint: '(max-width: 760px)',
            currentlyTablet : false,
            currentlyMobile : false,
            doACheck : function(){
                var tablet = window.matchMedia(this.tabletBreakPoint);
                var mobile = window.matchMedia(this.mobileBreakPoint);
                if(mobile.matches) {
                    this.currentlyMobile = true;
                    this.body.addClass('mobile-view');
                    this.body.removeClass('tablet-view');
                    this.body.removeClass('desktop-view');
                }
                else if(tablet.matches) {
                    this.currentlyTablet = true;
                    this.body.addClass('tablet-view');
                    this.body.removeClass('mobile-view');
                    this.body.removeClass('desktop-view');
                }
                else {
                    this.currentlyMobile = false;
                    this.body.removeClass('tablet-view');
                    this.body.removeClass('mobile-view');
                    this.body.addClass('desktop-view');
                }
            },
            init: function(){
                this.doACheck();
                var resizeTimer;
                $(window).on('resize', function(){
                    if (resizeTimer) {
                        clearTimeout(resizeTimer); //cancel the previous timer.
                        resizeTimer = null;
                    }
                    resizeTimer = setTimeout(function(){
                        checkForDevices.doACheck();
                    }, 100);
                });
            }
        }; checkForDevices.init();


        //START / Inpage navigation functions

        //Build the inpage navigation
        var listsItems = $('.ip-nav-heading');

        var inpageList = $('.inpage-nav-ul');


        $.each(listsItems, function( index ){

            var listItemText    = $(this).html();

            var anchor          = $(this).attr('id');

            //Check if the anchor is in the content and give it a unique ID
            if(typeof anchor == 'undefined' && $(this).parent('#content').length > 0 || $(this).parent('section').length > 0){

                $(this).attr('id', 'ip-heading-content-'+index);

                anchor          = 'ip-heading-content-'+index;

            }

            if(inpageList.hasClass('direct-links')){
                var first_char = listItemText.substr(1, 1);
                if(first_char == 'a') {
                    var newListItemHtml = '<li class="no-underline">' + listItemText + '</li>';
                }
                else{
                    var newListItemHtml = '<li><a href="#'+anchor+'">'+listItemText+'</a></li>';
                }
            }
            else{
                var newListItemHtml = '<li><a href="#'+anchor+'">'+listItemText+'</a></li>';
            }


            inpageList.append(newListItemHtml);

        });

        //Give inpage nav a smooth scroll to anchor
        var anchorLinks = $('.inpage-nav-ul li a');

        anchorLinks.on('click', function(){

            var anchorHref = $(this).attr('href');
            var anchorPoint = $(anchorHref);

            $('html, body').animate({

                'scrollTop' : anchorPoint.offset().top - 150

            }, 500);

            return false;
        });


        //Make the inpage navigation stick on further scroll
        var inpageNavigation = $('nav#aside');
        var header = $('#top');
        var content = $('#content');
        var lastScrollTop = 0;
        var employeePage = false;
        if($('body').hasClass('single-employee')){
            employeePage = true;
        }
        $(window).on('scroll', function( event ){

            if($('body').find(inpageNavigation).length > 0){
                var windowScrollTop = $(this).scrollTop();

                var headerHeight = header.outerHeight() - 80;

                var difference = content.offset().top - (windowScrollTop + headerHeight);

                //Check for downward scroll
                if(difference <= 0 && windowScrollTop > lastScrollTop){

                    inpageNavigation.css({
                        'position'  : 'fixed',
                        'top'       : $('#top').outerHeight()+20
                    });

                } else if(difference > 0 && windowScrollTop < lastScrollTop){

                    if(employeePage == true){
                        inpageNavigation.css({
                            'top' : $('#top').outerHeight()+$('#cv').outerHeight()+102,
                            'position' : 'absolute'
                        });
                    } else {
                        inpageNavigation.css({
                            'top' : $('#top').outerHeight()+$('#featured').outerHeight()+$('#team').outerHeight()+102,
                            'position' : 'absolute'
                        });

                    }

                }

                lastScrollTop = windowScrollTop;
            }

        });
        //END / Inpage navigation functions


    });//END / Document Ready

    $(window).load(function(){//START / WINDOW LOAD

        var multipleNavHeadings = false;

        var contentNavHeadings = $('#content').find('.ip-nav-heading');

        var sections = $('#root .ip-nav-heading');

        var anchorArray = [];

        //Check if there is more than one nav heading in the content
        if(contentNavHeadings.length > 1){

            multipleNavHeadings = true;

        }

        $.each(sections , function(){

            anchorArray.push($(this));

        });

        if ($('#cv').length){
            // Manipulate CV before size
            var image_url = $('#cv').css('background-image'), image;
            var imgWidth;
            var imgHeight;

            // Remove url() or in case of Chrome url("")
            image_url = image_url.match(/^url\("?(.+?)"?\)$/);

            if (image_url[1]) {
                image_url = image_url[1];
                image = new Image();

                // just in case it is not already loaded
                $(image).load(function () {
                    imgWidth = image.width;
                    imgHeight = image.height;

                    resizeBeforeImageContainer();

                    $(window).resize(function(){
                        resizeBeforeImageContainer();
                    });
                });

                image.src = image_url;
            }
        }

        function resizeBeforeImageContainer() {
            var cvHeight            = $('#cv').outerHeight();
            var documentWidth       = $(window).width();
            var imgScalePercentage  = (cvHeight * 100) / imgHeight;
            var actualImgWidth      = (imgScalePercentage * imgWidth) / 100;
            var imgWidthPercentage  = (actualImgWidth * 100) / documentWidth;
            var beforeSelectorWidth = 100 - imgWidthPercentage + 0.1;

            if(documentWidth > 1750) {
                $('#before_cv').show();
                $('#cv').addClass('NotFullSized');
                $('#before_cv').css('width', beforeSelectorWidth + '%');
            }
            else {
                $('#before_cv').hide();
                $('#cv').removeClass('NotFullSized');

            }
        }

        // DOTS SCROLLBAR 
        var num_sections = 0;
        var section_ids = [];
        var section_positions = [];
        //We check all the different sections existing in the front-page and save their info into arrays (position and id)
        //If they don't have id we will add one
        $( ".frontpage" ).children().each(function() {
            num_sections++;
            if(this.id){
                section_ids.push(this.id);
                var position = $(this).position();
                position = position.top - 210;
                section_positions.push(position);
                $(this).attr("data-position", position);
            }
            else{
                var future_id = 'scroll-section'+num_sections;
                $(this).attr("id", future_id);
                section_ids.push(future_id);
                var position = $(this).position();
                position = position.top - 210;
                section_positions.push(position);
                $(this).attr("data-position", position);
            }
        });
        //We create a dot for each of the sections with the related data
        for (var i = 0; i < section_ids.length; i++) {
            if(i == 0){
                $(".scroll_dots").append('<div data-element="'+section_ids[i]+'" class="single_dot active"><span></span></a>');
            }else{
                $(".scroll_dots").append('<div data-element="'+section_ids[i]+'" class="single_dot"><span></span></a>');
            }
        };
        //We take the position of the element we want to scroll to and move the window position there
        $(document).on("click touchstart", '.single_dot', function () {
            $(".single_dot").removeClass("active");
            $(this).addClass("active");
            var element = $(this).data('element');
            var new_position = $("#"+element).data("position");
            window.scrollTo(0, new_position);
        });
        //This change the color of the dot if we are scrolling in its section
        $(window).scroll(function () {
            var currentPos = $(this).scrollTop();
            currentPos +=150;
            for (var i = 0; i < section_positions.length; i++) {

                if( currentPos > section_positions[i]){
                    $(".single_dot").removeClass("active");
                    $(".single_dot[data-element="+section_ids[i]+"]").addClass("active");
                }
            }
        });


            
        $(window).on('scroll', function() {

            if($('body').find($('nav#aside')).length > 0) {
                var anchor;

                var anchorsCount = anchorArray.length;

                var currentSection;

                var sectionBottom;

                $.each(anchorArray, function (index) {

                    var windowScrollTop = $(window).scrollTop();

                    var parent = anchorArray[index].parents('section');
                    if ($('body').hasClass('single-employee')) {
                        parent = anchorArray[index].parents('article');
                    }

                    var parentOffset = parent.offset().top;

                    var parentHeight = parent.height();

                    anchor = anchorArray[index];

                    var listItemHref = anchor.attr('id');

                    if (multipleNavHeadings == true) {

                        var howManyInContent = $('[id^="ip-heading-content-"]').length;

                        $.each(contentNavHeadings, function (index) {

                            var thisParentOffset = $(this).offset().top;

                            var nextItem = '';

                            if (howManyInContent >= index) {

                                nextItem = $('#ip-heading-content-' + (index));

                            } else {

                                nextItem = $(this).next('.ip-nav-heading');

                            }

                            parentOffset = thisParentOffset;

                            parentHeight = nextItem.offset().top;

                        });

                    }

                    sectionBottom = parentOffset + parentHeight;

                    if (windowScrollTop > anchorArray[index].offset().top - 170 && windowScrollTop < sectionBottom) {

                        currentSection = anchorArray[index];

                        $('.inpage-nav-ul li').removeClass('active');

                        $('a[href$=' + listItemHref + ']').parent('li').addClass('active');

                    } else if ($(window).scrollTop() + $(window).height() == $(document).height()) {

                        //On Scrolled to bottom highlight the last of the list items
                        $('.inpage-nav-ul li').removeClass('active');
                        $('.inpage-nav-ul li:last-child').addClass('active');


                    } else {

                        $('a[href$=' + listItemHref + ']').parent('li').removeClass('active');

                    }


                });
            }

        });

        ///// Newsletter list view \\\\\

        var footerHeight    = 385;
        var articleArray    = [];
        allowedToFetch      = true;

        update_article_array();


        $('.btn_year').on('click', function() {
            var $news_container = $('#news_list');
            var $this = $(this);

            $('.btn_year').removeClass('active');
            $this.addClass('active');

            $news_container.empty();
            fetch_this_year_only = $this.find('a').text();

            fetch_posts(5, fetch_already_shown_posts(), fetch_this_year_only, current_language);
            update_article_array();

            return false;
        });

        $(window).on('scroll', function() {
            if($('body').find($('.news-a')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = articleArray[0];

                $.each(articleArray, function (index) {
                    var $this = $(this);
                    var articleSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - articleSectionHeight) {
                        $currentSection = $this;
                    }
                });
            if(fetch_this_year_only === null){
                var year = $currentSection.data('article-year');


                $('#aside ul li').removeClass('active');
                $('#aside ul #nav_btn_' + year).addClass('active');
            }
                //// Fetch posts when bottom of page is reached \\\\

                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetch) {
                    allowedToFetch = false;

                    fetch_posts(5, fetch_already_shown_posts(), fetch_this_year_only, current_language);
                    update_article_array();
                }
            }
        });

        function update_article_array() {
            var articles        = $('#news_list').find('article');
            articleArray    = [];

            $.each(articles , function(){
                articleArray.push($(this));
            });
        }








        ///// Knowledge list view \\\\\

        var footerHeight        = 385;
        var knowledgeArray      = [];
        allowedToFetchKnowledge = true;
        fetch_all_knowledge     = true;

        update_knowledge_array();


        $('.btn_area').on('click', function() {

            fetch_all_knowledge = false;
            
            var $knowledge_container = $('#knowledge_list');
            var $this = $(this);

            $('.btn_area').removeClass('active');
            $this.addClass('active');

            $knowledge_container.empty();
            fetch_this_area_only = $this.find('a').text();

            fetch_knowledge_posts(5, fetch_already_shown_knowledge_posts(),fetch_this_area_only, current_language);
            update_knowledge_array();

            return false;
        });

        $(window).on('scroll', function() {
            if($('body').find($('.knowledge-a')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = knowledgeArray[0];

                $.each(knowledgeArray, function (index) {
                    var $this = $(this);
                    var articleSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - articleSectionHeight) {
                        $currentSection = $this;
                    }
                });
                
                if(fetch_all_knowledge){
                    fetch_this_area_only = 'all';
                }

                //// Fetch posts when bottom of page is reached \\\\

                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetchKnowledge) {
                    allowedToFetchKnowledge = false;

                    fetch_knowledge_posts(5, fetch_already_shown_knowledge_posts(), fetch_this_area_only, current_language);
                    update_knowledge_array();
                }
            }
        });

        
        function update_knowledge_array() {
            var articles        = $('#knowledge_list').find('article');
            knowledgeArray    = [];

            $.each(articles , function(){
                knowledgeArray.push($(this));
            });
        }










        ///// Employee list view \\\\\

        var footerHeight    = 385;
        var employeeArray    = [];
        allowedToFetchEmployees      = true;

        update_employee_array();

        $(window).on('scroll', function() {
            if($('body').find($('.employee-list')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = employeeArray[0];

                $.each(employeeArray, function (index) {
                    var $this = $(this);
                    var employeeSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - employeeSectionHeight) {
                        $currentSection = $this;
                    }
                });

                //// Fetch posts when bottom of page is reached \\\\
                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetchEmployees) {
                    allowedToFetchEmployees = false;
                    fetch_employees(12, fetch_already_shown_employees(), current_language);
                    update_employee_array();
                }
            }
        });

        function update_employee_array() {
            var employees        = $('.employee-list').find('li');
            employeeArray    = [];

            $.each(employees , function(){
                employeeArray.push($(this));
            });

        }















    });//END / WINDOW LOAD

    //Newsletter spinner
    function fetch_already_shown_posts()
    {
        var $news_container = $('#news_list');
        var array_of_shown_post_ids = [];

        $.each($news_container.find('article'), function() {
            var $this   = $(this);
            var thisID  = $this.data('article-id');
            array_of_shown_post_ids.push(thisID);
        });



        return array_of_shown_post_ids;
    }

    function fetch_posts($num_of_posts, $exclude_posts, $year, $language)
    {
        $('#icon_spinner').show();

        $.ajax(
        {
            type: 'post',
            url: template_dir+'/template_parts/fetch_posts.php',
            data:
            {
                "num_of_posts"  : $num_of_posts,
                "exclude_posts" : $exclude_posts,
                "year"          : $year,
                "language"      : $language
            },
            success: function(data)
            {
                if(data != '') {
                    allowedToFetch = true;
                }
                $('#icon_spinner').hide();
                $('#news_list').append(data);

            }
        });
    }

    //Knowledge spinner
    function fetch_already_shown_knowledge_posts()
    {
        var $knowledge_container = $('#knowledge_list');
        var array_of_shown_knowledge_post_ids = [];

        $.each($knowledge_container.find('article'), function() {
            var $this   = $(this);
            var thisID  = $this.data('article-id');
            array_of_shown_knowledge_post_ids.push(thisID);
        });

        return array_of_shown_knowledge_post_ids;
    }

    function fetch_knowledge_posts($num_of_posts, $exclude_posts, $area, $language)
    {
        $('#icon_spinner').show();
        $.ajax(
        {
            type: 'post',
            url: template_dir+'/template_parts/fetch_knowledge_posts.php',
            data:
            {
                "num_of_posts"  : $num_of_posts,
                "exclude_posts" : $exclude_posts,
                "area"          : $area,
                "language"      : $language
            },
            success: function(data)
            {
                if(data != '') {
                    allowedToFetchKnowledge = true;
                }
                $('#icon_spinner').hide();
                $('#knowledge_list').append(data);

            }
        });
    }



    function fetch_already_shown_employees()
    {
        var $employee_container = $('.employee-list');
        var array_of_shown_employee_ids = [];

        $.each($employee_container.find('li'), function() {
            var $this   = $(this);
            var thisID  = $this.attr('data-employee-id');

            array_of_shown_employee_ids.push(thisID);
        });

        return array_of_shown_employee_ids;
    }
    function fetch_employees($num_of_employees, $exclude_employees, $language)
    {
        var searchParamsInput   = $('.search-parameters');
        var employeeNameSearch  = '';
        var employeeSpecSearch  = '';
        var employeeJobTitleSearch  = '';
        var employeeCountrySearch  = '';

        if(searchParamsInput.data('name-param')){
            employeeNameSearch =  searchParamsInput.data('name-param');
        }

        if(searchParamsInput.data('specialty-param')){
            employeeSpecSearch =  searchParamsInput.data('specialty-param');
        }

        if(searchParamsInput.data('jobtitle-param')){
            employeeJobTitleSearch =  searchParamsInput.data('jobtitle-param');
        }

        if(searchParamsInput.data('country-param')){
            employeeCountrySearch =  searchParamsInput.data('country-param');
        }

        $('#icon_spinner').show();
        $.ajax(
            {
                type: 'post',
                url: template_dir+'/template_parts/fetch_employees.php',
                data:
                {
                    "num_of_employees"  : $num_of_employees,
                    "exclude_employees" : $exclude_employees,
                    "language"          : $language,
                    "searchName"        : employeeNameSearch,
                    "searchSpec"        : employeeSpecSearch,
                    "searchJobTitle"    : employeeJobTitleSearch,
                    "searchCountry"     : employeeCountrySearch
                },
                success: function(data)
                {
                    if(data != '') {
                        allowedToFetchEmployees = true;
                    }
                    $('#icon_spinner').hide();
                    $('.employee-list').append(data);

                }
            });
    }

})(jQuery);
var a,b;
(function($) {

    var selector = '';
    if(isHandheld()){
        $(window).load(function(){
            selector = $(document).find('input#mobile-search');
            $.autocompleteSearch.init(selector);
        });
    } else {
        $(document).on('ready',function() {
            selector = $("input#sa");
            $.autocompleteSearch.init(selector);
        });
    }

    $.autocompleteSearch = {
        action: 'search_autocomplete',
        language: '',
        languageCode: '',
        init : function(selector){
            this.language = $('.language_code_search').val();
            this.languageCode = $('.icl_language_code').val();
            selector.autocomplete({
                minLength: 2,
                appendTo: '.autocomplete_container',
                search: function(event, ui) {
                    $('.autocomplete_container #icon_spinner').show();
                },
                response: function(event, ui) {
                    $('.autocomplete_container #icon_spinner').hide();
                },
                source: function(req, response){
                    req.lang = $.autocompleteSearch.language;
                    req.lang_code = $.autocompleteSearch.languageCode;
                    $.getJSON(MyAcSearch.url+'?callback=?&action='+ $.autocompleteSearch.action+'&format=json', req, response);
                },
                select: function(event, ui) {
                    window.location.href=ui.item.link;
                }
            }).data("ui-autocomplete")._renderItem = function (a, b) {
                return $("<li></li>")
                    .data("item.autocomplete", b)
                    .append('<a href="#"> ' + b.label + '</a>  ')
                    .appendTo(a);
            };
        }
    };

    function isHandheld(){
        var handheld, body;
        handheld = false;
        var mq = window.matchMedia( "(max-width: 780px)" );
        if (mq.matches) {
            handheld = true;
        }
        return handheld;
    }


})(jQuery);
(function($) {

    $(document).on('ready', function(){

        //Check the page for map to see if this script is at all needed
        if(mapExists) {

            //Makes the map auto zoom and pan based on markers
            var bounds = new google.maps.LatLngBounds();

            //Sets the stylers of the Google Maps Canvas
            var stylers = get_the_stylers();

            //Set the map options
            var mapOptions = get_map_options(stylers);

            //Create the map
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            //Build Each Marker and place them on the map
            var markers = $('input.marker');

            //Places markers on the map
            var markerArray = place_markers(markers, map, bounds);

            //Adds click events on markers and info windows
            add_click_events_to_markers(map, markerArray);

            //Adjusts the zoom based on the placed markers
            map.fitBounds(bounds);

            //Hack: adjusts the center of the map after it loads to make the Tallinn info window fit in the map
            adjust_center(map);

        }

    });

    //Checking if the page has a map
    function mapExists(){
        return $('html, body').find('#map-canvas').length > 0;
    }

    //Adds stylers to the map
    function get_the_stylers(){
        return [
            {
                "stylers": [
                    {"saturation": -100}
                ]
            },
            {
                "featureType": "landscape.natural",
                "stylers": [
                    {"saturation": -100},
                    {"lightness": 100}
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {"visibility": "off"}
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {"lightness": 29}
                ]
            }
        ];
    }

    //Set the map options
    function get_map_options(stylers){
        return {
            disableDefaultUI: true,
            zoomControl: false,
            mapTypeControl: false,
            panControl: false,
            styles: stylers,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    }

    //Places the markers on the map
    function place_markers(markers, map, bounds){
        var markerArray = [];

        $.each(markers, function( index ){
            var marker = $(this);
            var markerJSON = marker.val();
            var markerInfo = JSON.parse(markerJSON);
            var markerLatitude = markerInfo.latitude;
            var markerLongitude = markerInfo.longitude;
            var officeArray = [];
            var locationMarker = new google.maps.Marker({
                position: new google.maps.LatLng( markerLatitude,markerLongitude ),
                map: map,
                icon: template_dir+'/img/pin.png'
            });
            bounds.extend(locationMarker.position);

            //Build the HTML info window
            var markerInfoHtml = get_marker_html(markerInfo);

            //Assign the info content to the right info window
            var infoWindow = new google.maps.InfoWindow({
                content: markerInfoHtml
            });
            officeArray['marker'] = locationMarker;
            officeArray['info'] = infoWindow;
            markerArray[index] = officeArray;
        });

        return markerArray;
    }

    //Adds click events to the markers. Clicking outside an open info window will close it
    function add_click_events_to_markers(map, markerArray){
        $.each(markerArray, function(){
            var singleMarker = this.marker;
            var singleMarkerInfo = this.info;
            google.maps.event.addListener(singleMarker, 'click', function() {
                singleMarkerInfo.open(map,singleMarker);
            });
            //Close all info windows on click outside on the map
            google.maps.event.addListener(map, 'click', function() {
                singleMarkerInfo.close(map,singleMarker);
            });
        });
    }


    //Builds the html for the markers
    function get_marker_html(markerInfo){
        return  '<article class="vcard" itemscope itemtype="http://schema.org/Organization" style="bottom:-25px;left: 10px;">' +
                    '<h3 class="fn org" itemprop="name">'+ markerInfo.name +'</h3>' +
                    '<p class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">' +
                        '<span class="street-address"itemprop="streetAddress" style="display:block;">'+ markerInfo.street +'</span>' +
                        '<span class="postal-code" itemprop="postalCode" style="display:block;">'+ markerInfo.zipcode +'</span>' +
                        '<span class="locality" itemprop="addressLocality" style="display:block;">'+ markerInfo.city +'</span> ' +
                        '<span class="country-name" itemprop="addressCountry" style="display:block;">'+ markerInfo.country +'</span>' +
                    '</p>' +
                    '<p>Telephone: <span class="tel" itemprop="telephone">'+ markerInfo.phonenumber +'</span>' +
                        '<br>'+
                        '<a href="'+ markerInfo.url +'">Go to NJORD '+markerInfo.country+'</a>' +
                    '</p>' +
                '</article>';
    }

    //The top info window goes outside the map so we have to adjust the center a litte down to make it fit
    function adjust_center(map){
        var listener = google.maps.event.addListener(map, "idle", function() {
            var center = map.getCenter();
            map.setCenter({
                lat: center.lat()+1.1,
                lng: center.lng()
            });
            google.maps.event.removeListener(listener);
        });
    }

})(jQuery);
/**
 * HTML24 Cookie Accept Script (requires jQuery)
 * - Shows the accept cookie box only if cookies have not yet been accepted
 * - Adds a class to the HTML element, when the cookie acceptance box is shown, to allow special styling
 */

(function($){

    /**
     * The cookie key used for storing the accept cookies value
     * @type {string}
     */
    var COOKIE_KEY = "acceptCookies";

    /**
     * The amount of days the cookie will be saved
     * @type {number}
     */
    var COOKIE_EXPIRE_DAYS = 60;

    /**
     * The jQuery selector that selects the cookie acceptance box
     * @type {string}
     */
    var CONTAINER_SELECTOR = ".cookie-box";

    /**
     * The jQuery selector for the accept cookies button
     * @type {string}
     */
    var ACCEPT_BUTTON_SELECTOR = ".cookie-allow";

    /**
     * The CSS class that is applied to the HTML element, when the cookie acceptance box is shown
     * @type {string}
     */
    var VISIBLE_CLASS = "cookies-visible";

    /**
     * The jQuery instance of the cookie box
     * @type {jQuery}
     */
    var $cookieBox = null;

    /**
     * Checks if cookies has been accepted
     * @returns {boolean}
     */
    function hasAcceptedCookies()
    {
        var acceptCookies;
        acceptCookies = readCookie(COOKIE_KEY);
        return acceptCookies === "true";
    }

    /**
     * Changes the cookie acceptance value
     * @param {boolean} value
     */
    function setAcceptedCookies(value)
    {
        eraseCookie(COOKIE_KEY);
        createCookie(COOKIE_KEY,"true",COOKIE_EXPIRE_DAYS);
    }

    /**
     * Shows the cookie acceptance box
     */
    function showCookieAcceptBox()
    {
        getCookieAcceptBox().show();
        $("html").addClass(VISIBLE_CLASS);
    }

    /**
     * Hides the cookie acceptance box
     */
    function hideCookieAcceptBox()
    {
        getCookieAcceptBox().hide();
        $("html").removeClass(VISIBLE_CLASS);
    }

    /**
     * Gets the cookie acceptance boxs jQuery instance, or creates it if it doesnt exist
     * @returns {jQuery}
     */
    function getCookieAcceptBox()
    {

        if($cookieBox === null)
        {
            $cookieBox = $(CONTAINER_SELECTOR);
        }
        return $cookieBox;
    }

    /**
     * Creates a new cookie
     * @param {string} name
     * @param {string} value
     * @param {int} days
     */
    function createCookie(name,value,days) {
        var expires = "";
        if(days)
        {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }
        document.cookie = name+"="+value+expires+"; path=/";
    }

    /**
     * Gets a cookie
     * Returns null if cookie does not exist
     * @param {string} name
     * @returns {string|null}
     */
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    /**
     * Deletes a cookie
     * @param {string} name
     */
    function eraseCookie(name) {
        createCookie(name,"",-1);
    }


    /**
     * Document ready handler
     */
    $(document).ready(function()
    {
        if(hasAcceptedCookies())
        {
            hideCookieAcceptBox();
            $('.cookie-box').css( "display", "none" );
        }

        //Show cookie box if user has not accepted
        else if(!hasAcceptedCookies())
        {
            showCookieAcceptBox();
            $('.cookie-box').css( "bottom", "0" );
            
        }

        // Eventlistener for accept cookies
        getCookieAcceptBox().on("click",ACCEPT_BUTTON_SELECTOR,function(e)
        {
            e.preventDefault();
            setAcceptedCookies(true);
            hideCookieAcceptBox();
        });
    });

})(jQuery);

(function($) {
		    	
	function validForm() {
    	var response = grecaptcha.getResponse();
		if(response.length == 0){
			return false;
			
		}
		else {
			return true;
			
		}
	}


	$(document).on("click",".newsletterformButton",function() {
		var inputValid = [false, false, false];
		var recaptchaValid = false;
		var inputFields = $('.validInput');
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
  		var emailInput = $('.email').val();

		    	
	 	for (var i = 0; i < inputFields.length; i++) {
	 		   	 	
	 		if (inputFields[i].value == "") {
	 			$(inputFields[i]).attr('style', 'border-color: red');
	 			    	
	 		} else {
	 			inputValid[i] = true;
	 			$(inputFields[i]).attr('style', 'border-color: limegreen');
	 			    	
	 		}
  		}

  		if (testEmail.test(emailInput)) {
  			inputValid[2] = true;
		 	$(inputFields[2]).attr('style', 'border-color: limegreen');   	
  		} else {
  			inputValid[2] = false;
		  	$(inputFields[2]).attr('style', 'border-color: red');  	
  		}

		if (validForm()) {
			$('.recaptchaResponse iframe').css({"border": "2px solid limegreen"});
			recaptchaValid = true;
		}
		else {
			$('.recaptchaResponse iframe').css({"border": "2px solid red"});
		}
		
		if (inputValid && recaptchaValid) {
			$('#newsletterForm').submit();
		}	
		
		// else {
		// 	console.log("not");
		// 	// $('#newsletterForm').submit();
		// }
	});
})(jQuery);

    	
        
(function($) {
	
	$(window).on('load', function(){
		// Find all YouTube videos
		var $allVideos = $("iframe[src*='//www.youtube.com']");

			var iframeParent = $allVideos.parent();
			$fluidEl = $(iframeParent);

		// Figure out and save aspect ratio for each video
		$allVideos.each(function() {

		  $(this)
			.data('aspectRatio', this.height / this.width)

			// and remove the hard coded width/height
			.removeAttr('height')
			
			.removeAttr('width');

		});
		
		// When the window is resized
		$(window).resize(function() {

		  var newWidth = $fluidEl.width();

		  // Resize all videos according to their own aspect ratio
		  $allVideos.each(function() {

			var $el = $(this);
			$el
			  .width(newWidth)
			  .height(newWidth * $el.data('aspectRatio'));

		  });

		// Kick off one resize to fix all videos on page load
		}).resize();	
	});
	
})(jQuery);
(function($) {
	$(document).on("click",".button",function() {
		$("li").slideDown("slow");
		$(".button").hide();
	});
	    	
})(jQuery);
(function($) {
	$(document).on("click",".knowledge-link",function() {
		$('.knowledge-content').removeClass('knowledge--open');
		$(this).find('.knowledge-content').toggleClass('knowledge--open');

	});
	    	
})(jQuery);

/*
 * Replace all SVG images with inline SVG
 */
jQuery(function ($) {
	jQuery('img.svg').each(function(){
	    var $img = jQuery(this);
	    var imgID = $img.attr('id');
	    var imgClass = $img.attr('class');
	    var imgURL = $img.attr('src');

	    jQuery.get(imgURL, function(data) {
	        // Get the SVG tag, ignore the rest
	        var $svg = jQuery(data).find('svg');

	        // Add replaced image's ID to the new SVG
	        if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        // Add replaced image's classes to the new SVG
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }

	        // Remove any invalid XML tags as per http://validator.w3.org
	        $svg = $svg.removeAttr('xmlns:a');

	        // Replace image with new SVG
	        $img.replaceWith($svg);

	    }, 'xml');

	});

});