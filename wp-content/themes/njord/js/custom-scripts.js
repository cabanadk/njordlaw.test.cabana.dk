(function($) {

    var allowedToFetch = true;
    var noMorePosts = false;
    var allowedToFetchEmployees = true;
    var fetch_this_year_only = null;

    $(document).on('ready', function(){

        //Check in which country page are we to display the correct language switchers
        var current_country = $('#nav ul.c li#current_country_page span').text();
        current_country = current_country.replace(/ /g,'');
        current_country = current_country.replace(/\r?\n|\r/g,'');
        var lang = $('html').attr('lang');
        if(current_country == 'Denmark' || current_country=='Danmark' || current_country=='Dänemark' /*|| current_country== 'Taani' */){
            $('#nav ul.d li.dk_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Estonia' || current_country== 'Eesti' || current_country=='Эстония' /*|| current_country== 'Estland'*/ ){
            $('#nav ul.d li.et_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Latvia' || current_country=='Latvija' || current_country== 'Латвия'){
            $('#nav ul.d li.lv_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        } else if(current_country == 'Lithuania' || current_country=='Lietuva'){
            $('#nav ul.d li.lt_lang').removeClass('hidden');
            $('#nav ul.d li.en').removeClass('hidden');
            $('#nav ul.d li.'+lang).addClass('active');
            $('.scroll_dots').show();
        }

        //Set cookie based on country, when you change a language
        var countryCookie = {
            NAME : 'country_code',
            VALUE : '',
            EXPIRE_DAYS : 7,
            PATH : '/',
            setCookie : function(){
                $.cookie(this.NAME, this.VALUE, { expires : this.EXPIRE_DAYS, path : this.PATH } );
            },
            exists : function(){
                this.setCookie();
            },
            init : function(){
                $('.switch_country').on('click', function(){
                    countryCookie.VALUE = $(this).data('country-code');
                    countryCookie.exists();
                });
            }
        }; countryCookie.init();


        //Prevent the header search form to submit to empty search page
        $('#search').submit(function(){
            return false;
        });


        //Detect email in footer and create mailto link
        var text = $('#footer div');
        var regEx = /(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)/;
        text.filter(function() {
            return $(this).html().match(regEx);
        }).each(function() {
            $(this).html($(this).html().replace(regEx, "<a href=\"mailto:$1\">$1</a>"));
        });

        //Split sub menu items into more lists
        var navigationParent = $('ul.a > li.menu-item-has-children div');

        if(navigationParent.length > 0){

            $.each(navigationParent, function(){

                if($(this).parent('li').hasClass('no-split')){
                    return;
                }

                var navigationUl = $(this).find('ul');

                var childItems = navigationUl.children();

                //remove the old list
                navigationUl.remove();

                //append all the list items again without a wrapper
                $(this).append(childItems);

                //wrap every nth list item in a seperate lists
                var x = childItems.length;
                var liBreakpoint;

                switch(true) {
                    case(x <= 4):
                        liBreakpoint = 1;
                        break;
                    case(x <= 8):
                        liBreakpoint = 2;
                        break;
                    case(x <= 12):
                        liBreakpoint = 3;
                        break;
                    case(x <= 16):
                        liBreakpoint = 4;
                        break;
                    case(x <= 20):
                        liBreakpoint = 5;
                        break;
                    case(x <= 24):
                        liBreakpoint = 6;
                        break;
                    case(x <= 28):
                        liBreakpoint = 7;
                        break;
                    case(x <= 32):
                        liBreakpoint = 8;
                        break;
                    case(x <= 36):
                        liBreakpoint = 9;
                        break;
                    case(x <= 40):
                        liBreakpoint = 10;
                        break;
                    case(x <= 44):
                        liBreakpoint = 11;
                        break;
                    case(x <= 48):
                        liBreakpoint = 12;
                        break;
                    case(x <= 52):
                        liBreakpoint = 13;
                        break;
                    case(x <= 56):
                        liBreakpoint = 14;
                        break;
                    case(x <= 60):
                        liBreakpoint = 15;
                        break;
                    case(x <= 64):
                        liBreakpoint = 16;
                        break;
                    case(x <= 68):
                        liBreakpoint = 17;
                        break;
                }

                for (var i = 0; i < childItems.length; i += liBreakpoint) {
                    var $ul = $("<ul></ul>");
                    childItems.slice(i, i + liBreakpoint).wrapAll($ul);
                }

            });

        }

        //This conflicts with around line 135 in scripts.js and thats why it has been commented out
        //$('#nav-mobile div').wrapInner('<ul></ul>').children('ul').unwrap().children('ul').wrap('<li></li>').before('<a>-</a>');


        var checkForDevices = {
            window: $('window'),
            body: $('body'),
            tabletBreakPoint: '(max-width: 1000px)',
            mobileBreakPoint: '(max-width: 760px)',
            currentlyTablet : false,
            currentlyMobile : false,
            doACheck : function(){
                var tablet = window.matchMedia(this.tabletBreakPoint);
                var mobile = window.matchMedia(this.mobileBreakPoint);
                if(mobile.matches) {
                    this.currentlyMobile = true;
                    this.body.addClass('mobile-view');
                    this.body.removeClass('tablet-view');
                    this.body.removeClass('desktop-view');
                }
                else if(tablet.matches) {
                    this.currentlyTablet = true;
                    this.body.addClass('tablet-view');
                    this.body.removeClass('mobile-view');
                    this.body.removeClass('desktop-view');
                }
                else {
                    this.currentlyMobile = false;
                    this.body.removeClass('tablet-view');
                    this.body.removeClass('mobile-view');
                    this.body.addClass('desktop-view');
                }
            },
            init: function(){
                this.doACheck();
                var resizeTimer;
                $(window).on('resize', function(){
                    if (resizeTimer) {
                        clearTimeout(resizeTimer); //cancel the previous timer.
                        resizeTimer = null;
                    }
                    resizeTimer = setTimeout(function(){
                        checkForDevices.doACheck();
                    }, 100);
                });
            }
        }; checkForDevices.init();


        //START / Inpage navigation functions

        //Build the inpage navigation
        var listsItems = $('.ip-nav-heading');

        var inpageList = $('.inpage-nav-ul');


        $.each(listsItems, function( index ){

            var listItemText    = $(this).html();

            var anchor          = $(this).attr('id');

            //Check if the anchor is in the content and give it a unique ID
            if(typeof anchor == 'undefined' && $(this).parent('#content').length > 0 || $(this).parent('section').length > 0){

                $(this).attr('id', 'ip-heading-content-'+index);

                anchor          = 'ip-heading-content-'+index;

            }

            if(inpageList.hasClass('direct-links')){
                var first_char = listItemText.substr(1, 1);
                if(first_char == 'a') {
                    var newListItemHtml = '<li class="no-underline">' + listItemText + '</li>';
                }
                else{
                    var newListItemHtml = '<li><a href="#'+anchor+'">'+listItemText+'</a></li>';
                }
            }
            else{
                var newListItemHtml = '<li><a href="#'+anchor+'">'+listItemText+'</a></li>';
            }


            inpageList.append(newListItemHtml);

        });

        //Give inpage nav a smooth scroll to anchor
        var anchorLinks = $('.inpage-nav-ul li a');

        anchorLinks.on('click', function(){

            var anchorHref = $(this).attr('href');
            var anchorPoint = $(anchorHref);

            $('html, body').animate({

                'scrollTop' : anchorPoint.offset().top - 150

            }, 500);

            return false;
        });


        //Make the inpage navigation stick on further scroll
        var inpageNavigation = $('nav#aside');
        var header = $('#top');
        var content = $('#content');
        var lastScrollTop = 0;
        var employeePage = false;
        if($('body').hasClass('single-employee')){
            employeePage = true;
        }
        $(window).on('scroll', function( event ){

            if($('body').find(inpageNavigation).length > 0){
                var windowScrollTop = $(this).scrollTop();

                var headerHeight = header.outerHeight() - 80;

                var difference = content.offset().top - (windowScrollTop + headerHeight);

                //Check for downward scroll
                if(difference <= 0 && windowScrollTop > lastScrollTop){

                    inpageNavigation.css({
                        'position'  : 'fixed',
                        'top'       : $('#top').outerHeight()+20
                    });

                } else if(difference > 0 && windowScrollTop < lastScrollTop){

                    if(employeePage == true){
                        inpageNavigation.css({
                            'top' : $('#top').outerHeight()+$('#cv').outerHeight()+102,
                            'position' : 'absolute'
                        });
                    } else {
                        inpageNavigation.css({
                            'top' : $('#top').outerHeight()+$('#featured').outerHeight()+$('#team').outerHeight()+102,
                            'position' : 'absolute'
                        });

                    }

                }

                lastScrollTop = windowScrollTop;
            }

        });
        //END / Inpage navigation functions


    });//END / Document Ready

    $(window).load(function(){//START / WINDOW LOAD

        var multipleNavHeadings = false;

        var contentNavHeadings = $('#content').find('.ip-nav-heading');

        var sections = $('#root .ip-nav-heading');

        var anchorArray = [];

        //Check if there is more than one nav heading in the content
        if(contentNavHeadings.length > 1){

            multipleNavHeadings = true;

        }

        $.each(sections , function(){

            anchorArray.push($(this));

        });

        if ($('#cv').length){
            // Manipulate CV before size
            var image_url = $('#cv').css('background-image'), image;
            var imgWidth;
            var imgHeight;

            // Remove url() or in case of Chrome url("")
            image_url = image_url.match(/^url\("?(.+?)"?\)$/);

            if (image_url[1]) {
                image_url = image_url[1];
                image = new Image();

                // just in case it is not already loaded
                $(image).load(function () {
                    imgWidth = image.width;
                    imgHeight = image.height;

                    resizeBeforeImageContainer();

                    $(window).resize(function(){
                        resizeBeforeImageContainer();
                    });
                });

                image.src = image_url;
            }
        }

        function resizeBeforeImageContainer() {
            var cvHeight            = $('#cv').outerHeight();
            var documentWidth       = $(window).width();
            var imgScalePercentage  = (cvHeight * 100) / imgHeight;
            var actualImgWidth      = (imgScalePercentage * imgWidth) / 100;
            var imgWidthPercentage  = (actualImgWidth * 100) / documentWidth;
            var beforeSelectorWidth = 100 - imgWidthPercentage + 0.1;

            if(documentWidth > 1750) {
                $('#before_cv').show();
                $('#cv').addClass('NotFullSized');
                $('#before_cv').css('width', beforeSelectorWidth + '%');
            }
            else {
                $('#before_cv').hide();
                $('#cv').removeClass('NotFullSized');

            }
        }

        // DOTS SCROLLBAR 
        var num_sections = 0;
        var section_ids = [];
        var section_positions = [];
        //We check all the different sections existing in the front-page and save their info into arrays (position and id)
        //If they don't have id we will add one
        $( ".frontpage" ).children().each(function() {
            num_sections++;
            if(this.id){
                section_ids.push(this.id);
                var position = $(this).position();
                position = position.top - 210;
                section_positions.push(position);
                $(this).attr("data-position", position);
            }
            else{
                var future_id = 'scroll-section'+num_sections;
                $(this).attr("id", future_id);
                section_ids.push(future_id);
                var position = $(this).position();
                position = position.top - 210;
                section_positions.push(position);
                $(this).attr("data-position", position);
            }
        });
        //We create a dot for each of the sections with the related data
        for (var i = 0; i < section_ids.length; i++) {
            if(i == 0){
                $(".scroll_dots").append('<div data-element="'+section_ids[i]+'" class="single_dot active"><span></span></a>');
            }else{
                $(".scroll_dots").append('<div data-element="'+section_ids[i]+'" class="single_dot"><span></span></a>');
            }
        };
        //We take the position of the element we want to scroll to and move the window position there
        $(document).on("click touchstart", '.single_dot', function () {
            $(".single_dot").removeClass("active");
            $(this).addClass("active");
            var element = $(this).data('element');
            var new_position = $("#"+element).data("position");
            window.scrollTo(0, new_position);
        });
        //This change the color of the dot if we are scrolling in its section
        $(window).scroll(function () {
            var currentPos = $(this).scrollTop();
            currentPos +=150;
            for (var i = 0; i < section_positions.length; i++) {

                if( currentPos > section_positions[i]){
                    $(".single_dot").removeClass("active");
                    $(".single_dot[data-element="+section_ids[i]+"]").addClass("active");
                }
            }
        });


            
        $(window).on('scroll', function() {

            if($('body').find($('nav#aside')).length > 0) {
                var anchor;

                var anchorsCount = anchorArray.length;

                var currentSection;

                var sectionBottom;

                $.each(anchorArray, function (index) {

                    var windowScrollTop = $(window).scrollTop();

                    var parent = anchorArray[index].parents('section');
                    if ($('body').hasClass('single-employee')) {
                        parent = anchorArray[index].parents('article');
                    }

                    var parentOffset = parent.offset().top;

                    var parentHeight = parent.height();

                    anchor = anchorArray[index];

                    var listItemHref = anchor.attr('id');

                    if (multipleNavHeadings == true) {

                        var howManyInContent = $('[id^="ip-heading-content-"]').length;

                        $.each(contentNavHeadings, function (index) {

                            var thisParentOffset = $(this).offset().top;

                            var nextItem = '';

                            if (howManyInContent >= index) {

                                nextItem = $('#ip-heading-content-' + (index));

                            } else {

                                nextItem = $(this).next('.ip-nav-heading');

                            }

                            parentOffset = thisParentOffset;

                            parentHeight = nextItem.offset().top;

                        });

                    }

                    sectionBottom = parentOffset + parentHeight;

                    if (windowScrollTop > anchorArray[index].offset().top - 170 && windowScrollTop < sectionBottom) {

                        currentSection = anchorArray[index];

                        $('.inpage-nav-ul li').removeClass('active');

                        $('a[href$=' + listItemHref + ']').parent('li').addClass('active');

                    } else if ($(window).scrollTop() + $(window).height() == $(document).height()) {

                        //On Scrolled to bottom highlight the last of the list items
                        $('.inpage-nav-ul li').removeClass('active');
                        $('.inpage-nav-ul li:last-child').addClass('active');


                    } else {

                        $('a[href$=' + listItemHref + ']').parent('li').removeClass('active');

                    }


                });
            }

        });

        ///// Newsletter list view \\\\\

        var footerHeight    = 385;
        var articleArray    = [];
        allowedToFetch      = true;

        update_article_array();


        $('.btn_year').on('click', function() {
            var $news_container = $('#news_list');
            var $this = $(this);

            $('.btn_year').removeClass('active');
            $this.addClass('active');

            $news_container.empty();
            fetch_this_year_only = $this.find('a').text();

            fetch_posts(5, fetch_already_shown_posts(), fetch_this_year_only, current_language);
            update_article_array();

            return false;
        });

        $(window).on('scroll', function() {
            if($('body').find($('.news-a')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = articleArray[0];

                $.each(articleArray, function (index) {
                    var $this = $(this);
                    var articleSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - articleSectionHeight) {
                        $currentSection = $this;
                    }
                });
            if(fetch_this_year_only === null){
                var year = $currentSection.data('article-year');


                $('#aside ul li').removeClass('active');
                $('#aside ul #nav_btn_' + year).addClass('active');
            }
                //// Fetch posts when bottom of page is reached \\\\

                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetch) {
                    allowedToFetch = false;

                    fetch_posts(5, fetch_already_shown_posts(), fetch_this_year_only, current_language);
                    update_article_array();
                }
            }
        });

        function update_article_array() {
            var articles        = $('#news_list').find('article');
            articleArray    = [];

            $.each(articles , function(){
                articleArray.push($(this));
            });
        }








        ///// Knowledge list view \\\\\

        var footerHeight        = 385;
        var knowledgeArray      = [];
        allowedToFetchKnowledge = true;
        fetch_all_knowledge     = true;

        update_knowledge_array();


        $('.btn_area').on('click', function() {

            fetch_all_knowledge = false;
            
            var $knowledge_container = $('#knowledge_list');
            var $this = $(this);

            $('.btn_area').removeClass('active');
            $this.addClass('active');

            $knowledge_container.empty();
            fetch_this_area_only = $this.find('a').text();

            fetch_knowledge_posts(5, fetch_already_shown_knowledge_posts(),fetch_this_area_only, current_language);
            update_knowledge_array();

            return false;
        });

        $(window).on('scroll', function() {
            if($('body').find($('.knowledge-a')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = knowledgeArray[0];

                $.each(knowledgeArray, function (index) {
                    var $this = $(this);
                    var articleSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - articleSectionHeight) {
                        $currentSection = $this;
                    }
                });
                
                if(fetch_all_knowledge){
                    fetch_this_area_only = 'all';
                }

                //// Fetch posts when bottom of page is reached \\\\

                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetchKnowledge) {
                    allowedToFetchKnowledge = false;

                    fetch_knowledge_posts(5, fetch_already_shown_knowledge_posts(), fetch_this_area_only, current_language);
                    update_knowledge_array();
                }
            }
        });

        
        function update_knowledge_array() {
            var articles        = $('#knowledge_list').find('article');
            knowledgeArray    = [];

            $.each(articles , function(){
                knowledgeArray.push($(this));
            });
        }










        ///// Employee list view \\\\\

        var footerHeight    = 385;
        var employeeArray    = [];
        allowedToFetchEmployees      = true;

        update_employee_array();

        $(window).on('scroll', function() {
            if($('body').find($('.employee-list')).length > 0){

                var windowScrollTop = $(window).scrollTop();
                var $currentSection = employeeArray[0];

                $.each(employeeArray, function (index) {
                    var $this = $(this);
                    var employeeSectionHeight = $this.height();

                    if (windowScrollTop > $this.offset().top - employeeSectionHeight) {
                        $currentSection = $this;
                    }
                });

                //// Fetch posts when bottom of page is reached \\\\
                if ($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight && allowedToFetchEmployees) {
                    allowedToFetchEmployees = false;
                    fetch_employees(12, fetch_already_shown_employees(), current_language);
                    update_employee_array();
                }
            }
        });

        function update_employee_array() {
            var employees        = $('.employee-list').find('li');
            employeeArray    = [];

            $.each(employees , function(){
                employeeArray.push($(this));
            });

        }















    });//END / WINDOW LOAD

    //Newsletter spinner
    function fetch_already_shown_posts()
    {
        var $news_container = $('#news_list');
        var array_of_shown_post_ids = [];

        $.each($news_container.find('article'), function() {
            var $this   = $(this);
            var thisID  = $this.data('article-id');
            array_of_shown_post_ids.push(thisID);
        });



        return array_of_shown_post_ids;
    }

    function fetch_posts($num_of_posts, $exclude_posts, $year, $language)
    {
        $('#icon_spinner').show();

        $.ajax(
        {
            type: 'post',
            url: template_dir+'/template_parts/fetch_posts.php',
            data:
            {
                "num_of_posts"  : $num_of_posts,
                "exclude_posts" : $exclude_posts,
                "year"          : $year,
                "language"      : $language
            },
            success: function(data)
            {
                if(data != '') {
                    allowedToFetch = true;
                }
                $('#icon_spinner').hide();
                $('#news_list').append(data);

            }
        });
    }

    //Knowledge spinner
    function fetch_already_shown_knowledge_posts()
    {
        var $knowledge_container = $('#knowledge_list');
        var array_of_shown_knowledge_post_ids = [];

        $.each($knowledge_container.find('article'), function() {
            var $this   = $(this);
            var thisID  = $this.data('article-id');
            array_of_shown_knowledge_post_ids.push(thisID);
        });

        return array_of_shown_knowledge_post_ids;
    }

    function fetch_knowledge_posts($num_of_posts, $exclude_posts, $area, $language)
    {
        $('#icon_spinner').show();
        $.ajax(
        {
            type: 'post',
            url: template_dir+'/template_parts/fetch_knowledge_posts.php',
            data:
            {
                "num_of_posts"  : $num_of_posts,
                "exclude_posts" : $exclude_posts,
                "area"          : $area,
                "language"      : $language
            },
            success: function(data)
            {
                if(data != '') {
                    allowedToFetchKnowledge = true;
                }
                $('#icon_spinner').hide();
                $('#knowledge_list').append(data);

            }
        });
    }



    function fetch_already_shown_employees()
    {
        var $employee_container = $('.employee-list');
        var array_of_shown_employee_ids = [];

        $.each($employee_container.find('li'), function() {
            var $this   = $(this);
            var thisID  = $this.attr('data-employee-id');

            array_of_shown_employee_ids.push(thisID);
        });

        return array_of_shown_employee_ids;
    }
    function fetch_employees($num_of_employees, $exclude_employees, $language)
    {
        var searchParamsInput   = $('.search-parameters');
        var employeeNameSearch  = '';
        var employeeSpecSearch  = '';
        var employeeJobTitleSearch  = '';
        var employeeCountrySearch  = '';

        if(searchParamsInput.data('name-param')){
            employeeNameSearch =  searchParamsInput.data('name-param');
        }

        if(searchParamsInput.data('specialty-param')){
            employeeSpecSearch =  searchParamsInput.data('specialty-param');
        }

        if(searchParamsInput.data('jobtitle-param')){
            employeeJobTitleSearch =  searchParamsInput.data('jobtitle-param');
        }

        if(searchParamsInput.data('country-param')){
            employeeCountrySearch =  searchParamsInput.data('country-param');
        }

        $('#icon_spinner').show();
        $.ajax(
            {
                type: 'post',
                url: template_dir+'/template_parts/fetch_employees.php',
                data:
                {
                    "num_of_employees"  : $num_of_employees,
                    "exclude_employees" : $exclude_employees,
                    "language"          : $language,
                    "searchName"        : employeeNameSearch,
                    "searchSpec"        : employeeSpecSearch,
                    "searchJobTitle"    : employeeJobTitleSearch,
                    "searchCountry"     : employeeCountrySearch
                },
                success: function(data)
                {
                    if(data != '') {
                        allowedToFetchEmployees = true;
                    }
                    $('#icon_spinner').hide();
                    $('.employee-list').append(data);

                }
            });
    }

})(jQuery);