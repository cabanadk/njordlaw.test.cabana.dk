var a,b;
(function($) {

    var selector = '';
    if(isHandheld()){
        $(window).load(function(){
            selector = $(document).find('input#mobile-search');
            $.autocompleteSearch.init(selector);
        });
    } else {
        $(document).on('ready',function() {
            selector = $("input#sa");
            $.autocompleteSearch.init(selector);
        });
    }

    $.autocompleteSearch = {
        action: 'search_autocomplete',
        language: '',
        languageCode: '',
        init : function(selector){
            this.language = $('.language_code_search').val();
            this.languageCode = $('.icl_language_code').val();
            selector.autocomplete({
                minLength: 2,
                appendTo: '.autocomplete_container',
                search: function(event, ui) {
                    $('.autocomplete_container #icon_spinner').show();
                },
                response: function(event, ui) {
                    $('.autocomplete_container #icon_spinner').hide();
                },
                source: function(req, response){
                    req.lang = $.autocompleteSearch.language;
                    req.lang_code = $.autocompleteSearch.languageCode;
                    $.getJSON(MyAcSearch.url+'?callback=?&action='+ $.autocompleteSearch.action+'&format=json', req, response);
                },
                select: function(event, ui) {
                    window.location.href=ui.item.link;
                }
            }).data("ui-autocomplete")._renderItem = function (a, b) {
                return $("<li></li>")
                    .data("item.autocomplete", b)
                    .append('<a href="#"> ' + b.label + '</a>  ')
                    .appendTo(a);
            };
        }
    };

    function isHandheld(){
        var handheld, body;
        handheld = false;
        var mq = window.matchMedia( "(max-width: 780px)" );
        if (mq.matches) {
            handheld = true;
        }
        return handheld;
    }


})(jQuery);