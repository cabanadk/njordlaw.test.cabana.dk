(function($) {
		    	
	function validForm() {
    	var response = grecaptcha.getResponse();
		if(response.length == 0){
			return false;
			
		}
		else {
			return true;
			
		}
	}


	$(document).on("click",".newsletterformButton",function() {
		var inputValid = [false, false, false];
		var recaptchaValid = false;
		var inputFields = $('.validInput');
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
  		var emailInput = $('.email').val();

		    	
	 	for (var i = 0; i < inputFields.length; i++) {
	 		   	 	
	 		if (inputFields[i].value == "") {
	 			$(inputFields[i]).attr('style', 'border-color: red');
	 			    	
	 		} else {
	 			inputValid[i] = true;
	 			$(inputFields[i]).attr('style', 'border-color: limegreen');
	 			    	
	 		}
  		}

  		if (testEmail.test(emailInput)) {
  			inputValid[2] = true;
		 	$(inputFields[2]).attr('style', 'border-color: limegreen');   	
  		} else {
  			inputValid[2] = false;
		  	$(inputFields[2]).attr('style', 'border-color: red');  	
  		}

		if (validForm()) {
			$('.recaptchaResponse iframe').css({"border": "2px solid limegreen"});
			recaptchaValid = true;
		}
		else {
			$('.recaptchaResponse iframe').css({"border": "2px solid red"});
		}
		
		if (inputValid && recaptchaValid) {
			$('#newsletterForm').submit();
		}	
		
		// else {
		// 	console.log("not");
		// 	// $('#newsletterForm').submit();
		// }
	});
})(jQuery);

    	
        