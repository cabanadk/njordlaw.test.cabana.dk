(function($) {

    $(document).on('ready', function(){

        //Check the page for map to see if this script is at all needed
        if(mapExists) {

            //Makes the map auto zoom and pan based on markers
            var bounds = new google.maps.LatLngBounds();

            //Sets the stylers of the Google Maps Canvas
            var stylers = get_the_stylers();

            //Set the map options
            var mapOptions = get_map_options(stylers);

            //Create the map
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            //Build Each Marker and place them on the map
            var markers = $('input.marker');

            //Places markers on the map
            var markerArray = place_markers(markers, map, bounds);

            //Adds click events on markers and info windows
            add_click_events_to_markers(map, markerArray);

            //Adjusts the zoom based on the placed markers
            map.fitBounds(bounds);

            //Hack: adjusts the center of the map after it loads to make the Tallinn info window fit in the map
            adjust_center(map);

        }

    });

    //Checking if the page has a map
    function mapExists(){
        return $('html, body').find('#map-canvas').length > 0;
    }

    //Adds stylers to the map
    function get_the_stylers(){
        return [
            {
                "stylers": [
                    {"saturation": -100}
                ]
            },
            {
                "featureType": "landscape.natural",
                "stylers": [
                    {"saturation": -100},
                    {"lightness": 100}
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {"visibility": "off"}
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {"lightness": 29}
                ]
            }
        ];
    }

    //Set the map options
    function get_map_options(stylers){
        return {
            disableDefaultUI: true,
            zoomControl: false,
            mapTypeControl: false,
            panControl: false,
            styles: stylers,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    }

    //Places the markers on the map
    function place_markers(markers, map, bounds){
        var markerArray = [];

        $.each(markers, function( index ){
            var marker = $(this);
            var markerJSON = marker.val();
            var markerInfo = JSON.parse(markerJSON);
            var markerLatitude = markerInfo.latitude;
            var markerLongitude = markerInfo.longitude;
            var officeArray = [];
            var locationMarker = new google.maps.Marker({
                position: new google.maps.LatLng( markerLatitude,markerLongitude ),
                map: map,
                icon: template_dir+'/img/pin.png'
            });
            bounds.extend(locationMarker.position);

            //Build the HTML info window
            var markerInfoHtml = get_marker_html(markerInfo);

            //Assign the info content to the right info window
            var infoWindow = new google.maps.InfoWindow({
                content: markerInfoHtml
            });
            officeArray['marker'] = locationMarker;
            officeArray['info'] = infoWindow;
            markerArray[index] = officeArray;
        });

        return markerArray;
    }

    //Adds click events to the markers. Clicking outside an open info window will close it
    function add_click_events_to_markers(map, markerArray){
        $.each(markerArray, function(){
            var singleMarker = this.marker;
            var singleMarkerInfo = this.info;
            google.maps.event.addListener(singleMarker, 'click', function() {
                singleMarkerInfo.open(map,singleMarker);
            });
            //Close all info windows on click outside on the map
            google.maps.event.addListener(map, 'click', function() {
                singleMarkerInfo.close(map,singleMarker);
            });
        });
    }


    //Builds the html for the markers
    function get_marker_html(markerInfo){
        return  '<article class="vcard" itemscope itemtype="http://schema.org/Organization" style="bottom:-25px;left: 10px;">' +
                    '<h3 class="fn org" itemprop="name">'+ markerInfo.name +'</h3>' +
                    '<p class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">' +
                        '<span class="street-address"itemprop="streetAddress" style="display:block;">'+ markerInfo.street +'</span>' +
                        '<span class="postal-code" itemprop="postalCode" style="display:block;">'+ markerInfo.zipcode +'</span>' +
                        '<span class="locality" itemprop="addressLocality" style="display:block;">'+ markerInfo.city +'</span> ' +
                        '<span class="country-name" itemprop="addressCountry" style="display:block;">'+ markerInfo.country +'</span>' +
                    '</p>' +
                    '<p>Telephone: <span class="tel" itemprop="telephone">'+ markerInfo.phonenumber +'</span>' +
                        '<br>'+
                        '<a href="'+ markerInfo.url +'">Go to NJORD '+markerInfo.country+'</a>' +
                    '</p>' +
                '</article>';
    }

    //The top info window goes outside the map so we have to adjust the center a litte down to make it fit
    function adjust_center(map){
        var listener = google.maps.event.addListener(map, "idle", function() {
            var center = map.getCenter();
            map.setCenter({
                lat: center.lat()+1.1,
                lng: center.lng()
            });
            google.maps.event.removeListener(listener);
        });
    }

})(jQuery);