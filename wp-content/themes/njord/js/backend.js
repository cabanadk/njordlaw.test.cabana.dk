(function($) {


    $(window).on('load', function(){//START / WINDOW LOAD

        var checkCookies = $(this);

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
            }
            return "";
        }

        // var adminLanguageCode = getCookie('_icl_current_language');
        var adminLanguageCode = icl_this_lang;

        //Find the options items in the admin menu
        var optionsItems = $('.toplevel_page_acf-options-general > ul.wp-submenu > li');

            $.each(optionsItems, function(){

                var languageSpecificAddress = "Address "+adminLanguageCode;

                var languageSpecificFooter = "Footer "+adminLanguageCode;

                var languageSpecificPageLinksOptions = "Page links " + adminLanguageCode;

                var languageSpecificNewsletterPopup = "Newsletter Popup " + adminLanguageCode;

                if ($(this).text().indexOf("Footer") >= 0){

                    if($(this).text() == languageSpecificFooter){

                        $(this).css('display','block');

                    } else {

                        $(this).css('display','none');

                    }

                }

                if ($(this).text().indexOf("Page links") >= 0){

                    if($(this).text() == languageSpecificPageLinksOptions){

                        $(this).css('display','block');

                    } else {

                        $(this).css('display','none');

                    }

                }

                if ($(this).text().indexOf("Address") >= 0){

                    if($(this).text() == languageSpecificAddress){

                        $(this).css('display','block');

                    } else {

                        $(this).css('display','none');

                    }

                }

                if ($(this).text().indexOf("Newsletter Popup") >= 0) {

                    if ($(this).text() == languageSpecificNewsletterPopup) {

                        $(this).css('display', 'block');

                    } else {

                        $(this).css('display', 'none');

                    }

                }

                if ($(this).text().indexOf("General") >= 0){ $(this).css('display','none');}

            });





    });//END / WINDOW LOAD


})(jQuery);
