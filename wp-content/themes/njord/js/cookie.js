/**
 * HTML24 Cookie Accept Script (requires jQuery)
 * - Shows the accept cookie box only if cookies have not yet been accepted
 * - Adds a class to the HTML element, when the cookie acceptance box is shown, to allow special styling
 */

(function($){

    /**
     * The cookie key used for storing the accept cookies value
     * @type {string}
     */
    var COOKIE_KEY = "acceptCookies";

    /**
     * The amount of days the cookie will be saved
     * @type {number}
     */
    var COOKIE_EXPIRE_DAYS = 60;

    /**
     * The jQuery selector that selects the cookie acceptance box
     * @type {string}
     */
    var CONTAINER_SELECTOR = ".cookie-box";

    /**
     * The jQuery selector for the accept cookies button
     * @type {string}
     */
    var ACCEPT_BUTTON_SELECTOR = ".cookie-allow";

    /**
     * The CSS class that is applied to the HTML element, when the cookie acceptance box is shown
     * @type {string}
     */
    var VISIBLE_CLASS = "cookies-visible";

    /**
     * The jQuery instance of the cookie box
     * @type {jQuery}
     */
    var $cookieBox = null;

    /**
     * Checks if cookies has been accepted
     * @returns {boolean}
     */
    function hasAcceptedCookies()
    {
        var acceptCookies;
        acceptCookies = readCookie(COOKIE_KEY);
        return acceptCookies === "true";
    }

    /**
     * Changes the cookie acceptance value
     * @param {boolean} value
     */
    function setAcceptedCookies(value)
    {
        eraseCookie(COOKIE_KEY);
        createCookie(COOKIE_KEY,"true",COOKIE_EXPIRE_DAYS);
    }

    /**
     * Shows the cookie acceptance box
     */
    function showCookieAcceptBox()
    {
        getCookieAcceptBox().show();
        $("html").addClass(VISIBLE_CLASS);
    }

    /**
     * Hides the cookie acceptance box
     */
    function hideCookieAcceptBox()
    {
        getCookieAcceptBox().hide();
        $("html").removeClass(VISIBLE_CLASS);
    }

    /**
     * Gets the cookie acceptance boxs jQuery instance, or creates it if it doesnt exist
     * @returns {jQuery}
     */
    function getCookieAcceptBox()
    {

        if($cookieBox === null)
        {
            $cookieBox = $(CONTAINER_SELECTOR);
        }
        return $cookieBox;
    }

    /**
     * Creates a new cookie
     * @param {string} name
     * @param {string} value
     * @param {int} days
     */
    function createCookie(name,value,days) {
        var expires = "";
        if(days)
        {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }
        document.cookie = name+"="+value+expires+"; path=/";
    }

    /**
     * Gets a cookie
     * Returns null if cookie does not exist
     * @param {string} name
     * @returns {string|null}
     */
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    /**
     * Deletes a cookie
     * @param {string} name
     */
    function eraseCookie(name) {
        createCookie(name,"",-1);
    }


    /**
     * Document ready handler
     */
    $(document).ready(function()
    {
        if(hasAcceptedCookies())
        {
            hideCookieAcceptBox();
            $('.cookie-box').css( "display", "none" );
        }

        //Show cookie box if user has not accepted
        else if(!hasAcceptedCookies())
        {
            showCookieAcceptBox();
            $('.cookie-box').css( "bottom", "0" );
            
        }

        // Eventlistener for accept cookies
        getCookieAcceptBox().on("click",ACCEPT_BUTTON_SELECTOR,function(e)
        {
            e.preventDefault();
            setAcceptedCookies(true);
            hideCookieAcceptBox();
        });
    });

})(jQuery);
