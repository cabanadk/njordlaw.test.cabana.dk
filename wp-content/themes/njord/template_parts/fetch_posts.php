<?php
require_once '../../../../wp-load.php';
$num_of_posts   = $_POST['num_of_posts'];
$exclude_posts  = $_POST['exclude_posts'];
$year           = $_POST['year'];
$language       = $_POST['language'];

$DOM            = '';

global $sitepress;
//changes to the default language
$sitepress->switch_lang( $language );

$args = array(
    'post_type'         => 'post',
    'posts_per_page'    => $num_of_posts,
    'post__not_in'     =>  $exclude_posts,
    'year'              => $year,
);

query_posts($args);

if(have_posts())
{
    while(have_posts())
    {
        the_post();

        $DOM .= '<article data-article-id="'.get_the_ID().'" data-article-year="'. get_the_date('Y').'">';
        $DOM .=     '<header>';
        $DOM .=         '<p>'.get_the_date('d.m.Y').'</p>';
        $DOM .=         '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
        $DOM .=     '</header>';

        if(has_shortcode($content = get_the_content(),'intro')){
            $introText = check_for_intro_text($content);
            $DOM .=    wpautop($introText);
        } else {
            $DOM .=     '<p>'.str_replace(get_the_title(), '', get_the_excerpt()).'</p>';
        }

        $DOM .= '</article>';
    }
}

echo $DOM;

?>