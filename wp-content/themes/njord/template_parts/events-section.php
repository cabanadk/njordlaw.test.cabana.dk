<?php
$background         = get_sub_field('frontpage_events_background');
?>
<section class="news-b double<?php if($background == 'birch'){echo ' b';}else{echo ' a';}?>">

    <header>

        <h2>

            <?php _e('Events', 'html24');?>

        </h2>

        <?php
        $languagePrefix = ICL_LANGUAGE_CODE;
        $eventPageUrl = get_field($languagePrefix.'_event_list_page' , 'options');

        if(!empty($eventPageUrl)):
        ?>
        <p class="link">

            <a href="<?php echo $eventPageUrl; ?>">

                <?php _e('See more events' , 'html24');?>

            </a>

        </p>
        <?php endif; ?>

    </header>

    <?php

        $pageEvents = get_sub_field('chosen_events', $post->ID);

        if(!empty($pageEvents)){

            $countEvent = count($pageEvents);

            foreach($pageEvents as $event){

                $eventID = $event['chosen_event']->ID;
                $eventTitle = $event['chosen_event']->post_title;
                $eventLinkText = get_field('event_optional_linktext', $eventID);
                $eventLink = get_permalink($eventID);
                ?>

                <article<?php if($countEvent === 1){echo ' class="center_event"';}?>>

                    <h3>

                        <?php echo $eventTitle;?>

                    </h3>

                    <?php echo get_field('event_teaser_text', $eventID); ?>

                    <?php if($eventLinkText != ''):?>

                        <p>

                            <a href="<?php echo $eventLink; ?>">

                                <?php echo $eventLinkText; ?>

                            </a>

                        </p>

                    <?php endif; ?>

                </article>

                <?php
            }
        }
    ?>

</section>