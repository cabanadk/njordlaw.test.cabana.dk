<?php
    $main_text          = get_sub_field('main_text');
    $left_column_text   = get_sub_field('left_column_text');
    $right_column_text  = get_sub_field('right_column_text');
    $background         = get_sub_field('frontpage_text_section_background');
?>

<section class="news-b double<?php if($background == 'birch'){echo ' b';}else{echo ' a';}?>">
    <header>
        <?php echo append_class_to_paragraph($main_text); ?>
    </header>
    <article>
        <?php echo $left_column_text; ?>
    </article>
    <article>
        <?php echo $right_column_text; ?>
    </article>
</section>
