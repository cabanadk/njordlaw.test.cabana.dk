<?php

include "../../../../wp-load.php";
require_once __DIR__ . '/../includes/frontend/VCard.php';

use JeroenDesloovere\VCard\VCard;

$employeeID = $_GET['employeeID'];
$fullname = get_the_title($employeeID);
$phone_direct = get_field('phone_direct', $employeeID);
$phone_mobile = get_field('phone_mobile', $employeeID);
$email = get_field('email_address', $employeeID);
$photo = wp_get_attachment_url(get_post_thumbnail_id($employeeID));

$location = $_GET['location'];

$customFieldJobTitles = false;
$aJobtitles = get_field('jobtitles_repeater', $employeeID);
if (!empty($aJobtitles)) {
    $customFieldJobTitles = true;
} else {
    $aJobtitles = wp_get_post_terms($employeeID, 'jobtitle');
}

$jobtitles = "";
foreach ($aJobtitles as $jobtitle) {
    if ($customFieldJobTitles) {
        $jobtitle = $jobtitle['jobtitle_single'];
        $translatedJobtitle = icl_object_id($jobtitle->term_id, 'jobtitle', true, ICL_LANGUAGE_CODE);
    }
    if ($jobtitle->parent == 0) {
        continue;
    }
    $jobtitles .= (strlen($jobtitles) == 0 ) ? $jobtitle->name : ', ' . $jobtitle->name;
}

$exploded = explode(' ', $fullname);
$lastName = array_pop($exploded);
$firstName = implode(' ', $exploded);

// define vcard
$vcard = new VCard();


// add personal data
$vcard->addName($lastName, $firstName);



// add work data
$vcard->addCompany('NJORD');
$vcard->addJobtitle($jobtitles);
$vcard->addURL('www.njordlaw.com');

$marker = get_field('employee_marker_rel', $employeeID);
$markerID = $marker->ID;

$addressName    = get_the_title($markerID);
$addressStreet  = get_field('marker_street_address',$markerID);
$addressZip  = get_field('marker_postal_code',$markerID);
$addressCity  = get_field('marker_city',$markerID);
$addressCountry  = get_field('marker_country',$markerID);


$vcard->addAddress($addressName, null, $addressStreet, $addressCity, null, $addressZip, $addressCountry, null);

$vcard->addEmail($email);
$vcard->addPhoto($photo);
$vcard->addPhoneNumber($phone_direct, 'WORK');
$vcard->addPhoneNumber($phone_mobile, 'CELL');


// return vcard as a download
return $vcard->download();
