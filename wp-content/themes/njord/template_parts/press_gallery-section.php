<section>

    <h3 class="ip-nav-heading">

        <?php _e('Press gallery', 'html24');?>

    </h3>

    <p class="mb-c">

        <?php _e('How to:' , 'html24'); ?>

        <br>

        <?php _e('Click on an image to enlarge it.' , 'html24'); ?>

        <br>

        <?php _e("To download it, right-click on the image and press 'save image as'" , "html24"); ?>

    </p>

    <div class="double-a a">
        <?php
            // check if the repeater field has rows of data
            if( have_rows('add_press_images_repeater') ):
                // loop through the rows of data
                while ( have_rows('add_press_images_repeater') ) : the_row();
                    // sub field values
                    $image          = get_sub_field('press_image');
                    $description    = get_sub_field('image_description');
                    ?>

                    <figure>

                        <a href="<?php echo $image['url']; ?>">

                            <img src="<?php echo $image['sizes']['press-thumb']; ?>" alt="<?php echo $image['alt']; ?>">

                        </a>

                        <figcaption>

                            <?php echo $description; ?>

                        </figcaption>

                    </figure>

                <?php
                endwhile;
            else :
                // no rows found
            endif;
        ?>
    </div>

</section>