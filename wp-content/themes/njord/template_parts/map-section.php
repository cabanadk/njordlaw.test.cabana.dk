<section id="map">

    <figure class="map-container">

        <?php

            $markerArgs = array(
                'post_type'         => 'marker',
                'post_status'       => 'publish',
                'posts_per_page'    => -1
            );

            $markerQuery = new WP_Query( $markerArgs );
            $markers = $markerQuery->posts;


            foreach ($markers as $key => $marker){

                $markerID           = $marker->ID;
                $markerName         = $marker->post_title;
                $markerLatLng       = get_field('marker_latitude_longitude', $markerID );
                    $markerLatLngExplode    = explode(',', $markerLatLng);
                    $markerLatitude         = $markerLatLngExplode[0];
                    $markerLongitude        = $markerLatLngExplode[1];
                $markerAddress      = get_field('marker_street_address', $markerID );
                $markerPostalCode   = get_field('marker_postal_code', $markerID );
                $markerCity         = get_field('marker_city', $markerID );
                $markerCountry      = get_field('marker_country', $markerID );
                $markerPhoneLabel   = get_field('marker_phonenumber_label',$markerID);
                $markerPhoneNumber  = get_field('marker_phonenumber',$markerID);
                $markerUrl          = get_field('marker_url',$markerID);

                $markerInfoArray = array(
                    'name'          => $markerName,
                    'latitude'      => $markerLatitude,
                    'longitude'     => $markerLongitude,
                    'street'        => $markerAddress,
                    'zipcode'       => $markerPostalCode,
                    'city'          => $markerCity,
                    'country'       => $markerCountry,
                    'phonelabel'    => $markerPhoneLabel,
                    'phonenumber'   => $markerPhoneNumber,
                    'url'           => $markerUrl
                );

                ?>
                <input class='marker' id='marker-no<?php echo $key;?>' type='hidden' value='<?php echo json_encode($markerInfoArray);?>'/>
                <?php

            }


        ?>

        <div id="map-canvas"></div>

    </figure>

</section>