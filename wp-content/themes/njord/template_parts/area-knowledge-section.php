<?php
if(get_field('show_knowledge_posts')):
$languagePrefix = ICL_LANGUAGE_CODE;

//Check if frontpage
if(get_page_template_slug( get_queried_object_id() ) == 'template-frontpage.php'){
    $background = get_sub_field('frontpage_news_background');
}

//Get the correct background for this section
if($background === ''){
    $background = get_sub_field('post_latest_news_background', $post->ID);
}

?>

<section class="news-b b knowledge-section">

    <header>

        <h2 class="ip-nav-heading" id="latest_news_anchor">
            <strong>
                <?php _e('Knowledge', 'html24'); ?>
            </strong>
        </h2>

        <?php

        $knowledgePageUrl = get_field($languagePrefix.'_knowledge_list_page', 'options');
        if(!empty($knowledgePageUrl) &&  get_post_status(url_to_postid($knowledgePageUrl)) == "publish") :?>

            <p class="link">

                <a href="<?php echo $knowledgePageUrl; ?>">

                    <?php _e('View more knowledge articles', 'html24'); ?>

                </a>

            </p>

        <?php endif;?>

    </header>

    <?php
    // The Query
    $queryArgs = array(
        'post_type' => 'knowledge',
        'posts_per_page'    => -1,
    );
    $knowledge = array();
    if( $chosenKnowledgePosts = get_field('knowledge_posts_repeater', get_the_ID()) ){
        
        foreach($chosenKnowledgePosts as $knowledgePost){
            $knowledge[] = $knowledgePost['knowledge_posts_single']->ID;
        }
        $queryArgs['post__in'] = $knowledge;
    } else {
        $queryArgs['posts_per_page'] = 3;
        $queryArgs['orderby'] = 'rand';
    }

    $the_query = new WP_Query( $queryArgs );

    // The Loop
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $this_post_areas = '';
            $categories='';
            $types = get_the_terms(get_the_ID(), 'type-of-knowledge' );
            foreach ( $types as $type ) {
                $categories .= $type->name.'  ';
            }
            $areas_of_the_post = get_field('choose_area_of_expertise');
            if( count($areas_of_the_post) > 0 ){
                foreach ($areas_of_the_post as $area_of_the_post) {
                    $area_name = get_the_title( $area_of_the_post );
                    $this_post_areas .= ' | '.$area_name;
                    $this_post_areas_array[] = $area_name;
                }
            }
            ?>
            <?php
            /****************************/
            //if ( in_array($current_area, $this_post_areas_array) ) {
            ?>

            <article>

                <a href="<?php the_permalink(); ?>">

                    <h3>

                        <?php the_title(); ?>

                    </h3>

                </a>

                <p>

                    <?php

                    $introText = get_string_between('[intro]', '[/intro]', $post->post_content);

                    if( has_shortcode($post->post_content, 'intro') && $introText != '' ){

                        echo $introText;

                    } else {

                        echo str_replace(get_the_title(), '', get_the_excerpt(190));

                    }

                    ?>

                </p>

                <p class="link-a">

                    <a href="<?php the_permalink(); ?>">

                        <?php _e('Read more', 'html24'); ?>

                    </a>

                </p>

            </article>

                <?php
                unset($this_post_areas_array);
                $this_post_areas_array = array();
            //}  //end if
        }
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();
    ?>

</section>
<?php endif; ?>
