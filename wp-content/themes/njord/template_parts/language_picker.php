<?php
$languageCode = ICL_LANGUAGE_CODE;
$country = checkCountry();
?>

<ul class="c">
    <li id="current_country_page">
        <?php echo get_main_country_nav($languageCode, $country); ?>
    </li>
</ul>

<ul class="d">
    <?php echo get_sub_country_nav($languageCode, $country); ?>
</ul>

<?php

function get_main_country_nav($languageCode, $countryCode){
    if($countryCode != null){$languageCode = $countryCode;}
    ob_start();
    ?>
    <span>
    <?php
    switch ($languageCode) {
        case "da": case "de": case "en":
        _e('Denmark', 'html24');
        break;
        case "et": case "et_en": case "ru-ee":
        _e('Estonia', 'html24');
        break;
        case "lv": case "lv_en": case "ru":
        _e('Latvia', 'html24');
        break;
        case "lt": case "lt_en":
        _e('Lithuania', 'html24');
        break;
    }

    $languages = icl_get_languages();
    ?>
    </span>
    <ul>
        <li><a href="<?php echo $languages['da']['url'] ?>" data-country-code="da" class="switch_country"><?php _e('Denmark', 'html24');?></a></li>
        <li><a href="<?php echo $languages['et']['url'] ?>" data-country-code="et" class="switch_country"><?php _e('Estonia', 'html24');?></a></li>
        <li><a href="<?php echo $languages['lv']['url'] ?>" data-country-code="lv" class="switch_country"><?php _e('Latvia', 'html24');?></a></li>
        <li><a href="<?php echo $languages['lt']['url'] ?>" data-country-code="lt" class="switch_country"><?php _e('Lithuania', 'html24');?></a></li>
    </ul>
    <?php
    return ob_get_clean();
}

function get_sub_country_nav($languageCode, $country){

    if($country != null && $languageCode === 'en'){
        //$languageCode = $country.'_en';
    }

    $languages = icl_get_languages();
    ob_start();
    //This is how it was build before deleting the different english pages
    /*switch ($languageCode) {
        case "da": case "de": case "en":
        echo "<li class='dk_lang ".($languageCode == 'da' ? 'active' : false)."'><a href='".$languages['da']['url']."'>DK</a></li>";
        echo "<li class='dk_lang ".($languageCode == 'en' ? 'active' : false)."'><a href='".$languages['en']['url']."'>EN</a></li>";
        echo "<li class='dk_lang ".($languageCode == 'de' ? 'active' : false)."'><a href='".$languages['de']['url']."'>DE</a></li>";
        break;

        case "et": case "et_en": case "ru-ee":
        echo "<li class='et_lang ".($languageCode == 'et' ? 'active' : false)."'><a href='".$languages['et']['url']."'>ET</a></li>";
        echo "<li class='et_lang en ".($languageCode == 'et_en' ? 'active' : false)."'><a href='".$languages['en']['url']."'>EN</a></li>";
        echo "<li class='et_lang ".($languageCode == 'ru-ee' ? 'active' : false)."' style='display: none;'><a href='".$languages['ru-ee']['url']."'>RU</a></li>";
        break;

        case "lv": case "lv_en": case "ru":
        echo "<li class='lv_lang ".($languageCode == 'lv' ? 'active' : false)."'><a href='".$languages['lv']['url']."'>LV</a></li>";
        echo "<li class='lv_lang en ".($languageCode == 'lv_en' ? 'active' : false)."'><a href='".$languages['en']['url']."'>EN</a></li>";
        echo "<li class='lv_lang ".($languageCode == 'ru' ? 'active' : false)."'><a href='".$languages['ru']['url']."'>RU</a></li>";
        break;

        case "lt": case "lt_en":
        echo "<li class='lt_lang ".($languageCode == 'lt' ? 'active' : false)."'><a href='".$languages['lt']['url']."'>LT</a></li>";
        echo "<li class='lt_lang en ".($languageCode == 'lt_en' ? 'active' : false)."'><a href='".$languages['en']['url']."'>EN</a></li>";
        break;
    }*/
    //This is without the english pages so the language switchers change depending on the country page you are
    switch ($languageCode) {
        case "da": case "de": case "en": case "et": case "et_en": case "ru-ee": case "lv": case "lv_en": case "ru": case "lt": case "lt_en":
        echo "<li class='dk_lang da ".($languageCode == 'da' ? 'active' : 'hidden')."'><a href='".$languages['da']['url']."'>DK</a></li>";
        echo "<li class='dk_lang en ".($languageCode == 'en' ? 'active' : false)."'><a href='".$languages['en']['url']."'>EN</a></li>";
        echo "<li class='dk_lang de ".($languageCode == 'de' ? 'active' : 'hidden')."'><a href='".$languages['de']['url']."'>DE</a></li>";
        echo "<li class='et_lang et ".($languageCode == 'et' ? 'active' : 'hidden')."'><a href='".$languages['et']['url']."'>EE</a></li>";
        echo "<li class='et_lang ru-ee ".($languageCode == 'ru-ee' ? 'active' : 'hidden')."' style='display: none;'><a href='".$languages['ru-ee']['url']."'>RU</a></li>";
        echo "<li class='lv_lang lv ".($languageCode == 'lv' ? 'active' : 'hidden')."'><a href='".$languages['lv']['url']."'>LV</a></li>";
        echo "<li class='lv_lang ru ".($languageCode == 'ru' ? 'active' : 'hidden')."'><a href='".$languages['ru']['url']."'>RU</a></li>";
        echo "<li class='lt_lang lt ".($languageCode == 'lt' ? 'active' : 'hidden')."'><a href='".$languages['lt']['url']."'>LT</a></li>";
        break;
        
    }
    return ob_get_clean();
}

function checkCountry(){
    $countryCode = $_COOKIE['country_code'];

    //Fix for wrong named language
    if($_COOKIE['country_code'] == 'er'){ $countryCode = 'et'; }

    return $countryCode;
}