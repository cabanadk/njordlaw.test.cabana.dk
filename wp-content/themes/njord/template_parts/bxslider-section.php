<section id="featured">
    <?php
        // check if the repeater field has rows of data
        if( have_rows('bxslider_repeater') ):
            // loop through the rows of data
            while ( have_rows('bxslider_repeater') ) : the_row(); ?>
                <article>

                    <h1>
                        <?php the_sub_field('heading'); ?>
                    </h1>

                    <p>
                        <?php the_sub_field('bodytext'); ?>
                    </p>

                    <p class="link-a">

                        <?php
                        $linkID = get_sub_field('read_more_link');
                        $linkUrl = get_permalink($linkID[0]);
                        ?>

                        <a href="<?php echo $linkUrl; ?>">
                            <?php _e('Read more', 'html24');?>
                        </a>

                    </p>

                    <figure>

                        <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt'] ?>" width="1572" height="1145">

                    </figure>

                </article>

            <?php endwhile;
        else :
            // no rows found
        endif;
    ?>
</section>