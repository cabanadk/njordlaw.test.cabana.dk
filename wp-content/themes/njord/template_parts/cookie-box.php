<div class="cookie-box">
    <div class="cookie-box-inner">
        <div class="cookie-box-content">
        <?php echo get_field(ICL_LANGUAGE_CODE.'_cookie_text', 'options');?>
        </div>

        <div class="cookie-box-accept">
        <button class="cookie-allow"><?php _e('Accept', 'html24');?></button>
        </div>
    </div>
</div>
