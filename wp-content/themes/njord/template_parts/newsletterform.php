<?php $german = false; if(ICL_LANGUAGE_CODE  == 'de'){ $german = true; } ?>

<style>
#newsletterlink{
    padding-left: 46px;
    text-decoration: none;
    font-weight: bold; 
}
</style>
<form method="post" class="validate">
    <div class="input-group">
        <?php if(!$german):?>
            <p class="scheme-a"><?php _e('Vi deler gerne vores viden med dig. For når du er opdateret på lovgivning og retspraksis, så kender du dine muligheder. Og vores samarbejde bliver stærkere.', 'html24');?></p>
            <p><?php _e('Vi baserer vores nyheder på vigtige afgørelser og seneste praksis, og vi sender løbende tips og seneste nyt om principielle domme. Det giver dig et tidssvarende overblik over lovgivningen på området – direkte i din indbakke. ', 'html24');?></p>
            <p><?php _e('Vælg de nyhedsbreve, der har din interesse, så lover vi at holde dig opdateret.', 'html24');?></p>
        <?php else: $danishNewsletterLink = get_field('da_footer_fourth_col_link','options'); ?>
            <p class="link-b">
                <a href="<?php echo $danishNewsletterLink;?>"><?php _e('For more newsletters in danish go to this link', 'html24');?></a>
            </p>
        <?php endif; ?>
        <ul class="checklist-a u">
            <?php if(!$german):?>
                <li><label for="ansaettelsesret"><input type="checkbox" value="e03d5fdef1" name="list[]" id="ansaettelsesret"><?php _e('Employment law','html24'); ?></label></li>
                <li><label for="bankogfinans"><input type="checkbox" value="c1146d0283" name="list[]" id="bankogfinans"><?php _e('Banking & Finance','html24'); ?></label></li>
            <?php endif; ?>

            <li><label for="newsletterlink"><input type="checkbox" value="f997153888" name="list[]" id="newsletterlink"><!--<a href="http://eepurl.com/bdU0or">--><?php _e('Fast Ejendom & Entreprise','html24'); ?><!--</a>--></label></li>

            <li><label for="germannordic"><input type="checkbox" value="f997153888" name="list[]" id="germannordic"><?php _e('German Nordic ','html24'); ?><span class="not_bold"><?php _e('(udgives på tysk)','html24'); ?></span></label></li>
            
            <?php if(!$german):?>
                <li><label for="lejeret"><input type="checkbox" value="b53b9dd032" name="list[]" id="lejeret"><?php _e('Rent law','html24'); ?></label></li>
                <li><label for="lifescience"><input type="checkbox" value="4099baf86a" name="list[]" id="lifescience"><?php _e('Life Sciences','html24'); ?></label></li>
                <li><label for="offentligret"><input type="checkbox" value="809dfd29a6" name="list[]" id="offentligret"><?php _e('Public law','html24'); ?></label></li>
                <li><label for="selskabsret"><input type="checkbox" value="668e19d77c" name="list[]" id="selskabsret"><?php _e('Company law','html24'); ?></label></li>
                <li><label for="transport"><input type="checkbox" value="1cd6a8204c" name="list[]" id="transport"><?php _e('Transport','html24'); ?></label></li>
                <li><label for="dronesrobots"><input type="checkbox" value="069123d8ba" name="list[]" id="dronesrobots"><?php _e('Drones & Robots','html24'); ?></label></li>
                <!--<li><a id="newsletterlink" href="http://eepurl.com/bdU0or">Fast Ejendom & Entreprise (klik link til ekstern side for at signe op)</a></li>-->
            <?php endif; ?>

        </ul>
    </div>
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_ea385526fc54a74f2a2de90d1_f8923a216d" tabindex="-1" value=""></div>
    <p class="input-a">
        <span>
            <label for="fname" style="margin-top: 0px;"><?php _e('First name', 'html24'); ?></label>
            <input type="text" id="fname" value="" name="FNAME" class="" required>
        </span>
        <span>
            <label for="lname"><?php _e('Last name', 'html24'); ?></label>
            <input type="text" id="lname" value="" name="LNAME" class="" required>
        </span>
        <span>
            <label for="email"><?php _e('E-mailadresse', 'html24'); ?></label>
            <input type="email" id="email" value="" name="EMAIL" class="required email" required>
        </span>
        <span>
            <input type="submit" value="<?php _e('Subscribe', 'html24'); ?>" name="subscribe" class="button">
        </span>
    </p>
</form>

<?php if(isset($_POST) && !empty($_POST)){

    $listIds         = $_POST['list'];
    $fname           = $_POST['FNAME'];
    $lname           = $_POST['LNAME'];
    $email           = $_POST['EMAIL'];

    if(empty($listIds)){
        return;
    }

    if(!empty($listIds) && is_array($listIds)){

        foreach($listIds as $listId){
            $ip             = '77.66.108.8';
            $apiKey         = 'eaa50562db0c3864f400aeea65735e72-us3';
            $userId         = 'ea385526fc54a74f2a2de90d1&amp;';

            $merge_vars=array(
                'OPTIN_IP'=>$ip,
                'OPTIN-TIME'=>"now",
                'FNAME'=>ucwords(strtolower(trim($fname))),
                'LNAME'=>ucwords(strtolower(trim($lname))),
            );

            $send_data=array(
                'email'=>array('email'=>$email),
                'apikey'=>$apiKey,
                'id'=>$listId,
                'merge_vars'=>$merge_vars,
                'double_optin'=>false,
                'update_existing'=>true,
                'replace_interests'=>false,
                'send_welcome'=>true,
                'email_type'=>"html",
            );

            $payload=json_encode($send_data);
            $submit_url="https://us3.api.mailchimp.com/2.0/lists/subscribe.json";
            $ch=curl_init();
            curl_setopt($ch,CURLOPT_URL,$submit_url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$payload);
            $result=curl_exec($ch);
            curl_close($ch);

            $mcdata=json_decode($result);

            if (!empty($mcdata->status) && $mcdata->status === "error") {
                return "Mailchimp Error: " . $mcdata->error;
            }
        }
    }


};

?>
