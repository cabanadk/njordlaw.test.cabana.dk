<?php
require_once '../../../../wp-load.php';
$num_of_posts   = $_POST['num_of_posts'];
$exclude_posts  = $_POST['exclude_posts'];
$area           = $_POST['area'];
$language       = $_POST['language'];

$DOM            = '';


$postCounter = 0;


global $sitepress;
//changes to the default language
$sitepress->switch_lang( $language );


//If we have no specific area
if($area == 'all'){

    $args = array(
        'post_type'         => 'knowledge',
        'posts_per_page'    => -1,
        'post__not_in'      => $exclude_posts,
    );

    query_posts($args);

    if(have_posts()) {
        while (have_posts()) {
            the_post();

            //We get the post categories
            $categories = '';
            $this_post_areas = '';
            $types = get_the_terms(get_the_ID(), 'type-of-knowledge');
            foreach ($types as $type) {
                $categories .= $type->name . '  ';
            }

            /*$areas_of_the_post = get_field('choose_area_of_expertise');
            if( count($areas_of_the_post) > 0 ){
                foreach ($areas_of_the_post as $area_of_the_post) {
                    $area_name = get_the_title( $area_of_the_post );
                    $this_post_areas .= ' | '.$area_name;
                }
            }*/
            if ( $postCounter <= $num_of_posts) {
                $postCounter++;
                $DOM .= '<article data-article-id="' . get_the_ID() . '">';
                $DOM .= '<header>';
                //$DOM .=         '<p>'.$categories.' '.$this_post_areas.'</p>';
                $DOM .= '<p>' . $categories . '</p>';
                $DOM .= '<h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>';
                $DOM .= '</header>';

                if (has_shortcode($content = get_the_content(), 'intro')) {
                    $introText = check_for_intro_text($content);
                    $DOM .= wpautop($introText);
                } else {
                    $DOM .= '<p>' . str_replace(get_the_title(), '', get_the_excerpt()) . '</p>';
                }

                $DOM .= '</article>';
            }
        }
    }
}
else{
    $args = array(
        'post_type'         => 'knowledge',
        'posts_per_page'    => -1,
        'post__not_in'      => $exclude_posts,
    );

    query_posts($args);

    if(have_posts()) {
        while (have_posts()) {
            the_post();

            //We get the areas the post is related to and loop through them
            $areas_of_the_post = get_field('choose_area_of_expertise');
            if ($areas_of_the_post) {
                foreach ($areas_of_the_post as $area_of_the_post) {
                    //$this_post_areas = '';
                    $area_name = get_the_title($area_of_the_post);
                    //$this_post_areas .= ' | '.$area_name;

                    //If the area is the same we are looking for, we'll display the post up to the maximum number of posts
                    if ($area_name == $area && $postCounter <= $num_of_posts) {
                        $postCounter++;
                        //We get the post categories
                        $categories = '';
                        $types = get_the_terms(get_the_ID(), 'type-of-knowledge');
                        foreach ($types as $type) {
                            $categories .= $type->name . '  ';
                        }

                        $DOM .= '<article data-article-id="' . get_the_ID() . '">';
                        $DOM .= '<header>';
                        //$DOM .=         '<p>'.$categories.' '.$this_post_areas.'</p>';
                        $DOM .= '<p>' . $categories . '</p>';
                        $DOM .= '<h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>';
                        $DOM .= '</header>';

                        if (has_shortcode($content = get_the_content(), 'intro')) {
                            $introText = check_for_intro_text($content);
                            $DOM .= wpautop($introText);
                        } else {
                            $DOM .= '<p>' . str_replace(get_the_title(), '', get_the_excerpt()) . '</p>';
                        }

                        $DOM .= '</article>';
                    }
                }
            }
        }
    }
}



//
//
//$args = array(
//    'post_type'         => 'knowledge',
//    'posts_per_page'    => $num_of_posts,
//    'post__not_in'      => $exclude_posts,
//);
//
//
//
//query_posts($args);
//
//if(have_posts())
//{
//    while(have_posts())
//    {
//        the_post();
//
//        //If we have no specific area
//        if($area == 'all'){
//            //We get the post categories
//            $categories='';
//            $this_post_areas = '';
//            $types = get_the_terms(get_the_ID(), 'type-of-knowledge' );
//            foreach ( $types as $type ) {
//                $categories .= $type->name.'  ';
//            }
//
//            /*$areas_of_the_post = get_field('choose_area_of_expertise');
//            if( count($areas_of_the_post) > 0 ){
//                foreach ($areas_of_the_post as $area_of_the_post) {
//                    $area_name = get_the_title( $area_of_the_post );
//                    $this_post_areas .= ' | '.$area_name;
//                }
//            }*/
//
//            $DOM .= '<article data-article-id="'.get_the_ID().'">';
//            $DOM .=     '<header>';
//            //$DOM .=         '<p>'.$categories.' '.$this_post_areas.'</p>';
//            $DOM .=         '<p>'.$categories.'</p>';
//            $DOM .=         '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
//            $DOM .=     '</header>';
//
//            if(has_shortcode($content = get_the_content(),'intro')){
//                $introText = check_for_intro_text($content);
//                $DOM .=    wpautop($introText);
//            } else {
//                $DOM .=     '<p>'.str_replace(get_the_title(), '', get_the_excerpt()).'</p>';
//            }
//
//            $DOM .= '</article>';
//        }
//        //If we have a specific area
//        else{
//            //We get the areas the post is related to and loop through them
//            $areas_of_the_post = get_field('choose_area_of_expertise' );
//            if($areas_of_the_post){
//                foreach ($areas_of_the_post as $area_of_the_post) {
//                    //$this_post_areas = '';
//                    $area_name = get_the_title( $area_of_the_post );
//                    //$this_post_areas .= ' | '.$area_name;
//
//                    //If the area is the same we are looking for, we'll display the post
//                    if($area_name == $area){
//                        //We get the post categories
//                        $categories='';
//                        $types = get_the_terms(get_the_ID(), 'type-of-knowledge' );
//                        foreach ( $types as $type ) {
//                            $categories .= $type->name.'  ';
//                        }
//
//                        $DOM .= '<article data-article-id="'.get_the_ID().'">';
//                        $DOM .=     '<header>';
//                        //$DOM .=         '<p>'.$categories.' '.$this_post_areas.'</p>';
//                        $DOM .=         '<p>'.$categories.'</p>';
//                        $DOM .=         '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
//                        $DOM .=     '</header>';
//
//                        if(has_shortcode($content = get_the_content(),'intro')){
//                            $introText = check_for_intro_text($content);
//                            $DOM .=    wpautop($introText);
//                        } else {
//                            $DOM .=     '<p>'.str_replace(get_the_title(), '', get_the_excerpt()).'</p>';
//                        }
//
//                        $DOM .= '</article>';
//                    }
//                }
//            }
//        }
//    }
//}

echo $DOM;

?>