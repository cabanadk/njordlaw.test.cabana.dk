<?php if(get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_show', 'options') == '1'): ?>

<div class="newsletter-popup">
    <h3><?php echo get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_title', 'options') ?></h3>
    <p><?php echo get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_description', 'options') ?></p>
    <a href="<?php echo get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_link', 'options') ?>" class="button">
        <?php echo get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_button_text', 'options') ?>
    </a>
    <a href="#" class="close"></a>
</div>
<script>
    jQuery(function ($) {
        var $popup = $('.newsletter-popup');

        var posttype = "<?php echo get_post_type() ?>";
        
        if ($.cookie('newsletter-popup') != '1' && posttype != 'extranet' ) {
        console.log("Show Pop");
            
            $popup.css({ 'bottom': ($popup.outerHeight() * -1) + 'px', 'display': 'block' });
        } else {
            console.log("Don't show pop up")
        }

        setTimeout(function () {
            $popup.animate({
                bottom: 10
            }, 500);
        }, <?php echo get_field(ICL_LANGUAGE_CODE . '_newsletter_popup_delay', 'options') ?>);

        $popup.find('.close').on('click', function (e) {
            e.preventDefault();
            $popup.delay(100).animate({
                bottom: ($popup.outerHeight() * -1)
            }, 500);
            console.log("CLose cookie");
            $.cookie('newsletter-popup', '1', { expires: 30, path: '/' });    
        });

        $popup.find('.button').on('click', function(){
            $.cookie('newsletter-popup', '1');
        });
    });
            
</script>

<?php endif; ?>