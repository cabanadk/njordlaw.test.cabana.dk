<?php
    $employeeRepeater = get_sub_field('add_employee_repeater', $post->id);
?>

<aside>

    <?php
    foreach($employeeRepeater as $foundEmployees){
        $employee = $foundEmployees['post_single_employee'];
        if(!empty($employee)):
        ?>

        <div class="vcard a" itemscope itemtype="http://schema.org/Person">

            <figure>

                <a href="<?php echo get_permalink($employee->ID); ?>">

                    <?php echo get_the_post_thumbnail($employee->ID, 'employee-thumb', array('class' => 'photo', 'itemprop' => 'image')); ?>

                </a>

            </figure>

            <h2 class="fn" itemprop="name">

                <?php echo $employee->post_title;?>

            </h2>

            <p>
                <?php
                $customJobtitles = get_field('jobtitles_repeater', $employee->ID);
                if(!empty($customJobtitles)):
                    foreach($customJobtitles as $customJobtitle) : ?>
                        <span itemprop="jobtitle"><?php echo $customJobtitle['jobtitle_single']->name.', '; ?></span>
                    <?php endforeach;
                else :
                    $aJobtitles = wp_get_post_terms( $employee->ID, 'jobtitle' );
                    if(!empty($aJobtitles)):
                        foreach($aJobtitles as $jobtitle) :
                            if($jobtitle->parent != 0): ?>
                                <span itemprop="jobtitle"><?php echo $jobtitle->name.', '; ?></span>
                            <?php endif;
                        endforeach;
                    endif;
                endif; ?>

                <span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <?php
                    $locations = wp_get_post_terms($employee->ID,'country');
                    if(!empty($locations)){
                        foreach($locations as $location) {
                            if($location->parent != 0) {
                                echo '<span class="locality" itemprop="addressLocality">'.$location->name.'</span>';
                            }
                        }
                    }
                    ?>
                </span>
            </p>

            <?php
            $directPhone    = get_field('phone_direct', $employee->ID);
            $mobilePhone    = get_field('phone_mobile', $employee->ID);
            $email          = get_field('email_address', $employee->ID);
            ?>
            <p>

                <?php if(!empty($directPhone)):?>
                    <?php _e('Direct:', 'html24'); ?>
                    <span class="tel" itemprop="telephone"><?php echo $directPhone; ?></span>

                    <br>

                <?php endif; ?>

                <?php if(!empty($mobilePhone)):?>
                    <?php _e('Mobile:', 'html24'); ?>
                    <span class="tel" itemprop="telephone"><?php echo $mobilePhone; ?></span>

                    <br>
                <?php endif; ?>

                <?php if(!empty($email)):?>
                    <a href="mailto:<?php echo $email; ?>" class="mail" itemprop="email"><?php echo $email; ?></a>

                <?php endif; ?>

            </p>

        </div>

    <?php
        endif;
    }
    ?>

</aside>