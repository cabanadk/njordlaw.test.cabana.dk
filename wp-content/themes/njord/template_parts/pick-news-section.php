
<?php
$languagePrefix = ICL_LANGUAGE_CODE;

//Check if frontpage
if(get_page_template_slug( get_queried_object_id() ) == 'template-frontpage.php'){
    $background = get_sub_field('frontpage_news_background');
}

//Get the correct background for this section
if($background === ''){
    $background = get_sub_field('post_latest_news_background', $post->ID);
}

?>

<section class="news-b<?php if($background == 'white'){echo ' a';}else{echo ' b';} ?>">
    <header>
        <h2 class="ip-nav-heading" id="latest_news_anchor">
            <strong>
                <?php _e('Latest news', 'html24'); ?>
            </strong>
        </h2>
        <?php

        $newsPageUrl = get_field($languagePrefix.'_news_list_page', 'options');
        if(!empty($newsPageUrl)) :
            ?>
            <p class="link">
                <a href="<?php echo $newsPageUrl; ?>">
                    <?php _e('View more news', 'html24'); ?>
                </a>
            </p>
        <?php endif;?>
    </header>

    <?php

    // check if the repeater field has rows of data
    if( have_rows('related_news_repeater') ):
        // loop through the rows of data
        while ( have_rows('related_news_repeater') ) : the_row();
            // display a sub field value
            $this_post = get_sub_field('related_news')[0];

            $id = $this_post->ID;
            $temp = $post;
            $post = get_post( $id );
            setup_postdata( $post );
            ?>

            <article>
                <a href="<?php echo get_permalink($this_post->ID); ?>">
                    <h3>
                        <?php echo get_the_title($this_post->ID); ?>
                    </h3>
                </a>
                <p>
                    <?php

                        $useIntroAsExcerptOnFrontpage = get_sub_field('use_news_intro_text_as_excerpt_on_frontpage');

                        $useIntroAsExcerptOnDefaultPage = get_sub_field('use_news_intro_text_as_excerpt_on_page');

                    if($useIntroAsExcerptOnFrontpage == true || $useIntroAsExcerptOnDefaultPage == true){

                        $introText = get_string_between('[intro]', '[/intro]', $post->post_content);

                        if( has_shortcode($post->post_content, 'intro') && $introText != '' ){

                            echo $introText;

                        } else {
                            echo str_replace(get_the_title(), '', get_the_excerpt(190));
                        }

                    } else {
                        echo str_replace(get_the_title(), '', get_the_excerpt(190));
                    }
                    ?>
                </p>
                <p class="link-a">
                    <a href="<?php echo get_permalink($this_post->ID); ?>">
                        <?php _e('Read more', 'html24'); ?>
                    </a>
                </p>
            </article>

            <?php
            wp_reset_postdata();
            $post = $temp;

            ?>

            <?php
        endwhile;
    else :
        // no rows found
    endif;
    ?>

</section>