<?php
require_once '../../../../wp-load.php';

$num_of_employees   = $_POST['num_of_employees'];
$exclude_employees  = $_POST['exclude_employees'];
$language           = $_POST['language'];

$empName        = $_POST['searchName'];
$empField       = $_POST['searchSpec'];
$empTitle       = $_POST['searchJobTitle'];
$empCountry     = $_POST['searchCountry'];
$searchTerms    = '';

$DOM            = '';

global $sitepress;
//changes to the default language
$sitepress->switch_lang( $language );

$customQueryBase = '';

$excludeCurrent = true;

$parentTermLanguage = get_parent_based_term($language);

//Check if there has been a search
if(!empty($empName) || !empty($empField) || !empty($empTitle) || !empty($empCountry) && $language !== 'en' ){

    $searchResults = get_custom_query($empName, $empField, $empTitle, $empCountry, $exclude_employees);

    $employees = $searchResults;

} else {


    $employeeArgs = array(
        'post_type'         => 'employee',
        'post__not_in'      => $exclude_employees,
        'posts_per_page'    => $num_of_employees,
        'orderby'           => 'title',
        'order'             =>  'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'country',
                'field'    => 'slug',
                'terms'    => $parentTermLanguage,
            ),
        ),
    );

    $empQuery = new WP_Query( $employeeArgs );

    $employees = $empQuery->posts;

}

function getDirectPhoneString() {
    _e("Direct", "html24");
}
ob_start();
getDirectPhoneString();
$directPhoneString = ob_get_clean();

function getMobilePhoneString() {
    _e("Mobile", "html24");
}
ob_start();
getMobilePhoneString();
$mobilePhoneString = ob_get_clean();



if($employees){

    foreach ($employees as $employee){

            if(!empty($empName) || !empty($empField) || !empty($empTitle) || !empty($empCountry)){
                if ($employee->source_language_code !== $language) {
                break;
                }
            }

           $employeeID = $employee->ID;

            if ( has_post_thumbnail($employeeID) ) {
                $thumbnail = get_the_post_thumbnail($employeeID, 'employee-thumb', array('class' => 'photo', 'itemprop' => 'image')); 
            } else {
                
                $uploads = wp_upload_dir(); 
                $thumbnail = '<img src="' . esc_url( $uploads['baseurl'] . '/2017/07/placeholder-image-thumbnail.png' ) . '" href="#">'; 
            }


            $DOM .= '<li class="vcard" itemscope itemtype="http://schema.org/Person" data-employee-id="' . $employeeID . '">';
            $DOM .= '<a class="img" href="' . get_the_permalink($employeeID) . '">';
            $DOM .= $thumbnail;
            $DOM .= '</a>';
            $DOM .= '<span class="fn" itemprop="name">' . get_the_title($employeeID) . '</span>';
            $DOM .= '<span class="titles_and_city">';

            $customJobtitles = get_field('jobtitles_repeater', $employeeID);
            if(!empty($customJobtitles)):
                foreach($customJobtitles as $key => $customJobtitle) :
                    $DOM .=    '<span itemprop="jobtitle" data-employee-jobtitle="'.$customJobtitle['jobtitle_single']->term_id.'">'.$customJobtitle['jobtitle_single']->name.', </span>';
                endforeach;
            else :
                $aJobtitles = wp_get_post_terms( $employeeID, 'jobtitle' );
                if(!empty($aJobtitles)):
                    foreach($aJobtitles as $jobtitle) :
                        if($jobtitle->parent != 0):
                            $DOM .= '<span itemprop="jobtitle" data-employee-jobtitle=' . $jobtitle->term_id . '>'. $jobtitle->name.', </span>';
                        endif;
                    endforeach;
                endif;
            endif;

            $DOM .= '<span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
            $locations = wp_get_post_terms($employeeID, 'country');
            foreach($locations as $location) {
                if ($location->parent != 0) {
                    $DOM .= '<span class="locality" itemprop="addressLocality">' . $location->name . '</span>';
                }
            }
            $DOM .= '</span>';
            $DOM .= '</span>';
            $DOM .= '<br>';

            $directPhone    = get_field('phone_direct', $employeeID);
            $mobilePhone    = get_field('phone_mobile', $employeeID);
            $email          = get_field('email_address', $employeeID);

            if(!empty($directPhone)){
                $DOM .= $directPhoneString.': ';
                $DOM .= '<span class="tel" itemprop="telephone">'.$directPhone.'</span>';
                $DOM .= '<br>';
            }

            if(!empty($mobilePhone)){
                $DOM .= $mobilePhoneString.': ';
                $DOM .= '<span class="tel" itemprop="telephone">'.$mobilePhone.'</span>';
                $DOM .= '<br>';
            }

            if(!empty($email)){
                $DOM .= '<a href="mailto:'.$email.'" class="email" itemprop="email">'.$email.'</a>';
            }

            $DOM .= '</li>';

         
    }

}

echo $DOM;
