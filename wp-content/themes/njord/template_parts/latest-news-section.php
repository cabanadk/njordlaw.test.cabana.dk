
<?php
$languagePrefix = ICL_LANGUAGE_CODE;

//Check if frontpage
if(get_page_template_slug( get_queried_object_id() ) == 'template-frontpage.php'){
    $background = get_sub_field('frontpage_news_background');
}

//Get the correct background for this section
if($background === ''){
    $background = get_sub_field('post_latest_news_background', $post->ID);
}

?>

<section class="news-b<?php if($background == 'white'){echo ' a';}else{echo ' b';} ?>">

    <header>

        <h2 class="ip-nav-heading" id="latest_news_anchor">
            <strong>
                <?php _e('Latest news', 'html24'); ?>
            </strong>
        </h2>

        <?php

        $newsPageUrl = get_field($languagePrefix.'_news_list_page', 'options');
        if(!empty($newsPageUrl)) :?>

            <p class="link">

                <a href="<?php echo $newsPageUrl; ?>">

                    <?php _e('View more news', 'html24'); ?>

                </a>

            </p>

        <?php endif;?>

    </header>

    <?php
        // The Query
        $the_query = new WP_Query( array(
            'post_type' => 'post',
            'posts_per_page' => 3
        ));

        // The Loop
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>

                <article>

                    <a href="<?php the_permalink(); ?>">

                        <h3>

                            <?php the_title(); ?>

                        </h3>

                    </a>

                    <p>

                        <?php

                        $introText = get_string_between('[intro]', '[/intro]', $post->post_content);

                        if( has_shortcode($post->post_content, 'intro') && $introText != '' ){

                            echo $introText;

                        } else {

                            echo str_replace(get_the_title(), '', get_the_excerpt(190));

                        }

                        ?>

                    </p>

                    <p class="link-a">

                        <a href="<?php the_permalink(); ?>">

                            <?php _e('Read more', 'html24'); ?>

                        </a>

                    </p>

                </article>

    <?php }
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
        wp_reset_query();
    ?>

</section>