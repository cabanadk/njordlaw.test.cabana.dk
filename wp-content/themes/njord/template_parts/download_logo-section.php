<section>
    <h3><?php _e('Download logo', 'html24');?></h3>
    <?php
        $image_for_print    = get_sub_field('logo_for_print');
        $image_for_web      = get_sub_field('logo_for_web');
    ?>
    <figure><?php if(!empty($image_for_web)) {?><img src="<?php echo $image_for_web['sizes']['thumbnail']; ?>" alt="Njord" width="114" height="22"><?php }; ?></figure>
    <p class="link-b">
        <?php if(!empty($image_for_print)) { ?>
            <a href="<?php echo $image_for_print['url']; ?>">NJORD logo .eps (<?php _e('for print', 'html24');?>)</a><br>
        <?php }; ?>
        <?php if(!empty($image_for_web)) { ?>
            <a href="<?php echo $image_for_web['url'] ?>">NJORD logo .png (<?php _e('for web', 'html24');?>)</a>
        <?php }; ?>
    </p>
</section>