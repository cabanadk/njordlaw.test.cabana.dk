<?php

$languagePrefix = ICL_LANGUAGE_CODE;
$employeeOptionsPage = get_field($languagePrefix.'_employee_list_page', 'options');
?>

<form action="<?php echo esc_url($employeeOptionsPage); ?>" method="post" id="team">
    <fieldset>
        <h2><?php _e('Find employee', 'html24');?></h2>
        <p>
            <span>
                <label for="ta"><?php _e('Name', 'html24');?></label>
                <input type="text" id="ta" name="ta">
            </span>
            <span>
                <label class="hidden" for="tf"><?php _e('Area of Expertise', 'html24');?></label>
                <select id="tf" name="tf">
                    <option disabled selected><?php _e('Area of Expertise', 'html24');?></option>
                    <?php
                    $specialties = get_specialties();
                    if(!empty($specialties)){
                        foreach ($specialties as $key => $specialty):?>

                        <option value="<?php echo $specialty->ID; ?>"><?php echo $specialty->post_title;?></option>

                        <?php endforeach;
                    }
                    ?>
                </select>
            </span>
            <span>
                <label class="hidden" for="tc"><?php _e('Title', 'html24');?></label>
                <select id="tc" name="tc">
                    <option disabled selected><?php _e('Title', 'html24');?></option>
                    <?php
                    $taxonomyTitles = get_terms('jobtitle');
                    if(is_array($taxonomyTitles)){
                        foreach ($taxonomyTitles as $key => $taxonomyTitle):
                            if($taxonomyTitle->parent == 0):
                            ?>

                            <option value="<?php echo $taxonomyTitle->term_id; ?>"><?php echo $taxonomyTitle->name;?></option>

                        <?php
                            endif;
                        endforeach;
                    }
                    ?>
                </select>
            </span>
            <!-- We hide this selector on all pages except .com website  -->
            <?php  if ( $languagePrefix == 'en' ) { ?>
            <span>
                <label class="hidden" for="td"><?php _e('Countries', 'html24');?></label>
                <select id="td" name="td">
                    <option disabled selected><?php _e('Countries', 'html24');?></option>
                    <?php
                    $taxonomyCountries = get_terms('country');
                    if(is_array($taxonomyCountries)){
                        foreach ($taxonomyCountries as $key => $taxonomyCountry):?>

                            <?php
                            if($taxonomyCountry->parent != 0){
                                continue;
                            }
                            ?>

                            <option value="<?php echo $taxonomyCountry->term_id; ?>"><?php echo $taxonomyCountry->name;?></option>

                        <?php endforeach;
                    }
                    ?>
                </select>
            </span>
            <?php  } ?>
            <button type="submit" class="thisis"> <?php _e('Search', 'html24');?> </button>
        </p>
    </fieldset>
</form>
