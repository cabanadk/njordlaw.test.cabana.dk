
    <div class="share-buttons">

        <ul>

            <!--LinkedIn-->
            <li>
                <?php
                $shareOnLI = new ShareOnLI();
                $shareOnLI->getShareButton();
                ?>
            </li>

            <!--Twitter-->
            <li>
                <?php
                $shareOnTW = new ShareOnTW();
                $shareOnTW->getShareButton();
                ?>
            </li>

            <!--Facebook-->
            <li>
                <?php
                $shareOnFB      = new ShareOnFB();
                $shareOnFB->getShareButton();
                ?>
            </li>


            <!--Email-->
            <li>

                <?php
                $ShareOnEmail = new ShareOnEM();
                $ShareOnEmail->getShareButton();
                ?>

            </li>

            <!--Print-->
            <li>

                <?php
                $shareOnPrint = new ShareOnPrint();
                $shareOnPrint->getShareButton();
                ?>

            </li>


        </ul>

    </div>
