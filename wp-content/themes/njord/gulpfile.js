var gulp = require('gulp'),
    compass = require('gulp-compass'),
    browserSync = require('browser-sync');

// Default task
gulp.task('default', function() {
    gulp.run('init-browsersync');
    gulp.watch('scss/**/*.scss', ['compile-sass']);
    gulp.watch('css/*.css', ['reload-browser']);
    gulp.watch('*.php', ['reload-browser']);
});

// Will initialize server for browserSync
gulp.task('init-browsersync', function() {
    browserSync({
        proxy: "192.168.0.149"
    });
});

// Will compile Sass
gulp.task('compile-sass', function() {
    gulp.src('scss/**/*.scss')
        .pipe(compass({
            config_file: './config.rb',
            css: 'css',
            sass: 'scss'
        }))
        .pipe(gulp.dest('css'))
});

// Will reload the browser
gulp.task('reload-browser', function() {
    browserSync.reload();
});

// Build the project and put destination files in the production folder
gulp.task('build', function() {
    gulp.src(['./**', '!./{node_modules,node_modules/**}', '!./{scss,scss/**}', '!./{build,build/**}', '!gulpfile.js'], {base: '.'})
        .pipe(gulp.dest('build'))
});