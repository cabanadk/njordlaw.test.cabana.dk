<?php get_header(); ?>

    <?php
    $newsThumbnail = get_the_post_thumbnail($post->ID, 'full', array('width'=>"1572",'height'=>"500"));
    if(!empty($newsThumbnail)):
    ?>
    <figure id="featured">

        <?php echo $newsThumbnail;?>

    </figure>
    <?php endif; 

        $categories='';
        $types = get_the_terms(get_the_ID(), 'type-of-knowledge' ); 
        foreach ( $types as $type ) { 
            $categories .= $type->name.'  ';
        }

        $areas_of_the_post = get_field('choose_area_of_expertise');
        if($areas_of_the_post){
            if( count($areas_of_the_post) > 0 ){
                foreach ($areas_of_the_post as $area_of_the_post) {
                    $area_name = get_the_title( $area_of_the_post );
                    $this_post_areas .= ' | '.$area_name;
                }
            }
        }
    ?>
    <section id="content" class="cols-a">
        <article>
            <p class="scheme-b date_post"><?php the_date('d.m.Y'); ?></p>
            <p class="scheme-b before_title"><?php echo $categories; ?> <?php //echo $this_post_areas; ?></p>
            <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
            <h1><?php the_title();?></h1>
            <?php
            $content = do_shortcode(wpautop($post->post_content));
            echo str_replace(get_the_title(), '', $content); ?>
            <?php include ('template_parts/social-sharing-buttons.php');?>
        </article>

        <?php

        //Set latest news to false and do a check for the section in the loop
        $latestNews = false;

        // check if the flexible content field has rows of data
        if( have_rows('post_flexible_content') ):

            // loop through the rows of data
            while ( have_rows('post_flexible_content') ) : the_row();

            if( get_row_layout() == 'post_employee_section' ):
                include "template_parts/employee-section.php";

            elseif(get_row_layout('post_latest_news_section')):

                $latestNews = true;
                $background = get_sub_field('post_latest_news_background');

            endif;

            endwhile;
        else :
                // no layouts found
        endif;
        ?>


    </section>

            <?php

                if ($latestNews == true){

                    include('template_parts/latest-news-section.php');

                }

            ?>

<?php get_footer(); ?>