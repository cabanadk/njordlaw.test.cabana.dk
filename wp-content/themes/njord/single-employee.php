<?php get_header();?>

<article id="cv" class="vcard">
    <span id="before_cv"></span>
    <figure>
        <?php 
            if ( has_post_thumbnail(get_the_ID()) ) {
                echo get_the_post_thumbnail( get_the_ID(), 'full' );
            } else { ?>
                <?php 
                $uploads = wp_upload_dir(); 
                echo '<img src="' . esc_url( $uploads['baseurl'] . '/2017/07/placeholder-image-article.png' ) . '" href="#">'; }
            ?>
    </figure>
    <div class="emp-content">
    <header>
        <h1 class="fn org" itemprop="name"><?php the_title(); ?></h1>
        <p>
            <?php
            $customJobtitles = get_field('jobtitles_repeater', get_the_ID());
            if(!empty($customJobtitles)){
                foreach($customJobtitles as $key => $customJobtitle)
                {
                    echo '<span itemprop="jobtitle">'.$customJobtitle['jobtitle_single']->name.', </span>';
                }
            } else {
                $aJobtitles = wp_get_post_terms( $post->ID, 'jobtitle' );
                if(!empty($aJobtitles)){
                    foreach($aJobtitles as $jobtitle) {
                        if($jobtitle->parent != 0){
                            echo '<span itemprop="jobtitle">'.$jobtitle->name.', </span>';
                        }
                    }
                }
            }
            ?>
            <br>
            <?php
                $locations = wp_get_post_terms( $post->ID, 'country' );
                $currentLocation = '';
                if(!empty($locations)){
                    foreach($locations as $location) {
                        if($location->parent != 0) {
                            $currentLocation = $location->slug;
                            echo '<span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span class="locality" itemprop="addressLocality">'.$location->name.'</span></span></p>';
                        }
                    }
                }
            ?>
    </header>
    <?php
    $aSpecialties = get_field('specialties_rel_employee');
    if(!empty($aSpecialties)){?>
    <h3><?php _e('Areas of Expertise' , 'html24');?></h3>
        <p>
            <?php

                    foreach($aSpecialties as $specialty) {
                        $specLink = get_permalink($specialty->ID);

                        if (get_field('custom_title_on_employee_page', $specialty->ID)) {
                            $specTitle = get_field('custom_title_on_employee_page', $specialty->ID);
                            echo '<a href="'.$specLink.'">'.$specTitle.'</a><br/>';
                        } else {
                            echo '<a href="'.$specLink.'">'.$specialty->post_title.'</a><br />';
                        }
                    }
            ?>
        </p>
    <?php
    }
    ?>


    <h3><?php _e('Contact' ,'html24'); ?></h3>

    <?php
    $directPhone = get_field('phone_direct');
    $mobilePhone = get_field('phone_mobile');
    $email = get_field('email_address');
    ?>

    <div class="double-a b">
        <p>
        <?php if(!empty($directPhone)):?>
            <?php _e('Direct:', 'html24');?> <span class="tel" itemprop="telephone"><?php echo $directPhone; ?></span>
            <br>
        <?php endif; ?>

        <?php if(!empty($mobilePhone)):?>
            <?php _e('Mobile:', 'html24');?> <span class="tel" itemprop="telephone"><?php echo $mobilePhone; ?></span>
            <br>
        <?php endif; ?>

        <?php if(!empty($email)):?>
            <a class="email" itemprop="email"><?php echo $email; ?></a>
        <?php endif; ?>

        </p>
        <ul class="list-c">
            <?php $linkedInUrl = get_field('linkedin_profile_link');
            if(!empty($linkedInUrl)):?>
            <li class="li"><a href="<?php echo $linkedInUrl; ?>"><?php _e('LinkedIn Profile','html24')?></a></li>
            <?php endif; ?>
            <!-- Send also current location to vCard  -->
            <li class="vc"><a href="<?php bloginfo('template_directory'); ?>/template_parts/vcard-download.php?employeeID=<?php the_ID(); ?>&langCode=<?php echo ICL_LANGUAGE_CODE;?>&location=<?php echo $currentLocation; ?>" target="_blank"><?php _e('Download vCard', 'html24'); ?></a></li>
        </ul>
    </div>
    </div>
</article>


<?php
if(get_field('show_inpage_navigation', $post->ID)){
    include('template_parts/inpage-navigation.php');
}
?>
<article id="content">
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>
    <?php include ('template_parts/social-sharing-buttons.php');?>
</article>


<?php get_footer(); ?>