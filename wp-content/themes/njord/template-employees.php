<?php
/*
 Template Name: Employee list
 */
?>

<?php get_header(); ?>

<?php include ('template_parts/employee-filter.php');?>

<section id="content" class="a">

    <?php

    $languageCode = ICL_LANGUAGE_CODE;
    $employees = getTheEmployees($languageCode);

    //Check if a search is posted and add this hidden input to use in the AJAX function
    ?>
        <input type="hidden" class="search-parameters"
            <?php

            if($_POST['ta']){
                echo ' data-name-param="'.$_POST["ta"].'"';
            }

            if($_POST['tf']){
                echo ' data-specialty-param="'.$_POST["tf"].'"';
            }

            if($_POST['tc']){
                echo ' data-jobtitle-param="'.$_POST["tc"].'"';
            }

            if($_POST['td']){
                echo ' data-country-param="'.$_POST["td"].'"';
            }

            ?>
            />
    <?php
    if(!empty($employees)){ ?>

        <ul class="gallery-a employee-list">

            <?php

            //Check if the user has searched for Jobtitle AND Country
            $jobTitleAndCountry = false;
            if(!empty($_POST['tc']) && !empty($_POST['td'])){
                $jobTitleAndCountry = true;
            }

            foreach($employees as $key => $employee):
                $employeeMatch = false;
                if($jobTitleAndCountry == true){

                    $jobTitleArray = array();
                    $countryArray = array();

                    $employeeJobTitleTerms = wp_get_post_terms($employee->ID, 'jobtitle');
                    foreach($employeeJobTitleTerms as $employeeJobTitleTerm){
                        $jobTitleArray[] = $employeeJobTitleTerm->term_id;
                    }

                    $employeeCountryTerms = wp_get_post_terms($employee->ID, 'country');
                    foreach($employeeCountryTerms as $employeeCountryTerm){
                        $countryArray[] = $employeeCountryTerm->term_id;
                    }

                    if(in_array($_POST['tc'], $jobTitleArray) && in_array($_POST['td'], $countryArray)){
                        $employeeMatch = true;
                    }

                }

                if($jobTitleAndCountry == true && $employeeMatch != true){
                    continue;
                }

                $empSpecialties = get_field('specialties_rel_employee', $employee->ID);
                    $empSpecArray = array();
                if(!empty($empSpecialties)){
                    foreach ($empSpecialties as $empSpec){
                        $empSpecArray[] = $empSpec->ID;
                    }
                }
                ?>

                <li class="vcard" itemscope itemtype="http://schema.org/Person" data-employee-id="<?php echo $employee->ID; ?>" data-employee-specialty='<?php echo json_encode($empSpecArray);?>'>
                    <a class="img" href="<?php echo get_permalink($employee->ID); ?>">

                        <?php 
                        if ( has_post_thumbnail($employee->ID) ) {
                            echo get_the_post_thumbnail($employee->ID, 'employee-thumb', array('class' => 'photo', 'itemprop' => 'image')); 
                        } else { ?>
                            <?php 
                            $uploads = wp_upload_dir(); 
                            echo '<img src="' . esc_url( $uploads['baseurl'] . '/2017/07/placeholder-image-thumbnail.png' ) . '" href="#">'; }
                        ?>

                    </a>

                    <span class="fn" itemprop="name"><?php echo $employee->post_title;?></span>

                    <span class="titles_and_city">
                        <?php
                        $customJobtitles = get_field('jobtitles_repeater', $employee->ID);
                        if(!empty($customJobtitles)):
                            foreach($customJobtitles as $customJobtitle) : ?>
                                <span itemprop="jobtitle" data-employee-jobtitle='<?php echo $customJobtitle['jobtitle_single']->term_id; ?>'><?php echo $customJobtitle['jobtitle_single']->name.', '; ?></span>
                            <?php endforeach;
                        else :
                            $aJobtitles = wp_get_post_terms( $employee->ID, 'jobtitle' );
                            if(!empty($aJobtitles)):
                                foreach($aJobtitles as $jobtitle) :
                                    if($jobtitle->parent != 0): ?>
                                        <span itemprop="jobtitle" data-employee-jobtitle='<?php echo $jobtitle->term_id; ?>'><?php echo $jobtitle->name.', '; ?></span>
                                    <?php endif;
                                endforeach;
                            endif;
                        endif; ?>

                        <span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

                            <?php
                                $locations = wp_get_post_terms($employee->ID, 'country');
                                foreach($locations as $location) {
                                    if($location->parent != 0) {
                                        echo '<span class="locality" itemprop="addressLocality" data-employee-country="'.$location->ID.'">'.$location->name.'</span>';
                                    }
                                }
                            ?>



                        </span>

                    </span>
                    <br/>

                    <?php
                    $directPhone    = get_field('phone_direct', $employee->ID);
                    $mobilePhone    = get_field('phone_mobile', $employee->ID);
                    $email          = get_field('email_address', $employee->ID);
                    ?>

                    <?php if(!empty($directPhone)):?>
                        <?php _e('Direct', 'html24'); ?>:
                        <span class="tel" itemprop="telephone"><?php echo $directPhone; ?></span>
                        <br>
                    <?php endif; ?>

                    <?php if(!empty($mobilePhone)):?>
                        <?php _e('Mobile', 'html24'); ?>:
                        <span class="tel" itemprop="telephone"><?php echo $mobilePhone; ?></span>
                        <br>
                    <?php endif; ?>

                    <?php if(!empty($email)):?>
                        <a href="mailto:<?php echo $email; ?>" class="email" itemprop="email"><?php echo $email; ?></a>
                    <?php endif; ?>
                </li>

            <?php endforeach; ?>

        </ul>
        <p class="loading-a" id="icon_spinner" style="display: none;"><?php _e('Loading', 'html24');?></p>
    <?php
        } else {

        ?>

        <h1>
            <?php _e('No employees found','html24'); ?>
        </h1>

        <?php

        }
    ?>

</section>

<?php get_footer(); ?>