<?php get_header(); ?>

    <section id="content" class="a">
        <h1 class="header-a"><?php _e('404 - File not found', '404');?></h1>
        <p class="scheme-a scheme-d">

            <?php _e('We could not find what you were looking for.' , '404');?>
            <br>
            <?php _e('Instead try our', '404');?>

                <a href="./"><?php _e('search function', '404');?></a>,

            <?php _e('or go back to the front page', '404');?>
        </p>
        <form action="./" method="post" class="form-a">
            <fieldset>
                <p>
                    <label for="faa"><?php _e('What are you looking for?', '404');?></label>
                    <input type="text" id="faa" name="faa" required>
                    <button type="submit"><?php _e('Search', 'html24');?></button>
                </p>
            </fieldset>
        </form>
    </section>

<?php get_footer(); ?>