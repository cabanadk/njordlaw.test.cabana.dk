<?php
/*
Template Name: Block Grid
 */
?>
<?php get_header(); ?>

<?php $postThumbnailUrl = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>

<?php if($postThumbnailUrl): ?>
<figure id="featured">
    <img src="<?php echo $postThumbnailUrl; ?>" alt="<?php the_title(); ?>" width="1572" height="500" />
</figure>
<?php endif; ?>

<section id="content">
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>

    <?php if( have_rows('block_grid_repeater') ): ?>
    <ul class="block-grid block-grid-left">
        <?php while( have_rows('block_grid_repeater') ):
              the_row(); ?>
        <?php if(get_sub_field('position') == 'left'): ?>
        <li>
            <h5>
                <?php the_sub_field('title'); ?>
            </h5>
            <p>
                <?php the_sub_field('description'); ?>
            </p>            
            <p>
                <a href="<?php the_sub_field('link_url'); ?>">
                    <?php the_sub_field('link_title'); ?>
                </a>
            </p>
        </li>
        <?php endif; ?>
        <?php endwhile; ?>
    </ul>
    <ul class="block-grid block-grid-right">
        <?php while( have_rows('block_grid_repeater') ):
                  the_row(); ?>
        <?php if(get_sub_field('position') == 'right'): ?>
        <li>
            <h5>
                <?php the_sub_field('title'); ?>
            </h5>
            <p>
                <?php the_sub_field('description'); ?>
            </p>
            <p>
                <a href="<?php the_sub_field('link_url'); ?>">
                    <?php the_sub_field('link_title'); ?>
                </a>
            </p>
        </li>
        <?php endif; ?>
        <?php endwhile; ?>
    </ul>
    <?php endif; ?>

    <div class="clear"></div>

    <?php include('template_parts/social-sharing-buttons.php');?>
</section>
<div class="clear"></div>

<script>
    jQuery(function ($) {
        $('.block-grid').each(function (index, element) {
            var $blockGrid = $(element);
            $blockGrid.find('li').each(function (index, element) {
                var $blockGridItem = $(element);
                $blockGridItem.find('h5').on('click', function (e) {
                    $('.block-grid').find('li').not($blockGridItem).find('p').hide();
                    $blockGridItem.find('p').toggle();                    
                });
            });
        });
    });
</script>

<?php get_footer();?>