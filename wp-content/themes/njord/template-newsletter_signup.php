<?php
/*
 Template Name: Newsletter signup form
 */
?>

<?php get_header(); ?>

<?php
$post_thumbnail_id = get_post_thumbnail_id();
$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
$languageCode = ICL_LANGUAGE_CODE;

?>
	<figure id="featured"><img src="<?php echo $post_thumbnail_url; ?>" width="1572" height="500"></figure>
    <article id="content" class="newsletter-content">
    	<?php if(!empty($_POST) && array_key_exists('list', $_POST)):?>
				<p class="scheme-a"><?php _e('A confirmation mail has been sent to your email address' , 'html24'); ?></p>
		<?php endif;?>
    	<?php the_content(); ?>
		<form method="post" class="validate" id="newsletterForm">
			<?php 
				$danishNewsletterLink = site_url() . "/da/nyhedsbrev"; 
				$germanNewsletterLink = site_url() . "/de/newsletter"; 
				$englisgNewsletterLink = site_url() . "/newsletter"; 

				switch ($languageCode) {
			        case "da": ?>
			        	<p class="link-b">
							<a href="<?php echo $englisgNewsletterLink;?>"><?php _e('Do you prefer your legal news in English? Click here', 'html24');?></a><br>
							<a href="<?php echo $germanNewsletterLink;?>"><?php _e('Do you prefer your legal news in German? Click here', 'html24');?></a>
						</p>
			        <?php
			        break;
			        case "de": ?>
			        	<p class="link-b">
							<a href="<?php echo $danishNewsletterLink;?>"><?php _e('Do you prefer your legal news in Danish? Click here', 'html24');?></a><br>
							<a href="<?php echo $englisgNewsletterLink;?>"><?php _e('Do you prefer your legal news in English? Click here', 'html24');?></a>
						</p>
			        <?php break;

			        case "en": ?>

		        		<p class="link-b">
				        	<?php if (!get_field("english_newsletter_link_text_danish")) { ?>
				        		<a href="<?php echo $danishNewsletterLink;?>"><?php _e('Do you prefer your legal news in Danish? Click here', 'html24');?></a><br>
				        	<?php } else { ?>
				        		<a href="<?php echo $danishNewsletterLink;?>"><?php the_field("english_newsletter_link_text_danish") ?></a><br>						
				        	<?php 
				        	} 

				        	if (!get_field("english_newsletter_link_text_german")) { ?>
				        		<a href="<?php echo $germanNewsletterLink;?>"><?php _e('Do you prefer your legal news in German? Click here', 'html24');?></a>
				        	<?php } 
				        	else { ?>
				        		<a href="<?php echo $germanNewsletterLink;?>"><?php the_field("english_newsletter_link_text_german"); ?></a>	
				        	<?php 
				        	}
				        	?>
			        	</p>
			        	
			        <?php break;
			    }
			?>

			<ul class="checklist-a u">
				<?php

				// check if the repeater field has rows of data
				if( have_rows('newsletter_check_boxes') ):

				 	// loop through the rows of data
				    while ( have_rows('newsletter_check_boxes') ) : the_row(); ?>
					<?php 

						$string = get_sub_field("newsletter_check_box_name");
						$string = str_replace(' ', '', $string);

						$otherLang = get_sub_field("newsletter_diffrent_languge");
						if ($otherLang == "") { ?>
							<li><label for="<?php echo $string; ?>"><input type="checkbox" value="<?php the_sub_field("newsletter_check_box_id") ?>" name="list[]" id="<?php echo $string; ?>"><?php _e(the_sub_field("newsletter_check_box_name"),'html24') ?></label></li>
						<?php } else { ?>
							<li><label for="<?php echo $string; ?>"><input type="checkbox" value="<?php the_sub_field("newsletter_check_box_id") ?>" name="list[]" id="<?php echo $string; ?>"><?php _e(the_sub_field("newsletter_check_box_name"),'html24') ?><?php _e(' (Published in '. $otherLang[0] . ')','html24'); ?></label></li>
						<?php }
						
						 ?>
				   <?php endwhile;

				else :
				    // no rows found

				endif;

				?>
	
			</ul>
			<div class="recaptchaResponse">
				<div class="g-recaptcha" data-sitekey="6LfmCCwUAAAAAERZrHuCBkfmAnxAqZYyw1Hmcvv3"></div>
			</div>
			<div style="position: absolute; left: -5000px;"><input type="text" name="b_ea385526fc54a74f2a2de90d1_f8923a216d" tabindex="-1" value=""></div>
			<p class="input-a">
				<span>
					<label for="fname" style="margin-top: 0px;"><?php _e('First name', 'html24'); ?></label>
					<input type="text" id="fname" value="" name="FNAME" class="validInput" required>
				</span>
				<span>
					<label for="lname"><?php _e('Last name', 'html24'); ?></label>
					<input type="text" id="lname" value="" name="LNAME" class="validInput" required>
				</span>
				<span>
					<label for="email"><?php _e('E-mail address', 'html24'); ?></label>
					<input type="email" id="email" value="" name="EMAIL" class="validInput required email" required>
				</span>
				<span>
					<input type="button" value="<?php _e('Subscribe', 'html24'); ?>" name="subscribe" class="newsletterformButton">
				</span>
			</p>

		</form>
 <?php 
 if(isset($_POST) && !empty($_POST)){

    $listIds         = $_POST['list'];
    $fname           = $_POST['FNAME'];
    $lname           = $_POST['LNAME'];
    $email           = $_POST['EMAIL'];

    if(empty($listIds)){
        return;
    }

    if(!empty($listIds) && is_array($listIds)){

        foreach($listIds as $listId){
            $ip             = '77.66.108.8';
            $apiKey         = '17ef0446aa83370e971e1dae9be35065-us3';
            $userId         = 'ea385526fc54a74f2a2de90d1&amp;';

            $merge_vars=array(
                'OPTIN_IP'=>$ip,
                'OPTIN-TIME'=>"now",
                'FNAME'=>ucwords(strtolower(trim($fname))),
                'LNAME'=>ucwords(strtolower(trim($lname))),
            );

            $send_data=array(
                'email'=>array('email'=>$email),
                'apikey'=>$apiKey,
                'id'=>$listId,
                'merge_vars'=>$merge_vars,
                'double_optin'=>true,
                'update_existing'=>true,
                'replace_interests'=>false,
                'send_welcome'=>true,
                'email_type'=>"html",
            );

            $payload=json_encode($send_data);
            $submit_url="https://us3.api.mailchimp.com/2.0/lists/subscribe.json";
            $ch=curl_init();
            curl_setopt($ch,CURLOPT_URL,$submit_url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$payload);
            $result=curl_exec($ch);
            curl_close($ch);

            $mcdata=json_decode($result);

            if (!empty($mcdata->status) && $mcdata->status === "error") {
                return "Mailchimp Error: " . $mcdata->error;
            }
        }
    }


};

?>	
</article>

<?php get_footer(); ?>