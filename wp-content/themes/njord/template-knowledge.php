<?php
/*
 Template Name: Knowledge list
 */

$languageCode = ICL_LANGUAGE_CODE;
$num_of_posts   = 3;
$postCounter = 0;
?>

<?php get_header(); ?>

<style type="text/css">
    .knowledge-a header h3{
        margin: 0 0 4px;
    }
    .knowledge-a header p{
        margin: 0 0 1px;
    }
</style>

<?php

// The Query
$the_query = new WP_Query( array(
    'post_type' => 'knowledge',
    'posts_per_page' => -1
) );


// The Loop
$used_areas = array();
if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $areas_of_the_post = get_field('choose_area_of_expertise');
        if( !empty($areas_of_the_post) ){
            foreach ($areas_of_the_post as $area_of_the_post) {
                $area_name = get_the_title( $area_of_the_post );
                if ( !in_array($area_name, $used_areas) ) {
                    $used_areas[] = $area_name;
                }
            }
        }
    }
}
wp_reset_postdata();
?>

<nav id="aside">
    <ul>
        <?php
        foreach ($used_areas as $used_area) {
            echo '<li class="btn_area" date-current-lang="'.$languageCode.'"><a>'.$used_area.'</a></li>';
        }
        ?>
    </ul>
</nav>
<section id="content">
    <h1><?php the_title(); ?></h1>
    <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
    <?php the_content(); ?>
    <div class="knowledge-a" id="knowledge_list">
    <?php
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $this_post_areas = '';
                $categories='';
                $types = get_the_terms(get_the_ID(), 'type-of-knowledge' );
                foreach ( $types as $type ) { 
                    $categories .= $type->name.'  ';
                }

                /* This was used to display also the name of the areas each post had. But for now they didn't want it anymore.
                $areas_of_the_post = get_field('choose_area_of_expertise');
                if($areas_of_the_post){
                    if( count($areas_of_the_post) > 0 ){
                        
                        foreach ($areas_of_the_post as $area_of_the_post) {

                            $area_name = get_the_title( $area_of_the_post );
                            
                            $this_post_areas .= ' | '.$area_name;
                            
                            
                            if ( !in_array($area_name, $used_areas) ) {
                                $used_areas[] = $area_name; 
                            }
                        }
                    }
                }*/

                if ( $postCounter <= $num_of_posts) {
                    $postCounter++;
                    ?>
                    <article data-article-id="<?php the_ID(); ?>">

                        <header>
                            <p><?php echo $categories; ?></p>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </header>
                        <?php
                        if (has_shortcode($content = get_the_content(), 'intro')) {
                            $introText = check_for_intro_text($content);
                            echo wpautop($introText);
                        } else {
                            echo wpautop(str_replace(get_the_title(), '', get_the_excerpt()));
                        }
                        ?>
                    </article>
                    <?php
                }
            }//endwhile
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
    </div>
    <p class="loading-a" id="icon_spinner" style="display: none;"><?php _e('Loading', 'html24');?></p>
</section>
<?php get_footer(); ?>
