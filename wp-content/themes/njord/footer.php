<?php wp_footer(); ?>

<?php

    $languagePrefix = ICL_LANGUAGE_CODE;

?>
<?php
        $youtubeLink    = get_field($languagePrefix.'_youtube_url', 'options');
        $facebookLink   = get_field($languagePrefix.'_facebook_url', 'options');
        $linkedInLink   = get_field($languagePrefix.'_linkedin_url', 'options');
        $instagram   = get_field($languagePrefix.'_instagram_url', 'options');
        ?>

<footer id="footer">
    <nav>
        <div>
            <h2>
                <?php 
                    $value = get_field($languagePrefix.'_footer_first_col_heading', 'options');
                    if( $value ) {
                        echo $value;
                    } else {
                        echo '<br/>';
                    }
                ?>          
            </h2>

            <?php
            $firstColPageLinks = get_field($languagePrefix.'_footer_first_col_links', 'options');
            if($firstColPageLinks):?>
                <ul>
                    <?php foreach ($firstColPageLinks as $pageLink):?>

                            <li><a href="<?php echo get_permalink($pageLink->ID); ?>"><?php echo $pageLink->post_title; ?></a></li>

                    <?php endforeach; ?>
                </ul>
            <?php endif;?>
            <ul class="social-a">
            <?php if($linkedInLink != ''):?>
            <li class="li">
                <a rel="external" href="http://<?php echo $linkedInLink; ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
                    <g>
                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707   c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21   v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824   C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463   c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z    M5.477,420.56h92.184v-277.32H5.477V420.56z" fill="#ccc"/>
                    </g>

</svg>
                </a>
            </li>
            <?php endif; ?>
            <?php if($facebookLink != ''):?>
            <li class="fb"><a rel="external" href="http://<?php echo $facebookLink; ?>" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" fill="#ccc" viewBox="0 0 50 50" enable-background="new 0 0 50 50" width="35px" height="35px"><path d="M26 20v-3c0-1.3.3-2 2.4-2H31v-5h-4c-5 0-7 3.3-7 7v3h-4v5h4v15h6V25h4.4l.6-5h-5z"/></svg>
            </a></li>
            <?php endif; ?>
            <?php if($youtubeLink != ''):?>
                <li class="yt">
                    <a rel="external" href="http://<?php echo $youtubeLink; ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96" width="20px"><path fill="#ccc" d="M94.98,28.84c0,0-0.94-6.6-3.81-9.5c-3.64-3.81-7.72-3.83-9.59-4.05c-13.4-0.97-33.52-0.85-33.52-0.85s-20.12-0.12-33.52,0.85c-1.87,0.22-5.95,0.24-9.59,4.05c-2.87,2.9-3.81,9.5-3.81,9.5S0.18,36.58,0,44.33v7.26c0.18,7.75,1.14,15.49,1.14,15.49s0.93,6.6,3.81,9.5c3.64,3.81,8.43,3.69,10.56,4.09c7.53,0.72,31.7,0.89,32.54,0.9c0.01,0,20.14,0.03,33.54-0.94c1.87-0.22,5.95-0.24,9.59-4.05c2.87-2.9,3.81-9.5,3.81-9.5s0.96-7.75,1.02-15.49v-7.26C95.94,36.58,94.98,28.84,94.98,28.84z M38.28,61.41v-27l25.74,13.5L38.28,61.41z"/></svg>    
                    </a>
                </li>
            <?php endif; ?>
            <?php if($instagram != ''):?>
            <li class="li">
                <a rel="external" href="http://<?php echo $instagram; ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#ccc" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <rect x="2" y="2" width="20" height="20" rx="5" ry="5"/>
                        <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"/>
                        <line x1="17.5" y1="6.5" x2="17.5" y2="6.5"/>
                    </svg>
                </a>
            </li>
            <?php endif; ?>
        </ul>
        </div>
        <div>
            <h2>
                <?php 
                    $value = get_field($languagePrefix.'_footer_second_col_heading', 'options');
                    if( $value ) {
                        echo $value;
                    } else {
                        echo '<br/>';
                    }
                ?>
            </h2>
            <p><?php echo get_field($languagePrefix.'_footer_second_col_content', 'options');?></p>
        </div>
        <div>
            <h2>
                <?php 
                    $value = get_field($languagePrefix.'_footer_third_col_heading', 'options');
                    if( $value ) {
                        echo $value;
                    } else {
                        echo '<br/>';
                    }
                ?>
                
            </h2>
            <p><?php echo get_field($languagePrefix.'_footer_third_col_content', 'options');?></p>
        </div>
        <div class="newsletter_footer_col">
            <h2>
                <?php 
                    $value = get_field($languagePrefix.'_footer_fourth_col_heading', 'options');
                    if( $value ) {
                        echo $value;
                    } else {
                        echo '<br/>';
                    }
                ?>
            </h2>
            <p><?php echo get_field($languagePrefix.'_footer_fourth_col_content', 'options');?></p>
            <?php $newsletterButton = get_field($languagePrefix.'_footer_fourth_col_link', 'options');
            if(!empty($newsletterButton)):
            ?>
            <p class="link-a"><a href="<?php echo $newsletterButton; ?>"><?php echo get_field($languagePrefix.'_footer_fourth_col_link_text', 'options');?></a></p>
            <?php endif;?>
        </div>
    </nav>
    <p>&copy; <span class="date"></span> <?php echo get_field($languagePrefix.'_copyright_name', 'options');?></p>
</footer>
</div> <!-- END root -->

<?php include ('template_parts/cookie-box.php');?>
<?php include ('template_parts/newsletter-popup.php');?>

<script>
	var forceRedraw = function (element) {
		if (!element) { return; }
		var n = document.createTextNode(' ');
		var disp = element.style.display;
		element.appendChild(n);
		element.style.display = 'none';
		setTimeout(function () {
			element.style.display = disp;
			n.parentNode.removeChild(n);
		}, 500);
	}
	forceRedraw(document.getElementById('root'));
</script>

</body>
</html>