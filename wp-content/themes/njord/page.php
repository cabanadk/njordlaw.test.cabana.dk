<?php get_header(); ?>

<?php $postThumbnailUrl = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>

    <figure id="featured"><img src="<?php echo $postThumbnailUrl; ?>" alt="Placeholder" width="1572" height="500"></figure>


<?php if(get_field('show_inpage_navigation')){include('template_parts/inpage-navigation.php');}?>

    <section id="content">
        <?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
        <?php the_content(); ?>
        <?php include('template_parts/social-sharing-buttons.php');?>


    </section>
    <div class="clear"></div>

<?php
    // check if the flexible content field has rows of data
    if( have_rows('defaultpage_flexible_content') ):
        // loop through the rows of data
        while ( have_rows('defaultpage_flexible_content') ) : the_row();
            if( get_row_layout() == 'contacts_section' ):
                include('template_parts/contacts-section.php');
            elseif( get_row_layout() == 'latest_news_section' ):
                $related_news_boolean = get_sub_field('related_news_boolean');

                if($related_news_boolean)
                {
                    include('template_parts/pick-news-section.php');
                }
                else
                {
                    include('template_parts/latest-news-section.php');
                }
            endif;
        endwhile;
    else :
        // no layouts found
    endif;
?>

<?php get_footer();?>