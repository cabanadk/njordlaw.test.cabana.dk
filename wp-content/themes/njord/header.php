<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, target-densitydpi=device-dpi">
    <meta property="og:image" content="<?php bloginfo('template_directory');?>/img/social-default-img.jpg" />
		
	<meta http-equiv="x-ua-compatible" content="IE=edge" >
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" media="screen, print" href="<?php bloginfo('template_directory'); ?>/css/app<?php echo get_fingerprint_id(); ?>.css" />
    <link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo('template_directory'); ?>/css/print<?php echo get_fingerprint_id(); ?>.css" />
    <!-- Google analytics tracking code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-58417403-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        //Define the template directory for the scripts
        var template_dir = '<?php bloginfo('template_directory');?>';
        var current_language = '<?php echo ICL_LANGUAGE_CODE; ?>';
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <?php $languageCode = ICL_LANGUAGE_CODE; ?>
    <script src="https://www.google.com/recaptcha/api.js?hl=<?php echo $languageCode ?>" async defer></script>
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!-- This script makes the HTML5 tags readable for IE -->
    <!--[if lt IE 9]> <script src="<?php bloginfo('template_directory'); ?>/js/html5shiv.js"></script> <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="root">
    <header id="top">
        <p id="logo"><a href="<?php echo home_url(); ?>" accesskey="h">Njord</a></p>
        <nav id="nav">
            <ul class="a">
                <?php
                    wp_nav_menu(array(
                        'menu'    => 'Menu A', //menu id
                        'walker'  => new Menu_B(), //use our custom walker
                        'container' => '', // remove the div container
                        'items_wrap' => '%3$s' // remove the ul item wrap
                    ));
                ?>
                <li class="a"><a href="./"><?php _e('Search' ,'html24'); ?></a> <em></em></li>
            </ul>
            <ul class="b">
                <?php
                    wp_nav_menu(array(
                        'menu'    => 'Menu B', //menu id
                        'walker'  => new Menu_A(), //use our custom walker
                        'container' => '', // remove the div container
                        'items_wrap' => '%3$s' // remove the ul item wrap
                    ));
                ?>
            </ul>

            <?php include('template_parts/language_picker.php'); ?>

        </nav>
        
    </header>
    <form role="search" action="<?php echo home_url( '/' ); ?>" method="get" class="search-form" id="search">
        <fieldset>
            <p>
                <label for="sa"><?php _e('Search' ,'html24'); ?></label>
                <input type="search" id="sa" required name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                <?php $langInfo = wpml_get_language_information(get_the_ID()); ?>
                <?php if(!is_wp_error($langInfo)): ?>
                <input type="hidden" class="language_code_search" value="<?php echo $langInfo['locale'];?>"/>
                <?php endif; ?>
                <input type="hidden" class="icl_language_code" value="<?php echo ICL_LANGUAGE_CODE;?>"/>
                <a class="close" href="./"><?php _e('Close' ,'html24'); ?></a>
            </p>
            <div class="autocomplete_container">
                <p class="loading-a" id="icon_spinner" style="display: none;"><?php _e('Loading', 'html24');?></p>
            </div>
        </fieldset>
    </form>