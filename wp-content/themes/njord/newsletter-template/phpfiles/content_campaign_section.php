<tr mc:repeatable="module" mc:variant="content with background image">
    <td valign="top">

        <!-- START / TOP IMAGE -->
        <table cellpadding="0" cellspacing="0" border="0" height="292" width="640" style="min-width: 640px;width: 640px;max-width: 640px;">
            <tr>

                <!-- THESE TWO URLS CAN BE CHANGED TO ANOTHER URL FOR A DIFFERENT BACKGROUND IMAGE ALTHOUGH THE URLS HAVE TO BE THE SAME-->
                <!-- 1-->
                <td background="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/d837deb9-dc1f-4d59-80a3-093e3a1c0a73.jpg" bgcolor="#ffffff" width="640" height="367" valign="top" style="min-width:640px;width:640px;max-width:640px; height: 367px;" class="campaign-box">
                    <!-- 2 -->
                    <!--[if gte mso 9]>
                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;height:367px;">
                        <v:fill type="frame" src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/d837deb9-dc1f-4d59-80a3-093e3a1c0a73.jpg" color="#ffffff" />


                        <v:textbox inset="0,0,0,0">
                    <![endif]-->
                    <div>


                        <!-- START / SECOND CONTENT -->

                        <table cellpadding="0" border="0" cellspacing="0" style="width: 640px;max-width: 640px;min-width: 640px;">
                            <tr><td height="20" style="max-height:20px;min-height:20px;height:20px;"></td></tr><!-- Transparent Breaker -->
                            <tr>
                                <td valign="top" width="20px" style="max-width:20px;width:20px;min-width:20px;"></td>
                                <td valign="top">

                                    <!-- // Begin Module: Standard Content \\ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="min-width: 600px;width: 600px;max-width: 600px;">

                                        <tr>
                                            <td colspan="3" valign="top" width="600" style="min-width: 600px;width: 600px;max-width: 600px;">
                                                <div class="article" mc:edit="imagewithlinkheading">
                                                    *heading*
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" valign="top" width="600" style="min-width: 600px;width: 600px;max-width: 600px;">
                                                <div class="article" mc:edit="imagewithlinkcontent">
                                                    *content*
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="campaign-box-button">
                                            <td colspan="3" valign="top" width="600" style="min-width: 600px;width: 600px;max-width: 600px;">
                                                <table>
                                                    <tr>
                                                        <td valign="top" colspan="1" width="237" style="width: 237px;min-width: 237px;max-width: 237px;"></td>

                                                        <td valign="middle" colspan="1" height="40" width="125" style="width: 125px;min-width: 125px;max-width: 125px; border: 1px solid black; text-align: center;" mc:edit="article_link">
                                                            <a href="http://www.njordlaw.com" style="color: #60dd49; text-decoration: none;">
                                                                <div>Læs mere</div>
                                                            </a>
                                                        </td>

                                                        <td valign="top" colspan="1" width="237" style="width: 237px;min-width: 237px;max-width: 237px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" width="20px" style="max-width:20px;width:20px;min-width:20px;"></td>
                            </tr>
                            <tr><td height="20" style="max-height:20px;min-height:20px;height:20px;"></td></tr><!-- Transparent Breaker -->
                        </table>

                        <!-- END / SECOND CONTENT -->

                    </div>
                    <!--[if gte mso 9]>
                    </v:textbox>
                    </v:rect>
                    <![endif]-->
                </td>
            </tr>
        </table>
        <!-- END / MIDDLE IMAGE -->

    </td>
</tr>