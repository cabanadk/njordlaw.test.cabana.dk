<tr class="fullwidth heightauto" mc:repeatable="module" mc:variant="content full width">
    <td valign="top">


        <!-- START / FIRST CONTENT -->

        <table cellpadding="20" cellspacing="0" border="0">
            <tr>
                <td>

                    <!-- // Begin Module: Standard Content \\ -->
                    <table border="0" cellpadding="3" cellspacing="0" width="600">
                        <tr>
                            <td valign="top">
                                <div class="boxsize article contentimage" mc:edit="article_heading">
                                    *heading*
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="boxsize article contentimage" mc:edit="article_content">
                                    *content*
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="article fullwidth boxsize" style="width: 292px;min-width: 292px;max-width: 292px;" mc:edit="article_link">
                                <a href="http://www.njordlaw.com" style="color: #60dd49;">Læs mere</a>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <!-- END / FIRST CONTENT -->


    </td>
</tr>