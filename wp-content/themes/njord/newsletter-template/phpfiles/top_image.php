<tr>
    <td valign="top">

        <!-- START / TOP IMAGE -->
        <table class="heightauto" cellpadding="0" cellspacing="0" border="0" height="292" width="640" style="min-width: 640px;width: 640px;max-width: 640px;">
            <tr>
                <!-- THESE TWO URLS CAN BE CHANGED TO ANOTHER URL FOR A DIFFERENT BACKGROUND IMAGE ALTHOUGH THE URLS HAVE TO BE THE SAME-->
                <!-- 1-->
                <td background="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/0df0b31f-354e-408c-8bbe-133e212576e0.jpg" bgcolor="#ffffff" width="640" height="292" valign="top" style="width:640px; height: 292px;">
                    <!-- 2 -->
                    <!--[if gte mso 9]>
                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;height:292px;">
                        <v:fill type="frame" src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/0df0b31f-354e-408c-8bbe-133e212576e0.jpg" color="#ffffff" />


                        <v:textbox inset="0,0,0,0">
                    <![endif]-->
                    <div>


                        <!-- START / HEADING TEXT -->
                        <table border="0" cellpadding="0" cellspacing="0" width="640">
                            <!-- Transparent Breaker -->
                            <tr>
                                <td height="30" style="max-height:30px;min-height:30px;height:30px;"></td>
                            </tr>
                            <!-- Transparent Breaker -->
                            <tr>
                                <td valign="bottom" height="80" width="640" style="min-width: 640px;width: 640px;text-align: center;max-width: 640px; ">
                                    <img src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/f516169f-475d-48a1-898f-2e28493b5184.png" alt="Arrow">
                                </td>
                            </tr>
                            <tr>
                                <td mc:edit="newsletter_heading" valign="middle" width="640" height="60" style="min-width: 640px;width: 640px;font-size: 26px;text-align: center;max-width: 640px; color: white;">
                                    HEADING
                                </td>
                            </tr>
                            <!-- Transparent Breaker -->
                            <tr>
                                <td height="30" style="max-height:30px;min-height:30px;height:30px;"></td>
                            </tr>
                            <!-- Transparent Breaker -->
                        </table>
                        <!-- END / HEADING TEXT -->


                    </div>
                    <!--[if gte mso 9]>
                    </v:textbox>
                    </v:rect>
                    <![endif]-->
                </td>
            </tr>
        </table>

        <!-- END / TOP IMAGE -->

    </td>
</tr>