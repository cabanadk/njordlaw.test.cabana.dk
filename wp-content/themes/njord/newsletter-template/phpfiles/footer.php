</table>

<!-- END Of Wrapper for center -->


</td>
</tr>

<!--START FOOTER-->
<tr style="background-color: #252525;border-bottom: 1px solid #f4f4f4;">
    <td>

        <table width="640" align="center" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top">

                    <table width="640" align="center" cellspacing="0" cellpadding="0" border="0">
                        <!-- Transparent Breaker -->
                        <tr>
                            <td colspan="2" height="50" style="max-height:50px;min-height:50px;height:50px;width: 600px;min-width:600px;max-width: 600px;"></td>
                        </tr>
                        <!-- Transparent Breaker -->

                        <tr>
                            <td mc:edit="footeraddressleft" valign="top" style="color: #ffffff;text-align: center;">

                                <span style="color: #777777;font-size: 14px;">NJORD KØBENHAVN</span>
                                <br><br>
                                Pilestræde 58 <br>
                                DK-1112 Copenhagen, Denmark <br>
                                Telephone: +45 33 12 45 22 <br>
                                Fax: +45 33 93 60 23 <br>
                                <br><br>
                            </td>
                            <td mc:edit="footeraddressright" valign="top" style="color: #ffffff;text-align: center;">

                                <span style="color: #777777;font-size: 14px;">NJORD AARHUS</span>
                                <br><br>
                                Klostergade 28 <br>
                                DK-8000 Aarhus C, Denmark<br>
                                Telephone: +45 33 12 45 22<br>
                                Fax: +45 33 93 60 23<br>
                                <br><br>
                            </td>
                        </tr>

                        <!-- Transparent Breaker -->
                        <tr>
                            <td colspan="2" height="50" style="max-height:50px;min-height:50px;height:50px;width: 600px;min-width:600px;max-width: 600px;"></td>
                        </tr>
                        <!-- Transparent Breaker -->
                    </table>

                </td>
            </tr>
            <tr>
                <td>
                    <table width="640" align="center" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td mc:edit="footerlinks" valign="top" style="color: #ffffff;text-align: center;">

                                <a href="http://www.njordlaw.com" style="color: #60dd49;text-decoration: none;">
                                    www.njordlaw.com
                                </a>

                                <br><br>

                                <a href="*|ARCHIVE|*" style="color: #ffffff;text-decoration: none;">
                                    Se nyhedsbrev i browser
                                </a>

                                <br><br>

                                <a href="*|FORWARD|*" style="color: #ffffff;text-decoration: none;">
                                    Videresend nyhedsbrev
                                </a>

                                <br><br>

                                <a href="*|LIST:SUBSCRIBE|*" style="color: #ffffff;text-decoration: none;">
                                    Tilmeld nyhedsbrev
                                </a>

                                <br><br>

                                <a href="*|UNSUB|*" style="color: #ffffff;text-decoration: none;">
                                    Afmeld nyhedsbrev
                                </a>

                            </td>
                        </tr>
                        <!-- Transparent Breaker -->
                        <tr>
                            <td colspan="2" height="20" style="max-height:20px;min-height:20px;height:20px;width: 600px;min-width:600px;max-width: 600px;"></td>
                        </tr>
                        <!-- Transparent Breaker -->
                    </table>


                </td>
            </tr>

        </table>

    </td>
</tr>
<!-- Border -->
<tr>
    <td height="1" style="max-height:1px;min-height:1px;height:1px;width: 640px;min-width:640px;max-width: 640px;"></td>
</tr>
<!-- Border -->
<tr style="background-color: #252525;">
    <td class="boxsize">
        <table width="640" align="center" cellspacing="0" cellpadding="0" border="0">
            <!-- Transparent Breaker -->
            <tr>
                <td colspan="2" height="20" style="max-height:20px;min-height:20px;height:20px;width: 600px;min-width:600px;max-width: 600px;"></td>
            </tr>
            <!-- Transparent Breaker -->
            <tr>
                <td mc:edit="footertext" valign="top" style="font-size: 12px;text-align: left;color: #ffffff;">
                    Dette nyhedsbrev er kun vejledende og skal ikke betragtes som en erstatning for at søge juridisk rådgivning.
                    Vi anbefaler, at der søges uafhængig juridisk rådgivning i forbindelse med konkrete juridiske problemstillinger.
                    MAQS Law Firm påtager sig intet ansvar for tab lidt i forbindelse med trufne beslutninger eller manglende
                    handlinger i tillid til oplysningerne i dette nyhedsbrev.
                </td>
            </tr>
            <!-- Transparent Breaker -->
            <tr>
                <td colspan="2" height="50" style="max-height:50px;min-height:50px;height:50px;width: 600px;min-width:600px;max-width: 600px;"></td>
            </tr>
            <!-- Transparent Breaker -->
        </table>
    </td>
</tr>
<tr style="background-color: #252525;">
    <td valign="top" class="boxsize">
        <table width="640" align="center" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td class="textaligncenter" mc:edit="copyright" valign="top" align="left" style="text-align: left;font-size: 12px;;color: #ffffff;">

                    © 2014 NJORD

                </td>
                <td class="textaligncenter paddingtop" mc:edit="socialsharing" valign="top" align="right" style="text-align: right;font-size: 12px;;color: #ffffff;">

                    <a href="#" style="text-decoration: none;">
                        <img src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/19b964e7-7009-4684-a1e7-15f9a43fd241.png" alt="Twitter">
                    </a>

                    <a href="#" style="text-decoration: none;">
                        <img src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/c913b045-ef99-4d9b-b3e7-ef8e6baa7565.png" alt="Facebook">
                    </a>

                    <a href="#" style="text-decoration: none;">
                        <img src="https://gallery.mailchimp.com/ea385526fc54a74f2a2de90d1/images/ddabfc5b-d6b3-418b-a079-cb37904c8a20.png" alt="LinkedIn">
                    </a>

                </td>
            </tr>
        </table>
    </td>
</tr>
<!--END FOOTER-->
</table>

</font>
</body>
</html>