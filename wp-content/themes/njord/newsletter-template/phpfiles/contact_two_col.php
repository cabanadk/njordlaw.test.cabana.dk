<tr class="fullwidth heightauto" mc:repeatable="module" mc:variant="contact section 2 columns without heading">
    <td valign="top">


        <!-- START / FIRST CONTENT -->

        <table cellpadding="20" cellspacing="0" border="0" width="600">
            <tr>
                <td colspan="2"></td>
                <td left="" image="" class="fullwidth textaligncenter floatleft contentimage boxsize" valign="top" colspan="1" width="150" style="text-align:center;min-width: 150px;width: 150px;max-width: 150px;">
                    <img src="http://placehold.it/150x150/000000/ffffff" style="width: 150px;min-width: 150px;max-width: 150px;" mc:edit="contact_image_first">

                    <div class="increasefontsize paddingtop employee" mc:edit="contactcontentfirst">
                        <br>
                        *content*
                    </div>

                </td>
                <td left="" image="" class="fullwidth textaligncenter floatleft contentimage boxsize" valign="top" colspan="1" width="150" style="text-align:center;min-width: 150px;width: 150px;max-width: 150px;">
                    <img src="http://placehold.it/150x150/000000/ffffff" style="width: 150px;min-width: 150px;max-width: 150px;" mc:edit="contact_image_second">

                    <div class="increasefontsize paddingtop employee" mc:edit="contactcontentsecond">
                        <br>
                        *content*
                    </div>
                </td>
                <td colspan="2"></td>
            </tr>

            <!-- Transparent Breaker -->
            <tr>
                <td colspan="6" height="5" style="max-height:5px;min-height:5px;height:5px;width: 600px;min-width:600px;max-width: 600px;"></td>
            </tr>
            <!-- Transparent Breaker -->
        </table>
        <!-- END / CONTACT SECTION -->


        <!-- END / WrapperContent -->
    </td>
</tr>