<tr class="fullwidth" mc:repeatable="module" mc:variant="content with left image">
    <td valign="top" class="fullwidth displayblock">

        <table cellpadding="20" border="0" cellspacing="0">
            <tr>
                <td class="fullwidth textaligncenter contentimage boxsize" colspan="2" valign="top" width="268" style="width: 268px;min-width: 268px;max-width: 268px;">
                    <img class="displayblock fullwidth" src="http://placehold.it/268x210/000000/ffffff" style="width: 268px;min-width: 268px;max-width: 268px;" mc:edit="article_left_image">
                    <br>
                    <br>
                </td>

                <td class="fullwidth contentimage" colspan="2" valign="top" width="292" style="width: 292px;min-width: 292px;max-width: 292px;">
                    <table cellpadding="3" border="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="article fullwidth boxsize" style="width: 292px;min-width: 292px;max-width: 292px;" mc:edit="article_content_left_heading">
                                    *heading*
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="article fullwidth boxsize" style="width: 292px;min-width: 292px;max-width: 292px;" mc:edit="article_content_left_content">
                                    *content*
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="article fullwidth boxsize" style="width: 292px;min-width: 292px;max-width: 292px;" mc:edit="article_link">
                                <a href="http://www.njordlaw.com" style="color: #60dd49;">Læs mere</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>