<?php
/*
 Template Name: CSR Template
 */
?>

<?php get_header(); ?>

<?php
$post_thumbnail_id = get_post_thumbnail_id();
$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
$languageCode = ICL_LANGUAGE_CODE;

?>
	<figure id="featured"><img src="<?php echo $post_thumbnail_url; ?>" width="1572" height="500"></figure>
	<section id="content" class="cols-a">
      
	    <article id="content" class="csr-content">
	    	<?php remove_filter( 'the_content', 'append_class_to_paragraph' ); ?>
	    	<h1><?php echo get_the_title() ?></h1>
	    	<?php  

	    		$content = do_shortcode(wpautop($post->post_content));
                echo str_replace(get_the_title(), '', $content);
        	?>
			<div class="csr_cards">
				<?php

					// check if the repeater field has rows of data
					if( have_rows('csr_card') ):

					 	// loop through the rows of data
					    while ( have_rows('csr_card') ) : the_row(); ?>
						<?php ?>
						<div class="csr_card">
							<ul>
							
								<li class="csr_icon">
									<div class="svg-wrapper">
										<img class="svg" src="<?php bloginfo('template_directory'); ?>/img/icons/<?php the_sub_field("csr_icon") ?>.svg" alt="<?php the_sub_field("csr_icon") ?>">
									</div>
								</li>
								<li class="csr_title"><?php the_sub_field("csr_title") ?></li>
								<li class="csr_text"><?php the_sub_field("csr_text") ?></li>
								<li class="csr_link">
									<a href="<?php the_sub_field("csr_link") ?>">
										<?php the_sub_field("link_text") ?>
									</a>
								</li>
							</ul>
						</div>
						
						 
					   <?php endwhile;

					else :
					    echo "NO ROWS";

					endif;

				?>
			</div>
			<?php include ('template_parts/social-sharing-buttons.php');?>

		</article>
	</section>

<?php get_footer(); ?>